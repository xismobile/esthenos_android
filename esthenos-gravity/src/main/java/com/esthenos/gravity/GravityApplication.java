package com.esthenos.gravity;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import io.fabric.sdk.android.Fabric;


public class GravityApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new CrashlyticsNdk(), new Crashlytics());
    }
}
