package com.esthenos.gravity.models;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.ApplicationModel.Application;
import com.esthenos.gravity.R;
import com.esthenos.commons.models.ProductsModel.*;


import java.util.List;

public class ApplicantGRTListAdapter extends BaseAdapter{

    Context context;
    List<Application> mApplicantList;
    List<Product> mProductList;

    public ApplicantGRTListAdapter(Context ctx, List<Application> applicantlist, List<Product> mList){
        this.context = ctx;
        this.mProductList = mList;
        this.mApplicantList = applicantlist;
    }

    @Override
    public int getCount() {
        if(mApplicantList !=null)
            return mApplicantList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return mApplicantList.get(position);
    }

    @Override
    public long getItemId(int position) {
        if(mApplicantList !=null)
            return mApplicantList.get(position).hashCode();
        return 0;
    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString()
                    .equalsIgnoreCase(myString)) {
                index = i;
                i = spinner.getCount();
            }
        }
        return index;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Application applicant = mApplicantList.get(position);
        View vi=convertView;

        if(vi==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.grt_baseadapter, null);

            applicant.product_double = "false";
            applicant.pass_fail = "Pass";

        }

        final TextView mApplicantName=(TextView)vi.findViewById(R.id.applicant_name);
        final Spinner mProductListValue=(Spinner)vi.findViewById(R.id.change_product);
        final CheckBox mDoubleProduct=(CheckBox)vi.findViewById(R.id.double_check);
        final BootstrapButton mPass=(BootstrapButton)vi.findViewById(R.id.pass_grt);
        final BootstrapButton mFail=(BootstrapButton)vi.findViewById(R.id.fail_grt);

        mApplicantName.setText(applicant.applicant_name);

        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.layout_spinner_item, mProductList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        mProductListValue.setAdapter(adapter);
        final String selectedValue = applicant.product_name;

        int spinnerPosition = getIndex(mProductListValue, selectedValue);
        mProductListValue.setSelection(spinnerPosition);

        mDoubleProduct.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                applicant.product_double = Boolean.toString(isChecked);

            }
        });

        mPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicant.pass_fail = "Pass";
                mPass.setEnabled(false);
                mFail.setEnabled(true);
            }
        });
        mFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applicant.pass_fail = "Fail";
                mPass.setEnabled(true);
                mFail.setEnabled(false);
            }
        });

        mProductListValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Product product = mProductList.get(position);

                if (!selectedValue.equalsIgnoreCase(mProductListValue.getItemAtPosition(position).toString())) {
                    mPass.setEnabled(true);
                    mFail.setEnabled(true);
                    mDoubleProduct.setChecked(false);
                }

                applicant.product_id = product.product_id;
                applicant.product_name = product.product_name;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return vi;
    }
}
