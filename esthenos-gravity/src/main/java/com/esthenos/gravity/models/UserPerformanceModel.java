package com.esthenos.gravity.models;


import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.Request;

public class UserPerformanceModel {

    public static Request getRequest(SingletonConfig instance) {
        return new Request.Builder()
                .url(instance.getUserPerformance())
                .build();
    }

    public class UserPerformanceItem {
        public int target;
        public int achieved;
        public int percentage;
    }

    public class UserPerformance {
        public int pending_grt;
        public int pending_cgt1;
        public int pending_cgt2;

        public UserPerformanceItem apps_sourced;
        public UserPerformanceItem apps_disbursed;
        public UserPerformanceItem amount_disbursed;

        public UserPerformanceItem groups_disbursed;
        public UserPerformanceItem centers_disbursed;
    }
}
