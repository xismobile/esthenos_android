package com.esthenos.gravity.models;


import android.util.Log;

import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.CenterGroupsModel.Center;
import com.esthenos.commons.models.CenterGroupsModel.Group;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CGTModel {

    private static String getEndPoint(SingletonConfig instance, String requestType) {
        return instance.getCGTQuestions(requestType);
    }

    public static Request postRequest(SingletonConfig instance, CGTApplicantResponses applicantResponses, String requestType) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String requestUrl = getEndPoint(instance, requestType);
        RequestBody body = RequestBody.create(JSON, HttpUtils.toJson(applicantResponses));

        Log.d("CGT_Data",HttpUtils.toJson(applicantResponses).toString());
        return new Request.Builder()
                .url(requestUrl)
                .post(body)
                .build();
    }

    public static Request getRequest(SingletonConfig instance, String requestType) {
        String requestUrl = getEndPoint(instance, requestType);

        return new Request.Builder()
                .url(requestUrl)
                .build();
    }

    public static CGTApplicantResponses getCGTApplicantResponses(SingletonConfig instance, List<ApplicationModel.Application> applications, String status,String lat,String lon) {
        CGTApplicantResponses responses = new CGTApplicantResponses(instance.getGroup(), instance.getCenter(), status,lat,lon);
        for (ApplicationModel.Application application : applications) {
            CGTApplicantResponse response = new CGTApplicantResponse(application);
            responses.applications.add(response);
        }
        return responses;
    }

    public static CGTApplicantResponses getGRTApplicantResponses(SingletonConfig instance, List<ApplicationModel.Application> applications, String status,String lat,String lon) {
        CGTApplicantResponses responses = new CGTApplicantResponses(instance.getGroup(), instance.getCenter(), status,lat,lon);
        for (ApplicationModel.Application application : applications) {
            CGTApplicantResponse response = new CGTApplicantResponse(application);
            responses.applications.add(response);
        }
        return responses;
    }

    public static class CGTApplicantResponse {
        public String id;
        public String status;
        public String customer_id;
        public String product_id;
        public String product_double;

        public CGTApplicantResponse(ApplicationModel.Application application) {
            this.id = application.id;
            this.status = application.pass_fail;
            this.product_id = application.product_id;
            this.customer_id = application.customer_id;
            this.product_double = application.product_double;
        }
    }

    public static class CGTApplicantResponses {
        public String group_id;
        public String center_id;
        public String question_pass_fail;
        public String lat;
        public String lon;
        public List<CGTApplicantResponse> applications;

        public CGTApplicantResponses(Group group, Center center, String pass_fail,String lat,String lon) {
            this.applications = new ArrayList<>();
            this.group_id = group.group_id;
            this.center_id = center.center_id;
            this.question_pass_fail = pass_fail;
            this.lat = lat;
            this.lon = lon;
        }
    }
}
