package com.esthenos.gravity.models;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.ApplicationModel.Application;

import com.esthenos.commons.utils.Constants;
import com.esthenos.gravity.R;

import java.util.List;

public class ApplicantListAdapter extends ArrayAdapter<Application> {

    private Context context;
    private List<Application> itemList;
    private String requestType;

    public ApplicantListAdapter(Context ctx, List<Application> itemList,String requestType) {
        super(ctx, android.R.layout.simple_list_item_1, itemList);
        this.context = ctx;
        this.itemList = itemList;
        this.requestType = requestType;
    }

    public int getCount() {
        if (itemList != null)
            return itemList.size();
        return 0;
    }

    public Application getItem(int position) {
        return itemList.get(position);
    }

    public long getItemId(int position) {
        if (itemList != null)
            return itemList.get(position).hashCode();
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.cgtmodelquestion, null);
        }

        final Application application = itemList.get(position);
        final EditText mOmniId = (EditText)v.findViewById(R.id.omni_id);
        if(requestType.equalsIgnoreCase(Constants.REQ_QUESTIONS_CGT2)){
            mOmniId.setVisibility(View.GONE);
        }

        TextView applicantname = (TextView) v.findViewById(R.id.member_name1);
        applicantname.setText(String.format("%s", application.applicant_name));

        application.pass_fail = "pass";
        final BootstrapButton cancelBtn = (BootstrapButton) v.findViewById(R.id.fail_button);
        final BootstrapButton okBtn = (BootstrapButton) v.findViewById(R.id.pass_button);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Application application = itemList.get(position);
                application.pass_fail = "pass";
                okBtn.setEnabled(false);
                cancelBtn.setEnabled(true);

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Application application = itemList.get(position);
                application.pass_fail = "fail";
                okBtn.setEnabled(true);
                cancelBtn.setEnabled(false);
            }
        });

        mOmniId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Application application = itemList.get(position);
                application.customer_id = s.toString();
            }
        });


        return v;
    }

    public List<Application> getItemList() {
        return itemList;
    }

    public void setItemList(List<Application> itemList) {
        this.itemList = itemList;
    }
}
