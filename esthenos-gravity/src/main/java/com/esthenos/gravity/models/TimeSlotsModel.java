package com.esthenos.gravity.models;


import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimeSlotsModel {

    public static Request getRequest(SingletonConfig instance) {
        return new Request.Builder()
                .url(instance.getTimeSlots())
                .build();
    }

    public class TimeSlot {
        public String day;
        public List<String> times;

        public CharSequence[] getTimes() {
            return times.toArray(new CharSequence[times.size()]);
        }
    }

    public class TimeSlots {
        public List<TimeSlot> timeslots;

        public CharSequence[] getDays() {
            List <String> days = new ArrayList<>();
            for (TimeSlot slot : timeslots) { days.add(slot.day); }
            return days.toArray(new CharSequence[days.size()]);
        }
    }
}
