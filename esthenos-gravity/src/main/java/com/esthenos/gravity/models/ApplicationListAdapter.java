package com.esthenos.gravity.models;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.esthenos.gravity.R;
import com.esthenos.commons.models.ApplicationModel.Application;
import com.esthenos.commons.utils.Constants;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class ApplicationListAdapter extends ArrayAdapter<Application> {

    private Context context;
    private List<Application> itemList;

    public ApplicationListAdapter(List<Application> itemList, Context ctx) {
        super(ctx, android.R.layout.simple_list_item_1, itemList);
        this.context = ctx;
        this.itemList = itemList;
    }

    public int getCount() {
        if (itemList != null)
            return itemList.size();
        return 0;
    }

    public Application getItem(int position) {
        return itemList.get(position);
    }

    public long getItemId(int position) {
        if (itemList != null)
            return itemList.get(position).hashCode();
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item, null);
        }


        Application application = itemList.get(position);
        TextView text = (TextView) v.findViewById(R.id.application_id);
        text.setText(String.format("%s %s", application.applicant_name, application.id));

        int check = application.status;
        TextView text1 = (TextView) v.findViewById(R.id.application_status);
        if(check==0) {
            text1.setTextColor(v.getResources().getColor(R.color.red));
        }
        text1.setText(String.format("%s", application.current_status));

        TextView text2 = (TextView) v.findViewById(R.id.application_updated);
        text2.setText(String.format("Loan Eligibility: %s", application.loan_eligibility_based_on_company_policy));

        TextView mLoanDetail = (TextView)v.findViewById(R.id.loan_detail_all);
        TextView mLoanTotal = (TextView)v.findViewById(R.id.loan_total);
        TextView mLoanDefault = (TextView)v.findViewById(R.id.loan_default);

        try {

            mLoanTotal.setText(application.loan_total);
            if(application.loan_total.trim().length() <=0){
                mLoanTotal.setText("Loan Total: No Active Record Found");
            }

            String detail = application.loan;
            if (detail.trim().length() <= 0) {
                mLoanDetail.setText("Loan Details: No Active Record Found");
            } else {
                mLoanDetail.setText(String.format("%s", detail));
            }
            mLoanDefault.setText(String.format("%s",application.loan_default));
            if (application.loan_default.equalsIgnoreCase(Constants.COLOR_CHANGE)) {
                mLoanDefault.setTextColor(v.getResources().getColor(R.color.red));
            }else if(application.loan_default.trim().length()<=0){
                mLoanDefault.setText("Tatal Default: No Active Record Found");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return v;
    }

    public List<Application> getItemList() {
        return itemList;
    }

    public void setItemList(List<Application> itemList) {
        this.itemList = itemList;
    }
}
