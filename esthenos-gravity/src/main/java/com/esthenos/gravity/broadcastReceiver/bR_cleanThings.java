package com.esthenos.gravity.broadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.esthenos.commons.utils.Autosave_Data;

public class bR_cleanThings extends BroadcastReceiver {
    private static final String TAG ="Gravity" ;
    Autosave_Data autosave_data = new Autosave_Data();


    public bR_cleanThings() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: "+intent.getAction());
        Toast.makeText(context, "Cache Cleaner Running", Toast.LENGTH_SHORT).show();
        autosave_data.isFolderExist();
        Autosave_Data.deleteCache(context.getApplicationContext());
    }
}
