package com.esthenos.gravity.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.gravity.R;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.CenterGroupsModel.Groups;
import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.Request;

import java.util.ArrayList;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ActivitySelectGroup extends Activity implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

    private static final String TAG = "SearchViewFilterMode";

    private GridView mListView;
    private SearchView mSearchView;

    private SingletonConfig instance = SingletonConfig.getInstance();

    private ArrayAdapter<CenterGroupsModel.Group> mAdapter;
    private ArrayList<CenterGroupsModel.Group> GROUPS = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.searchview_filter);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySelectGroup.this.finish();
            }
        });

        mSearchView = (SearchView) findViewById(R.id.search_view);
        AutoCompleteTextView search_text = (AutoCompleteTextView) mSearchView.findViewById(mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null));
        search_text.setTextColor(Color.WHITE);
        search_text.setHintTextColor(Color.LTGRAY);

        int searchButtonId = getResources().getIdentifier("android:id/search_button", null, null);
        ImageView searchButtonImage = (ImageView) mSearchView.findViewById(searchButtonId);
        searchButtonImage.setImageResource(R.drawable.search);

        int closeButtonId = getResources().getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButtonImage = (ImageView) mSearchView.findViewById(closeButtonId);
        closeButtonImage.setImageResource(R.drawable.close);

        ((TextView) findViewById(R.id.tvfilter)).setText("Select Groups");
        mListView = (GridView) findViewById(R.id.list_view);
        try {
            mListView.setAdapter(mAdapter = new ArrayAdapter<>(this, R.layout.grid_item, GROUPS));
            mListView.setTextFilterEnabled(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                instance.setGroup(GROUPS.get(position));
                setResult(RESULT_OK, new Intent());
                finish();
            }
        });
        setupSearchView();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Request request = CenterGroupsModel.getRequest(instance, getIntent().getStringExtra(Constants.REQ_TYPE));
                    Groups response = HttpUtils.request(request, Groups.class);
                    for (CenterGroupsModel.Group group : response.groups) {
                            GROUPS.add(group);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivitySelectGroup.this, "Failed To Fetch Group Details", Toast.LENGTH_SHORT).show();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        String suggestion = getSuggestion(position);
        mSearchView.setQuery(suggestion, true);
        return true;
    }

    private String getSuggestion(int position) {
        Cursor cursor = (Cursor) mSearchView.getSuggestionsAdapter().getItem(position);
        return cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Groups");
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            mListView.clearTextFilter();
        } else {
            mListView.setFilterText(newText);
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
