package com.esthenos.gravity.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.UserPerformanceModel;
import com.esthenos.gravity.models.UserPerformanceModel.UserPerformance;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.ArrayList;


public class ActivityUserPerformance extends Activity {

    private BootstrapButton mNext;
    private TextView mCgt1, mCgt2, mPending, mPendingApp;
    private TextView mAppsSource, mAmountDisbured, mAppsdisbursed, mCenterDisbured, mGroupDisbured;
    private HorizontalBarChart targetLoanCountChart, targetLoanAmountChart, pendingCGT1Chart, pendingCGT2Chart, disburstmentChart;

    private SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_performance);

        mCgt1 = (TextView) findViewById(R.id.button_cgt1);
        mCgt2 = (TextView) findViewById(R.id.button_cgt2);
        mPending = (TextView) findViewById(R.id.button_grt);
        mPendingApp = (TextView) findViewById(R.id.pending_count_application);

        mAppsSource = (TextView) findViewById(R.id.apps_source_performance);
        mAppsdisbursed = (TextView) findViewById(R.id.app_disb_performance);
        mGroupDisbured = (TextView) findViewById(R.id.group_disbursed);
        mCenterDisbured = (TextView) findViewById(R.id.center_disbused);
        mAmountDisbured = (TextView) findViewById(R.id.amount_disb_performance);

        targetLoanCountChart = getBarChart(R.id.target_loans_no);
        targetLoanAmountChart = getBarChart(R.id.target_loans_amount);

        pendingCGT1Chart = getBarChart(R.id.pending_cgt1);
        pendingCGT2Chart = getBarChart(R.id.pending_cgt2);
        disburstmentChart = getBarChart(R.id.pending_disburtments);

        mNext = (BootstrapButton) findViewById(R.id.next);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityHome.class);
                ActivityUserPerformance.this.startActivity(intent);
            }
        });

        (new AsyncDashboardLoader()).execute();
    }

    private HorizontalBarChart getBarChart(int resourceId) {
        HorizontalBarChart chart = (HorizontalBarChart) findViewById(resourceId);
        chart.animateXY(2000, 2000);
        chart.setDescription("");
        chart.invalidate();
        chart.getLegend().setEnabled(false);
        chart.setScaleEnabled(false);

        // hides horizontal grid lines inside chart
        chart.getXAxis().setEnabled(false);
        chart.getXAxis().setDrawLabels(false);

        // hides horizontal grid lines with below line
        chart.getAxisLeft().setEnabled(false);
        chart.getAxisLeft().setDrawLabels(false);

        // hides horizontal grid lines with below line
        chart.getAxisRight().setEnabled(false);
        chart.getAxisRight().setDrawLabels(false);

        return chart;
    }

    private ArrayList<BarDataSet> getBarChartData(UserPerformanceModel.UserPerformanceItem response) {
        ArrayList<BarDataSet> dataSets = new ArrayList<>();

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        valueSet1.add(new BarEntry(response.target, 0));
        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        valueSet2.add(new BarEntry(response.achieved, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        return xAxis;
    }

    private class AsyncDashboardLoader extends AsyncTask<String, Void, UserPerformance> {

        private final ProgressDialog dialog = new ProgressDialog(ActivityUserPerformance.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("loading performance details...");
            dialog.show();
        }

        @Override
        protected void onPostExecute(UserPerformance response) {
            super.onPostExecute(response);
            dialog.dismiss();

            mCgt1.setText(String.format("%d", response.pending_cgt1));
            mCgt2.setText(String.format("%d", response.pending_cgt2));
            mPending.setText(String.format("%d", response.pending_grt));
            mPendingApp.setText(String.format("%s", getOfflineFormCount()));

            mAppsSource.setText(String.format("%d", response.apps_sourced.percentage));
            mAppsdisbursed.setText(String.format("%d", response.apps_disbursed.percentage));
            mGroupDisbured.setText(String.format("%d", response.groups_disbursed.percentage));
            mCenterDisbured.setText(String.format("%d", response.centers_disbursed.percentage));
            mAmountDisbured.setText(String.format("%d", response.amount_disbursed.percentage));

            BarData data = new BarData(getXAxisValues(), getBarChartData(response.apps_sourced));
            targetLoanCountChart.setData(data);

            BarData data2 = new BarData(getXAxisValues(), getBarChartData(response.apps_disbursed));
            pendingCGT1Chart.setData(data2);

            BarData data4 = new BarData(getXAxisValues(), getBarChartData(response.groups_disbursed));
            disburstmentChart.setData(data4);

            BarData data3 = new BarData(getXAxisValues(), getBarChartData(response.centers_disbursed));
            pendingCGT2Chart.setData(data3);

            BarData data1 = new BarData(getXAxisValues(), getBarChartData(response.amount_disbursed));
            targetLoanAmountChart.setData(data1);
        }

        @Override
        protected UserPerformance doInBackground(String... strings) {
            UserPerformance response = null;
            try {
                Request request = UserPerformanceModel.getRequest(instance);
                response = HttpUtils.request(request, UserPerformance.class);

            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ActivityUserPerformance.this, "Failed To Fetch Performance Details", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
            return response;
        }

        public int getOfflineFormCount() {
            try (MySQLiteDB db = new MySQLiteDB(getApplicationContext()); Cursor cursor = db.getAllSubmissionData()) {
                return cursor.getCount();
            }
        }
    }
}
