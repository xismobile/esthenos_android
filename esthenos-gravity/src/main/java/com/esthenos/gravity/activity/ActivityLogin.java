package com.esthenos.gravity.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.crashlytics.android.Crashlytics;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.broadcastReceiver.bR_cleanThings;

import java.util.Calendar;


public class ActivityLogin extends AppCompatActivity {


    private static final int MY_PERMISSIONS_REQUEST_READ_STORAGE = 92;
    private final String TAG = "Gravity";
    Intent myIntent;
    int id = 92;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    AutoSaveData autoSaveData;
    Autosave_Data data = new Autosave_Data();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        autoSaveData = new AutoSaveData(getApplicationContext());
        //autoSaveData.clearCache(getApplicationContext(),7);
        data.isFolderExist();
        Autosave_Data.deleteCache(getApplicationContext());
        // autoSaveData.deleteCache(getApplicationContext());
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {

            if (ContextCompat.checkSelfPermission(ActivityLogin.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_STORAGE);
            }
        }

        setBR();

        final EditText mEmailText = (EditText) findViewById(R.id.emailText);
        final EditText mPasswordText = (EditText) findViewById(R.id.passwordText);
        final BootstrapButton button = (BootstrapButton) findViewById(R.id.login_button);

        final TextView footer = (TextView) findViewById(R.id.BottomFooter1);
        footer.setText(String.format("%s, %s", getString(R.string.app_name), Constants.getVersion(getApplicationContext())));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            SingletonConfig instance = SingletonConfig.getLoginToken(
                                    mEmailText.getText().toString(),
                                    mPasswordText.getText().toString()
                            );

                            if (instance != null) {
                                logUser(instance);
                                Intent intent = new Intent(ActivityLogin.this, ActivityCGTGroup.class);
                                ActivityLogin.this.startActivity(intent);

                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog alertDialog = new AlertDialog.Builder(ActivityLogin.this).create();
                                        alertDialog.setTitle("Error");
                                        alertDialog.setMessage("Authentication Failed");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();
                                    }
                                });
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ActivityLogin.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    private void logUser(SingletonConfig instance) {
        Crashlytics.setUserEmail(instance.getUser().email);
        Crashlytics.setUserIdentifier(instance.getUser().id);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    data.isFolderExist();
                    data.deleteCache(ActivityLogin.this);
                    Toast.makeText(ActivityLogin.this, "Temp file will be deleted", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    Toast.makeText(ActivityLogin.this, "You must grant Permission to delete Temp files", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void setBR() {


        myIntent = new Intent(getBaseContext(), bR_cleanThings.class);
        myIntent.setAction("CleanThings");
        pendingIntent = PendingIntent.getBroadcast(getBaseContext(), id, myIntent, 0);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        Calendar rightNow = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, rightNow.get(Calendar.DAY_OF_YEAR));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        System.out.println("Old is set@ :== " + calendar.getTime());
        // long interval = calendar.getTimeInMillis() + 604800000L;
        // System.out.println("Next Millis = " + interval);
        Log.d("Gravity", "Gravitylogin" + "IN setbr()" + calendar.getTime());

      /*
        Intent intent = new Intent(getBaseContext(), bR_cleanThings.class);//the same as up
        intent.setAction("CleanThings");//the same as up
*/
        boolean isWorking = (PendingIntent.getBroadcast(getBaseContext(), id, myIntent, PendingIntent.FLAG_NO_CREATE) != null);//just changed the flag

        Log.d(TAG, "alarm is " + (isWorking ? "" : "not") + " working...");
        if (!isWorking) {
            // PendingIntent.getBroadcast(getBaseContext(), id, myIntent, PendingIntent.FLAG_NO_CREATE).cancel();
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY * 7, pendingIntent);
        }

    }

}
