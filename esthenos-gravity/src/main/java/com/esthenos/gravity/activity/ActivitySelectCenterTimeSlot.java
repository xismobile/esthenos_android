package com.esthenos.gravity.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.CenterGroupsModel.Center;
import com.esthenos.commons.models.CenterGroupsModel.CenterTime;
import com.esthenos.commons.models.MiscModel;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.TimeSlotsModel;
import com.esthenos.gravity.models.TimeSlotsModel.TimeSlot;
import com.esthenos.gravity.models.TimeSlotsModel.TimeSlots;
import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.Request;

import static com.esthenos.commons.utils.Constants.REQ_CENTERS;
import static com.esthenos.commons.utils.Constants.REQ_TYPE;


public class ActivitySelectCenterTimeSlot extends ActionBarActivity {

    private TimeSlot slot;
    private TimeSlots slots;
    private CenterTime centerTime;
    private Button btnSelectTime, btnSelectDay;
    private SingletonConfig instance = SingletonConfig.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.center_timeslot_layout);

        btnSelectDay = (Button) findViewById(R.id.select_day);
        btnSelectTime = (Button) findViewById(R.id.select_timeslot);

        btnSelectDay.setEnabled(false);
        btnSelectTime.setEnabled(false);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySelectCenterTimeSlot.this.finish();
            }
        });

        Button button = (Button) findViewById(R.id.select_center);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySelectCenterTimeSlot.this, ActivitySelectCenter.class);
                intent.putExtra(REQ_TYPE, REQ_CENTERS);
                ActivitySelectCenterTimeSlot.this.startActivityForResult(intent, 10);
            }
        });


        btnSelectDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySelectCenterTimeSlot.this);
                builder.setTitle("Select Day");

                try {
                    final CharSequence[] days = slots.getDays();
                    builder.setSingleChoiceItems(days, -1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            slot = slots.timeslots.get(item);
                            centerTime = new CenterTime(slot.day);
                            Toast.makeText(getApplicationContext(), slot.day, Toast.LENGTH_SHORT).show();
                            btnSelectDay.setText(slot.day);

                            if (!btnSelectDay.getText().toString().equalsIgnoreCase("Select Day")) {
                                btnSelectTime.setEnabled(true);
                            }
                            dialog.dismiss();
                        }
                    });

                AlertDialog alert = builder.create();
                alert.show();
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Fail To Fetch Data",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });


        btnSelectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySelectCenterTimeSlot.this);
                builder.setTitle("Select TimeSlot");

                try {
                    final CharSequence[] times = slot.getTimes();
                    builder.setSingleChoiceItems(times, -1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            centerTime.time = times[item].toString();
                            Toast.makeText(getApplicationContext(), centerTime.time, Toast.LENGTH_SHORT).show();
                            btnSelectTime.setText(centerTime.time);
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Fail To Fetch Data",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        BootstrapButton button3 = (BootstrapButton) findViewById(R.id.show_members);
        button3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final Center center = instance.getCenter();
                if (center == null) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectCenterTimeSlot.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select Center");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else if ((btnSelectDay.getText().toString()).equalsIgnoreCase("SELECT DAY")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectCenterTimeSlot.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select Meeting Day");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else if ((btnSelectTime.getText().toString()).equalsIgnoreCase("SELECT TIMESLOT")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectCenterTimeSlot.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select Meeting Time");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else if (centerTime != null) {

                    center.timeslot = centerTime;
                    new AsyncCenterTimeSubmit().execute();
                } else {

                    startActivity(new Intent(ActivitySelectCenterTimeSlot.this, ActivityLocCenter.class));
                }
            }
        });

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Request request = TimeSlotsModel.getRequest(instance);
                    slots = HttpUtils.request(request, TimeSlots.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       try {
           switch (requestCode) {
               case 10:
                   if (resultCode == RESULT_OK) {
                       CenterGroupsModel.Center center = instance.getCenter();
                       Toast.makeText(this, String.format("Selected Center: %s", center.name), Toast.LENGTH_LONG).show();

                       TextView tv = (TextView) findViewById(R.id.center_name);
                       tv.setText(center.name);

                       if (tv.getText().toString() != null) {
                           btnSelectDay.setEnabled(true);
                       }
                       if (center.timeslot != null) {
                           btnSelectDay.setText(center.timeslot.day);
                           btnSelectDay.setEnabled(false);

                           btnSelectTime.setText(center.timeslot.time);
                           btnSelectTime.setEnabled(false);

                       } else {
                           btnSelectDay.setText("Select Day");
                           btnSelectDay.setEnabled(true);

                       }
                       break;
                   }
           }
       }catch (Exception e){
           e.printStackTrace();
       }
    }


    private class AsyncCenterTimeSubmit extends AsyncTask<String, Void, MiscModel.SubmitForm> {
        private final ProgressDialog dialog = new ProgressDialog(ActivitySelectCenterTimeSlot.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Submitting Center Meeting Time");
            dialog.show();
        }

        @Override
        protected MiscModel.SubmitForm doInBackground(String... params) {
            MiscModel.SubmitForm response = null;
            try {
                Request request = CenterGroupsModel.postRequest(instance);
                response = HttpUtils.request(request, MiscModel.SubmitForm.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(MiscModel.SubmitForm response) {
            if (dialog.isShowing()) dialog.dismiss();
            try {
                Toast.makeText(getApplicationContext(), response.message, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ActivitySelectCenterTimeSlot.this, ActivityLocCenter.class));

            }catch (Exception e){
                e.printStackTrace();}
        }
    }
}
