package com.esthenos.gravity.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;


public class ActivityCGTGroup extends Activity {

    TextView greeting;
    Button mCgt1, mCgt2, mSourcing, mGrt;
    SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Constants.USER_ROLE_CM.equalsIgnoreCase(instance.getUser().role)) {
            setContentView(R.layout.activity_cgt_group_page);
            greeting = (TextView) findViewById(R.id.tvGreeting);
            greeting.setText(Constants.setGreeting());

            mCgt1 = (Button) findViewById(R.id.button_cgt1);
            mCgt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityCGTGroup.this, ActivitySelectGroupCGT.class);
                    intent.putExtra(Constants.TITLE, "Select Group For CGT1");
                    intent.putExtra(Constants.REQ_TYPE, Constants.REQ_QUESTIONS_CGT1);
                    ActivityCGTGroup.this.startActivity(intent);
                }
            });

            mCgt2 = (Button) findViewById(R.id.button_cgt2);
            mCgt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivitySelectGroupCGT.class);
                    intent.putExtra(Constants.TITLE, "Select Group For CGT2");
                    intent.putExtra(Constants.REQ_TYPE, Constants.REQ_QUESTIONS_CGT2);
                    ActivityCGTGroup.this.startActivity(intent);
                }
            });

            mSourcing = (Button) findViewById(R.id.button_sourcing);
            mSourcing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivityHome.class);
                    startActivity(intent);
                }
            });

        } else if (Constants.USER_ROLE_BM.equalsIgnoreCase(instance.getUser().role)) {
            setContentView(R.layout.activity_grt_group_page);
            greeting = (TextView) findViewById(R.id.tvGreeting);
            greeting.setText(Constants.setGreeting());

            mGrt = (Button) findViewById(R.id.button_grt);
            mGrt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivitySelectGroupCGT.class);
                    intent.putExtra(Constants.TITLE, "Select Group For GRT");
                    intent.putExtra(Constants.REQ_TYPE, Constants.REQ_QUESTIONS_GRT);
                    ActivityCGTGroup.this.startActivity(intent);
                }
            });
        }
    }
}
