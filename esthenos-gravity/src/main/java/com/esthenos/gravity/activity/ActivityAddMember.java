package com.esthenos.gravity.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.gravity.R;
import com.esthenos.gravity.kyc.KYCGravityActivity;

import static com.esthenos.commons.utils.Constants.ACTIVITY;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_DOCS_OTHER;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_FORM;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_KYC;
import static com.esthenos.commons.utils.Constants.ACTIVITY_GUARANTOR_KYC;
import static com.esthenos.commons.utils.Constants.UPLOAD_KYC;


public class ActivityAddMember extends ActionBarActivity {

    Autosave_Data autosave_data = new Autosave_Data();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_member_activity);

        Button buttonkyc = (Button) findViewById(R.id.add_kyc_button);
        buttonkyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_KYC);
                ActivityAddMember.this.startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        Button buttonOtherKyc = (Button) findViewById(R.id.add_other_button);
        buttonOtherKyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_DOCS_OTHER);
                ActivityAddMember.this.startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        Button buttonKycG = (Button) findViewById(R.id.add_guarantors_button);
        buttonKycG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_GUARANTOR_KYC);
                startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        Button buttonFillAPP = (Button) findViewById(R.id.fill_application);
        buttonFillAPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = autosave_data.isExist();
                if (i == 1) {
                    DialogFragment dg = new DialogFragment() {
                        // Log.v("1111111111111111", "dialog");
                        @Override
                        public Dialog onCreateDialog(Bundle savedInstanceState) {
                            return new AlertDialog.Builder(getActivity())
                                    .setMessage("Do You Want To GetBack Previous Data?")
                                    .setPositiveButton(
                                            "No",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    autosave_data.cleardata(ActivityAddMember.this);
                                                    Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                                                    intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_FORM);
                                                    ActivityAddMember.this.startActivity(intent);
                                                    //mWizardModel = new KYCGravityFormPage(getContext());

                                                }
                                            }) .setNegativeButton("Yes",new DialogInterface.OnClickListener(){
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                                            intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_FORM);
                                            ActivityAddMember.this.startActivity(intent);
                                            //mWizardModel = new KYCGravityFormPage(getContext());
                                        }
                                    }).create();
                        }
                    };
                    dg.show(getSupportFragmentManager(), "GetData_Back_Dialog");
                }else{

                    Intent intent = new Intent(ActivityAddMember.this, KYCGravityActivity.class);
                    intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_FORM);
                    ActivityAddMember.this.startActivity(intent);
                }
            }
        });

        ImageView imgback = (ImageView) findViewById(R.id.back_btn);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityAddMember.this.finish();
            }
        });
    }
}
