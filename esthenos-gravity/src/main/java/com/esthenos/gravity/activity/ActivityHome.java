package com.esthenos.gravity.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.service.SyncService;
import com.esthenos.commons.service.UploadApplicationService;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.gravity.R;


public class ActivityHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        try (MySQLiteDB db = new MySQLiteDB(this); Cursor cursor = db.getAllSubmissionData();) {
            int total = cursor.getCount();
            cursor.close();

            TextView mNoOfApplication = (TextView) findViewById(R.id.no_of_application);

            if (total >= 2) {mNoOfApplication.setText(String.format("%d Applications", total));
            } else {mNoOfApplication.setText(String.format("%d Application", total));}

        }catch (Exception e){e.printStackTrace();}

        TextView greetingText = (TextView) findViewById(R.id.tvGreeting);
        greetingText.setText(Constants.setGreeting());

        Button btnCreateGroup = (Button) findViewById(R.id.select_update_center);
        btnCreateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this, ActivitySelectCenterTimeSlot.class);
                ActivityHome.this.startActivity(intent);
            }
        });

        Button btnUploadApplication = (Button) findViewById(R.id.upload_application);
        btnUploadApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this, UploadApplicationService.class);
                startService(intent);
                Toast.makeText(ActivityHome.this, "Service started", Toast.LENGTH_LONG).show();
            }
        });

        Button btnViewStatus = (Button) findViewById(R.id.view_status);
        btnViewStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this, ActivityViewStatus.class);
                ActivityHome.this.startActivity(intent);
            }
        });
    }
}
