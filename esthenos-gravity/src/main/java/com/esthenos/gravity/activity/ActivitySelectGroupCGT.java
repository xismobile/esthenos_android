package com.esthenos.gravity.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.MiscModel;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.TimeSlotsModel;
import com.squareup.okhttp.Request;

import static com.esthenos.commons.utils.Constants.REQ_GROUPS_AVAILABLE;
import static com.esthenos.commons.utils.Constants.REQ_QUESTIONS_CGT1;
import static com.esthenos.commons.utils.Constants.REQ_QUESTIONS_CGT2;
import static com.esthenos.commons.utils.Constants.REQ_TYPE;

public class ActivitySelectGroupCGT extends Activity {

    private TimeSlotsModel.TimeSlot slot;
    private TimeSlotsModel.TimeSlots slots;
    private CenterGroupsModel.CenterTime centerTime;
    private CenterGroupsModel.CenterChange centerChange;

    ImageView mBack;
    EditText mShowCenter, mShowGroup;
    Button mSelectCenter, mSelectGroup,mTimeslot,mDaySlot;
    BootstrapButton mNext;
    SingletonConfig instance = SingletonConfig.getInstance();
    CenterGroupsModel.Center center = instance.getCenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_status_layout);
        String title = getIntent().getStringExtra(Constants.TITLE);
        final String requestType = getIntent().getStringExtra(Constants.REQ_TYPE);

        mShowCenter = (EditText) findViewById(R.id.center_name);
        mShowGroup=(EditText)findViewById(R.id.group_name);
        mTimeslot=(Button)findViewById(R.id.select_timeslot_cgt);
        mDaySlot=(Button)findViewById(R.id.select_day_cgt);

        mDaySlot.setVisibility(View.GONE);
        mTimeslot.setVisibility(View.GONE);

        if(requestType.equalsIgnoreCase(Constants.GRT_PASS)) {

            mDaySlot.setVisibility(View.VISIBLE);
            mTimeslot.setVisibility(View.VISIBLE);
            mShowCenter.setEnabled(false);
            mShowGroup.setEnabled(false);

        }
        if(requestType.equalsIgnoreCase(REQ_QUESTIONS_CGT2)) {
            mShowCenter.setEnabled(false);
            mShowGroup.setEnabled(false);
        }

            mDaySlot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySelectGroupCGT.this);
                    builder.setTitle("Select Day");

                    try {
                        final CharSequence[] days = slots.getDays();
                        builder.setSingleChoiceItems(days, -1, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                slot = slots.timeslots.get(item);
                                centerTime = new CenterGroupsModel.CenterTime(slot.day);
                                Toast.makeText(getApplicationContext(), slot.day, Toast.LENGTH_SHORT).show();
                                mDaySlot.setText(slot.day);

                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }catch (Exception e){
                        Toast.makeText(getApplicationContext(),"You dont have right to change the Days",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            });

            mTimeslot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySelectGroupCGT.this);
                    builder.setTitle("Select TimeSlot");

                    try {
                        final CharSequence[] times = slot.getTimes();
                        builder.setSingleChoiceItems(times, -1, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                centerTime.time = times[item].toString();
                                Toast.makeText(getApplicationContext(), centerTime.time, Toast.LENGTH_SHORT).show();
                                mTimeslot.setText(centerTime.time);
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }catch (Exception e){
                        Toast.makeText(getApplicationContext(),"You dont have right to change the Timeslot",Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            });

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Request request = TimeSlotsModel.getRequest(instance);
                    slots = HttpUtils.request(request, TimeSlotsModel.TimeSlots.class);
                    System.out.print("Time Slot value"+slots.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        mBack = (ImageView) findViewById(R.id.back_btn);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitySelectGroupCGT.this.finish();
            }
        });

        ((TextView) findViewById(R.id.tvName)).setText(title);
        mSelectCenter = (Button) findViewById(R.id.select_center);
        mSelectCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySelectGroupCGT.this, ActivitySelectCenter.class);
                intent.putExtra(REQ_TYPE, REQ_GROUPS_AVAILABLE);
                ActivitySelectGroupCGT.this.startActivityForResult(intent, 100);
            }
        });

        mSelectGroup = (Button) findViewById(R.id.select_group);
        mSelectGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShowCenter.getText().toString().equalsIgnoreCase("Select Center")) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectGroupCGT.this).create();
                        alertDialog.setTitle("Error");
                        alertDialog.setMessage("Select Center First");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                } else {
                    Intent intent = new Intent(ActivitySelectGroupCGT.this, ActivitySelectGroup.class);
                    intent.putExtras(getIntent());
                    ActivitySelectGroupCGT.this.startActivityForResult(intent, 200);
                }
            }
        });
        mNext= (BootstrapButton) findViewById(R.id.next_for_members);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mShowGroup.getText().toString().equalsIgnoreCase("Select Group")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectGroupCGT.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select Group First");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                } else {

                    if (requestType.equalsIgnoreCase(REQ_QUESTIONS_CGT1)) {
                        centerChange = new CenterGroupsModel.CenterChange();
                        centerChange.name = mShowCenter.getText().toString();
                        (new AsyncCenterUpdateSubmit()).execute();
                        (new AsyncGroupUpdateSubmit()).execute(mShowGroup.getText().toString());
                    }

                    if (requestType.equalsIgnoreCase(Constants.GRT_PASS)) {

                        centerTime = new CenterGroupsModel.CenterTime(mDaySlot.getText().toString());
                        centerTime.time = mTimeslot.getText().toString();
                        (new AsyncCenterTimeSubmit()).execute();

                    }

                    if (Constants.USER_ROLE_BM.equalsIgnoreCase(instance.getUser().role)){
                        Intent intent = new Intent(ActivitySelectGroupCGT.this, ActivityGRT.class);
                        intent.putExtras(getIntent());
                        ActivitySelectGroupCGT.this.startActivity(intent);
                    }else {
                        Intent intent = new Intent(ActivitySelectGroupCGT.this, ActivityCGT.class);
                        intent.putExtras(getIntent());
                        ActivitySelectGroupCGT.this.startActivity(intent);
                    }
                }
                }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case 100:
                    try {
                        if (resultCode == RESULT_OK) {
                            CenterGroupsModel.Center center = instance.getCenter();
                            mShowCenter.setText(center.name);

                            if (center.timeslot != null) {
                                mDaySlot.setText(center.timeslot.day);
                                mTimeslot.setText(center.timeslot.time);
                            }
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                case 200:
                    if (resultCode == RESULT_OK) {
                        CenterGroupsModel.Group group = instance.getGroup();
                        mShowGroup = (EditText) findViewById(R.id.group_name);
                        mShowGroup.setText(group.name);
                        break;
                    }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class AsyncCenterTimeSubmit extends AsyncTask<String, Void, MiscModel.SubmitForm> {
        private final ProgressDialog dialog = new ProgressDialog(ActivitySelectGroupCGT.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Submitting Center Meeting Time");
            dialog.show();
        }

        @Override
        protected MiscModel.SubmitForm doInBackground(String... params) {
            MiscModel.SubmitForm response = null;
            try {

                Request request = CenterGroupsModel.postUpdateTimeRequest(instance, centerTime);
                response = HttpUtils.request(request, MiscModel.SubmitForm.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(MiscModel.SubmitForm response) {
            if (dialog.isShowing()) dialog.dismiss();
            try {
                Toast.makeText(getApplicationContext(), response.message, Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    private class AsyncGroupUpdateSubmit extends AsyncTask<String, Void, MiscModel.SubmitForm> {
        private final ProgressDialog dialog = new ProgressDialog(ActivitySelectGroupCGT.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("updating Group and Center");
            dialog.show();
        }

        @Override
        protected MiscModel.SubmitForm doInBackground(String... params) {
            MiscModel.SubmitForm response = null;
            try {

                Request request = CenterGroupsModel.postUpdatedGroupName(instance, params[0]);
                response = HttpUtils.request(request, MiscModel.SubmitForm.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(MiscModel.SubmitForm response) {
            if (dialog.isShowing()) dialog.dismiss();
            try {
                Toast.makeText(getApplicationContext(), response.message, Toast.LENGTH_SHORT).show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class AsyncCenterUpdateSubmit extends AsyncTask<String, Void, MiscModel.SubmitForm> {

        @Override
        protected MiscModel.SubmitForm doInBackground(String... params) {
            MiscModel.SubmitForm response = null;
            try {
                Request request = CenterGroupsModel.postUpdatedCenterName(instance, centerChange);
                response = HttpUtils.request(request, MiscModel.SubmitForm.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(MiscModel.SubmitForm response) {
           try {
               Toast.makeText(getApplicationContext(), response.message, Toast.LENGTH_SHORT).show();
           }catch (Exception e){
               Toast.makeText(getApplicationContext(), "Centre not updated,Please try again", Toast.LENGTH_SHORT).show();
           }
        }
    }
}
