package com.esthenos.gravity.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.ApplicationModel.Application;
import com.esthenos.commons.models.CenterGroupsModel.Group;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.ApplicationListAdapter;
import com.squareup.okhttp.Request;

import java.util.ArrayList;
import java.util.List;


public class ActivityListApplications extends ActionBarActivity {

    private ApplicationListAdapter adapter;
    private SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.application_list_activity);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityListApplications.this.finish();
            }
        });

        adapter = new ApplicationListAdapter(new ArrayList<Application>(), this);
        ListView lView = (ListView) findViewById(R.id.listView);
        lView.setAdapter(adapter);

        (new AsyncListViewLoader()).execute();
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, List<Application>> {
        private final ProgressDialog dialog = new ProgressDialog(ActivityListApplications.this);
        final String requestType = getIntent().getStringExtra(Constants.REQ_TYPE);

        @Override
        protected void onPostExecute(List<Application> result) {
            super.onPostExecute(result);
            dialog.dismiss();
            adapter.setItemList(result);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("loading applications...");
            dialog.show();
        }

        @Override
        protected List<Application> doInBackground(String... params) {
            List<Application> result = null;
            try {
                Request request = ApplicationModel.getRequest(instance,requestType);
                ApplicationModel.ApplicationResponse response = HttpUtils.request(request, ApplicationModel.ApplicationResponse.class);
                result = response.applications;

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ActivityListApplications.this, "Failed To Applications List", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
            return result;
        }
    }
}


