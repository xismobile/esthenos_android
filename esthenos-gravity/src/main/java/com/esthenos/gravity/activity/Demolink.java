package com.esthenos.gravity.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.esthenos.gravity.R;
import com.esthenos.gravity.pages.FamilyAssetFragment;

public class Demolink extends Activity {

    Button fafrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demolink);
        fafrag = (Button)findViewById(R.id.familyassetfrag);
        fafrag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i= new Intent(Demolink.this, FamilyAssetFragment.class);
                startActivity(i);
            }
        });



    }


    public int visiblity(int oppcode,View view){

        switch (oppcode){
            case 1:
                if (view.getVisibility()==View.VISIBLE){

                    view.setVisibility(View.INVISIBLE);
                }


        }
        return 0;
    }
}
