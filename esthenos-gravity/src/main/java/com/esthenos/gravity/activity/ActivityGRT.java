
package com.esthenos.gravity.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.MiscModel;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.ApplicantGRTListAdapter;
import com.esthenos.gravity.models.CGTModel;
import com.esthenos.commons.models.ProductsModel;
import com.squareup.okhttp.Request;

import java.util.ArrayList;
import java.util.List;

public class ActivityGRT extends Activity {

    ListView listView;
    ApplicantGRTListAdapter adapter;
    BootstrapButton mSubmit;
    CheckBox mQuestionChecked;
    TextView mCenterName, mGroupName, mGreeting,mCGTQuestions;

    ApplicationModel.ApplicationResponse responses = null;
    List<ApplicationModel.Application> applications = null;
    SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cgt_applicant_list);
        final String requestType = getIntent().getStringExtra(Constants.REQ_TYPE);

        mCGTQuestions = (TextView) findViewById(R.id.cgtquestions);
        mCGTQuestions.setText(requestType + "completed as per organisations guidelines and to the best of my knowledge. Feedback submitted");

        mQuestionChecked = (CheckBox) findViewById(R.id.cgtcheck);

        mSubmit = (BootstrapButton) findViewById(R.id.submit_button);
        mSubmit.setEnabled(false);

        mQuestionChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mQuestionChecked.isChecked()) {
                    mSubmit.setEnabled(true);
                } else {
                    mSubmit.setEnabled(false);
                }
            }
        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checked = Boolean.toString(mQuestionChecked.isChecked());

                Intent intent = new Intent(getApplicationContext(),ActivityLocCenter.class);
                intent.putExtra(Constants.REQ_TYPE,requestType);
                intent.putExtra(Constants.CHECKED,checked);
                startActivity(intent);
            }
        });

        try {
            CenterGroupsModel.Center center = instance.getCenter();
            mCenterName = (TextView) findViewById(R.id.showcentername);
            mCenterName.setText(center.name);

            CenterGroupsModel.Group group = instance.getGroup();
            mGroupName = (TextView) findViewById(R.id.showgroup);
            mGroupName.setText(group.name);

            mGreeting = (TextView) findViewById(R.id.tvGreeting);
            String greeting = String.format("%s", requestType.toUpperCase());
            mGreeting.setText(greeting);
            (new AsyncFetchApplicants()).execute(requestType);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class AsyncFetchApplicants extends AsyncTask<String, Void, ApplicationModel.ApplicationResponse> {
        private final ProgressDialog dialog = new ProgressDialog(ActivityGRT.this);
        final String requestType = getIntent().getStringExtra(Constants.REQ_TYPE);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading Applicants");
            dialog.show();
        }

        @Override
        protected ApplicationModel.ApplicationResponse doInBackground(String... params) {
            try {
                Request request = ApplicationModel.getRequest(instance, requestType);
                responses = HttpUtils.request(request, ApplicationModel.ApplicationResponse.class);

                for (ProductsModel.Product product : responses.products) {
                    PRODUCTS.add(product);
                }
                applications = responses.applications;
                instance.setApplicationResponse(responses);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return responses;
        }

        @Override
        protected void onPostExecute(ApplicationModel.ApplicationResponse response) {
            if (dialog.isShowing()) dialog.dismiss();
            try {
                listView = (ListView) findViewById(R.id.list);
                adapter = new ApplicantGRTListAdapter(ActivityGRT.this, applications, PRODUCTS);
                listView.setAdapter(adapter);
                listView.setTextFilterEnabled(true);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public ArrayList<ProductsModel.Product> PRODUCTS = new ArrayList<ProductsModel.Product>();
}

