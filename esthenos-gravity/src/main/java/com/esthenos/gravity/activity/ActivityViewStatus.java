package com.esthenos.gravity.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;

import static com.esthenos.commons.utils.Constants.REQ_GROUPS_STATUS;
import static com.esthenos.commons.utils.Constants.REQ_TYPE;
import static com.esthenos.commons.utils.Constants.VIEW_STATUS;


public class ActivityViewStatus extends ActionBarActivity {

    private EditText groupView, centerView;
    private Button mBtnTime,mBtnDay;

    private SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_status_layout);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityViewStatus.this.finish();
            }
        });

        ((TextView) findViewById(R.id.tvName)).setText("View Status");

        mBtnDay=(Button)findViewById(R.id.select_day_cgt);
        mBtnTime=(Button)findViewById(R.id.select_timeslot_cgt);

        mBtnTime.setVisibility(View.GONE);
        mBtnDay.setVisibility(View.GONE);

        centerView = (EditText) findViewById(R.id.center_name);
        centerView.setEnabled(false);
        Button button = (Button) findViewById(R.id.select_center);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityViewStatus.this, ActivitySelectCenter.class);
                intent.putExtra(REQ_TYPE, REQ_GROUPS_STATUS);
                ActivityViewStatus.this.startActivityForResult(intent, 10);
            }
        });

        groupView = (EditText) findViewById(R.id.group_name);
        groupView.setEnabled(false);
        Button button2 = (Button) findViewById(R.id.select_group);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (centerView.getText().length() == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivityViewStatus.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select a center");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }

                Intent intent = new Intent(ActivityViewStatus.this, ActivitySelectGroup.class);
                intent.putExtra(REQ_TYPE, REQ_GROUPS_STATUS);
                ActivityViewStatus.this.startActivityForResult(intent, 20);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case 10:
                    if (resultCode == RESULT_OK) {
                        CenterGroupsModel.Center center = instance.getCenter();
                        Toast.makeText(this, String.format("Selected Center: %s", center.name), Toast.LENGTH_LONG).show();
                        centerView.setText(center.name);
                        break;
                    }

                case 20:
                    if (resultCode == RESULT_OK) {
                        CenterGroupsModel.Group group = instance.getGroup();
                        Toast.makeText(this, String.format("Selected Group: %s", group.name), Toast.LENGTH_LONG).show();
                        this.groupView.setText(group.name);

                        Intent intent = new Intent(ActivityViewStatus.this, ActivityListApplications.class);
                        intent.putExtra(REQ_TYPE,VIEW_STATUS);
                        ActivityViewStatus.this.startActivity(intent);
                        break;
                    }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
