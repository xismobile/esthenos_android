package com.esthenos.gravity.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.ProductsModel;
import com.esthenos.gravity.R;
import com.esthenos.commons.utils.SingletonConfig;
import static com.esthenos.commons.utils.Constants.*;


public class ActivitySelectGroupProduct extends ActionBarActivity {

    private SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_product_layout);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySelectGroupProduct.this.finish();
            }
        });

        Button button = (Button) findViewById(R.id.select_a_group);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySelectGroupProduct.this, ActivitySelectGroup.class);
                intent.putExtra(REQ_TYPE, REQ_GROUPS_AVAILABLE);
                ActivitySelectGroupProduct.this.startActivityForResult(intent, 10);
            }
        });

        Button button2 = (Button) findViewById(R.id.select_a_product);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySelectGroupProduct.this, ActivitySelectProduct.class);
                ActivitySelectGroupProduct.this.startActivityForResult(intent, 20);
            }
        });
        BootstrapButton button3 = (BootstrapButton) findViewById(R.id.show_members);

        button3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final TextView group_name = (TextView) findViewById(R.id.group_name);
                final TextView product_name = (TextView) findViewById(R.id.product_name);
                if (group_name.getText().length() == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectGroupProduct.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select a group");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else if (product_name.getText().length() == 0) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ActivitySelectGroupProduct.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Select a product");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();

                } else {
                    Intent intent = new Intent(ActivitySelectGroupProduct.this, ActivityShowMembers.class);
                    ActivitySelectGroupProduct.this.startActivity(intent);
                }
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            switch (requestCode) {
                case 10:
                    if (resultCode == RESULT_OK) {
                        CenterGroupsModel.Group group = instance.getGroup();
                        Toast.makeText(this, String.format("Selected Group: %s", group.name), Toast.LENGTH_LONG).show();
                        TextView tv = (TextView) findViewById(R.id.group_name);
                        tv.setText(group.name);
                        break;
                    }
                case 20:
                    if (resultCode == RESULT_OK) {
                        ProductsModel.Product product = instance.getProduct();
                        Toast.makeText(this, String.format("Selected Product: %s", product), Toast.LENGTH_LONG).show();
                        TextView tv = (TextView) findViewById(R.id.product_name);
                        tv.setText(product.product_name);
                        break;
                    }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
