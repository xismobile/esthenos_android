package com.esthenos.gravity.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.ApplicationModel.Application;
import com.esthenos.commons.models.ApplicationModel.ApplicationResponse;
import com.esthenos.commons.models.CenterGroupsModel;
import com.esthenos.commons.models.MiscModel.SubmitForm;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.gravity.models.ApplicationListAdapter;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.ArrayList;


public class ActivityShowMembers extends ActionBarActivity {

    private ApplicationListAdapter adapter;

    private SingletonConfig instance = SingletonConfig.getInstance();
    private final CenterGroupsModel.Group group = instance.getGroup();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_member_activity);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityShowMembers.this.finish();
            }
        });

        BootstrapButton button = (BootstrapButton) findViewById(R.id.add_member);
        TextView tv = (TextView) findViewById(R.id.GroupName);
        tv.setText(String.format("Group Name:" + group.name));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (group.size_group == 0) {
                    updateGroupDetails();
                } else{
                    Intent intent = new Intent(ActivityShowMembers.this, ActivityAddMember.class);
                ActivityShowMembers.this.startActivity(intent);
            }
            }
        });

        adapter = new ApplicationListAdapter(new ArrayList<Application>(), this);
        ListView lView = (ListView) findViewById(R.id.listView);
        lView.setAdapter(adapter);

             if (group.size_group > 0 && group.complete) {
                        button.setEnabled(false);
                        button.setClickable(false);

                    } else {
                        (new AsyncListViewLoader()).execute();
                    }
                }

    private void updateGroupDetails() {
        LayoutInflater layoutInflater = LayoutInflater.from(ActivityShowMembers.this);
        final View promptView = layoutInflater.inflate(R.layout.group_input_dialog_layout, null);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ActivityShowMembers.this);

        alertDialogBuilder.setView(promptView);
        alertDialogBuilder.setTitle("Update Group Details");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final EditText dgGroupSize = (EditText) promptView.findViewById(R.id.dialog_group_size);
                final EditText dgLeaderName = (EditText) promptView.findViewById(R.id.dialog_group_leader_name);
                final EditText dgLeaderNumber = (EditText) promptView.findViewById(R.id.dialog_group_leader_number);

                final String sGroupSize = dgGroupSize.getText().toString();
                final String sLeaderName = dgLeaderName.getText().toString();
                final String sLeaderNumber = dgLeaderNumber.getText().toString();

                int sGroupSizeValue = sGroupSize.trim().length() != 0 ? Integer.parseInt(sGroupSize) : 0;

                if (sGroupSize.trim().length() == 0 ||
                        sLeaderName.trim().length() == 0 ||
                        sLeaderNumber.trim().length() == 0) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please fill all fields", Toast.LENGTH_LONG).show();
                        }
                    });
                    finish();

                } else if (sGroupSizeValue < Integer.parseInt(instance.getProduct().group_min) ||
                        sGroupSizeValue > Integer.parseInt(instance.getProduct().group_max)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(
                                    getApplicationContext(), "Please give min" + " " + Integer.parseInt(instance.getProduct().group_min)
                                            + " and max " + Integer.parseInt(instance.getProduct().group_max), Toast.LENGTH_LONG).show();
                            finish();
                        }
                    });
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                System.out.println("product_id" + instance.getProduct().product_id);
                                Request request = CenterGroupsModel.postGroupDetails(
                                        instance,
                                        dgGroupSize.getText().toString(),
                                        dgLeaderName.getText().toString(),
                                        dgLeaderNumber.getText().toString(),
                                        ""
                                );
                                final SubmitForm response = HttpUtils.request(request, SubmitForm.class);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //todo fix this later.
                                        group.size_group = Integer.parseInt(dgGroupSize.getText().toString());
                                        try {
                                            Toast.makeText(ActivityShowMembers.this, response.message, Toast.LENGTH_SHORT).show();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            } catch (IOException e) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ActivityShowMembers.this, "Failed To Update Group Details.", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                e.printStackTrace();
                            }
                        }

                    }).start();
                }
            }
        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private class AsyncListViewLoader extends AsyncTask<String, Void, ApplicationResponse> {
        private final ProgressDialog dialog = new ProgressDialog(ActivityShowMembers.this);
        final String requestType = Constants.VIEW_STATUS;

        @Override
        protected void onPostExecute(final ApplicationResponse response) {
            super.onPostExecute(response);
            dialog.dismiss();
            try {
                adapter.setItemList(response.applications);
                adapter.notifyDataSetChanged();
                CenterGroupsModel.Group group = instance.getGroup();

                System.out.println("result_data" + response.applications.toString());
                System.out.println("result" + response.applications.toString());
                System.out.println("result" + response.applications);

                TextView tv = (TextView) findViewById(R.id.GroupSize);
                tv.setText(String.format("Group Size: %s / %s", group.size_members, group.size_group));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Loading Applications...");
            dialog.show();
        }

        @Override
        protected ApplicationResponse doInBackground(String... params) {
            ApplicationResponse response = null;
            try {
                Request request = ApplicationModel.getRequest(instance,requestType);
                response = HttpUtils.request(request, ApplicationResponse.class);

            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ActivityShowMembers.this, "Failed To Applications List", Toast.LENGTH_SHORT).show();
                    }
                });
                e.printStackTrace();
            }
            return response;
        }
    }
}


