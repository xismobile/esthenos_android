package com.esthenos.gravity.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.esthenos.commons.models.ProductsModel;
import com.esthenos.commons.models.ProductsModel.ProductsResponse;
import com.squareup.okhttp.Request;

import java.io.IOException;
import java.util.ArrayList;


public class ActivitySelectProduct extends Activity implements SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

    private static final String TAG = "SearchViewFilterMode";

    private GridView mListView;
    private SearchView mSearchView;
    private SingletonConfig instance = SingletonConfig.getInstance();
    private ArrayAdapter<ProductsModel.Product> mAdapter;
    private ArrayList<ProductsModel.Product> PRODUCTS = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.searchview_filter);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySelectProduct.this.finish();
            }
        });

        mSearchView = (SearchView) findViewById(R.id.search_view);
        final int identifier = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        AutoCompleteTextView search_text = (AutoCompleteTextView) mSearchView.findViewById(identifier);
        search_text.setTextColor(Color.WHITE);
        search_text.setHintTextColor(Color.LTGRAY);

        int searchButtonId = getResources().getIdentifier("android:id/search_button", null, null);
        ImageView searchButtonImage = (ImageView) mSearchView.findViewById(searchButtonId);
        searchButtonImage.setImageResource(R.drawable.search);

        int closeButtonId = getResources().getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButtonImage = (ImageView) mSearchView.findViewById(closeButtonId);
        closeButtonImage.setImageResource(R.drawable.close);

        ((TextView) findViewById(R.id.tvfilter)).setText("Select Products");
        mListView = (GridView) findViewById(R.id.list_view);
        try {
            mListView.setAdapter(mAdapter = new ArrayAdapter<ProductsModel.Product>(this, R.layout.grid_item, PRODUCTS));
            mListView.setTextFilterEnabled(true);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent returnIntent = new Intent();
                    instance.setProduct(PRODUCTS.get(position));
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
        setupSearchView();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Request request = ProductsModel.getRequest(instance);
                    ProductsResponse response = HttpUtils.request(request, ProductsResponse.class);
                    for (ProductsModel.Product product : response.products) {
                        PRODUCTS.add(product);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ActivitySelectProduct.this, "Failed To Fetch Product Details", Toast.LENGTH_SHORT).show();
                        }
                    });
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        String suggestion = getSuggestion(position);
        mSearchView.setQuery(suggestion, true); // submit query now
        return true; // replace default search manager behaviour
    }

    private String getSuggestion(int position) {
        Cursor cursor = (Cursor) mSearchView.getSuggestionsAdapter().getItem(
                position);
        String suggest1 = cursor.getString(cursor
                .getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1));
        return suggest1;
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Hint text");
    }

    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            mListView.clearTextFilter();
        } else {
            mListView.setFilterText(newText.toString());
        }
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
