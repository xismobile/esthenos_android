package com.esthenos.gravity.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.models.MiscModel;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.gravity.R;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.models.CGTModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.Request;

public class ActivityLocCenter extends FragmentActivity implements LocationListener {

    double latitude;
    double longitude;
    GoogleMap googleMap;
    Context mContext;
    private SingletonConfig instance = SingletonConfig.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        setContentView(R.layout.map_activity);
        try {
            SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
            googleMap = supportMapFragment.getMap();
            googleMap.setMyLocationEnabled(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);

        if (location != null) {
            onLocationChanged(location);
        }
        locationManager.requestLocationUpdates(locationManager.NETWORK_PROVIDER, 20000, 0, this);

        ((ImageView) findViewById(R.id.back_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLocCenter.this.finish();
            }
        });

        BootstrapButton button3 = (BootstrapButton) findViewById(R.id.show_members);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final String requestType = getIntent().getStringExtra(Constants.REQ_TYPE) == null ? "fail" : getIntent().getStringExtra(Constants.REQ_TYPE);
                    final String questionType = getIntent().getStringExtra(Constants.CHECKED);

                    instance.setLocation(Constants.LOCATION_CENTER, latitude, longitude);
                    String lat = Double.toString(latitude);
                    String lon = Double.toString(longitude);

                    if(requestType.equalsIgnoreCase(Constants.REQ_QUESTIONS_CGT1) || requestType.equalsIgnoreCase(Constants.REQ_QUESTIONS_CGT2) ||
                            requestType.equalsIgnoreCase(Constants.REQ_QUESTIONS_GRT)){

                        (new AsyncStatusSubmit()).execute(requestType,questionType,lat,lon);
                    }else {
                        Intent intent = new Intent(ActivityLocCenter.this, ActivitySelectGroupProduct.class);
                        ActivityLocCenter.this.startActivity(intent);
                    }
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Center Location tag fail",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);

            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }catch (Exception e){e.printStackTrace();}

    }
    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    private class AsyncStatusSubmit extends AsyncTask<String, Void, MiscModel.SubmitForm> {
        private final ProgressDialog dialog = new ProgressDialog(ActivityLocCenter.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Submitting Status");
            dialog.show();
        }

        @Override
        protected MiscModel.SubmitForm doInBackground(String... params) {
            MiscModel.SubmitForm response = null;
            try {
                CGTModel.CGTApplicantResponses responses = CGTModel.getCGTApplicantResponses(instance, instance.getApplicationResponse().applications, params[1],params[2],params[3]);
                Request request = CGTModel.postRequest(instance, responses, params[0]);
                response = HttpUtils.request(request, MiscModel.SubmitForm.class);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(final MiscModel.SubmitForm response) {
            if (dialog.isShowing()) dialog.dismiss();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(ActivityLocCenter.this, response.message, Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
            Intent intent = new Intent(getApplicationContext(), ActivityCGTGroup.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
