package com.esthenos.gravity.kyc;

import android.content.Context;

import com.esthenos.commons.pages.AdditionalFamilyDetailFirstPage;
import com.esthenos.commons.pages.AdditionalFamilyDetailPage;
import com.esthenos.commons.pages.AdditionalFamilyDetailSecondPage;
import com.esthenos.commons.pages.AppliedLoanDetailPage;
import com.esthenos.commons.pages.BankDetailsPage;
import com.esthenos.commons.pages.BazaarBusinessDetailsPage;
import com.esthenos.commons.pages.BazaarDetailsPage;
import com.esthenos.commons.pages.CustomerKYCDetailsPage;
import com.esthenos.commons.pages.CustomerPersonalDetailsPage;
import com.esthenos.commons.pages.FacilitatedProductPage;
import com.esthenos.commons.pages.FamilyExpenditurePage;
import com.esthenos.commons.pages.FamilyIndeptnessDetailPage;
import com.esthenos.commons.pages.FamilyMembersPage;
import com.esthenos.commons.pages.FinancialLiabilityPage;
import com.esthenos.commons.pages.GuarantorsInformationPage;
import com.esthenos.commons.pages.IncomefromBusinessPage;
import com.esthenos.commons.pages.LocationHomePage;
import com.esthenos.commons.pages.NeighbourFeedbackPage;
import com.esthenos.commons.pages.ProgressOutPropertyFirstPage;
import com.esthenos.commons.pages.ProgressOutPropertySecondPage;
import com.esthenos.commons.pages.RiskEvaluationPage;
import com.esthenos.commons.pages.SixCCompliancePage;
import com.esthenos.commons.pages.SpecifyRelationshipPage;
import com.esthenos.gravity.pages.FamilyAssetPage;
import com.esthenos.gravity.pages.GravityBusinessDetailPage;
import com.esthenos.gravity.pages.PrimaryBusinessDetailsSecondPage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.MultipleFixedChoicePage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.GUARANTOR1;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_BUSINESS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_FAMILY_DETAILS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_FAMILY_EXPENDITURE;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_KYC;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_LOAN_DETAILS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_OTHER;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_PERSONAL;


public class KYCGravityFormPage extends AbstractWizardModel {

    public KYCGravityFormPage(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(

                    new CustomerKYCDetailsPage(this,
                           new PageConfig(APPLICANT, PAGE_TYPE_KYC, "Personal Information")
                   ),
                new CustomerPersonalDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Personal Details")
                ),
                new FamilyMembersPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Member Details", "_members")
                ),

                new AdditionalFamilyDetailFirstPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 1", "_family_members1")
                ),
                new AdditionalFamilyDetailSecondPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 2", "_family_members2")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 3", "_family_members3")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 4", "_family_members4")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 5", "_family_members5")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 6", "_family_members6")
                ),

                new FamilyAssetPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Asset Page", "_assets1")
                ),

                new MultipleFixedChoicePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Other Assets", "_assets2")
                ).setChoices("Bike", "Cycle", "Tv", "Gas", "None of The Above"),


                new MultipleFixedChoicePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Source of drinking water", "_water_source")
                ).setChoices("Piped", "Borewell", "Shared Borewell", "Govt Bore Well", "Open Well"),

                new GuarantorsInformationPage(this,
                        new PageConfig(GUARANTOR1, PAGE_TYPE_KYC, "Guarantors Information")
                ),
//..................
                new BranchPage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Guarantors Relationship With Borrower", "_guarantor_borrower_relation"))
                        .addBranch("Brother")
                        .addBranch("Sister")
                        .addBranch("Spouse")
                        .addBranch("Mother")
                        .addBranch("Father")
                        .addBranch("Daughter")
                        .addBranch("Son")
                        .addBranch("Grand Mother")
                        .addBranch("Grand Father")
                        .addBranch("Grand Son")
                        .addBranch("Grand Daughter")
                        .addBranch("Relative")
                        .addBranch("Father in Law")
                        .addBranch("Mother in Law")
                        .addBranch("Brother in Law")
                        .addBranch("Daughter in Law")
                        .addBranch("Sister in Law",
                                new SpecifyRelationshipPage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Guarantors Relationship With Borrower", "_guarantor_borrower_relation_specify"))
                        ),
/*

                new BranchPage(this, new PageConfig(APPLICANT, PAGE_TYPE_NOMINEE, "Are Guarantors-Borrowers Nominee for each other ?"))
                        .addBranch("Yes")
                        .addBranch("No",
                                new NomineeDetailsPage(this, new PageConfig(APPLICANT, PAGE_TYPE_NOMINEE, "Borrower Nominee Details", "_borrower")),
                                new NomineeDetailsPage(this, new PageConfig(APPLICANT, PAGE_TYPE_NOMINEE, "Guarantor Nominee Details", "_guarantor"))
                        ),
*/

                new PrimaryBusinessDetailsSecondPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Primary Business Details", "_details1")
                ),

                new GravityBusinessDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Secondary Business Details", "_details2")
                ),

                new GravityBusinessDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Tertiary Business Details", "_details3")
                ),

                new FamilyExpenditurePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_EXPENDITURE, "Family Expenditure")
                ),

                new FinancialLiabilityPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Financial Liabilities(Monthly)", "_liabilities")
                ),
//........................
                new AppliedLoanDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Applied Loan Details", "_applied_loan")
                ),

                new BankDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Bank Details", "_bank_details")
                ),

                new FamilyIndeptnessDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Indebtness Details 1", "_indent_Detail_1")
                ),

                new FamilyIndeptnessDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Indebtness Details 2", "_indent_Detail_2")
                ),

                new ProgressOutPropertyFirstPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Progress Out of Poverty Index 1", "_progress_poverty1")
                ),

                new ProgressOutPropertySecondPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Progress Out of Poverty Index 2", "_progress_poverty2")
                ),
//....................
                new BranchPage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Is Loan Application for Bazaar Customers ?"))
                        .addBranch("Yes",
                                new BazaarDetailsPage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Bazaar Details", "_bazar_details")),
                                new BazaarBusinessDetailsPage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Bazaar Business Details", "_bazaar_business_detail")),
                                new MultipleFixedChoicePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Peak Business Months", "_peak_business_month"))
                                        .setChoices("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
                                new MultipleFixedChoicePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Lean Business Months", "_lean_business_month"))
                                        .setChoices("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"),
                                new IncomefromBusinessPage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Income from Business", "_income_buss")),
                                new SixCCompliancePage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "6 C Compliance(For Bazaar Customers)", "_six_comp_bazaar")))
                        .addBranch("No"),

                new NeighbourFeedbackPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Neighbour Feedback", "_neighbour_feedback")
                ),

                new FacilitatedProductPage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Facilitated Product", "_facilitated")
                ),

                new RiskEvaluationPage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Risk Evaluation", "_risk_evaluation")
                ),

                new SixCCompliancePage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "6 C Compliance", "_six_compliance")
                ),

                new LocationHomePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Home Location", "_lochome")
                )
        );
    }
}
