package com.esthenos.gravity.kyc;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.esthenos.commons.kyc.KYCModelApplicant;
import com.esthenos.commons.service.UploadApplicationService;
import com.esthenos.commons.uploadservice.AbstractUploadServiceReceiver;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.gravity.R;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.ReviewItem;
import com.tech.freak.wizardpager.ui.NumberFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;
import com.tech.freak.wizardpager.ui.ReviewFragment;
import com.tech.freak.wizardpager.ui.StepPagerStrip;
import com.tech.freak.wizardpager.ui.TextFragment;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.esthenos.commons.utils.Constants.ACTIVITY;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_DOCS_OTHER;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_FORM;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_KYC;
import static com.esthenos.commons.utils.Constants.ACTIVITY_GUARANTOR_KYC;


public class KYCGravityActivity extends ActionBarActivity implements PageFragmentCallbacks, ReviewFragment.Callbacks, ModelCallbacks {

    Autosave_Data autosave_data = new Autosave_Data();

/*
    private static KYCGravityActivity mInstance = null;
    private final Context mCxt;


    public static KYCGravityActivity getInstance(Context ctx) {
        */
    /**
     * use the application context as suggested by CommonsWare.
     * this will ensure that you dont accidentally leak an Activitys
     * context (see this article for more information:
     * http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
     *//*
        if (mInstance == null) {
            mInstance = new KYCGravityActivity(ctx.getApplicationContext());
        }
        return mInstance;
    }
    private KYCGravityActivity(Context ctx) {
        this.mCxt = ctx;
    }*/

    private static final String TAG = "KYCActivity";
    private final AbstractUploadServiceReceiver uploadReceiver = new AbstractUploadServiceReceiver() {
        @Override
        public void onProgress(String uploadId, int progress) {
            Log.i(TAG, "The progress of the upload with ID " + uploadId + " is: " + progress);
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            String message = "Error in upload with ID: " + uploadId + ". " + exception.getLocalizedMessage();
            Log.e(TAG, message, exception);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, String serverResponseMessage) {
            String message = "Upload with ID " + uploadId + " is completed: " + serverResponseCode + ", " + serverResponseMessage;
            Log.i(TAG, message);
        }
    };
    private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;
    private boolean mEditingAfterReview;
    private AbstractWizardModel mWizardModel;
    private boolean mConsumePageSelectedEvent;
    private Button mNextButton;
    private Button mPrevButton;
    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;
    private SingletonConfig instance = SingletonConfig.getInstance();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc_main);

        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }

        switch (getIntent().getStringExtra(ACTIVITY)) {

            case ACTIVITY_APPLICANT_KYC:
                mWizardModel = new KYCModelApplicant(this);
                break;

            case ACTIVITY_GUARANTOR_KYC:
                mWizardModel = new KYCGravityGuarantor(this);
                break;

            case ACTIVITY_APPLICANT_FORM:
                    mWizardModel = new KYCGravityFormPage(this);
                    break;

            case ACTIVITY_APPLICANT_DOCS_OTHER:
                mWizardModel = new KYCGravityOtherDocs(this);
                break;
        }

        mWizardModel.registerListener(this);
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);
                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }
                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = mPagerAdapter.getItem(mPager.getCurrentItem() + 1);
                if (fragment.getClass().equals(NumberFragment.class)) {
                    //show number key board
                    // Hide the keyboard.
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .showSoftInputFromInputMethod(mPager.getWindowToken(), 0);
                } else if (fragment.getClass().equals(TextFragment.class)) {
                    //show text key board
                    ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                            .showSoftInputFromInputMethod(mPager.getWindowToken(), 0);
                }
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                    DialogFragment dg = new DialogFragment() {
                        @Override
                        public Dialog onCreateDialog(Bundle savedInstanceState) {
                            return new AlertDialog.Builder(getActivity())
                                    .setMessage(R.string.submit_confirm_message)
                                    .setPositiveButton(
                                            R.string.submit_confirm_button,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    new SubmitApplicationAsync().execute();
                                                }
                                            })
                                    .setNegativeButton(android.R.string.cancel,
                                            null).create();
                        }
                    };
                    dg.show(getSupportFragmentManager(), "place_order_dialog");
                } else {

                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {


                       // Toast.makeText(KYCGravityActivity.this, " frag : " + mPager.getCurrentItem(), Toast.LENGTH_SHORT).show();

                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        onPageTreeChanged();
        updateBottomBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        uploadReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1);
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure, You want to go home page ?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {

                    finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg, int arg2) {

                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
        return true;
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText(R.string.finish);
            mNextButton.setBackgroundResource(R.drawable.finish_background);
            mNextButton.setTextAppearance(this, R.style.TextAppearanceFinish);

        } else {
            mNextButton.setText(mEditingAfterReview ? R.string.review : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.selectable_item_background);

            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }

        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getPageId().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    public boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void checkfragment() {


    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            return Math.min(mCutOffPage + 1, mCurrentPageSequence == null ? 1 : mCurrentPageSequence.size() + 1);
        }

        public int getCutOffPage() {
            return mCutOffPage;
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }
    }

    private class SubmitApplicationAsync extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(Void... params) {

            final SingletonConfig instance = SingletonConfig.getInstance();
            final String appId = instance.getAppId();
            final String url = instance.getGroupApplications("");
            final Map<String, Object> form = new HashMap<String, Object>() {{
                putAll(instance.getAppDetails());
            }};

            try {
                for (Page page : mWizardModel.getCurrentPageSequence()) {
                    Map<String, String> pageFormItems = new HashMap<String, String>();
                    ArrayList<ReviewItem> pageReviewItems = new ArrayList<ReviewItem>();

                    page.getReviewItems(pageReviewItems);
                    for (ReviewItem item : pageReviewItems) {
                        String value = item.getDisplayValue();
                        String name = item.getTitle()
                                .toLowerCase()
                                .replace("'", "_")
                                .replace("/", "_")
                                .replace("-", "_")
                                .replace(".", "")
                                .replace(" ", "_")
                                .replace("(", "_")
                                .replace(")", "")
                                .replace(":", "_")
                                .replace("?", "")
                                .replace("&", "");
                        pageFormItems.put(name, value);
                    }

                    form.put(page.getFormId(), new JSONObject(pageFormItems));
                }

                String formData = new JSONObject(form).toString();
                Log.d(TAG, "form-data.length: " + formData.length());

                int chunkCount = formData.length() / 4000;
                for (int i = 0; i <= chunkCount; i++) {
                    int max = 4000 * (i + 1);
                    if (max >= formData.length()) {
                        Log.d(TAG, "form-data.chunk:" + i + " of " + chunkCount + ":" + formData.substring(4000 * i));
                    } else {
                        Log.d(TAG, "form-data.chunk:" + i + " of " + chunkCount + ":" + formData.substring(4000 * i, max));
                    }
                }

                try (MySQLiteDB db = new MySQLiteDB(getApplicationContext())) {
                    db.insertSubmissionData(appId, url, formData);
                    startService(new Intent(KYCGravityActivity.this, UploadApplicationService.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "successful : " + appId;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            instance.reset();
            finish();
        }

    }
}
