package com.esthenos.gravity.kyc;

import android.content.Context;

import com.esthenos.commons.pages.ImageCapturePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.*;
import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.PAGE_BRANCH;


public class KYCGravityOtherDocs extends AbstractWizardModel {
    public KYCGravityOtherDocs(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
            new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, "Add Other Documents"))
                .addBranch("Borrowers Photograph",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Borrowers Photograph", "_borrower"))
                )
                .addBranch("Co-Borrowers Photograph",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Co-Borrowers Photograph", "_coborrower"))
                )
                .addBranch("Ration Card",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Ration Card", "_ration_card"))
                )
                .addBranch("Bank Account Statement",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Bank Account Statement", "_bank_statement"))
                )
                .addBranch("Other Document",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Other Document", "_other_doc"))
                )
        );
    }
}
