/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.esthenos.gravity.kyc;

import android.content.Context;

import com.esthenos.commons.kyc.CapturePageAadhaarCard;
import com.esthenos.commons.pages.ImageCapturePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.GUARANTOR1;
import static com.esthenos.commons.utils.Constants.PAGE_BRANCH;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_OTHER;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_VOTERID;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_KYC;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_VOTERID;


public class KYCGravityGuarantor extends AbstractWizardModel {

    public KYCGravityGuarantor(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
            new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, "Add Guarantor's KYC Documents"))
                .addBranch(PAGE_TITLE_AADHAR,
                        new CapturePageAadhaarCard(this, new PageConfig(GUARANTOR1, PAGE_TYPE_AADHAR, PAGE_TITLE_AADHAR))
                )
                .addBranch(PAGE_TITLE_VOTERID,
                        new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_VOTERID, PAGE_TITLE_VOTERID))
                )
                .addBranch(PAGE_TITLE_PAN,
                        new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_PAN, PAGE_TITLE_PAN))
                )
                .addBranch(PAGE_TITLE_OTHER,
                        new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_KYC, PAGE_TITLE_OTHER, "_other"))
                )
        );
    }
}
