package com.esthenos.gravity.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class GravityBusinessDetailPage extends Page {

    public static final String BIZ_CATEGORY = "biz-category";
    public static final String BIZ_ACTIVITY = "biz-activity";
    public static final String BIZ_EXP_MONTHLY = "biz-expense-monthly";
    public static final String BIZ_INCOME_MONTHLY = "biz-income-monthly";

    public GravityBusinessDetailPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return GravityBusinessDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(BIZ_ACTIVITY, mData.getString(BIZ_ACTIVITY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_INCOME_MONTHLY, mData.getString(BIZ_INCOME_MONTHLY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_CATEGORY, mData.getString(BIZ_CATEGORY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_EXP_MONTHLY, mData.getString(BIZ_EXP_MONTHLY), getPageId(), -1));

    }


//    @Override
//    public boolean isCompleted(){
//        return  !TextUtils.isEmpty(mData.getString(BIZ_CATEGORY)) && !TextUtils.isEmpty(mData.getString(BIZ_ACTIVITY)) &&
//                !TextUtils.isEmpty(mData.getString(BIZ_INCOME_MONTHLY));
//    }

}
