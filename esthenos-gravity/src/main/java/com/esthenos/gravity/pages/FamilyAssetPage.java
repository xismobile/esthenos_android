package com.esthenos.gravity.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FamilyAssetPage extends Page {
    public static final String KEY_TYPES_OF_HOUSE = "Type of House";
    public static final String KEY_TYPE_OF_AREA = "Type of Area";
    public static final String KEY_TYPE_LAND = "Land Type";
    public static final String KEY_LAND_MEASUREMENT = "Land Measurement in Sq Ft";
    public static final String KEY_HOUSE_MEASUREMENT = "house Measurement";
    public static final String KEY_NO_OF_ROOMS = "No of Rooms in the House";
    public static final String KEY_ATTACHED = "Attached Toilet";
    public static final String KEY_SEPARATE = "Separate Cooking Space";
    public static final String KEY_RUNNING = "Running Water Inside House";
    public static final String KEY_SOURCE = "Source of Energy of Cooking";
    public static final String KEY_QUALITY_OF_HOUSE = "Quality of House";
    public static final String KEY_STAYING_IN_HOUSE = "How long are you staying in house (in years)";
    public static final String KEY_ASSETS_LAND = "Family Assets-Land(Acres)";
    public static final String KEY_ASSETS_ORCHARD = "Family Assets-Orchard (Acres)";
    public static final String KEY_NUMBER_OF_COWS = "Family Assets-Number of Cows";
    public static final String KEY_NUMBER_OF_SHEEPS = "Family Assets-Number of Sheeps";

    public FamilyAssetPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FamilyAssetFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_TYPES_OF_HOUSE, mData.getString(KEY_TYPES_OF_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_QUALITY_OF_HOUSE, mData.getString(KEY_QUALITY_OF_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_STAYING_IN_HOUSE, mData.getString(KEY_STAYING_IN_HOUSE), getPageId(), -1));

        dest.add(new ReviewItem(KEY_NUMBER_OF_COWS, mData.getString(KEY_NUMBER_OF_COWS), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NUMBER_OF_SHEEPS, mData.getString(KEY_NUMBER_OF_SHEEPS), getPageId(), -1));

        dest.add(new ReviewItem(KEY_ASSETS_LAND, mData.getString(KEY_ASSETS_LAND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ASSETS_ORCHARD, mData.getString(KEY_ASSETS_ORCHARD), getPageId(), -1));

        dest.add(new ReviewItem(KEY_TYPE_OF_AREA, mData.getString(KEY_TYPE_OF_AREA), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NO_OF_ROOMS, mData.getString(KEY_NO_OF_ROOMS), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ATTACHED, mData.getString(KEY_ATTACHED), getPageId(), -1));
        dest.add(new ReviewItem(KEY_SEPARATE, mData.getString(KEY_SEPARATE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_RUNNING, mData.getString(KEY_RUNNING), getPageId(), -1));
        dest.add(new ReviewItem(KEY_SOURCE, mData.getString(KEY_SOURCE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_TYPE_LAND, mData.getString(KEY_TYPE_LAND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_LAND_MEASUREMENT, mData.getString(KEY_LAND_MEASUREMENT), getPageId(), -1));

    }
    /*@Override
    public boolean isCompleted(){
        return  !TextUtils.isEmpty(mData.getString(KEY_TYPES_OF_HOUSE))&& !TextUtils.isEmpty(mData.getString(KEY_QUALITY_OF_HOUSE))&&
                !TextUtils.isEmpty(mData.getString(KEY_TYPE_OF_AREA))&& !TextUtils.isEmpty(mData.getString(KEY_STAYING_IN_HOUSE))&&
                !TextUtils.isEmpty(mData.getString(KEY_NO_OF_ROOMS))&& !TextUtils.isEmpty(mData.getString(KEY_ATTACHED))&&
                !TextUtils.isEmpty(mData.getString(KEY_SEPARATE))&& !TextUtils.isEmpty(mData.getString(KEY_RUNNING))&&
                !TextUtils.isEmpty(mData.getString(KEY_SOURCE))&& !TextUtils.isEmpty(mData.getString(KEY_LAND_MEASUREMENT))&&
                !TextUtils.isEmpty(mData.getString(KEY_TYPE_LAND));
    }*/
}
