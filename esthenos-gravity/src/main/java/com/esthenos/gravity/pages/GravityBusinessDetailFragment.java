package com.esthenos.gravity.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.gravity.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class GravityBusinessDetailFragment extends PageFragment<GravityBusinessDetailPage> {

    private Spinner bizCateory;
    private EditText bizActivity, bizIncomeMonthly, bizExpMonth;
    
    private static final String ARG_KEY = "key";
    final String[] bizCategories = {"","Trading", "Services", "Agriculture", "Allied Agriculture", "Small Manufacturing", "Labour","None"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static GravityBusinessDetailFragment create(String key) {
        GravityBusinessDetailFragment fragment = new GravityBusinessDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gravity_business_detail, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_GravityBusinessDetailFragbizcategory=autosave_data.getdata(getContext(), mPage.getFormId(),GravityBusinessDetailPage.BIZ_CATEGORY);
        String s_GravityBusinessDetailFragActivity=autosave_data.getdata(getContext(), mPage.getFormId(),GravityBusinessDetailPage.BIZ_ACTIVITY);
        String s_GravityBusinessDetailFragIncomeMonthly=autosave_data.getdata(getContext(), mPage.getFormId(),GravityBusinessDetailPage.BIZ_INCOME_MONTHLY);
        String s_GravityBusinessDetailFragExpMonth=autosave_data.getdata(getContext(), mPage.getFormId(),GravityBusinessDetailPage.BIZ_EXP_MONTHLY);

        bizCateory=(Spinner)rootView.findViewById(R.id.business_grav_category);
        ArrayAdapter categoryAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,bizCategories);
        categoryAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bizCateory.setAdapter(categoryAdapter);
        mPage.getData().putString(GravityBusinessDetailPage.BIZ_CATEGORY, bizCateory.getSelectedItem().toString());
        if (!s_GravityBusinessDetailFragbizcategory.equals("nulldata")){
            bizCateory.setSelection(categoryAdapter.getPosition(s_GravityBusinessDetailFragbizcategory));
        }

        bizActivity = (EditText) rootView.findViewById(R.id.business_grav_activity);
        mPage.getData().putString(GravityBusinessDetailPage.BIZ_ACTIVITY, bizActivity.getText().toString());
        if (!s_GravityBusinessDetailFragActivity.equals("nulldata")){
            bizActivity.setText(s_GravityBusinessDetailFragActivity);
        }

        bizIncomeMonthly = (EditText) rootView.findViewById(R.id.business_income_grav_monthly);
        mPage.getData().putString(GravityBusinessDetailPage.BIZ_INCOME_MONTHLY, bizIncomeMonthly.getText().toString());
        if (!s_GravityBusinessDetailFragIncomeMonthly.equals("nulldata")){
            bizIncomeMonthly.setText(s_GravityBusinessDetailFragIncomeMonthly);
        }

        bizExpMonth = (EditText) rootView.findViewById(R.id.business_income_grav_express_monthly);
        mPage.getData().putString(GravityBusinessDetailPage.BIZ_EXP_MONTHLY, bizExpMonth.getText().toString());
        if (!s_GravityBusinessDetailFragExpMonth.equals("nulldata")){
            bizExpMonth.setText(s_GravityBusinessDetailFragExpMonth);
        }


        bizCateory.setOnItemSelectedListener(getOnItem());
        bizActivity.addTextChangedListener(new GenericTextGravityBusinessDetail(bizActivity));
        bizIncomeMonthly.addTextChangedListener(new GenericTextGravityBusinessDetail(bizIncomeMonthly));
        bizExpMonth.addTextChangedListener(new GenericTextGravityBusinessDetail(bizExpMonth));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        bizCateory.setOnItemSelectedListener(getOnItemSelectedListener(GravityBusinessDetailPage.BIZ_CATEGORY));
//        bizActivity.addTextChangedListener(getTextWatcher(GravityBusinessDetailPage.BIZ_ACTIVITY));
//        bizIncomeMonthly.addTextChangedListener(getTextWatcher(GravityBusinessDetailPage.BIZ_INCOME_MONTHLY));
//        bizExpMonth.addTextChangedListener(getTextWatcher(GravityBusinessDetailPage.BIZ_EXP_MONTHLY));
    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.business_grav_category) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), GravityBusinessDetailPage.BIZ_CATEGORY, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(GravityBusinessDetailPage.BIZ_CATEGORY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

    class GenericTextGravityBusinessDetail implements TextWatcher {

        private View view;

        private GenericTextGravityBusinessDetail(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + com.esthenos.commons.R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.business_grav_activity) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),GravityBusinessDetailPage.BIZ_ACTIVITY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(GravityBusinessDetailPage.BIZ_ACTIVITY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.business_income_grav_monthly) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),GravityBusinessDetailPage.BIZ_INCOME_MONTHLY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(GravityBusinessDetailPage.BIZ_INCOME_MONTHLY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.business_income_grav_express_monthly) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),GravityBusinessDetailPage.BIZ_EXP_MONTHLY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(GravityBusinessDetailPage.BIZ_EXP_MONTHLY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
