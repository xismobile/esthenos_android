package com.esthenos.gravity.pages;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;

public class FamilyAssetFragment extends PageFragment<FamilyAssetPage> {

    public static final String TABLE_FAMILYASSETS = "FamilyAssetPage";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;
    Autosave_Data autosave_data = new Autosave_Data();
    String[] houseType = {"", "Family Owned", "Self Owned", "Rented/Leased", "Govt Allocated"};
    String[] qualityOfHouse = {"", "Pakka/Concrete", "Kaccha/Mud", "Semi/Pakka", "Thatched Roof", "Tilled Roof"};
    String[] AreaType = {"", "Rural", "Semi Urban", "Urban"};
    String[] rooms = {"", "1", "2", "3", ">3"};
    String[] yesno = {"", "Yes", "No"};
    String[] cooking = {"", "Fuel Wood", "Kerosene", "Gas", "Charcoal", "Others"};
    String[] landType = {"", "Agricultural", "Residential"};

    Spinner sHouseType, sQuality, sAreaType, sNoOfRooms, sAttached, sSeparate, sRunningWater, sSource, sLandType;
    EditText mStaying, mAssetLand, mAssetOrchard, mCows, mSheeps, mMeasurement;

    public static FamilyAssetFragment create(String key) {
        FamilyAssetFragment fragment = new FamilyAssetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family_asset_gravity, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mydb = new MySQLiteDB(getContext());
        autosave = new AutoSaveData(getContext());


        sHouseType = (Spinner) rootView.findViewById(R.id.spiner_types_house);
        ArrayAdapter houseAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, houseType);
        houseAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sHouseType.setAdapter(houseAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_TYPES_OF_HOUSE, sHouseType.getSelectedItem().toString());

        sAreaType = (Spinner) rootView.findViewById(R.id.spiner_area_type);
        ArrayAdapter areaAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, AreaType);
        areaAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sAreaType.setAdapter(areaAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_TYPE_OF_AREA, sAreaType.getSelectedItem().toString());

        sLandType = (Spinner) rootView.findViewById(R.id.spiner_house_land_type);
        ArrayAdapter landAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, landType);
        landAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sLandType.setAdapter(landAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_TYPE_LAND, sLandType.getSelectedItem().toString());

        sNoOfRooms = (Spinner) rootView.findViewById(R.id.spiner_no_of_rooms);
        ArrayAdapter noOfRoomAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, rooms);
        noOfRoomAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sNoOfRooms.setAdapter(noOfRoomAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_NO_OF_ROOMS, sNoOfRooms.getSelectedItem().toString());

        sAttached = (Spinner) rootView.findViewById(R.id.spiner_attached_toilet);
        ArrayAdapter attachedAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, yesno);
        attachedAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sAttached.setAdapter(attachedAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_ATTACHED, sAttached.getSelectedItem().toString());


        sSeparate = (Spinner) rootView.findViewById(R.id.spiner_separate_cook);
        ArrayAdapter separateAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, yesno);
        separateAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sSeparate.setAdapter(separateAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_SEPARATE, sSeparate.getSelectedItem().toString());


        sRunningWater = (Spinner) rootView.findViewById(R.id.spiner_running_water);
        ArrayAdapter waterAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, yesno);
        waterAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sRunningWater.setAdapter(waterAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_RUNNING, sRunningWater.getSelectedItem().toString());


        sSource = (Spinner) rootView.findViewById(R.id.spiner_source_cooking);
        ArrayAdapter cookinAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, cooking);
        cookinAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sSource.setAdapter(cookinAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_SOURCE, sSource.getSelectedItem().toString());

        sQuality = (Spinner) rootView.findViewById(R.id.spiner_quality_house);
        ArrayAdapter qualityAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, qualityOfHouse);
        qualityAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sQuality.setAdapter(qualityAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_QUALITY_OF_HOUSE, sQuality.getSelectedItem().toString());

        mStaying = (EditText) rootView.findViewById(R.id.asset_stying_in_house);
        mPage.getData().putString(FamilyAssetPage.KEY_STAYING_IN_HOUSE, mStaying.getText().toString());
        mStaying.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mStaying.getText().toString().trim())) {


                        if (mStaying.getText().length() == 1) {


                            String nowstring = mStaying.getText().toString().trim();
                            mStaying.setText("0" + nowstring);

                        }
//                        mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_STAYING_IN_HOUSE, i, mStaying.getText().toString());
                    } else {

                        mStaying.setError("* Please enter House stay Duration    (In Years)");
                    }


                }
            }
        });

        mMeasurement = (EditText) rootView.findViewById(R.id.house_measurement);
        mPage.getData().putString(FamilyAssetPage.KEY_LAND_MEASUREMENT, mMeasurement.getText().toString());
        mMeasurement.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mMeasurement.getText().toString().trim())) {


                        if (mMeasurement.getText().length() == 1) {


                            String nowstring = mMeasurement.getText().toString().trim();
                            mMeasurement.setText("0" + nowstring);

                        }
                        // mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_LAND_MEASUREMENT, i, mMeasurement.getText().toString());
                    } else {

                        mMeasurement.setError("* Please enter Land Measurement in Sq Ft ");
                    }


                }
            }
        });

        mAssetLand = (EditText) rootView.findViewById(R.id.family_asset_land);
        mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_LAND, mAssetLand.getText().toString());
        mAssetLand.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mAssetLand.getText().toString().trim())) {


                        if (mAssetLand.getText().length() == 1) {


                            String nowstring = mAssetLand.getText().toString().trim();
                            mAssetLand.setText("0" + nowstring);

                        }
                        // mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_ASSETS_LAND, i, mAssetLand.getText().toString());
                    } else {

                        mAssetLand.setHint("Please enter Land(Acres) if any");
                        mAssetLand.setHintTextColor(Color.GREEN);
                    }


                }
            }
        });

        mAssetOrchard = (EditText) rootView.findViewById(R.id.family_asset_orchand);
        mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_ORCHARD, mAssetOrchard.getText().toString());
        mAssetOrchard.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mAssetOrchard.getText().toString().trim())) {


                        if (mAssetOrchard.getText().length() == 1) {


                            String nowstring = mAssetOrchard.getText().toString().trim();
                            mAssetOrchard.setText("0" + nowstring);

                        }
                        // mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_ASSETS_ORCHARD, i, mAssetOrchard.getText().toString());
                    } else {

                        mAssetOrchard.setHint("Please enter Orchard(Acres) if any");
                        mAssetOrchard.setHintTextColor(Color.GREEN);
                    }


                }
            }
        });

        mCows = (EditText) rootView.findViewById(R.id.family_asset_cows);
        mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_COWS, mCows.getText().toString());
        mCows.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!TextUtils.isEmpty(mCows.getText().toString().trim())) {


                        if (mCows.getText().length() == 1) {


                            String nowstring = mCows.getText().toString().trim();
                            mCows.setText("0" + nowstring);

                        }
                        // mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_NUMBER_OF_COWS, i, mCows.getText().toString());
                    } else {

                        mCows.setHint("Please enter No of cows if any");
                        mCows.setHintTextColor(Color.GREEN);
                    }


                }
            }
        });
        mSheeps = (EditText) rootView.findViewById(R.id.family_asset_sheeps);
        mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_SHEEPS, mSheeps.getText().toString());
        mSheeps.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mSheeps.getText().toString().trim())) {


                        if (mSheeps.getText().length() == 1) {


                            String nowstring = mSheeps.getText().toString().trim();
                            mSheeps.setText("0" + nowstring);

                        }
                        //  mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_NUMBER_OF_SHEEPS, i, mSheeps.getText().toString());
                    } else {

                        mSheeps.setHint("Please enter No of sheeps if any");
                        mSheeps.setHintTextColor(Color.GREEN);
                    }


                }
            }
        });


        sLandType.setOnItemSelectedListener(getOnItemFAF());
        sHouseType.setOnItemSelectedListener(getOnItemFAF());
        sQuality.setOnItemSelectedListener(getOnItemFAF());
        sAreaType.setOnItemSelectedListener(getOnItemFAF());
        sNoOfRooms.setOnItemSelectedListener(getOnItemFAF());
        sAttached.setOnItemSelectedListener(getOnItemFAF());
        sSeparate.setOnItemSelectedListener(getOnItemFAF());
        sRunningWater.setOnItemSelectedListener(getOnItemFAF());
        sSource.setOnItemSelectedListener(getOnItemFAF());


        String S_sHouseType = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPES_OF_HOUSE);
        String S_sQuality = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_QUALITY_OF_HOUSE);
        String S_sAreaType = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPE_OF_AREA);
        String S_sNoOfRooms = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NO_OF_ROOMS);
        String S_sAttached = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ATTACHED);
        String S_sSeparate = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_SEPARATE);
        String S_sRunningWater = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_RUNNING);
        String S_sSource = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_SOURCE);
        String S_sLandType = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPE_LAND);


        if (!S_sHouseType.equals("nulldata")) {

            sHouseType.setSelection(houseAdapter.getPosition(S_sHouseType));
        }

        if (!S_sQuality.equals("nulldata")) {

            sQuality.setSelection(qualityAdapter.getPosition(S_sQuality));
        }
        if (!S_sAreaType.equals("nulldata")) {

            sAreaType.setSelection(areaAdapter.getPosition(S_sAreaType));
        }
        if (!S_sNoOfRooms.equals("nulldata")) {

            sNoOfRooms.setSelection(noOfRoomAdapter.getPosition(S_sNoOfRooms));
        }

        if (!S_sAttached.equals("nulldata")) {

            sAttached.setSelection(attachedAdapter.getPosition(S_sAttached));
        }

        if (!S_sSeparate.equals("nulldata")) {

            sSeparate.setSelection(separateAdapter.getPosition(S_sSeparate));
        }
        if (!S_sRunningWater.equals("nulldata")) {

            sRunningWater.setSelection(waterAdapter.getPosition(S_sRunningWater));
        }
        if (!S_sSource.equals("nulldata")) {

            sSource.setSelection(cookinAdapter.getPosition(S_sSource));
        }
        if (!S_sLandType.equals("nulldata")) {

            sLandType.setSelection(landAdapter.getPosition(S_sLandType));
        }


        mStaying.addTextChangedListener(new GenericTWFAF(mStaying));
        mAssetLand.addTextChangedListener(new GenericTWFAF(mAssetLand));
        mAssetOrchard.addTextChangedListener(new GenericTWFAF(mAssetOrchard));
        mCows.addTextChangedListener(new GenericTWFAF(mCows));
        mSheeps.addTextChangedListener(new GenericTWFAF(mSheeps));
        mMeasurement.addTextChangedListener(new GenericTWFAF(mMeasurement));


        String S_mSheeps = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NUMBER_OF_SHEEPS);
        String S_mCows = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NUMBER_OF_COWS);
        String S_mAssetOrchard = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ASSETS_ORCHARD);
        String S_mAssetLand = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ASSETS_LAND);
        String S_mMeasurement = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_LAND_MEASUREMENT);
        String S_mStaying = autosave_data.getdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_STAYING_IN_HOUSE);




        if (!S_mSheeps.equals("nulldata")) {
            mSheeps.setText(S_mSheeps);
        }
        if (!S_mCows.equals("nulldata")) {
            mCows.setText(S_mCows);
        }
        if (!S_mAssetOrchard.equals("nulldata")) {
            mAssetOrchard.setText(S_mAssetOrchard);
        }
        if (!S_mAssetLand.equals("nulldata")) {
            mAssetLand.setText(S_mAssetLand);
        }
        if (!S_mMeasurement.equals("nulldata")) {
            mMeasurement.setText(S_mMeasurement);
        }
        if (!S_mStaying.equals("nulldata")) {
            mStaying.setText(S_mStaying);
        }



        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mStaying.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_STAYING_IN_HOUSE));
        mAssetLand.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_ASSETS_LAND));
        mAssetOrchard.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_ASSETS_ORCHARD));
        mCows.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_NUMBER_OF_COWS));
        mSheeps.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_NUMBER_OF_SHEEPS));
        mMeasurement.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_LAND_MEASUREMENT));

       /* sHouseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
               // mydb.FirstAutoSaveData(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_TYPES_OF_HOUSE, value);
                //todo Alfishan put this in comments beacouse getting error on 3rd fragment
                //todo alfishan         i = mydb.getLastInsertedId(TABLE_FAMILYASSETS);
                Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
                //            autosave.Sharedsetvalue(getContext(),"FamilyMembername" ,"FamilyDetailKey" ,String.valueOf(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sQuality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                //   mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_QUALITY_OF_HOUSE, i, value);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sAreaType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                // mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_TYPE_OF_AREA, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sNoOfRooms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //todo alfishan    //String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan   //mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_NO_OF_ROOMS, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sAttached.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan      mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_ATTACHED, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sSeparate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //todo alfishan  String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan   mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_SEPARATE,i,value );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sRunningWater.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //todo alfishan   String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan  mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_RUNNING, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //todo alfishan    String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan    mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_SOURCE,i,value );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        sLandType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //todo alfishan   String value = String.valueOf(parent.getItemAtPosition(position));
                //todo alfishan   mydb.Autosavebyid(TABLE_FAMILYASSETS, MySQLiteDB.FAMILYASSETSKEY_TYPE_LAND,i,value );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
*/
    }

    public AdapterView.OnItemSelectedListener getOnItemFAF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.spiner_types_house) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPES_OF_HOUSE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(FamilyAssetPage.KEY_TYPES_OF_HOUSE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.spiner_quality_house) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_QUALITY_OF_HOUSE, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_QUALITY_OF_HOUSE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spiner_area_type) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPE_OF_AREA, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(FamilyAssetPage.KEY_TYPE_OF_AREA, parent.getSelectedItem().toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.spiner_no_of_rooms) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NO_OF_ROOMS, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_NO_OF_ROOMS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.spiner_attached_toilet) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ATTACHED, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_ATTACHED, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }


                if (parent.getId() == R.id.spiner_separate_cook) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_SEPARATE, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_SEPARATE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spiner_running_water) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_RUNNING, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_RUNNING, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spiner_source_cooking) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_SOURCE, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_SOURCE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spiner_house_land_type) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_TYPE_LAND, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FamilyAssetPage.KEY_TYPE_LAND, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTWFAF implements TextWatcher {

        private View view;

        private GenericTWFAF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in GenericTextASDF", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.asset_stying_in_house) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_STAYING_IN_HOUSE, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_STAYING_IN_HOUSE, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.house_measurement) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_LAND_MEASUREMENT, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_LAND_MEASUREMENT, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.family_asset_land) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ASSETS_LAND, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_LAND, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.family_asset_orchand) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_ASSETS_ORCHARD, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_ORCHARD, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.family_asset_cows) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NUMBER_OF_COWS, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_COWS, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.family_asset_sheeps) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), FamilyAssetPage.KEY_NUMBER_OF_SHEEPS, text);


                    mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_SHEEPS, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


        }

    }

}

