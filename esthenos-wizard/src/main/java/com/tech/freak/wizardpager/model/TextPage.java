package com.tech.freak.wizardpager.model;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.ui.TextFragment;

public class TextPage extends Page {

    public TextPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return TextFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(getTitle(), mData.getString(SIMPLE_DATA_KEY), getPageId()));
    }

    @Override
    public boolean isCompleted() {
        return !TextUtils.isEmpty(mData.getString(SIMPLE_DATA_KEY));
    }

    public TextPage setValue(String value) {
        mData.putString(SIMPLE_DATA_KEY, value);
        return this;
    }
}
