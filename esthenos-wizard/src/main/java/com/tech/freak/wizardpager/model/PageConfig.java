package com.tech.freak.wizardpager.model;


public class PageConfig {
    public final String pageId;
    public final String pageForm;
    public final String pageOwner;
    public final String pageType;
    public final String pageTitle;
    public final boolean pageRequired;

    public PageConfig(String pageOwner, String pageType, String pageTitle) {
        this(pageOwner, pageType, pageTitle, "", true);
    }

    public PageConfig(String pageOwner, String pageType, String pageTitle, String pageForm) {
        this(pageOwner, pageType, pageTitle, pageForm, true);
    }

    public PageConfig(String pageOwner, String pageType, String pageTitle, String pageForm, boolean pageRequired) {
        this.pageId = String.format("%s_%s_%s_%s", pageOwner, pageType, pageForm, pageTitle.toLowerCase());
        this.pageForm = String.format("%s_%s%s", pageOwner, pageType, pageForm);
        this.pageType = pageType;
        this.pageOwner = pageOwner;
        this.pageTitle = pageTitle;
        this.pageRequired = pageRequired;
    }
}
