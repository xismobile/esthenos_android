package com.tech.freak.wizardpager.ui;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;

public class NumberFragment extends TextFragment {
    public static NumberFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        NumberFragment f = new NumberFragment();
        f.setArguments(args);
        return f;
    }

    @SuppressLint("InlinedApi")
    protected void setInputType() {
        mEditTextInput.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setInputType();
    }
}
