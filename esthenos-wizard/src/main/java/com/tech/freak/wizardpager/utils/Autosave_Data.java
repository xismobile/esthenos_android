package com.tech.freak.wizardpager.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by root on 12/4/16.
 */
public class Autosave_Data {


    public static final String KEY_PREF_NAME = "Auto_Save_Pref";

    //private final Context mCxt;
    private static Autosave_Data mInstance = null;
    Context mContext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences getpref;

    public Autosave_Data() {

//       mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);


    }/*(Context ctx) {
        this.mCxt = ctx;
    }*/

    public static Autosave_Data getInstance(Context ctx) {


        if (mInstance == null) {
            //   mInstance = new Autosave_Data(ctx);

        }
        return mInstance;
    }

    /* public Autosave_Data(Context context) {
         this.mContext = context;
     }
 */
    public void putdata(Context mContext, String foramname, String f_name, String value) {

        pref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(foramname + "_" + f_name, value);
        editor.apply();

    }

    public String getdata(Context mContext, String foramname, String f_name) {
        String text;
        getpref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        text = getpref.getString(foramname + "_" + f_name, "nulldata");
        return text;
    }

    public void putArrraylistdata(Context mContext, String formname, String f_name, ArrayList<String> value,int totalcount) {


        pref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putInt(formname + "_" + f_name + "_size", value.size());


        for (int i = 0; i < totalcount; i++) {

          try {
              editor.remove(formname + "_" + f_name + "_values_" + i);
              //editor.putString(formname + "_" + f_name + "_values_"+i, value.get(i));
              //mEdit1.putString("Status_" + i, sKey.get(i));
          }

        catch(Exception e){
                e.printStackTrace();
            }
        }

        for (int i = 0; i < value.size(); i++) {
            editor.remove(formname + "_" + f_name + "_values_" + i);
            editor.putString(formname + "_" + f_name + "_values_"+i, value.get(i));
            //mEdit1.putString("Status_" + i, sKey.get(i));
        }

        editor.apply();

    }





    public ArrayList<String> getArrraylistdata(Context mContext, String formname, String f_name) {


        pref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        //editor = pref.edit();
        int size = pref.getInt(formname + "_" + f_name + "_size", 0);

        ArrayList<String> values = new ArrayList<String>();

        for (int i = 0; i < size; i++) {
            //  mEdit1.remove("Status_" + i);
            values.add(pref.getString(formname + "_" + f_name + "_values_" + i, "nulldata"));
            //mEdit1.putString("Status_" + i, sKey.get(i));
        }
        return values;


    }

}
