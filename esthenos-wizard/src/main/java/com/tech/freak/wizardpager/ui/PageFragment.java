package com.tech.freak.wizardpager.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.tech.freak.wizardpager.model.Page;


public class PageFragment<T extends Page> extends Fragment {


    protected static final String ARG_KEY = "key";
    protected String mKey;
    protected T mPage;
    protected PageFragmentCallbacks mCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (T) mCallbacks.onGetPage(mKey);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public TextWatcher getTextWatcher(final String key) {
        return new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable != null && editable.length() >= 0) {
                    mPage.getData().putString(key, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                /*if (editable != null && editable.length() >= 0 && isResumed()) {
                    mPage.getData().putString(key,
                            (editable != null) ? editable.toString() : null);
                    mPage.notifyDataChanged();
                }*/
            }
        };
    }






    public AdapterView.OnItemSelectedListener getOnItemSelectedListener(final String key) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() >= 0) {
                    mPage.getData().putString(key, parent.getItemAtPosition(position).toString());
                    mPage.notifyDataChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        };
    }
}
