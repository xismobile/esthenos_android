package com.esthenos.commons.uploadservice;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Service to upload files as a multi-part form data in background using HTTP POST with notification center progress
 * display.
 *
 * @author alexbbb (Alex Gotev)
 * @author eliasnaur
 */
public class UploadService extends IntentService {

    private static final String SERVICE_NAME = UploadService.class.getName();
    private static final String TAG = "UploadService11";

    private static final int BUFFER_SIZE = 4096;
    private static final String NEW_LINE = "\r\n";
    private static final String TWO_HYPHENS = "--";

    private static final String NAMESPACE = "com.esthenos.fos";
    private static final String ACTION_UPLOAD_SUFFIX = ".uploadservice.action.upload";

    protected static final String PARAM_APP_ID = "app_id";
    protected static final String PARAM_URL = "url";
    protected static final String PARAM_METHOD = "method";
    protected static final String PARAM_FILES = "files";
    protected static final String PARAM_REQUEST_HEADERS = "requestHeaders";
    protected static final String PARAM_REQUEST_PARAMETERS = "requestParameters";
    protected static final String PARAM_NOTIFICATION_CONFIG = "notificationConfig";

    private static final String BROADCAST_ACTION_SUFFIX = ".uploadservice.broadcast.status";
    public static final String UPLOAD_ID = "upload_id";
    public static final String UPLOAD_APP_ID = "app_id";
    public static final String STATUS = "status";
    public static final String PARAM_PICTURE_ID = "picture_id";
    public static final String EXTERNAL_UPLOAD_ID = "external_upload_id";

    public static final int STATUS_IN_PROGRESS = 1;
    public static final int STATUS_COMPLETED = 2;
    public static final int STATUS_ERROR = 3;
    public static final String PROGRESS = "progress";
    public static final String ERROR_EXCEPTION = "errorException";
    public static final String SERVER_RESPONSE_CODE = "serverResponseCode";
    public static final String SERVER_RESPONSE_MESSAGE = "serverResponseMessage";

    private NotificationManager notificationManager;
    private Builder notification;
    private PowerManager.WakeLock wakeLock;
    private UploadNotificationConfig notificationConfig;
    private int lastPublishedProgress;
    public static final String PARAM_UPLOAD_TYPE = "upload_type";

    public static String getActionBroadcast() {
        return NAMESPACE + BROADCAST_ACTION_SUFFIX;
    }

    public UploadService() {
        super(SERVICE_NAME);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notification = new Builder(this);

        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
                Log.d(TAG, "intent received.");

                notificationConfig = intent.getParcelableExtra(PARAM_NOTIFICATION_CONFIG);
                final String url = intent.getStringExtra(PARAM_URL);
                final String type = intent.getStringExtra(PARAM_UPLOAD_TYPE);
                final String appId = intent.getStringExtra(PARAM_APP_ID);
                final String method = intent.getStringExtra(PARAM_METHOD);
                final int picId = intent.getIntExtra(UploadService.PARAM_PICTURE_ID, 0);
                final ArrayList<FileToUpload> files = intent.getParcelableArrayListExtra(PARAM_FILES);
                final ArrayList<NameValue> headers = intent.getParcelableArrayListExtra(PARAM_REQUEST_HEADERS);
                final ArrayList<NameValue> parameters = intent.getParcelableArrayListExtra(PARAM_REQUEST_PARAMETERS);
                lastPublishedProgress = 0;
                wakeLock.acquire();

                try {
                    createNotification(picId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    handleFileUpload(appId, url, method, files, headers, parameters, type, picId);
                } catch (Exception exception) {
                    exception.printStackTrace();
                    broadcastError(appId, picId, exception);
                } finally {
                    wakeLock.release();
                }
    }

    private void handleFileUpload(final String appId,
                                  final String url, final String method,
                                  final ArrayList<FileToUpload> filesToUpload,
                                  final ArrayList<NameValue> requestHeaders,
                                  final ArrayList<NameValue> requestParameters, final String type, final int picId) throws IOException {

        Log.d(TAG, String.format("handle file upload appId:%s, url:%s", appId, url));

        final String boundary = getBoundary();
        final byte[] boundaryBytes = getBoundaryBytes(boundary);

        HttpURLConnection conn = null;
        OutputStream requestStream = null;
        InputStream responseStream = null;

        try {
            conn = getMultipartHttpURLConnection(url, method, boundary, filesToUpload.size());
            setRequestHeaders(conn, requestHeaders);

            requestStream = conn.getOutputStream();
            setRequestParameters(requestStream, requestParameters, boundaryBytes);

            uploadFiles(appId, picId, requestStream, filesToUpload, boundaryBytes);

            final byte[] trailer = getTrailerBytes(boundary);
            requestStream.write(trailer, 0, trailer.length);
            requestStream.flush();
            requestStream.close();

            final int serverResponseCode = conn.getResponseCode();
            if (serverResponseCode / 100 == 2) {
                responseStream = conn.getInputStream();
            } else { // getErrorStream if the response code is not 2xx
                responseStream = conn.getErrorStream();
            }

            final String serverResponseMessage = getResponseBodyAsString(responseStream);
            broadcastCompleted(appId, serverResponseCode, serverResponseMessage, picId, type);
            Log.d(TAG, "server-response:" + serverResponseMessage + ", type:" + type);

        } finally {
            closeOutputStream(requestStream);
            closeInputStream(responseStream);
            closeConnection(conn);
        }
    }

    private String getResponseBodyAsString(final InputStream inputStream) {
        StringBuilder outString = new StringBuilder();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                outString.append(line);
            }
        } catch (Exception exc) {
            try {
                if (reader != null)
                    reader.close();
            } catch (Exception readerExc) {
            }
        }

        return outString.toString();
    }

    private String getBoundary() {
        final StringBuilder builder = new StringBuilder();
        builder.append("---------------------------").append(System.currentTimeMillis());
        return builder.toString();
    }

    private byte[] getBoundaryBytes(final String boundary) throws UnsupportedEncodingException {
        final StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE).append(TWO_HYPHENS).append(boundary).append(NEW_LINE);
        return builder.toString().getBytes("US-ASCII");
    }

    private byte[] getTrailerBytes(final String boundary) throws UnsupportedEncodingException {
        final StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE).append(TWO_HYPHENS).append(boundary).append(TWO_HYPHENS).append(NEW_LINE);
        return builder.toString().getBytes("US-ASCII");
    }

    private HttpURLConnection getMultipartHttpURLConnection(final String url, final String method,
                                                            final String boundary, int totalFiles) throws IOException {

        final HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
        conn.setChunkedStreamingMode(0);
        conn.setRequestMethod(method);

        if (totalFiles <= 1) {
            conn.setRequestProperty("Connection", "close");
        } else {
            conn.setRequestProperty("Connection", "Keep-Alive");
        }

        conn.setRequestProperty("ENCTYPE", "multipart/form-data");
        //conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
        return conn;
    }

    private void setRequestHeaders(final HttpURLConnection conn, final ArrayList<NameValue> requestHeaders) {
        if (requestHeaders != null && !requestHeaders.isEmpty()) {
            for (final NameValue param : requestHeaders) {
                conn.setRequestProperty(param.getName(), param.getValue());
            }
        }
    }

    private void setRequestParameters(final OutputStream requestStream, final ArrayList<NameValue> requestParameters,
                                      final byte[] boundaryBytes) throws IOException, UnsupportedEncodingException {

        if (requestParameters != null && !requestParameters.isEmpty()) {
            for (final NameValue parameter : requestParameters) {
                requestStream.write(boundaryBytes, 0, boundaryBytes.length);
                byte[] formItemBytes = parameter.getBytes();
                requestStream.write(formItemBytes, 0, formItemBytes.length);
            }
        }
    }

    private void
    uploadFiles(final String appId, final int picId, final OutputStream requestStream,
                final ArrayList<FileToUpload> filesToUpload, final byte[] boundaryBytes) throws IOException {

        long uploadedBytes = 0;
        final long totalBytes = getTotalBytes(filesToUpload);

        for (FileToUpload file : filesToUpload) {
            requestStream.write(boundaryBytes, 0, boundaryBytes.length);
            byte[] headerBytes = file.getMultipartHeader();
            requestStream.write(headerBytes, 0, headerBytes.length);

            final InputStream stream = file.getStream();
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;

            try {
                while ((bytesRead = stream.read(buffer, 0, buffer.length)) > 0) {
                    requestStream.write(buffer, 0, bytesRead);
                    uploadedBytes += bytesRead;
                    broadcastProgress(appId, picId, uploadedBytes, totalBytes);
                }
            } finally {
                closeInputStream(stream);
            }
        }
    }

    private long getTotalBytes(final ArrayList<FileToUpload> filesToUpload) {
        long total = 0;

        for (FileToUpload file : filesToUpload) {
            total += file.length();
        }

        return total;
    }

    private void closeInputStream(final InputStream stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception exc) {
            }
        }
    }

    private void closeOutputStream(final OutputStream stream) {
        if (stream != null) {
            try {
                stream.flush();
                stream.close();
            } catch (Exception exc) {
            }
        }
    }

    private void closeConnection(final HttpURLConnection connection) {
        if (connection != null) {
            try {
                connection.disconnect();
            } catch (Exception exc) {
            }
        }
    }

    private void broadcastProgress(final String uploadId, final int picId, final long uploadedBytes, final long totalBytes) {

        final int progress = (int) (uploadedBytes * 100 / totalBytes);
        if (progress <= lastPublishedProgress)
            return;

        lastPublishedProgress = progress;
        updateNotificationProgress(progress, picId);

        final Intent intent = new Intent(getActionBroadcast());
        intent.putExtra(UPLOAD_ID, uploadId);
        intent.putExtra(STATUS, STATUS_IN_PROGRESS);
        intent.putExtra(PROGRESS, progress);
        sendBroadcast(intent);
    }

    private void broadcastCompleted(final String appId, final int responseCode, final String responseMessage, final int picId, String externalUploadId) {

        final String filteredMessage;
        if (responseMessage == null) {
            filteredMessage = "";
        } else {
            filteredMessage = responseMessage;
        }

        if (responseCode >= 200 && responseCode <= 299)
            updateNotificationCompleted(picId);
        else
            updateNotificationError(picId);

        final Intent intent = new Intent(getActionBroadcast());
        intent.putExtra(UPLOAD_APP_ID, appId);
        intent.putExtra(PARAM_PICTURE_ID, picId);
        intent.putExtra(STATUS, STATUS_COMPLETED);
        intent.putExtra(SERVER_RESPONSE_CODE, responseCode);
        intent.putExtra(EXTERNAL_UPLOAD_ID, externalUploadId);
        intent.putExtra(SERVER_RESPONSE_MESSAGE, filteredMessage);
        sendBroadcast(intent);
    }

    private void broadcastError(final String appId, final int picId, final Exception exception) {
        updateNotificationError(picId);

        final Intent intent = new Intent(getActionBroadcast());
        intent.setAction(getActionBroadcast());
        intent.putExtra(UPLOAD_ID, appId);
        intent.putExtra(STATUS, STATUS_ERROR);
        intent.putExtra(ERROR_EXCEPTION, exception);
        sendBroadcast(intent);
    }

    private void createNotification(int withId) {
        notification.setContentTitle(notificationConfig.getTitle())
                .setContentText(notificationConfig.getMessage())
                .setContentIntent(PendingIntent.getBroadcast(this, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(notificationConfig.getIconResourceID())
                .setProgress(100, 0, true)
                .setOngoing(true);

        startForeground(withId, notification.build());
    }

    private void updateNotificationProgress(final int progress, int withID) {
        notification.setContentTitle(notificationConfig.getTitle())
                .setContentText(notificationConfig.getMessage())
                .setSmallIcon(notificationConfig.getIconResourceID())
                .setProgress(100, progress, false)
                .setOngoing(true);

        startForeground(withID, notification.build());
    }

    private void updateNotificationCompleted(int withID) {
        stopForeground(notificationConfig.isAutoClearOnSuccess());

        if (!notificationConfig.isAutoClearOnSuccess()) {
            notification.setContentTitle(notificationConfig.getTitle())
                    .setContentText(notificationConfig.getCompleted())
                    .setSmallIcon(notificationConfig.getIconResourceID())
                    .setProgress(0, 0, false)
                    .setOngoing(false);

            notificationManager.notify(withID, notification.build());
        }
    }

    private void updateNotificationError(int withID) {
        stopForeground(false);
        notification.setContentTitle(notificationConfig.getTitle())
                .setContentText(notificationConfig.getError())
                .setSmallIcon(notificationConfig.getIconResourceID())
                .setProgress(0, 0, false).setOngoing(false);

        notificationManager.notify(withID, notification.build());
    }
}
