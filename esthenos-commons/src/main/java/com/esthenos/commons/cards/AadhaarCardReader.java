package com.esthenos.commons.cards;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.StringReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class AadhaarCardReader extends DefaultHandler {

    String TAG = "AadhaarCardReader";
    String currTagVal = "";
    Boolean currTag = false;
    AadhaarCard aadhaarCard = new AadhaarCard();

    public AadhaarCardReader(String barcodeXml) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser mSaxParser = factory.newSAXParser();
            XMLReader mXmlReader = mSaxParser.getXMLReader();
            mXmlReader.setContentHandler(this);
            mXmlReader.parse(new InputSource(new StringReader(barcodeXml)));

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    public AadhaarCard getAadhaarCard() {
        return aadhaarCard;
    }

    // This method receives notification character data inside an element
    // e.g. <post_title>Add EditText Pragmatically - Android</post_title>
    // It will be called to read "Add EditText Pragmatically - Android"
    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {
        if (currTag) {
            currTagVal = currTagVal + new String(ch, start, length);
            currTag = false;
        }
    }

    // Receives notification of end of element
    // e.g. <post_title>Add EditText Pragmatically - Android</post_title>
    // It will be called when </post_title> is encountered
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currTag = false;
    }

    // Receives notification of start of an element
    // e.g. <post_title>Add EditText Pragmatically - Android</post_title>
    // It will be called when <post_title> is encountered
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        Log.i(TAG, "TAG: " + localName);

        currTag = true;
        currTagVal = "";
        // Whenever <post> element is encountered it will create new object of PostValue
        if (localName.equals("PrintLetterBarcodeData")) {
            for (int i = 0; i < attributes.getLength(); i++) {
                String name = attributes.getQName(i);
                System.out.println("Name:" + name);

                String value = attributes.getValue(i);
                System.out.println("Value:" + value);

                if (name.equalsIgnoreCase("co"))
                    aadhaarCard.setCo(nullToEmpty(value));

                else if (name.equalsIgnoreCase("dist"))
                    aadhaarCard.setDist(nullToEmpty(value));

                else if (name.equalsIgnoreCase("gender"))
                    aadhaarCard.setGender(nullToEmpty(value));

                else if (name.equalsIgnoreCase("house"))
                    aadhaarCard.setHouse(nullToEmpty(value));

                else if (name.equalsIgnoreCase("lm"))
                    aadhaarCard.setLm(nullToEmpty(value));

                else if (name.equalsIgnoreCase("po"))
                    aadhaarCard.setPo(nullToEmpty(value));

                else if (name.equalsIgnoreCase("name"))
                    aadhaarCard.setName(nullToEmpty(value));

                else if (name.equalsIgnoreCase("pc"))
                    aadhaarCard.setPc(nullToEmpty(value));

                else if (name.equalsIgnoreCase("state"))
                    aadhaarCard.setState(nullToEmpty(value));

                else if (name.equalsIgnoreCase("street"))
                    aadhaarCard.setStreet(nullToEmpty(value));

                else if (name.equalsIgnoreCase("uid"))
                    aadhaarCard.setUid(nullToEmpty(value));

                else if (name.equalsIgnoreCase("vtc"))
                    aadhaarCard.setVtc(nullToEmpty(value));

                else if (name.equalsIgnoreCase("yob"))
                    aadhaarCard.setYob(Integer.parseInt(nullToEmpty(value)));

                else if (name.equalsIgnoreCase("loc"))
                    aadhaarCard.setLoc(nullToEmpty(value));
            }
        }
    }

    private String nullToEmpty(String value){
        return value == null || "null".equalsIgnoreCase(value) ? "" : value;
    }
}
