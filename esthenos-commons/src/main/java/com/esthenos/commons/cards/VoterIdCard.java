package com.esthenos.commons.cards;

public class VoterIdCard {
    public String id = "";
    public String name = "";
    public String fatherHusbandName = "";

    public VoterIdCard() {
    }

    public VoterIdCard(String id, String name, String fatherHusbandName) {
        this.id = id;
        this.name = name;
        this.fatherHusbandName = fatherHusbandName;
    }
}
