package com.esthenos.commons.cards;

public class PanCard {
    public String id = "";
    public String name = "";
    public String fatherHusbandName = "";

    public PanCard() {
    }

    public PanCard(String id, String name, String fatherHusbandName) {
        this.id = id;
        this.name = name;
        this.fatherHusbandName = fatherHusbandName;
    }
}
