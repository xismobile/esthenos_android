package com.esthenos.commons.cards;


import java.util.Calendar;

public class AadhaarCard {
    private int yob = -1;
    private int age = -1;
    private String uid = "";
    private String name = "";
    private String gender = "";
    private String co = "";
    private String po = "";
    private String pc = "";
    private String lm = "";
    private String loc = "";
    private String vtc = "";
    private String dist = "";
    private String state = "";
    private String house = "";
    private String street = "";
    private String spouse = "";
    private String spouseAge = "";

    public void setAge(int age) {
        this.age = age;
    }

    public String getSpouseAge() {
        return spouseAge;
    }

    public void setSpouseAge(String spouseAge) {
        this.spouseAge = spouseAge;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return this.age;
    }

    public int getYob() {
        return this.yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
        this.age = Calendar.getInstance().get(Calendar.YEAR) - yob;
    }

    public String getCo() {
        return this.co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getHouse() {
        return this.house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLm() {
        return this.lm;
    }

    public void setLm(String lm) {
        this.lm = lm;
    }

    public String getVtc() {
        return this.vtc;
    }

    public void setVtc(String vtc) {
        this.vtc = vtc;
    }

    public String getDist() {
        return this.dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPc() {
        return this.pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getAddress() {
        return String.format("%s%s%s%s%s", getHouse(), getStreet(), getLm(), getLoc(), getPo());
    }
}
