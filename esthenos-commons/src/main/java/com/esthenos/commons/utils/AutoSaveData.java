package com.esthenos.commons.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateUtils;
import android.util.Log;

import java.io.File;
import java.util.Date;

/**
 * Created by root on 1/4/16.
 */
public class AutoSaveData {

    private MySQLiteDB mydb;
    SharedPreferences pref;
    public static final String PREFS_NAME = "AOP_PREFS";
    public static final String PREFS_KEY = "AOP_PREFS_String";


    public AutoSaveData(Context context){
        mydb = new MySQLiteDB(context);
    }

    public void Sharedsetvalue(Context context,String dataname,String datakey, String data){
        SharedPreferences pref;
        SharedPreferences.Editor editor;

        pref = context.getSharedPreferences(dataname, Context.MODE_PRIVATE);
        editor = pref.edit();
       // editor.putString("PAGE_KEY",pagekey);
        editor.putString(datakey,data);
        Log.d("001111111111111111111",data);
        editor.commit();
    }

    public String Sharedgetvalue(Context context,String dataname, String datakey) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(dataname, Context.MODE_PRIVATE);
       // text = settings.getString("PAGE_KEY",null);
        text = settings.getString(datakey, null);
        return text;
    }

    public void Sharedclear(Context context,String dataname) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = context.getSharedPreferences(dataname, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.commit();
    }
//    public void OnFocusChanged(EditText edttext, final String data, final String TableName, final String ColumnName){
//        edttext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//
//                if (! hasFocus){
//                   // mydb.NAME = mName.getText().toString();
//                    mydb.FirstAutoSaveData(TableName, ColumnName, data);
//                  //  Toast.makeText(getContext(), "data added.." + data, Toast.LENGTH_SHORT).show();
//                   // Intent i = new Intent(AutoSaveData.this, AdditionalFamilyDetailFirstFragment.class);
//
//                  //  int i = getcustomerid(MySQLiteDB.CREATE_DETAILOFFAMILYMEMBER);
//                 //   Toast.makeText(getContext(),"id getted : " + i,Toast.LENGTH_LONG).show();
//                }
//                //  i = getcustomerid();
//            }
//        });
//    }

//    public void FirstAutoSaveData(String TableName,String ColumnName, String data){
//        SQLiteDatabase db = mydb.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put(ColumnName, data);
//        db.insert(TableName, null, values);
//    }

    public int getcustomerid(String TableName){
        int getpersonalid = 0;
        Log.v("8888888888888888", "this is getid method");
        try{
            SQLiteDatabase db = mydb.getReadableDatabase();
            String query = "SELECT " + MySQLiteDB.ID + " FROM " + TableName + " order by " + MySQLiteDB.ID + " DESC limit 1";
            Cursor c = db.rawQuery(query,null);
            if (c !=null &&c.moveToFirst() ){
                getpersonalid = c.getInt(0);
            }
        }catch (Resources.NotFoundException e){
            Log.v("000000000000000000000","resourse not found...");
            Log.v("000000000000000000000",e.toString());
        }
        return getpersonalid;
    }

    //.......................................
    static int clearCacheFolder(final File dir, final int numDays) {

        int deletedFiles = 0;

        if (dir!= null && dir.isDirectory()) {
            try {
                for (File child:dir.listFiles()) {

                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child, numDays);
                    }
                    if (child.lastModified() < new Date().getTime() - numDays * DateUtils.DAY_IN_MILLIS) {
                        if (child.delete()) {
                            deletedFiles++;
                        }
                    }
                }
            }
            catch(Exception e) {
                Log.e("2211000000000000000000", String.format("Failed to clean the cache, error %s", e.getMessage()));
            }
        }
        return deletedFiles;
    }

    public static void clearCache(final Context context, final int numDays) {
        int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            Log.d("000000000000000","deleted" + dir);
            return dir.delete();
        }
        else if(dir!= null && dir.isFile())
            return dir.delete();
        else {
            return false;
        }
    }



    //.......................................
}
