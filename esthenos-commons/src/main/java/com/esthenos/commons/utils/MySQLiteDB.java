package com.esthenos.commons.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tech.freak.wizardpager.model.ReviewItem;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;


public class MySQLiteDB extends SQLiteOpenHelper implements Closeable {
    public static final String KEY_ID = "id";
    public static final String KEY_APPLICATION_ID = "application_id";
    public static final String KEY_EXTERNAL_ID = "external_id";
    public static final String KEY_URL = "url";
    public static final String KEY_DATA = "data";
    public static final String KEY_PATH = "path";
    public static final String KEY_TYPE = "type";
    public static final String KEY_FLAG = "flag";
    public static final String KEY_NAME = "name";

    //.....................................................
    public static final String ID ="id";
    public static final String PersonalDetailPhysical = "physical_desability";
    public static final String PersonalDetailReligion = "religion";
    public static final String PersonalDetailCategori = "category";
    public static final String PersonalDetailEducation = "education";
    private static final String CREATE_PERSONALDETAIL = "create table PersonalDetails (" + ID + " INTEGER PRIMARY KEY,physical_desability text null, religion text not null, category text not null, education text not null);";
    public static final String TABLE_PERSONALDETAIL_NAME = "PersonalDetails";

    public static final String KEY_OCCUPATION_DETAILS = "Occupations_Details";
    public static final String KEY_INVOLVEMENY = "YearsofInvolvement";
    public static final String KEY_ANNUAL_INCOME = "Monthly_Income";
    public static final String KEY_RELATION = "Relationship";
    public static final String KEY_EDUCATION = "Education";
    public static String NAME = "Name";
    public static final String KEY_AGE = "Age";
    public static final String KEY_SEX = "Sex";
    public static final String CREATE_DETAILOFFAMILYMEMBER = "Detailoffamilymember";

    private static final String DATABASE_NAME = "Esthenos";

    public static final String TABLEPERSONAL_INFO = "Personal_Info";
    public static final String PERSONAL_INFONAME_KEY = "Name";
    public static final String PERSONAL_INFOEMAIL_ID_KEY = "EmailID";
    public static final String PERSONAL_INFOOCCUPATION_KEY = "Occupation";
    public static final String PERSONAL_INFOPERMANENT_ADDRESS_KEY = "PermanentAddress";
    public static final String PERSONAL_INFOGENDER_KEY = "Gender";
    public static final String PERSONAL_INFOUID_KEY = "UID";
    public static final String PERSONAL_INFOAGE_KEY = "Age";
    public static final String PERSONAL_INFOYOB_KEY = "DOB";
    public static final String CARD_KEY = "Type";

//    public static final String NAME_SUBTITLE_KEY = "Name SubTitle";
//    public static final String FATHER_HUSBAND_SUBTITLE_KEY = "Father's/Husband's SubTitle";
//    public static final String SPOUSE_NAME_SUBTITLE_KEY = "Spouse SubTitle";
//    public static final String MOTHER_NAME_SUBTITLE_KEY = "Mother SubTitle";

    public static final String PERSONAL_INFOTALUK_KEY = "Taluk";
    public static final String PERSONAL_INFODISTRICT_KEY = "District";
    public static final String PERSONAL_INFOSTATE_KEY = "State";
    public static final String PERSONAL_INFOCOUNTRY_KEY = "Country";
    public static final String PERSONAL_INFOADDRESS_KEY = "Address";
    public static final String PERSONAL_INFOPINCODE_KEY = "Pincode";
    public static final String PERSONAL_INFOTELE_PHONE_KEY = "PhoneNumber";
    public static final String PERSONAL_INFOMOBILE_PHONE_KEY = "MobileNumber";

    public static final String PERSONAL_INFOFATHER_NAME = "FatherName";
    public static final String PERSONAL_INFOMOTHER_KEY = "MotherName";
    public static final String PERSONAL_INFOSPOUSE_NAME = "SpouseName";
    public static final String PERSONAL_INFOLANDMARKS_KEY = "Landmark";
    public static final String PERSONAL_INFONICK_NAME_KEY = "NickName";
    public static final String PERSONAL_INFOPOLICE_KEY = "PoliceStation";
    public static final String PERSONAL_INFOPOST_OFFICE_KEY = "PostOffice";
    public static final String PERSONAL_INFOVOTER_ID_KEY = "VIDNumber";
    public static final String PERSONAL_INFOVOTER_ID_NAME_KEY = "VIDName";
    public static final String PERSONAL_INFOVOTER_ID_FATHER_HUSBAND_NAME = "VIDFather_HusbandName";

    public static final String PERSONAL_INFOPANCARD_KEY = "PCNumber";
    public static final String PERSONAL_INFOPANCARD_NAME_KEY = "PCName";
    public static final String PERSONAL_INFOPANCARD_FATHER_HUSBAND_NAME = "PCFather_HusbandName";

    public static final String PERSONAL_INFORATIONCARD_KEY = "RCNo";
    public static final String PERSONAL_INFOCENTERMEETING_KEY = "CenterMeatingPlace";

    public static final String PERSONAL_INFOSPOUSE_DOB_KEY = "SpouseDOB";
    public static final String PERSONAL_INFOSPOUSE_AGE_KEY = "SpouseAge";

    public static final String PERSONAL_INFOMARITAL_STATUS = "MaritalStatus";

    public static final String TABLE_FAMILY_MEMBERDETAILS = "FamilyMamberDetails";
    public static final String FAMILY_MEMBERS_COUNT = "NumberofFamilyMembers";
    public static final String FAMILY_MEMBER_LESS_THAN_18 = "Lessthan18";
    public static final String FAMILY_MEMBER_MORE_THAN_18 = "Above18";
    public static final String FAMILY_MEMBER_MALE_COUNT = "MaleCount";
    public static final String FAMILY_MEMBER_FEMALE_COUNT = "FemaleCount";
    public static final String FAMILY_MEMBER_EARNING_COUNT = "TotalEarningMembers";
    private static final String CREATE_TABLE_FAMILY_MEMBERDETAIL = "create table "+ TABLE_FAMILY_MEMBERDETAILS + "(" + ID + " INTEGER PRIMARY KEY," +
            "NumberofFamilyMembers number null, MaleCount number null, FemaleCount number null, Lessthan18 number null, Above18 number null, TotalEarningMembers number null);";

    public static final String TABLE_FAMILYASSETS = "FamilyAssetPage";
    public static final String FAMILYASSETSKEY_TYPES_OF_HOUSE = "TypeofHouse";
    public static final String FAMILYASSETSKEY_TYPE_OF_AREA = "TypeofArea";
    public static final String FAMILYASSETSKEY_TYPE_LAND = "LandType";
    public static final String FAMILYASSETSKEY_LAND_MEASUREMENT = "LandMeasurement";
    public static final String FAMILYASSETSKEY_NO_OF_ROOMS = "NoofRooms";
    public static final String FAMILYASSETSKEY_ATTACHED = "AttachedToilet";
    public static final String FAMILYASSETSKEY_SEPARATE = "SeparateCookingSpace";
    public static final String FAMILYASSETSKEY_RUNNING = "RunningWaterInsideHouse";
    public static final String FAMILYASSETSKEY_SOURCE = "SourceofEnergyofCooking";
    public static final String FAMILYASSETSKEY_QUALITY_OF_HOUSE = "QualityofHouse";
    public static final String FAMILYASSETSKEY_STAYING_IN_HOUSE = "stayinginhouseyears";
    public static final String FAMILYASSETSKEY_ASSETS_LAND = "FamilyAssetsLandAcres";
    public static final String FAMILYASSETSKEY_ASSETS_ORCHARD = "FamilyAssetsOrchardAcres";
    public static final String FAMILYASSETSKEY_NUMBER_OF_COWS = "FamilyssetsNumberofCows";
    public static final String FAMILYASSETSKEY_NUMBER_OF_SHEEPS = "FamilyAssetsNumberofSheeps";
    public static final String FAMILYASSETSKEY_OTHER_ASSET = "OtherAsset";
    public static final String FAMILYASSETSKEY_SOURCE_WATER = "SourceOfWater";
    private static final String CREATE_TABLEFAMILYASSETS = "create table " + TABLE_FAMILYASSETS + "(" + ID + "INTEGER PRIMARY KEY, TypeofHouse text null, QualityofHouse text null, TypeofArea text null,"+
            "stayinginhouseyears number null, NoofRooms text null, AttachedToilet text null, SeparateCookingSpace text null, RunningWaterInsideHouse text null, SourceofEnergyofCooking text null, FamilyAssetsLandAcres number null, "+
            "FamilyAssetsOrchardAcres number null, LandType text null,LandMeasurement number null,FamilyssetsNumberofCows number null,FamilyAssetsNumberofSheeps number null, OtherAsset text null, SourceOfWater text null)";

    public static final String TABLE_GUARANTERINFO = "GuaranterInfo";
    public static final String GUARANTEREMAIL_ID_KEY = "EmailID";
    public static final String GUARANTEROCCUPATION_KEY = "Occupation";
    public static final String GUARANTERPERMANENT_ADDRESS_KEY = "PermanentAddress";
    public static final String GUARANTERSINGLE_GENDER_KEY = "Gender";
    public static final String GUARANTERUID_KEY = "UID";
    public static final String GUARANTERAGE_KEY = "Age";
    public static final String GUARANTERYOB_KEY = "DOB";
    public static final String GUARANTERCARD_KEY = "Type";
    public static final String GUARANTERNAME_KEY = "Name";
    public static final String GUARANTERNAME_SUBTITLE_KEY = "NameSubTitle";
    public static final String GUARANTERFATHER_HUSBAND_SUBTITLE_KEY = "FatherHusbandSubTitle";
    public static final String GUARANTERSPOUSE_NAME_SUBTITLE_KEY = "SpouseSubTitle";
    public static final String GUARANTERMOTHER_NAME_SUBTITLE_KEY = "MotherSubTitle";
    public static final String GUARANTERTALUK_KEY = "Taluk";
    public static final String GUARANTERDISTRICT_KEY = "District";
    public static final String GUARANTERSTATE_KEY = "State";
    public static final String GUARANTERCOUNTRY_KEY = "Country";
    public static final String GUARANTERADDRESS_KEY = "Address";
    public static final String GUARANTERPINCODE_KEY = "Pincode";
    public static final String GUARANTERTELE_PHONE_KEY = "PhoneNumber";
    public static final String GUARANTERMOBILE_PHONE_KEY = "MobileNumber";
    public static final String GUARANTERFATHER_OR_HUSBAND_NAME = "FatherHusbandName";
    public static final String GUARANTERMOTHER_KEY = "MotherName";
    public static final String GUARANTERSPOUSE_KEY = "SpouseName";
    public static final String GUARANTERLANDMARKS_KEY = "Landmark";
    public static final String GUARANTERNICK_NAME_KEY = "NickName";
    public static final String GUARANTERPOLICE_KEY = "PoliceStation";
    public static final String GUARANTERPOST_OFFICE_KEY = "PostOffice";
    public static final String GUARANTERVOTER_ID_KEY = "VoterID";
    public static final String GUARANTERVOTER_ID_NAME_KEY = "VoterIDName";
    public static final String GUARANTERVOTER_ID_FATHER_OR_HUSBAND_NAME = "VoterIDFatherHusbandName";
    public static final String GUARANTERPANCARD_KEY = "PANCardID";
    public static final String GUARANTERPANCARD_NAME_KEY = "PANCardName";
    public static final String GUARANTERPANCARD_FATHER_OR_HUSBAND_NAME = "PANCardFatherHusbandName";
    public static final String GUARANTERRATIONCARD_KEY = "RationCardNo";
    public static final String GUARANTERCENTERMEETING_KEY = "CenterMeetingPlace";
    public static final String GUARANTERSPOUSE_DOB_KEY = "SpouseDOB";
    public static final String GUARANTERSPOUSE_AGE_KEY = "SpouseAge";
    public static final String GUARANTERSINGLE_MARITAL_STATUS = "MaritalStatus";
    private static final String CREATE_TABLEGUARANTERINFO = "create table " + TABLE_GUARANTERINFO + "("+ ID + "INTEGER PRIMARY KEY,Name text null, NickName text null,"+
            "UID number null, FatherHusbandSubTitle text null, FatherHusbandName text null, DOB Date null, Age number null, Gender text null, Address text null,Taluk text null,"+
            "District text null,State text null, Landmark text null, PermanentAddress text null, Country text null,Pincode number null, PostOffice text null, Occupation text null, PhoneNumber number null,"+
            "MobileNumber number null, EmailID text null,MotherSubTitle text null,MotherName text null, MaritalStatus text null, SpouseSubTitle text null, SpouseName text null, SpouseDOB number null, SpouseAge number null,VoterID number null,"+
            "VoterIDName text null, VoterIDFatherHusbandName text null,PANCardID number null, PANCardName text null,PANCardFatherHusbandName text null,RationCardNo number null, CenterMeetingPlace text null)";
    //....

    public static final String TABLE_FAMILY_EXPENDITURE = "FamilyExpenditure";
    public static final String EXPENDITURE_FOOD_KEY = "FamilyFoodExpenditure";
    public static final String EXPENDITURE_TRAVEL_KEY = "FamilyTravelExpenditure";
    public static final String EXPENDITURE_ENTERTAINMENT_KEY = "FamilyEntertainmentExpenditure";
    public static final String EXPENDITURE_FESTIVAL_KEY = "FamilyFestivalExpenditure";
    public static final String EXPENDITURE_MEDICAL_KEY = "FamilyMedicalExpenditure";
    public static final String EXPENDITURE_EDUCATION_KEY = "FamilyEducationExpenditure";
    public static final String EXPENDITURE_OTHER_KEY = "FamilyOtherExpenditure";
    private static final String CREATE_TABLE_FAMILY_EXPENDITURE = "create table "+ TABLE_FAMILY_EXPENDITURE + "(" + ID + " INTEGER PRIMARY KEY, FamilyFoodExpenditure number null, FamilyTravelExpenditure number null, FamilyEntertainmentExpenditure number null, FamilyFestivalExpenditure number null, FamilyMedicalExpenditure number null, FamilyEducationExpenditure number null,  FamilyOtherExpenditure number null)";

    public static final String TABLE_FINANCIALLIABILITIES = "FinancialLeabilities";
    public static final String LIABILITIES_CHIT_KEY = "LiabilitiesChits";
    public static final String LIABILITIES_BANK_KEY = "LiabilitiesBank";
    public static final String LIABILITIES_INSURANCE_KEY = "LiabilitiesInsurance";
    public static final String LIABILITIES_FRIENDS_KEY = "LiabilitiesFriends_Family";
    private static final String CREATE_TABLEFINANCIALLIABILITIES = "create table " + TABLE_FINANCIALLIABILITIES + "(" + ID +"INTEGER PRIMARY KEY,"+
            "LiabilitiesChits number null, LiabilitiesBank number null, LiabilitiesInsurance number null, LiabilitiesFriends_Family number null)" ;

    public static final String TABLE_APPLIEDLOAN = "Applied_Loan";
    public static final String LOAN_PURPOSE_KEY = "MainLoanPurpose";
    public static final String LOAN_REQUIRED_KEY = "RequiredLoanAmount";
    public static final String LOAN_REPAYMENT_KEY = "RepaymentOption";
    public static final String LOAN_SUB_PURPOSE_KEY = "SubPurpose";
    public static final String LOAN_AMOUNT_USED1_KEY = "LoanAmountto1";
    public static final String LOAN_AMOUNT_USED2_KEY = "LoanAmountto2";
    private static final String CREATE_TABLELOAN = "create table "+ TABLE_APPLIEDLOAN + "(" +ID+ " INTEGER PRIMARY KEY, MainLoanPurpose text null, SubPurpose text null, RequiredLoanAmount number null, RepaymentOption text null, LoanAmountto1 number null,LoanAmountto2 number null)";

    public static final String TABLE_BANKDETAIL = "BankDetails";
    public static final String BANK_NAME = "BankName";
    public static final String BANK_BRANCH = "BankBranch";
    public static final String BANK_BRANCH_IFSC = "BranchIFSCCode";
    public static final String BANK_ACCOUNT_NUMBER = "AccountNumber";
    public static final String BANK_ACCOUNT_HOLDER_NAME = "AccountHolderName";
    public static final String BANK_ACCOUNT_TYPE = "BankAccountType";
    public static final String BANK_ACCOUNT_OPERATIONAL = "AccountOperationalSince";
    private static final String CREATE_TABLEBANKDETAIL = "create table " +TABLE_BANKDETAIL + "(" +ID+ " INTEGER PRIMARY KEY, BankName text null, BankBranch text null, AccountHolderName text null,BankAccountType number null, AccountOperationalSince text null, BranchIFSCCode text null) ";

    public static final String TABLE_FAMILYINDENT = "Family_Indebtnes";
    public static final String FAMILY_INDENT_LOAN_SOURCE = "LoanSource";
    public static final String FAMILY_INDENT_NAME = "Name";
    public static final String FAMILY_INDENT_BORROW_AMT = "BorrowedAmount";
    public static final String FAMILY_INDENT_MONTHLY_REPAY = "MonthlyRepayment";
    public static final String FAMILY_INDENT_OUTSTANDING = "Outstanding";
    public static final String FAMILY_INDENT_REPAYMENT_TRACK = "RepaymentTrack";
    public static final String FAMILY_INDENT_PROGRESSOUTOFPROVERTY1 = "ProgressOutOfProverty1";
    public static final String FAMILY_INDENT_PROGRESSOUTOFPROVERTY2 = "ProgressOutOfProverty2";
    public static final String FAMILY_INDENT_LOANAPPLICABLE = "LoanApplicable";
    private static final String CREATE_TABLEFAMILYINDENT = "create table "+TABLE_FAMILYINDENT+"("+ID+ " INTEGER PRIMARY KEY, LoanSource text null, Name text null,BorrowedAmount number null, MonthlyRepayment number null, Outstanding number null,RepaymentTrack text null,ProgressOutOfProverty1 text null, ProgressOutOfProverty2 text null,LoanApplicable text null)";

    public static final String TABLE_NEIGHBOUR = "NeighbourFeedBack";
    public static final String NEIGHBOUR_OCUPATION="Occupation";
    public static final String NEIGHBOUR_LOCATION = "Location";
    public static final String NEIGHBOUR_NAME="NeighbourName";
    public static final String NEIGHBOUR_REMARKS="Remarks";
    public static final String NEIGHBOUR_PHONE="PhoneNo";
    public static final String NEIGHBOUR_OCUPATION_SECOND="Occupation2nd";
    public static final String NEIGHBOUR_LOCATION_SECOND = "Location2nd";
    public static final String NEIGHBOUR_NAME_SECOND="NeighbourName2nd";
    public static final String NEIGHBOUR_REMARKS_SECOND="Remarks2nd";
    public static final String NEIGHBOUR_PHONE_SECOND="PhoneNo2nd";
    private static final String CREATE_TABLENEIGHBOUR = "create table " + TABLE_NEIGHBOUR + " (" + ID + " INTEGER PRIMARY KEY, NeighbourName text null,Occupation text null, Location text null, PhoneNo number null, Remarks text null,NeighbourName2nd text null, Occupation2nd text null,Location2nd text null, PhoneNo2nd number null)"; // it has to "Remark" field ", Remarks text null" todo had error on this query status:solved

    public static final String TABLE_FACILITATEDPRODUCT = "FacilitateProduct";
    public static final String FACILITATED_INSURANCE_KEY= "Insurance";
    public static final String FACILITATED_NAME_KEY= "Name";
    public static final String FACILITATED_AGE_KEY= "Age";
    public static final String FACILITATED_GENDER_KEY= "Gender";
    public static final String FACILITATED_RELATIONSHIP_KEY= "Relationship";
    public static final String FACILITATED_NOMINEE_KEY= "NomineeisMinor";
    public static final String FACILITATED_SOLAR_KEY= "SolarLamp";
    public static final String FACILITATED_NAME_APPOINTEE= "NameofAppointee";
    private static final String CREATE_TABLEFACIITATEPRODUCT = "create table " + TABLE_FACILITATEDPRODUCT + "(" + ID + " INTEGER PRIMARY KEY,Insurance text null,Name text null,Age text null,Gender text null,Relationship text null,NomineeisMinor text null, NameofAppointee text null,SolarLamp text null)"; // todo had error on this query status:solved
    //.....................................................

    private static final String TAG = "DBAdapter";
   // private static final String DATABASE_NAME = "Esthenos.db";

    private static final int DATABASE_VERSION = 2;
    private static final String CREATE_TABLE =
            "create table submission_data (id INTEGER PRIMARY KEY, application_id text not null, url text not null,data text not null,flag INTEGER DEFAULT 0);";

    private static final String TABLE_SUBMISSION_NAME = "submission_data";

    private static final String CREATE_TABLE_PICTURE_UPLOAD =
            "create table picture_upload (id INTEGER PRIMARY KEY, application_id text not null, external_id text null, path text not null, type text not null, flag INTEGER DEFAULT 0, name text not null);";

    private static final String TABLE_PICTURE_UPLOAD_NAME = "picture_upload";

    public MySQLiteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
            db.execSQL(CREATE_TABLE_PICTURE_UPLOAD);

            //...................................
            String CREATE_DETAILOFFAMILYMEMBER_1TO6 = "CREATE TABLE " + CREATE_DETAILOFFAMILYMEMBER + " (" + ID + " INTEGER PRIMARY KEY, Name TEXT NULL, Age number null, Relationship text null, Occupations_Details text null, Education text null, YearsofInvolvement number null, Monthly_Income number null, Sex text null);";
            String CREATE_TABLEPERSONAL_INFO = "CREATE TABLE "+ TABLEPERSONAL_INFO + " (" + ID + " INTEGER PRIMARY KEY, Name TEXT NULL,"+ PERSONAL_INFONICK_NAME_KEY +" text null, UID number null, FatherName text null, DOB varchar null, Age number null, Gender text null, Address text null, Taluk text null, District text null, State text null, Landmark text null, PermanentAddress text null, Country text null, Pincode number null, PoliceStation text null, PostOffice text null, PhoneNumber number null, MobileNumber number  null, EmailID text null, MotherName text null, MaritialStatus text null, SpouseName text null, SpouseDOB varchar null, SpouseAge number null, VIDNumber number null, VIDName text null, VIDFather_HusbandName text null, PCNumber number null,PCName text null, PCFather_HusbandName text null, CenterMeatingPlace text null, RCNo number null, Occupation text null);";
            db.execSQL(CREATE_PERSONALDETAIL);
            db.execSQL(CREATE_DETAILOFFAMILYMEMBER_1TO6);
            db.execSQL(CREATE_TABLEPERSONAL_INFO);
            db.execSQL(CREATE_TABLE_FAMILY_MEMBERDETAIL);
            db.execSQL(CREATE_TABLE_FAMILY_EXPENDITURE);
            db.execSQL(CREATE_TABLEFINANCIALLIABILITIES);
            db.execSQL(CREATE_TABLEFAMILYASSETS);
            db.execSQL(CREATE_TABLEGUARANTERINFO);
            db.execSQL(CREATE_TABLELOAN);
            db.execSQL(CREATE_TABLEBANKDETAIL);
            db.execSQL(CREATE_TABLEFAMILYINDENT);
            db.execSQL(CREATE_TABLENEIGHBOUR);
            db.execSQL(CREATE_TABLEFACIITATEPRODUCT);
            Log.d("0000000000000000000000", "table created" + CREATE_TABLE_FAMILY_MEMBERDETAIL+ "000and :" + CREATE_TABLE_FAMILY_EXPENDITURE);

            //..................................
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        if(newVersion>oldVersion)
            //  copyDatabase();
            db.execSQL("DROP TABLE IF EXISTS submission_data");
        db.execSQL("DROP TABLE IF EXISTS picture_upload");

        //.................................................
        db.execSQL("DROP TABLE IF EXISTS PersonalDetails");
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_DETAILOFFAMILYMEMBER);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_FAMILY_MEMBERDETAIL);
        db.execSQL("DROP TABLE IF EXISTS "+ CREATE_TABLE_FAMILY_EXPENDITURE);
        db.execSQL("DROP TABLE IF EXISTS "+ CREATE_TABLEFINANCIALLIABILITIES);
        db.execSQL("DROP TABLE IF EXISTS "+CREATE_TABLEFAMILYASSETS);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLEGUARANTERINFO);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLELOAN);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLEBANKDETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLEFAMILYINDENT);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLENEIGHBOUR);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLEFACIITATEPRODUCT);
        //................................................
        onCreate(db);
    }

    public void createTable() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL(CREATE_TABLE);
            db.execSQL(CREATE_TABLE_PICTURE_UPLOAD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getLastInsertedPicId() {
        long lastId = 0;
        String query = "SELECT " + KEY_ID + " from " + TABLE_PICTURE_UPLOAD_NAME + " order by " + KEY_ID + " DESC limit 1";
        Cursor c = getReadableDatabase().rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            lastId = c.getLong(0);
        }
        return lastId;
    }

    public long insertSubmissionData(String app_id, String url, String data) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_URL, url);
        initialValues.put(KEY_APPLICATION_ID, app_id);
        initialValues.put(KEY_DATA, data);
        return getWritableDatabase().insert(TABLE_SUBMISSION_NAME, null, initialValues);
    }

    public long insertPictureData(String app_id, String type, String path, String key) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, key);
        initialValues.put(KEY_TYPE, type);
        initialValues.put(KEY_PATH, path);
        initialValues.put(KEY_APPLICATION_ID, app_id);
        return getWritableDatabase().insert(TABLE_PICTURE_UPLOAD_NAME, null, initialValues);
    }

    //.........................................................
    public long insertpersonalDetails(ArrayList<ReviewItem> dest) {
        ContentValues initialValues = new ContentValues();
        for (int i=0;i< dest.size();i++){
            initialValues.put(PersonalDetailPhysical, String.valueOf(dest.get(i)));
            initialValues.put(PersonalDetailReligion,  String.valueOf(dest.get(i)));
            initialValues.put(PersonalDetailCategori,  String.valueOf(dest.get(i)));
            initialValues.put(PersonalDetailEducation, String.valueOf(dest.get(i)));
        }
        return getWritableDatabase().insert(TABLE_PERSONALDETAIL_NAME, null, initialValues);
    }

    public void FirstAutoSaveData(String TableName,String ColumnName, String data){
       try {
         //  SQLiteDatabase db = getWritableDatabase();
           ContentValues values = new ContentValues();
           values.put(ColumnName, data);
           getWritableDatabase().insert(TableName, null, values);

       }catch (Exception e){
                e.printStackTrace();
       }
    }
    public void Autosavebyid (String TableName,String ColumnName,long i, String data){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE " + TableName + " SET "+ ColumnName + " ='" + data + "' WHERE " +MySQLiteDB.ID+" = "+ i ;
      //  Log.d("55555555555555555","update by id : "+ i + ",ColumnName : " + ColumnName + " data : " + data);
      //  String query = "INSERT INTO " + TableName + "(" + ColumnName + ") values " + data + " WHERE "+MySQLiteDB.ID +" = "+ i;
        db.execSQL(query);
        //Log.d("6666666666666666666", "value updated");
    }

    public long getLastInsertedId(String TableName) {
        long lastId = 0;
        String query = "SELECT " + ID + " from " + TableName + " order by " + ID + " DESC limit 1";
        Cursor c = getReadableDatabase().rawQuery(query, null);
        if (c != null && c.moveToFirst()) {
            lastId = c.getLong(0);
        }
        return lastId;
    }

//    public Cursor getAllData(String TableName, long i){
//        Log.d("3333333333333333333","this is select");
//        SQLiteDatabase db = getWritableDatabase();
//        String whereClause = ID + "= " + i;
//      //  String query = "SELECT * FROM " + TableName + " WHERE " + ID + " = " + i ;
//        Log.d("3333333333333333333","this is select");
//        Cursor cursor = db.query(TableName, null, whereClause, null, null, null, null);
//        Log.d("444444444444444444","cursor"+cursor);
//        return cursor;
//    }

    public HashMap<String, String> getUserDetails(String TableName, long i){
        HashMap<String,String> user = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + TableName + " WHERE " + ID + " = " + i;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            for (int j = 0; j <= cursor.getColumnCount()-1; j++){
                user.put("data" + j, cursor.getString(j));
                Log.d("6666666666666666666", "value updated : " + cursor.getString(j) );

            }
        }
        cursor.close();
        db.close();
        return user;
    }

    //........................................................
    public void deleteAll() {
        getWritableDatabase().delete(TABLE_SUBMISSION_NAME, null, null);
    }

    public Cursor getAllSubmissionData() {
        String whereClause = KEY_FLAG + "=? ";
        String[] whereArgs = {Integer.toString(0)};
        return getReadableDatabase().query(TABLE_SUBMISSION_NAME, new String[]{KEY_ID, KEY_APPLICATION_ID, KEY_URL, KEY_DATA, KEY_FLAG},
                whereClause, whereArgs, null, null, null);
    }

    public Cursor getAllPicturesDataForAppId(String appId) {
        String whereClause = KEY_APPLICATION_ID + "=? AND " + KEY_FLAG + "=? ";
        String[] whereArgs = {appId, Integer.toString(0)};
        return getReadableDatabase().query(TABLE_PICTURE_UPLOAD_NAME, new String[]{KEY_ID, KEY_TYPE, KEY_PATH, KEY_APPLICATION_ID, KEY_FLAG, KEY_NAME},
                whereClause, whereArgs, null, null, null);
    }

    public Cursor getAllUploadedPicturesDataForAppId(String appId) {
        String whereClause = KEY_APPLICATION_ID + "=? AND " + KEY_FLAG + "=? ";
        String[] whereArgs = {appId, Integer.toString(1)};
        return getReadableDatabase().query(TABLE_PICTURE_UPLOAD_NAME, new String[]{KEY_ID, KEY_TYPE, KEY_PATH, KEY_APPLICATION_ID, KEY_EXTERNAL_ID, KEY_FLAG},
                whereClause, whereArgs, null, null, null);
    }

    public boolean isAllPicturesUploaded(String appId) {
        String whereClause = KEY_APPLICATION_ID + "=?";
        String[] whereArgs = {appId};
        Cursor c1 = getReadableDatabase().query(TABLE_PICTURE_UPLOAD_NAME, new String[]{KEY_ID},
                whereClause, whereArgs, null, null, null);

        String whereClause2 = KEY_APPLICATION_ID + "=? AND " + KEY_FLAG + "=? ";
        String[] whereArgs2 = {appId, Integer.toString(1)};
        Cursor c2 = getReadableDatabase().query(TABLE_PICTURE_UPLOAD_NAME, new String[]{KEY_ID},
                whereClause2, whereArgs2, null, null, null);

        return c1.getCount() == c2.getCount();
    }

    public void setPicturesUploadedForAppId(String appId, int imageId, String externalId) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_FLAG, 1);
        cv.put(KEY_EXTERNAL_ID, externalId);
        String where = KEY_ID + "=?";
        String[] whereArgs = {Integer.toString(imageId)};
        getWritableDatabase().update(TABLE_PICTURE_UPLOAD_NAME, cv, where, whereArgs);
    }

    public void setUploadedForAppId(String appId) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_FLAG, 1);
        String where = KEY_APPLICATION_ID + "=?";
        String[] whereArgs = {appId};
        getWritableDatabase().update(TABLE_SUBMISSION_NAME, cv, where, whereArgs);
    }
}