package com.esthenos.commons.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.Date;

/**
 * Created by root on 12/4/16.
 */
public class Autosave_Data {


    public static final String KEY_PREF_NAME = "Auto_Save_Pref";
    private static final String TAG = "Gravity";

    //private final Context mCxt;
    private static Autosave_Data mInstance = null;
    Context mContext;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences getpref;

    public Autosave_Data() {

//       mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);


    }/*(Context ctx) {
        this.mCxt = ctx;
    }*/

    public static Autosave_Data getInstance(Context ctx) {

        if (mInstance == null) {
            //   mInstance = new Autosave_Data(ctx);

        }
        return mInstance;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {

        Date lastModDate = new Date(dir.lastModified());
        Log.d(TAG, "Cache last modified dir:" + dir.getName() + "-- path:" + dir.getPath() + "-- last modified:" + lastModDate.getDate());
        Date d = new Date();
        long diff = d.getTime() - lastModDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);


        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            Log.d(TAG, "deleted" + dir);
            if (7 < diffDays) {

                return dir.delete();

            } else {
                return true;
            }
        } else if (dir != null && dir.isFile()) {

            if (7 < diffDays) {

                return dir.delete();

            } else {
                return true;
            }

        } else {
            return false;
        }


    }

    /* public Autosave_Data(Context context) {
         this.mContext = context;
     }
 */
    public void putdata(Context mContext, String foramname, String f_name, String value) {

        pref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        File f = new File("/data/data/" + mContext.getPackageName() + "/shared_prefs/" + KEY_PREF_NAME + ".xml");

        editor = pref.edit();
        editor.putString(foramname + "_" + f_name, value);
        editor.apply();
    }

    public String getdata(Context mContext, String foramname, String f_name) {
        String text;
        getpref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        text = getpref.getString(foramname + "_" + f_name, "nulldata");
        return text;
    }

    public void cleardata(Context mContext) {

        SharedPreferences.Editor editor;
        getpref = mContext.getSharedPreferences(KEY_PREF_NAME, Context.MODE_PRIVATE);
        editor = getpref.edit();
        editor.clear();
        editor.commit();
    }

    public int isExist() {

//File f=getDataba
        File f = new File("/data/data/com.esthenos.gravity/shared_prefs/" + KEY_PREF_NAME + ".xml");

        if (f.exists()) {
            Log.d("file pref", "file exist" + f);
            return 1;
        } else {
            return 0;
        }
    }


    public void isFolderExist() {
        // File(String.format("%s/esthenos/%s", Environment.getExternalStorageDirectory()));


        File dir = new File(Environment.getExternalStorageDirectory() + "/esthenos");
        if (dir.exists()) {
            dr(dir);
            boolean result1 = deleteDirs(dir);
            Log.d(TAG, "file exist" + dir);
            Log.d(TAG, " is deleted " + result1);
        } else {
            Log.d(TAG, "file not exists");

        }

        // String dir_path = Environment.getExternalStorageDirectory() + "/esthenos";

    }

    public void dr(File dir) {
        // String dir_path = Environment.getExternalStorageDirectory() + "/esthenos";
        Date lastModDate = new Date(dir.lastModified());
        Log.d(TAG, "last modified dir:" + dir.getName() + "-- path:" + dir.getPath() + "-- last modified:" + lastModDate.getDate());
        Date d = new Date();
        long diff = d.getTime() - lastModDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        {


            try {

                if (7 < diffDays){

                    dir.delete();
                }

                if (dir != null && dir.isDirectory()) {

                    if (dir.length() > 1) {
                        for (File child : dir.listFiles())

                            dr(child);


                    } else {
                        if (dir != null) {

                            if (7 < diffDays && !dir.getName().equals("esthenos")) {
                                Boolean result = dir.delete();
                                Log.d(TAG, "IN dr() file deleted?=" + result);
                            }

                        }
                    }


                }

                if (dir != null) {
                    if (7 < diffDays && !dir.getName().equals("esthenos")) {
                        Boolean result = dir.delete();
                        Log.d(TAG, "IN dr() file deleted?=" + result);
                    }

                }


            } catch (NullPointerException n)

            {
                n.printStackTrace();

            }

        }


    }

    public boolean deleteDirs(File dir) {

        Date lastModDate = new Date(dir.lastModified());
        Log.d(TAG, "Cache last modified dir:" + dir.getName() + "-- path:" + dir.getPath() + "-- last modified:" + lastModDate.getDate());
        Date d = new Date();
        long diff = d.getTime() - lastModDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);


        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (int i = 0; i < children.length; i++) {
                    boolean success = deleteDir(new File(dir, children[i]));
                    if (!success) {
                        return false;
                    }
                }


            }
        }
        if (dir != null) {

            if (7 < diffDays) {

                dir.delete();
            }

        }
        return dir != null ? ((7 < diffDays) ? dir.delete() : true) : false;
    }


}
