package com.esthenos.commons.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Patterns;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Constants {

    public static final int BITMAP_SIZE = 70;
    public static final int UPLOAD_KYC = 10000;

    public static final String APPLICANT = "applicant";
    public static final String GUARANTOR1 = "guarantor1";
    public static final String GUARANTOR2 = "guarantor2";

    public static final String PAGE_BRANCH = "page_branch";
    public static final String PAGE_TYPE_KYC = "kyc_details";

    public static final String COLOR_CHANGE = "Total Default : 0";

    public static final String PAGE_TYPE_PAN = "pan_card";
    public static final String PAGE_TITLE_PAN = "PAN Card";

    public static final String PAGE_TYPE_AADHAR = "aadhar_card";
    public static final String PAGE_TITLE_AADHAR = "Aadhaar Card";

    public static final String PAGE_TYPE_VOTERID = "voter_card";
    public static final String PAGE_TITLE_VOTERID = "Voter ID Card";

    public static final String PAGE_TYPE_PHOTO = "photo";
    public static final String PAGE_TYPE_OTHER = "other_card";
    public static final String PAGE_TITLE_OTHER = "Other Document";

    public static final String PAGE_TYPE_LOAN_DETAILS = "loan_details";
    public static final String PAGE_TYPE_FAMILY_DETAILS = "family_details";
    public static final String PAGE_TYPE_FAMILY_EXPENDITURE = "family_expenditure";

    public static final String PAGE_TYPE_NOMINEE = "nominee_details";

    public static final String PAGE_TYPE_PERSONAL = "personal_docs";
    public static final String PAGE_TITLE_PERSONAL = "Personal Document";
    public static final String PAGE_TYPE_HYPOTHECATION = "hypothecation_goods";

    public static final String PAGE_TYPE_BUSINESS = "business_docs";
    public static final String PAGE_TITLE_BUSINESS = "Business Document";

    public static final List PAGE_OWNERS = Arrays.asList(APPLICANT, GUARANTOR1, GUARANTOR2);
    public static final List PAGE_IMAGE_BACK = Arrays.asList(PAGE_TYPE_VOTERID, PAGE_TYPE_AADHAR, PAGE_TYPE_OTHER);
    public static final List PAGE_IMAGE_FRONT = Arrays.asList(PAGE_TYPE_PAN, PAGE_TYPE_VOTERID, PAGE_TYPE_AADHAR, PAGE_TYPE_OTHER);

    public static final String GRT_PASS = "grt";
    public static final String CGT_FAIL = "fail";

    public static final String CHECKED = "checked";
    public static final String REQ_TYPE = "request-type";
    public static final String REQ_CENTERS = "centers-list";
    public static final String REQ_GROUPS_STATUS = "groups-status";
    public static final String REQ_GROUPS_AVAILABLE = "groups-available";
    public static final String VIEW_STATUS = "view_status";

    public static final String REQ_QUESTIONS_GRT = "grt";
    public static final String REQ_QUESTIONS_CGT1 = "cgt1";
    public static final String REQ_QUESTIONS_CGT2 = "cgt2";

    public static final String ACTIVITY = "activity";
    public static final String ACTIVITY_GUARANTOR_KYC = "guarantor-kyc";
    public static final String ACTIVITY_APPLICANT_KYC = "applicant-kyc";
    public static final String ACTIVITY_APPLICANT_FORM = "applicant-form";
    public static final String ACTIVITY_APPLICANT_DOCS_OTHER = "applicant-other-doc";

    public static final String LOCATION_HOME = "home";
    public static final String LOCATION_CENTER = "center";
    public static final String LOCATION_BUSINESS = "business";
    public static final String TITLE = "title";
    public static final String USER_ROLE_CM = "org_cm";
    public static final String USER_ROLE_BM = "org_bm";

    public static boolean shouldCaptureBack(String cardType) {
        return PAGE_IMAGE_BACK.contains(cardType);
    }

    public static boolean shouldCaptureFront(String cardType) {
        return PAGE_IMAGE_FRONT.contains(cardType);
    }

    public static int shouldTakeImages(String cardType) {
        return shouldCaptureFront(cardType) && shouldCaptureBack(cardType) ? 0 : 1;
    }

    public static boolean validOwner(String owner) {
        return PAGE_OWNERS.contains(owner);
    }

    public static boolean validEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String setGreeting() {
        Calendar c = Calendar.getInstance(Locale.getDefault());
        int hour = c.get(Calendar.HOUR_OF_DAY);

        if (hour < 12) {
            return "Good Morning !";

        } else if (hour < 18) {
            return "Good Afternoon !";

        } else return "Good Evening !";
    }

    public static String getVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "version-na";
    }
}
