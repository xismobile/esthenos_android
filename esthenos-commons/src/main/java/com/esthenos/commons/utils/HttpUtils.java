package com.esthenos.commons.utils;


import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class HttpUtils {

    private static final Gson gson = new Gson();
    private static final OkHttpClient client = new OkHttpClient();

    public static <K> K request(Request request, Class<K> responseClass) throws IOException {
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String content = response.body().string();
        System.out.println(request.httpUrl());
        System.out.println(content);
        return gson.fromJson(content, responseClass);
    }

    public static String toJson(Object object) {
        return gson.toJson(object);
    }
}
