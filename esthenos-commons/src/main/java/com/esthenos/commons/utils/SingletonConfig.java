package com.esthenos.commons.utils;

import android.util.Log;

import com.esthenos.commons.cards.AadhaarCard;
import com.esthenos.commons.cards.KYCCards;
import com.esthenos.commons.cards.PanCard;
import com.esthenos.commons.cards.VoterIdCard;
import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.ProductsModel.Product;
import com.esthenos.commons.models.CenterGroupsModel.Group;
import com.esthenos.commons.models.CenterGroupsModel.Center;
import com.esthenos.commons.models.UserModel;
import com.esthenos.commons.models.UserModel.User;
import com.squareup.okhttp.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class SingletonConfig {
    private String appId;
    private boolean isUploading;

    private String userServer;
    private User userLogin;
    private CenterDetails userCenter = new CenterDetails();
    private Map<String, HashMap<String, Double>> locations;

    private Map<String, HashMap<String, List<String>>> appDocs;

    private Map<String, KYCCards> kycCards;
    private static SingletonConfig instance;

    private static class CenterDetails {
        private Group group;
        private Center center;
        private Product product;
        private ApplicationModel.ApplicationResponse applicationResponse;
    }

    private SingletonConfig(String domain, User user,
                            CenterDetails center, Map<String, HashMap<String, Double>> locationsMap) {
        userLogin = user;
        userServer = domain;
        userCenter = center;
        locations = locationsMap;

        appId = UUID.randomUUID().toString();

        appDocs = new HashMap<String, HashMap<String, List<String>>>() {{
            put(Constants.APPLICANT, new HashMap<String, List<String>>());
            put(Constants.GUARANTOR1, new HashMap<String, List<String>>());
            put(Constants.GUARANTOR2, new HashMap<String, List<String>>());
        }};

        kycCards = new HashMap<String, KYCCards>() {{
            put(Constants.APPLICANT, new KYCCards());
            put(Constants.GUARANTOR1, new KYCCards());
            put(Constants.GUARANTOR2, new KYCCards());
        }};
    }

    public static SingletonConfig getInstance() {
        if (instance == null) {
            throw new RuntimeException("Instance not initialized");
        }
        return instance;
    }

    private static SingletonConfig getInstance(String domain, User user,
                                               CenterDetails center, Map<String, HashMap<String, Double>> locations) {
        instance = new SingletonConfig(domain, user, center, locations);
        return instance;
    }

    public User getUser() {
        return userLogin;
    }

    public String getUserToken() {
        return userLogin.token;
    }

    public String getUserBucket() {
        return userLogin.bucket;
    }

    public String getUserPoolId() {
        return userLogin.poolId;
    }

    public String getUserServer() {
        return userServer;
    }

    public String getAppId() {
        return appId;
    }

    public void reset() {
        instance = new SingletonConfig(userServer, userLogin, userCenter, locations);
    }

    public boolean isUploading() {
        return isUploading;
    }

    public void setUploading(boolean uploading) {
        isUploading = uploading;
    }

    public void addKYCCard(String owner, PanCard panCard) {
        kycCards.get(owner).panCard = panCard;
    }

    public void addKYCCard(String owner, AadhaarCard aadhaarCard) {
        kycCards.get(owner).aadhaarCard = aadhaarCard;
    }

    public void addKYCCard(String owner, VoterIdCard voterIdCard) {
        kycCards.get(owner).voterIdCard = voterIdCard;
    }

    public KYCCards getKYCCards(String owner) {
        return kycCards.get(owner);
    }

    public void addDocs(String owner, String type, String path) {
        if (Constants.validOwner(owner)) {
            List<String> items = appDocs.get(owner).containsKey(type) ? appDocs.get(owner).get(type) : new ArrayList<String>();
            items.add(path);
            appDocs.get(owner).put(type, items);
        } else {
            throw new RuntimeException("attempting to add documents for illegal owner: " + owner);
        }
    }

    public HashMap<String, List<String>> getDocs(String owner) {
        return appDocs.get(owner);
    }

    public Map<String, HashMap<String, List<String>>> getAppDocs() {
        return appDocs;
    }

    public String getServerUrl(String path) {
        return "http://" + userServer + path;
    }

    public String getProducts() {
        return getServerUrl("/api/organisation/products?instance_token=" + userLogin.token);
    }

    public String getTimeSlots() {
        return getServerUrl("/api/organisation/timeslots?instance_token=" + userLogin.token);
    }

    public String getUserPerformance() {
        return getServerUrl(String.format("/api/user/performance?instance_token=%s", userLogin.token));
    }

    public String getCenters() {
        return getServerUrl("/api/organisation/centers?instance_token=" + userLogin.token);
    }

    public String getCenterDetails() {
        return getServerUrl(String.format(
                "/api/organisation/centers/%s?instance_token=%s", userCenter.center.id, userLogin.token
        ));
    }

    public String getCenterGroupsStatus() {
        return getServerUrl(String.format(
                "/api/organisation/centers/%s/groups/list/final?instance_token=%s", userCenter.center.id, userLogin.token
        ));
    }

    public String getCenterGroupsAvailable() {
        return getServerUrl(String.format(
                "/api/organisation/centers/%s/groups/list/available?instance_token=%s", userCenter.center.id, userLogin.token
        ));
    }

    public String getGroupDetails() {
        return getServerUrl(String.format(
                "/api/organisation/groups/%s?instance_token=%s", userCenter.group.id, userLogin.token
        ));
    }
    
   public String getGroupApplications(String cgtType) {
       //todo-refactor this, into fos/gravity use-case.
       if (cgtType =="" ) {
           return getServerUrl(String.format(
                   "/api/organisation/applications/%s?instance_token=%s", cgtType, userLogin.token

           ));
       }
       return getServerUrl(String.format(

               "/api/organisation/groups/%s/applications/%s?instance_token=%s", userCenter.group.id,cgtType, userLogin.token
       ));
   }

    public String getCGTQuestions(String cgtTraining) {
        return getServerUrl(String.format(
                "/api/organisation/groups/%s/questions/%s?instance_token=%s", userCenter.group.id, cgtTraining, userLogin.token
        ));
    }

    public String getCGTReadyGroups(String cgtTraining) {
        return getServerUrl(String.format(
                "/api/organisation/centers/%s/groups/%s/none?instance_token=%s", userCenter.center.id, cgtTraining, userLogin.token
        ));
    }

    public void setLocation(String name, final double latitude, final double longitude) {
        HashMap<String, Double> location = new HashMap<String, Double>() {{
            put("lat", latitude);
            put("lng", longitude);
        }};
        this.locations.put(name, location);
        Log.d("Lat-long", name + ": " + latitude + ", " + longitude);
    }

    public Map<String, HashMap<String, Double>> getLocations() {
        return locations;
    }

    public Map<String, Object> getAppDetails() {
        return new HashMap<String, Object>() {{
            put("assets_id", getAppId());
            put("assets_map", getAppDocs());
            put("locations_map", getLocations());
            put("group", getGroupId());
            put("center", getCenterId());
            put("product", getProduct().product_id);
        }};
    }

    public ApplicationModel.ApplicationResponse getApplicationResponse(){
        return userCenter.applicationResponse;
    }

    public void setApplicationResponse(ApplicationModel.ApplicationResponse applicationResponse){
        this.userCenter.applicationResponse = applicationResponse;
    }

    public Group getGroup() {
        return userCenter.group;
    }

    public String getGroupId() {
        return userCenter.group == null ? "G-NA" : userCenter.group.id;
    }

    public void setGroup(Group group) {
        this.userCenter.group = group;
    }

    public Center getCenter() {
        return this.userCenter.center;
    }

    public String getCenterId() {
        return userCenter.center == null ? "C-NA" : userCenter.center.id;
    }

    public void setCenter(Center center) {
        this.userCenter.center = center;
    }

    public Product getProduct() {
        return this.userCenter.product;
    }

    public void setProduct(Product product) {
        this.userCenter.product = product;
    }

    public static SingletonConfig getLoginToken(String email, String password) throws Exception {
        if (!Constants.validEmail(email)) {
            throw new RuntimeException("Invalid Email Address");
        }

        String domain = email.split("@")[1];
        Request request = UserModel.getLoginForm(domain, email, password);
        User user = HttpUtils.request(request, User.class);
        return user.token == null ? null : getInstance(domain, user, new CenterDetails(), new HashMap<String, HashMap<String, Double>>());
    }
}
