package com.esthenos.commons.utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.esthenos.commons.R;
import com.esthenos.commons.pages.CustomerKYCDetailsFragment;

import java.util.HashMap;

/**
 * Created by root on 2/4/16.
 */
public class CustomeDialog extends Dialog implements
        android.view.View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes, no;
    public String TableName,Name,Nickname,UID,Fathername,DOB,Age,Gender,Address,Taluk,District,State,Landmark,PermentAddress, Country, Pincode, Policestation, Postoffice, Telephonenumber, Mobilenumber, Emailid,Mothername,Maritialstatus,Spousename,Spousedob,spouseage,VIDnumber,VIDname,VIDfather_husbandname,PCnumber,PCname,PCfather_husbandname,Centermeeting,RCno,Occupation;
    public long id;

    public MySQLiteDB mydb;
    CustomerKYCDetailsFragment cusomerkycdetailfrag;

    public CustomeDialog(Activity a,String TableName, long id) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.TableName = TableName;
        this.id = id;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custome_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);

        mydb = new MySQLiteDB(getContext());

        yes.setOnClickListener(this);
        no.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btn_yes) {

            HashMap<String,String> user = mydb.getUserDetails(TableName,id);

            Log.d("22222222222222222222", "Tablename : " + TableName + " , id : " + id);
            //then loop on the inputs of the cursor inside the hashmap and print them successfully
            System.out.println(user.get("name"));
//            try(Cursor cursor = mydb.getAllData(TableName,id)){
//                    while (cursor.moveToNext()) {
//                        Log.v("1122222222222222222", "bbbb");
//                        Name = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFONAME_KEY));
//                        Nickname = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFONICK_NAME_KEY));
//                        UID = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOUID_KEY));
//                        Fathername = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOFATHER_NAME));
//                        DOB = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOYOB_KEY));
//                        Age = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOAGE_KEY));
//                        Gender = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOGENDER_KEY));
//                        Address = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOADDRESS_KEY));
//                        Taluk = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOTALUK_KEY));
//                        District = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFODISTRICT_KEY));
//                        State = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOSTATE_KEY));
//                        Landmark = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOLANDMARKS_KEY));
//                        PermentAddress = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPERMANENT_ADDRESS_KEY));
//                        Country = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOCOUNTRY_KEY));
//                        Pincode = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPINCODE_KEY));
//                        Policestation = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPOLICE_KEY));
//                        Postoffice = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPOST_OFFICE_KEY));
//                        Telephonenumber = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOTELE_PHONE_KEY));
//                        Mobilenumber = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOMOBILE_PHONE_KEY));
//                        Emailid = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOEMAIL_ID_KEY));
//                        Mothername = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOMOTHER_KEY));
//                       // Maritialstatus = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOMARITAL_STATUS));
//                        Spousename = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOSPOUSE_NAME));
//                        Spousedob = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOSPOUSE_DOB_KEY));
//                        spouseage = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOSPOUSE_AGE_KEY));
//                        VIDnumber = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOVOTER_ID_KEY));
//                        VIDname = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOVOTER_ID_NAME_KEY));
//                        VIDfather_husbandname = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOVOTER_ID_FATHER_HUSBAND_NAME));
//                        PCnumber = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPANCARD_KEY));
//                        PCname = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPANCARD_NAME_KEY));
//                        PCfather_husbandname = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOPANCARD_FATHER_HUSBAND_NAME));
//                        Centermeeting = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOCENTERMEETING_KEY));
//                        RCno = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFORATIONCARD_KEY));
//                       // Occupation = cursor.getString(cursor.getColumnIndex(MySQLiteDB.PERSONAL_INFOOCCUPATION_KEY));
//                        Log.v("1122222222222222222",  Name + Nickname+ UID);
//
//                        CustomerKYCDetailsFragment.nameView.setText(Name);
//                        CustomerKYCDetailsFragment.mNickName.setText(Nickname);
//                        CustomerKYCDetailsFragment.uidView.setText(UID);
//                        CustomerKYCDetailsFragment.f_or_h_name.setText(Fathername);
//                        CustomerKYCDetailsFragment.yobView.setText(DOB);
//
//                        CustomerKYCDetailsFragment.addressView.setText(Address);
//                        CustomerKYCDetailsFragment.talukView.setText(Taluk);
//                        CustomerKYCDetailsFragment.districtView.setText(District);
//                        CustomerKYCDetailsFragment.stateView.setText(State);
//                        CustomerKYCDetailsFragment.mLandmarks.setText(Landmark);
//                        CustomerKYCDetailsFragment.mPermanentAddress.setText(PermentAddress);
//                        CustomerKYCDetailsFragment.countryView.setText(Country);
//                        CustomerKYCDetailsFragment.pincodeView.setText(Pincode);
//                        CustomerKYCDetailsFragment.mPoliceStaion.setText(Policestation);
//                        CustomerKYCDetailsFragment.mPostOffice.setText(Postoffice);
//                        CustomerKYCDetailsFragment.telephoneNumberView.setText(Telephonenumber);
//                        CustomerKYCDetailsFragment.mobileNumberView.setText(Mobilenumber);
//                        CustomerKYCDetailsFragment.mPersonalEmail.setText(Emailid);
//                        CustomerKYCDetailsFragment.mMotherName.setText(Mothername);
//
//                        CustomerKYCDetailsFragment.mSpouse.setText(Spousename);
//                        CustomerKYCDetailsFragment.mSpouseDob.setText(Spousedob);
//                        CustomerKYCDetailsFragment.mSpouseAge.setText(spouseage);
//                        CustomerKYCDetailsFragment.voterId.setText(VIDnumber);
//                        CustomerKYCDetailsFragment.voterIdName.setText(VIDname);
//                        CustomerKYCDetailsFragment.voterId_f_or_h_name.setText(VIDfather_husbandname);
//                        CustomerKYCDetailsFragment.panCardId.setText(PCnumber);
//                        CustomerKYCDetailsFragment.panCardName.setText(PCname);
//                        CustomerKYCDetailsFragment.panCard_f_or_h_name.setText(PCfather_husbandname);
//                        CustomerKYCDetailsFragment.mCenterMeetiong.setText(Centermeeting);
//                        CustomerKYCDetailsFragment.mRtionCard.setText(RCno);
//
//                    }
//
//            }catch (Exception e){
//                e.printStackTrace();
//            }

        } else if (i == R.id.btn_no) {
            dismiss();

        } else {
        }
        dismiss();
    }
}
