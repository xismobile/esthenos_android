package com.esthenos.commons.kyc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.cards.AadhaarCardReader;
import com.esthenos.commons.customcamera.ScannerActivity;
import com.esthenos.commons.customcamera.CustomCameraActivity;
import com.esthenos.commons.utils.SingletonConfig;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;


public class CapturePageAadhaarCardFragment extends Fragment implements View.OnFocusChangeListener {

    private static final String ARG_KEY = "key";
    private static final String BARCODE_KEY = "barcode_key";

    private String mKey;
    private SingletonConfig instance = SingletonConfig.getInstance();
    private CapturePageAadhaarCard mPage;

    private View viewInFocus;
    private PageFragmentCallbacks mCallbacks;

    private BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, final Intent intent) {
            String dataKey = mPage.getFormId();

            Log.d(dataKey, mKey);
            Log.d(dataKey, intent.getStringExtra("Format"));
            Log.d(dataKey, intent.getStringExtra("Contents"));

            String result = intent.getStringExtra("Contents");
            instance.addKYCCard(mPage.getPageOwner(), new AadhaarCardReader(result).getAadhaarCard());

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent customCam = new Intent(getActivity(), CustomCameraActivity.class);
                        customCam.putExtra("card_type", mPage.getPageType());
                        customCam.putExtra("card_name", mPage.getPageId());
                        customCam.putExtra("card_owner", mPage.getPageOwner());
                        startActivityForResult(customCam, 110);
                    }
                });
            }
        }
    };

    public static CapturePageAadhaarCardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);

        CapturePageAadhaarCardFragment fragment = new CapturePageAadhaarCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CapturePageAadhaarCardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CapturePageAadhaarCard) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.capture_aadhaar_image, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        ImageView iv = (ImageView) rootView.findViewById(R.id.aadhaar_image);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ScannerActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 110) {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        mCallbacks = (PageFragmentCallbacks) activity;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mUpdateUIReceiver, new IntentFilter(BARCODE_KEY));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mUpdateUIReceiver);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view != viewInFocus && !b)
            viewInFocus = view;
    }
}
