package com.esthenos.commons.kyc;

import android.content.Context;

import com.esthenos.commons.pages.ImageCapturePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.PAGE_BRANCH;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_VOTERID;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_KYC;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_VOTERID;


public class KYCModelApplicant extends AbstractWizardModel {

    public KYCModelApplicant(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
            new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, "Add Applicant's KYC Documents"))
                .addBranch(PAGE_TITLE_AADHAR,
                    new CapturePageAadhaarCard(this, new PageConfig(APPLICANT, PAGE_TYPE_AADHAR, PAGE_TITLE_AADHAR))
                )
                .addBranch(PAGE_TITLE_VOTERID,
                    new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_VOTERID, PAGE_TITLE_VOTERID))
                )
                .addBranch(PAGE_TITLE_PAN,
                    new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PAN, PAGE_TITLE_PAN))
                )
        );
    }
}
