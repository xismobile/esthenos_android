package com.esthenos.commons.kyc;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.esthenos.commons.R;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

import java.io.File;
import java.util.List;


public class CapturePageVoterIdCardFragment extends Fragment implements Validator.ValidationListener, View.OnFocusChangeListener, View.OnClickListener {
    private String mKey;
    private static final String ARG_KEY = "key";

    private View viewInFocus;
    private Validator validator;
    private CapturePageVoterIdCard mPage;
    private PageFragmentCallbacks mCallbacks;

    private static int OCR_NAME = 5;
    private static int OCR_IDNUMBER = 6;
    private static int OCR_DATE = 7;
    private static int OCR_ADDRESS = 8;
    private static int OCR_GENDER = 9;
    private static int OCR_FATHERNAME = 10;
    private static int OCR_NONE = 11;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;

    public static final String DATA_PATH = Environment.getExternalStorageDirectory().toString() + "/OCRDemo1/";
    public static final String lang = "eng";

    private int ocrMode;
    private Uri mImageCaptureUri;

    public static CapturePageVoterIdCardFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        CapturePageVoterIdCardFragment fragment = new CapturePageVoterIdCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public CapturePageVoterIdCardFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CapturePageVoterIdCard) mCallbacks.onGetPage(mKey);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.capture_voterid_card, container, false);
        Button buttonName = (Button) rootView.findViewById(R.id.btn_ocrname);
        Button buttonIdNumber = (Button) rootView.findViewById(R.id.btn_ocridnumber);
        Button buttonAddress = (Button) rootView.findViewById(R.id.btn_ocraddress);
        Button buttonDate = (Button) rootView.findViewById(R.id.btn_ocrdate);
        Button buttonGender = (Button) rootView.findViewById(R.id.btn_ocrgender);
        Button buttonFathername = (Button) rootView.findViewById(R.id.btn_ocrfathername);
        Button buttonChooseimage = (Button) rootView.findViewById(R.id.btn_setimage);

        buttonName.setOnClickListener(this);
        buttonIdNumber.setOnClickListener(this);
        buttonGender.setOnClickListener(this);
        buttonAddress.setOnClickListener(this);
        buttonDate.setOnClickListener(this);
        buttonFathername.setOnClickListener(this);
        buttonChooseimage.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }

        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onValidationSucceeded() {
        mPage.notifyDataChanged();
    }

    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText && view == viewInFocus) {
                ((EditText) view).setError(message);
            }
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view != viewInFocus && !b)
            viewInFocus = view;
        validator.validate();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ocrname) {
            ocrMode = OCR_NAME;

        } else if (v.getId() == R.id.btn_ocraddress) {
            ocrMode = OCR_ADDRESS;

        } else if (v.getId() == R.id.btn_ocridnumber) {
            ocrMode = OCR_IDNUMBER;

        } else if (v.getId() == R.id.btn_ocrdate) {
            ocrMode = OCR_DATE;

        } else if (v.getId() == R.id.btn_ocrgender) {
            ocrMode = OCR_GENDER;

        } else if (v.getId() == R.id.btn_ocrfathername) {
            ocrMode = OCR_FATHERNAME;

        } else if (v.getId() == R.id.btn_setimage) {
            ocrMode = OCR_NONE;
            ShowImageChooser();
        }

        if (v.getId() != R.id.btn_setimage) {
            if (mImageCaptureUri == null) {
                ShowImageChooser();
            } else {
                doCrop();
            }
        }
    }

    private void ShowImageChooser() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "tmp_img.jpg"));
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, PICK_FROM_CAMERA);
    }

    private void doCrop() {
        Context context = getActivity().getApplicationContext();
        PackageManager packageManager = context.getPackageManager();

        try {
            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setType("image/*");

            List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
            int size = list.size();

            if (size == 0) {
                Toast.makeText(context, "Can not find image crop app", Toast.LENGTH_SHORT).show();
                return;

            } else {
                intent.setData(mImageCaptureUri);

                // intent.putExtra("outputX", 200);
                // intent.putExtra("outputY", 200);
                // intent.putExtra("aspectX", 1);
                // intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("return-data", true);

                if (size >= 1) {
                    Intent i = new Intent(intent);
                    ResolveInfo res = list.get(0);

                    i.setComponent(new ComponentName(
                            res.activityInfo.packageName, res.activityInfo.name));

                    startActivityForResult(i, CROP_FROM_CAMERA);
                }
            }
        } catch (Exception exp) {

            String errorMessage = "Whoops - your device doesn't support the crop action!, more info: "
                    + exp.toString();
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            if (requestCode != CROP_FROM_CAMERA) {
                mImageCaptureUri = null;
            }
            return;
        }

        switch (requestCode) {

            case PICK_FROM_CAMERA:
                if (ocrMode != OCR_NONE) {
                    doCrop();
                }
                break;

            case PICK_FROM_FILE:
                mImageCaptureUri = data.getData();
                if (ocrMode != OCR_NONE) {
                    doCrop();
                }
                break;

            case CROP_FROM_CAMERA:
                Bundle extras = data.getExtras();

                if (extras != null) {
                    Bitmap photo = extras.getParcelable("data");
                    photo = getResizedBitmap(photo, photo.getWidth() * 3, photo.getHeight() * 3);
                    // photo = toGrayscale(photo);
                    photo = photo.copy(Bitmap.Config.ARGB_8888, true);

                    if (ocrMode == OCR_NAME) {
                        doOCR(photo, R.id.et_ocrname, R.id.img_ocrname);
                    } else if (ocrMode == OCR_ADDRESS) {
                        doOCR(photo, R.id.et_ocraddress, R.id.img_ocraddress);
                    } else if (ocrMode == OCR_IDNUMBER) {
                        doOCR(photo, R.id.et_ocridnumber, R.id.img_ocridnumber);
                    } else if (ocrMode == OCR_DATE) {
                        doOCR(photo, R.id.et_ocrdate, R.id.img_ocrdate);
                    } else if (ocrMode == OCR_GENDER) {
                        doOCR(photo, R.id.et_ocrgender, R.id.img_ocrgender);
                    } else if (ocrMode == OCR_FATHERNAME) {
                        doOCR(photo, R.id.et_ocrfathername, R.id.img_ocrfathername);
                    }
                }
                break;

        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    private void doOCR(final Bitmap bitmap, final int editTextR, final int previewImageR) {

        new AsyncTask<Bitmap, Void, String>() {

            @Override
            public void onPreExecute() {
                ((ImageView) getActivity().findViewById(previewImageR)).setImageResource(R.drawable.ic_loading);
            }

            @Override
            protected String doInBackground(Bitmap... bits) {
                String recognizedText = "";
                try {
                    TessBaseAPI baseApi = new TessBaseAPI();
                    baseApi.setDebug(true);
                    baseApi.init(DATA_PATH, lang, TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED);

                    // baseApi.init(IMG_DIR, lang);
                    baseApi.setImage(bitmap);
                    recognizedText = baseApi.getUTF8Text();
                    baseApi.end();

                    if (lang.equalsIgnoreCase("eng")) {
                        // recognizedText = recognizedText
                        // .replaceAll("[^a-zA-Z0-9]+", " ");
                    }
                    recognizedText = recognizedText.trim();

                } catch (Exception exp) {
                    exp.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), exp.toString(), Toast.LENGTH_SHORT).show();

                }
                return recognizedText;
            }

            public void onPostExecute(String str) {
                ((EditText) getActivity().findViewById(editTextR)).setText(str);
                Bitmap small = getResizedBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2);

                ((ImageView) getActivity().findViewById(previewImageR)).setImageBitmap(small);
                System.out.println("OCR text: " + str);
            }

        }.execute();

    }
}
