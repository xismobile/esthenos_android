package com.esthenos.commons.service;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;

import com.esthenos.commons.utils.MySQLiteDB;

public class SyncService extends IntentService {

    public SyncService() {
        super("DownloadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        PicUploadwithProgressBar service = new PicUploadwithProgressBar(this);

        try(MySQLiteDB db = new MySQLiteDB(this); Cursor cursor = db.getAllSubmissionData()) {

            if (cursor.moveToFirst()) {
                do {
                    String appId = cursor.getString(cursor.getColumnIndex(MySQLiteDB.KEY_APPLICATION_ID));
                    Cursor picsCursor = db.getAllPicturesDataForAppId(appId);

                    if (picsCursor.moveToFirst()) {
                        do {
                            String picName = picsCursor.getString(picsCursor.getColumnIndex(MySQLiteDB.KEY_NAME));
                            String picPath = picsCursor.getString(picsCursor.getColumnIndex(MySQLiteDB.KEY_PATH));
                            service.upload(picPath, picName);

                        } while (picsCursor.moveToNext());
                    }

                    Intent myIntent = new Intent(this, UploadApplicationService.class);
                    startService(myIntent);

                } while (cursor.moveToNext());

                cursor.close();
            }
        }
    }
}