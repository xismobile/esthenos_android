package com.esthenos.commons.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.os.Handler;
import android.widget.Toast;

import com.esthenos.commons.models.ApplicationModel;
import com.esthenos.commons.models.MiscModel.SubmitForm;
import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;


public class UploadApplicationService extends IntentService {

    public static final String TAG = "UploadAppService";
    public static final String RESULT = "result";
    public static final String MESSAGE = "message";
    public static final String NOTIFICATION = "com.esthenos.commons.service.receiver";
    public static final SingletonConfig instance = SingletonConfig.getInstance();
    private Handler handler;

    public UploadApplicationService() {
        super("UploadApplicationService");
    }
    PicUploadwithProgressBar service = new PicUploadwithProgressBar(this);

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d(TAG, "uploading offline applications");


            try (MySQLiteDB db = new MySQLiteDB(this); Cursor cursor = db.getAllSubmissionData()) {

                if (isDataAvailable()) {
                    if (cursor.moveToFirst()) {
                        do {
                            String appId = cursor.getString(cursor.getColumnIndex(MySQLiteDB.KEY_APPLICATION_ID));
                            Cursor picsCursor = db.getAllPicturesDataForAppId(appId);

                            if (picsCursor.moveToFirst()) {
                                if (isDataAvailable()) {
                                    do {
                                        String picName = picsCursor.getString(picsCursor.getColumnIndex(MySQLiteDB.KEY_NAME));
                                        String picPath = picsCursor.getString(picsCursor.getColumnIndex(MySQLiteDB.KEY_PATH));
                                        service.upload(picPath, picName);

                                    } while (picsCursor.moveToNext());
                                }
                            }
                            picsCursor.close();
                        } while (cursor.moveToNext());
                    }
                    if (isDataAvailable()) {
                        if (cursor.moveToFirst()) {
                            do {
                                try {
                                    String appId = cursor.getString(cursor.getColumnIndex(MySQLiteDB.KEY_APPLICATION_ID));
                                    String appData = cursor.getString(cursor.getColumnIndex(MySQLiteDB.KEY_DATA));
                                    Log.d(TAG, "uploading offline application: " + appId);
                                    Log.d(TAG, "uploading offline application: " + appData);

                                    Request request = ApplicationModel.postApplicationForm(instance, appData);
                                    SubmitForm response = HttpUtils.request(request, SubmitForm.class);
                                    if (response.success.equals("true")) {
                                        db.setUploadedForAppId(appId);
                                        publishResults("Uploaded Application :" + appId, Activity.RESULT_OK);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                    } else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Network Not Available submit Later", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Network Not Available submit Later", Toast.LENGTH_LONG).show();
                        }
                    });
                }


            }
    }
    private boolean isDataAvailable()
    {
        ConnectivityManager conxMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo mobileNwInfo = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNwInfo   = conxMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return ((mobileNwInfo== null? false : mobileNwInfo.isConnectedOrConnecting() )|| (wifiNwInfo == null? false : wifiNwInfo.isConnectedOrConnecting()));
    }
    private void publishResults(String message, int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(MESSAGE, message);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        return super.onStartCommand(intent, flags, startId);
    }

}