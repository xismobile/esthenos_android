package com.esthenos.commons.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;

import java.io.File;

public class PicUploadwithProgressBar {

    Context mContext;
    SingletonConfig instance = SingletonConfig.getInstance();

    public PicUploadwithProgressBar(Context context) {
        mContext = context;
    }

    public void uploadPhotos(String cardType, String cardOwner, String imagePath, String imageKey) {
        String appId = instance.getAppId();
        instance.addDocs(cardOwner, cardType, imageKey);

        try (MySQLiteDB db = new MySQLiteDB(mContext)) {
            db.insertPictureData(appId, cardType, imagePath, imageKey);

               if (isConnected(mContext) && !instance.isUploading()) {
                   instance.setUploading(true);
                   upload(imagePath, imageKey);
                   instance.setUploading(false);
               }

        }catch (Exception e){
           e.printStackTrace();
        }
    }

    public boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void upload(String imagePath, String imageKey) {
        try {
        CognitoCachingCredentialsProvider credentialsProvider =
                new CognitoCachingCredentialsProvider(mContext, instance.getUserPoolId(), Regions.US_EAST_1);

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);
        s3.setRegion(Region.getRegion(Regions.US_EAST_1));

        TransferUtility transferUtility = new TransferUtility(s3, mContext);

            TransferObserver observer = transferUtility.upload(
                    instance.getUserBucket(), imageKey, new File(imagePath)
            );
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    Toast.makeText(mContext, String.format("uploading %s", state.name()), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                }

                @Override
                public void onError(int id, Exception ex) {
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
