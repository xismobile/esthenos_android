package com.esthenos.commons.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.esthenos.commons.uploadservice.UploadService;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;

public class PicUploadReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.getString(UploadService.UPLOAD_ID) != null) {

            int resultCode = bundle.getInt(UploadService.STATUS);
            if (resultCode == UploadService.STATUS_COMPLETED) {
                int picId = bundle.getInt(UploadService.PARAM_PICTURE_ID);
                String externalId = bundle.getString(UploadService.EXTERNAL_UPLOAD_ID);

                try (MySQLiteDB db = new MySQLiteDB(context)) {
                    SingletonConfig instance = SingletonConfig.getInstance();
                    String appId = instance.getAppId();
                    db.setPicturesUploadedForAppId(appId, picId, externalId);

                    instance.setUploading(false);
                    if (db.isAllPicturesUploaded(appId)) {
                        Intent myIntent = new Intent(context, SyncService.class);
                        context.startService(myIntent);
                    }
                }
            }
        }
    }
} 
