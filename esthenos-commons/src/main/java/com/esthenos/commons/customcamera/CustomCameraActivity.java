package com.esthenos.commons.customcamera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.esthenos.commons.R;
import com.esthenos.commons.service.PicUploadwithProgressBar;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.SingletonConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static com.esthenos.commons.utils.Constants.shouldCaptureBack;
import static com.esthenos.commons.utils.Constants.shouldTakeImages;


public class CustomCameraActivity extends Activity implements SurfaceHolder.Callback {
    public static final String TAG = "CustomCameraView";
    private static final String KEY_IS_CAPTURING = "is_capturing";

    private android.hardware.Camera mCamera;
    private ImageView mCameraImage;
    private Bitmap rotatedBitmap;
    private SurfaceView mCameraPreview;
    private SurfaceHolder surfaceHolder;
    private Button mCaptureImageButton;
    private String mCardType;
    private String mCardName;
    private String mCardOwner;
    private byte[] mCameraData;
    private boolean mIsCapturing;
    private int mImageCount = -1;

    Button doneButton;
    PicUploadwithProgressBar mUploadService;
    SingletonConfig instance = SingletonConfig.getInstance();

    private OnClickListener mCaptureImageButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            captureImage();
        }
    };

    private OnClickListener mRecaptureImageButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            setupImageCapture();
        }
    };

    private OnClickListener mDoneButtonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                if (mCameraData != null && mImageCount < 2) {
                        mUploadService.uploadPhotos(mCardType, mCardOwner, getImagePath().getAbsolutePath(), getImageKey());
                        mImageCount += 1;
                        mIsCapturing = true;
                }

                if (shouldCaptureBack(mCardType) && mImageCount == 1) {
                    setupImageCapture();
                    Toast.makeText(CustomCameraActivity.this, "Capture back of card.", Toast.LENGTH_LONG).show();
                }

                if (!shouldCaptureBack(mCardType) || mImageCount >= 2) {
                    setResult(RESULT_OK);
                    finish();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    private OnClickListener mFrameBackgroundonClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                mCamera.autoFocus(new Camera.AutoFocusCallback() {
                    @Override
                    public void onAutoFocus(boolean success, Camera camera) {
                        Log.d(TAG, "autofocus complete: " + success);
                    }
                });
            }catch (Exception e){e.printStackTrace();}
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mCardType = getIntent().getStringExtra("card_type");
        mCardName = getIntent().getStringExtra("card_name");
        mCardOwner = getIntent().getStringExtra("card_owner");
        Log.d(TAG, "card_type : " + mCardType);
        Log.d(TAG, "card_name : " + mCardName);
        Log.d(TAG, "card_owner: " + mCardOwner);

        if (mCardType == null) {
            throw new RuntimeException("card-type cannot be null");
        }

        mImageCount = shouldTakeImages(mCardType);
        mUploadService = new PicUploadwithProgressBar(getApplicationContext());

        mCameraImage = (ImageView) findViewById(R.id.camera_image_view);
        mCameraImage.setVisibility(View.INVISIBLE);

        mCameraPreview = (SurfaceView) findViewById(R.id.preview_view);
        surfaceHolder = mCameraPreview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        FrameLayout layoutBackground = (FrameLayout) findViewById(R.id.camera_frame);
        layoutBackground.setOnClickListener(mFrameBackgroundonClickListener);

        mCaptureImageButton = (Button) findViewById(R.id.capture_image_button);
        mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);

        doneButton = (Button) findViewById(R.id.done_button);
        doneButton.setOnClickListener(mDoneButtonClickListener);

        mIsCapturing = true;
    }

    private int findBackCameraID() {
        int cameraId = -1;
        // Search for the front facing camera
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                Log.d(TAG, "Camera found");
                cameraId = i;
                doneButton.setVisibility(View.VISIBLE);
                break;
            }
        }
        return cameraId;
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        try {
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break;

                case Surface.ROTATION_90:
                    degrees = 90;
                    break;

                case Surface.ROTATION_180:
                    degrees = 180;
                    break;

                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }

            int result;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360;
                result = (360 - result) % 360;
            } else {
                result = (info.orientation - degrees + 360) % 360;
            }
            camera.setDisplayOrientation(result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(KEY_IS_CAPTURING, mIsCapturing);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        try {
            mIsCapturing = savedInstanceState.getBoolean(KEY_IS_CAPTURING, mCameraData == null);
            if (mCameraData != null) {
                setupImageDisplay();
            } else {
                setupImageCapture();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    void launchCamera() {
        try {
            mCamera = android.hardware.Camera.open();
            List<Camera.Size> sizes = mCamera.getParameters().getSupportedPictureSizes();

            try {
                mCamera.stopPreview();
            } catch (Exception e) {
                // ignore: tried to stop a non-existent preview
            }
            setCameraDisplayOrientation(this, findBackCameraID(), mCamera);

            Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    Log.d(TAG, "autofocus complete: " + success);
                }
            };
            //set camera to continually auto-focus
            Camera.Parameters params = mCamera.getParameters();

            if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                //Choose another supported mode
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.autoFocus(autoFocusCallback);
            }
            if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_EDOF)) {
                //Choose another supported mode
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_EDOF);
                mCamera.autoFocus(autoFocusCallback);
            }
            if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_MACRO)) {
                //Choose another supported mode
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
                mCamera.autoFocus(autoFocusCallback);
            }
            if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                mCamera.autoFocus(autoFocusCallback);
            }

            int max = 0;
            int index = 0;
            for (int i = 0; i < sizes.size(); i++) {
                Camera.Size s = sizes.get(i);
                int size = s.height * s.width;
                if (size > max) {
                    index = i;
                    max = size;
                }
            }

            params.setPictureSize(sizes.get(index).width, sizes.get(index).height);
            mCamera.setParameters(params);
            mCamera.setPreviewDisplay(mCameraPreview.getHolder());
            if (mIsCapturing) {
                mCamera.startPreview();
            }
            mCamera.autoFocus(autoFocusCallback);

        } catch (Exception e) {
            Toast.makeText(CustomCameraActivity.this, "Unable to open camera.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera == null) {
            launchCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera != null) {
            try {
                //mCamera.setPreviewDisplay(holder);
                mCamera.setPreviewDisplay(surfaceHolder);
                if (mIsCapturing) {
                    mCamera.startPreview();
                }
            } catch (IOException e) {
                Toast.makeText(CustomCameraActivity.this, "Unable to start camera preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    private void captureImage() {
        try {
            mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupImageCapture() {
        mCameraImage.setVisibility(View.INVISIBLE);
        mCameraPreview.setVisibility(View.VISIBLE);
        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }
        setCameraDisplayOrientation(this, findBackCameraID(), mCamera);
        //set camera to continually auto-focus
        Camera.Parameters params = mCamera.getParameters();
        if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        mCamera.setParameters(params);
        try {
            mCamera.setPreviewDisplay(mCameraPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mIsCapturing) {
            try {
                mCamera.startPreview();
            }catch (Exception e){
                e.printStackTrace();}
        }
        mCaptureImageButton.setText(R.string.capture_image);
        mCaptureImageButton.setOnClickListener(mCaptureImageButtonClickListener);
    }

    private void setupImageDisplay() {
        try {
            mCameraImage.setImageBitmap(rotatedBitmap);
            mCamera.stopPreview();
            mCameraPreview.setVisibility(View.INVISIBLE);
            mCameraImage.setVisibility(View.VISIBLE);
            mCaptureImageButton.setText(R.string.recapture_image);
            mCaptureImageButton.setOnClickListener(mRecaptureImageButtonClickListener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() { // <6>
        public void onShutter() {
            Log.d(TAG, "onShutter");
        }
    };

    Camera.PictureCallback rawCallback = new Camera.PictureCallback() { // <7>
        public void onPictureTaken(byte[] data, Camera camera) {
            mCameraData = data;
        }
    };

    Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] raw, Camera camera) {
            mCameraData = raw;

            Bitmap bitmap = BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length);
            Matrix matrix = new Matrix();
            matrix.setRotate(90);

            rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            setupImageDisplay();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, Constants.BITMAP_SIZE, bos);

            byte[] data = bos.toByteArray();

            try {
                final File dirPath = getImageDir();
                dirPath.mkdirs();

                final File imageFile = getImagePath();
                FileOutputStream outStream = new FileOutputStream(imageFile);
                outStream.write(data);
                outStream.flush();
                outStream.close();

                Toast.makeText(getBaseContext(), "Preview", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onPictureTaken - wrote path : " + imageFile.getAbsolutePath());
                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length);

            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Failed to Write Image", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Failed to Write Image");
            }
        }
    };

    private File getImageDir() {
        return new File(String.format("%s/esthenos/%s", Environment.getExternalStorageDirectory(), instance.getAppId()));
    }

    private File getImagePath() {
        return new File(getImageDir(), getImageName());
    }

    private String getImageName() {
        return String.format("%s_%s.jpg", mCardName, mImageCount);
    }

    private String getImageKey() {
        return String.format("%s/%s_%s.jpg", instance.getAppId(), mCardName, mImageCount);
    }
}