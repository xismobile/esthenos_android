package com.esthenos.commons.models;

import android.provider.Settings;

import com.esthenos.commons.utils.HttpUtils;
import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.util.List;

import static com.esthenos.commons.utils.Constants.REQ_CENTERS;
import static com.esthenos.commons.utils.Constants.REQ_GROUPS_AVAILABLE;
import static com.esthenos.commons.utils.Constants.REQ_GROUPS_STATUS;
import static com.esthenos.commons.utils.Constants.REQ_QUESTIONS_CGT1;
import static com.esthenos.commons.utils.Constants.REQ_QUESTIONS_CGT2;
import static com.esthenos.commons.utils.Constants.REQ_QUESTIONS_GRT;


public class CenterGroupsModel {

    public static Request getRequest(SingletonConfig instance, String requestType) {

        String requestUrl;
        switch (requestType) {
            case REQ_CENTERS:
                requestUrl = instance.getCenters();
                break;

            case REQ_GROUPS_STATUS :
                requestUrl = instance.getCenterGroupsStatus();
                break;

            case REQ_GROUPS_AVAILABLE :
                requestUrl = instance.getCenterGroupsAvailable();
                break;

            case REQ_QUESTIONS_GRT :
                requestUrl = instance.getCGTReadyGroups(REQ_QUESTIONS_GRT);
                break;

            case REQ_QUESTIONS_CGT1 :
                requestUrl = instance.getCGTReadyGroups(REQ_QUESTIONS_CGT1);
                break;

            case REQ_QUESTIONS_CGT2 :
                requestUrl = instance.getCGTReadyGroups(REQ_QUESTIONS_CGT2);
                break;

            default:
                throw new RuntimeException("invalid request type: " + requestType);
        }

        return new Request.Builder()
                .url(requestUrl)
                .build();
    }

    public static Request postRequest(SingletonConfig instance) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String requestUrl = instance.getCenterDetails();
        RequestBody body = RequestBody.create(JSON, HttpUtils.toJson(instance.getCenter().timeslot));

        return new Request.Builder()
                .url(requestUrl)
                .post(body)
                .build();
    }

    public static Request postUpdateTimeRequest(SingletonConfig instance,CenterTime grtcentertime) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String requestUrl = instance.getCenterDetails();
        RequestBody body = RequestBody.create(JSON, HttpUtils.toJson(grtcentertime));

        return new Request.Builder()
                .url(requestUrl)
                .post(body)
                .build();
    }

    public static Request postGroupDetails(SingletonConfig instance,
                                           String groupSize,
                                           String groupLeaderName,
                                           String groupLeaderNumber,
                                           String groupUpdate) {

        RequestBody formBody = new FormEncodingBuilder()
                .add("size", groupSize)
                .add("name", groupLeaderName)
                .add("number", groupLeaderNumber)
                .add("group_name",groupUpdate)
                .add("product_id", instance.getProduct().product_id)
                .build();

        return new Request.Builder()
                .url(instance.getGroupDetails())
                .post(formBody)
                .build();
    }

    public static Request postUpdatedGroupName(SingletonConfig instance,
                                                 String groupName){
        RequestBody formBody = new FormEncodingBuilder()
                .add("group_name", groupName)
                .build();

        return new Request.Builder()
                .url(instance.getGroupDetails())
                .post(formBody)
                .build();
    }

    public static Request postUpdatedCenterName(SingletonConfig instance, CenterChange centerChange) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        String requestUrl = instance.getCenterDetails();
        RequestBody body = RequestBody.create(JSON, HttpUtils.toJson(centerChange));

        return new Request.Builder()
                .url(requestUrl)
                .post(body)
                .build();
    }

    public static class CenterChange {
        public String name;
    }

    public class CGTStatus {
        public float score;
        public String state;
    }

    public class GroupCGT {
        public CGTStatus grt;
        public CGTStatus cgt1;
        public CGTStatus cgt2;
    }

    public class Group {
        public String id;
        public String group_id;
        public String name;
        public int size_min;
        public int size_max;
        public int size_group;
        public int size_members;
        public boolean complete;
        public String current_status;
        public String current_status_updated;
        public GroupCGT status;

        @Override
        public String toString() {
            return name;
        }
    }

    public class Groups {
        public int count;
        public Center center;
        public List<Group> groups;
    }


    public static class CenterTime {
        public String day;
        public String time;
        public CenterTime(String d) { day = d; }
    }

    public class Center {
        public String id;
        public String name;
        public String center_id;
        public CenterTime timeslot;

        @Override
        public String toString() {
            return name;
        }
    }

    public class Centers {
        public int count;
        public List<Center> centers;
    }
}
