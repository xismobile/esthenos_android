package com.esthenos.commons.models;

import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import java.util.List;


public class ApplicationModel {

    public static Request postApplicationForm(SingletonConfig instance, String appData) {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, appData);

        return new Request.Builder()
                .url(instance.getGroupApplications(""))
                .post(body)
                .build();
    }

    public static Request getRequest(SingletonConfig instance,String requestType ) {
        return new Request.Builder()
                .url(instance.getGroupApplications(requestType))
                .build();
    }

    public class Application {
        public String id;
        public String group;
        public String center;
        public String upload_type;
        public String date_created;
        public String current_status;
        public String applicant_name;
        public String pass_fail;
        public String product_double;
        public String product_name;
        public String product_id;
        public String loan;
        public int status;
        public String customer_id;
        public String loan_total;
        public String loan_default;
        public String loan_total_status;
        public double loan_eligibility_based_on_net_income;
        public double loan_eligibility_based_on_company_policy;
    }

    public class ApplicationResponse {
        public int group_size;
        public int group_members;
        public String group_id;
        public String group_name;
        public String current_status;
        public String current_status_updated;
        public List<Application> applications;
        public List<ProductsModel.Product> products;
    }

    public class CGTApplicant{
        public int count;
        public String applicant_id;
        public String applicant_name;
        public String passfailstatus;
        public List<Application> applinactlist;
    }
}
