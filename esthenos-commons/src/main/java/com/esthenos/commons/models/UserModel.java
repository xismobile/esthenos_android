package com.esthenos.commons.models;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

public class UserModel {

    public static Request getLoginForm(String domain, String email, String password) {
        RequestBody formBody = new FormEncodingBuilder()
                .add("email", email)
                .add("password", password)
                .build();

        return new Request.Builder()
                .url("http://" + domain + "/api/token/sourcing")
                .post(formBody)
                .build();
    }

    public class User {
        public String id;
        public String role;
        public String email;
        public String token;
        public String bucket;
        public String poolId;
        public String message;
    }
}
