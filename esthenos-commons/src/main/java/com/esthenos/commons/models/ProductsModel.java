package com.esthenos.commons.models;


import com.esthenos.commons.utils.SingletonConfig;
import com.squareup.okhttp.Request;

import java.util.List;

public class ProductsModel {

    public static Request getRequest(SingletonConfig instance) {
        return new Request.Builder()
                .url(instance.getProducts())
                .build();
    }

    public class Product {
        public String emi;
        public String last_emi;
        public String loan_amount;
        public String group_min;
        public String group_max;
        public String product_id;
        public String product_name;
        public String interest_rate;
        public String emi_repayment;
        public String life_insurance;
        public String eligible_cycle;
        public String processing_fee;
        public String insurance_period;
        public String number_installments;
        public String total_processing_fees;

        @Override
        public String toString() {
            return product_name;
        }
    }

    public class ProductsResponse {
        public List<Product> products;
    }
}
