package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class BazaarDetailsPage extends Page {

    public static final String BAZAR_NAME = "Name of Bazaar/Enterprise";
    public static final String BAZAR_ADDRESS = "Address of Bazaar/Enterprise";
    public static final String BAZAR_STALL = "Stall No";
    public static final String BAZAR_YEAR = "Years of Business in Bazaar";
    public static final String BAZAR_COMMODITIES = "Commodities Sold";
    public static final String BAZAR_BUSINESS_PROOF = "Business Proof";
    public static final String BAZAR_TYPE_OF_PROOF = "Type of Proof";
    public static final String BAZAR_UNIQUE = "Unique ID Proof of Bazaar";


    public BazaarDetailsPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return BazaarDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(BAZAR_NAME,mData.getString(BAZAR_NAME),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_ADDRESS,mData.getString(BAZAR_ADDRESS),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_STALL,mData.getString(BAZAR_STALL),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_YEAR,mData.getString(BAZAR_YEAR),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_COMMODITIES,mData.getString(BAZAR_COMMODITIES),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_BUSINESS_PROOF,mData.getString(BAZAR_BUSINESS_PROOF),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_TYPE_OF_PROOF,mData.getString(BAZAR_TYPE_OF_PROOF),getPageId(),-1));
        dest.add(new ReviewItem(BAZAR_UNIQUE,mData.getString(BAZAR_UNIQUE),getPageId(),-1));

    }
   /* @Override
    public boolean isCompleted(){
        return !TextUtils.isEmpty(mData.getString(BAZAR_NAME)) &&  !TextUtils.isEmpty(mData.getString(BAZAR_ADDRESS))&&
                !TextUtils.isEmpty(mData.getString(BAZAR_YEAR))&& !TextUtils.isEmpty(mData.getString(BAZAR_COMMODITIES))&&
                !TextUtils.isEmpty(mData.getString(BAZAR_BUSINESS_PROOF))&& !TextUtils.isEmpty(mData.getString(BAZAR_TYPE_OF_PROOF))&&
                !TextUtils.isEmpty(mData.getString(BAZAR_UNIQUE));
    }*/
}
