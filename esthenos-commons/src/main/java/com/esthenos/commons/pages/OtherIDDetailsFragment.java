package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;

public class OtherIDDetailsFragment extends PageFragment<OtherIDDetailsPage> {

    EditText mPancard, mRationCard, mDriving, mPassport, mVoterId;

    public static OtherIDDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        OtherIDDetailsFragment fragment = new OtherIDDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_other_id_details, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mPancard = (EditText) rootView.findViewById(R.id.panid);
        mPage.getData().putString(OtherIDDetailsPage.PAN_CARD, mPancard.getText().toString());

        mRationCard = (EditText) rootView.findViewById(R.id.rationcardid);
        mPage.getData().putString(OtherIDDetailsPage.RATION_CARD, mRationCard.getText().toString());

        mDriving = (EditText) rootView.findViewById(R.id.drivingno);
        mPage.getData().putString(OtherIDDetailsPage.DRIVING_LICENSE, mDriving.getText().toString());

        mVoterId = (EditText) rootView.findViewById(R.id.voterid);
        mPage.getData().putString(OtherIDDetailsPage.VOTER_ID, mVoterId.getText().toString());

        mPassport = (EditText) rootView.findViewById(R.id.passport_id);
        mPage.getData().putString(OtherIDDetailsPage.PASSPORT, mPassport.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPancard.addTextChangedListener(getTextWatcher(OtherIDDetailsPage.PAN_CARD));
        mRationCard.addTextChangedListener(getTextWatcher(OtherIDDetailsPage.RATION_CARD));
        mDriving.addTextChangedListener(getTextWatcher(OtherIDDetailsPage.DRIVING_LICENSE));
        mVoterId.addTextChangedListener(getTextWatcher(OtherIDDetailsPage.VOTER_ID));
        mPassport.addTextChangedListener(getTextWatcher(OtherIDDetailsPage.PASSPORT));
    }
}
