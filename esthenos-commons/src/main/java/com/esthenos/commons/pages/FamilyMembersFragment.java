package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;


public class FamilyMembersFragment extends PageFragment<FamilyMembersPage> {


    public static final String TABLE_FAMILY_MEMBERDETAILS = "FamilyMamberDetails";
    Autosave_Data autosave_data = new Autosave_Data();
    @NotEmpty
    EditText memberCount, memberAbove18, memberMaleCount, memberFemaleCount, memberEarning, memberBelow18;
    MySQLiteDB mydb;
    AutoSaveData autosave;
    String getshared;
    long i;

    public static FamilyMembersFragment create(String key) {

        FamilyMembersFragment fragment = new FamilyMembersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    /*    Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (FamilyMembersPage) mCallbacks.onGetPage(mKey);
*///......................
        mydb = new MySQLiteDB(getContext());
        autosave = new AutoSaveData(getContext());

//........................
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.borrowers_family_details, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
//....................................

        String S_memberCount = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.FAMILY_MEMBERS_COUNT);
        String S_memberMaleCount = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_MALE_COUNT);
        String S_memberFemaleCount = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_FEMALE_COUNT);
        String S_memberBelow18 = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_LESS_THAN_18);
        String S_memberAbove18 = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_MORE_THAN_18);
        String S_memberEarning = autosave_data.getdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_EARNING_COUNT);


        memberCount = (EditText) rootView.findViewById(R.id.family_members_count);
        mPage.getData().putString(FamilyMembersPage.FAMILY_MEMBERS_COUNT, memberCount.getText().toString());


        memberAbove18 = (EditText) rootView.findViewById(R.id.more_than_18);
        mPage.getData().putString(FamilyMembersPage.MEMBER_LESS_THAN_18, memberAbove18.getText().toString());



        memberBelow18 = (EditText) rootView.findViewById(R.id.less_than_18);
        mPage.getData().putString(FamilyMembersPage.MEMBER_LESS_THAN_18, memberBelow18.getText().toString());



        memberMaleCount = (EditText) rootView.findViewById(R.id.male_count);
        mPage.getData().putString(FamilyMembersPage.MEMBER_MALE_COUNT, memberMaleCount.getText().toString());



        memberFemaleCount = (EditText) rootView.findViewById(R.id.female_count);
        mPage.getData().putString(FamilyMembersPage.MEMBER_FEMALE_COUNT, memberFemaleCount.getText().toString());



        memberEarning = (EditText) rootView.findViewById(R.id.earning_members);
        mPage.getData().putString(FamilyMembersPage.MEMBER_EARNING_COUNT, memberEarning.getText().toString());



        memberCount.addTextChangedListener(new GenericTextW(memberCount));
        memberEarning.addTextChangedListener(new GenericTextW(memberEarning));
        memberAbove18.addTextChangedListener(new GenericTextW(memberAbove18));
        memberBelow18.addTextChangedListener(new GenericTextW(memberBelow18));
        memberMaleCount.addTextChangedListener(new GenericTextW(memberMaleCount));
        memberFemaleCount.addTextChangedListener(new GenericTextW(memberFemaleCount));

        if (!S_memberCount.equals("nulldata")) {
            memberCount.setText(S_memberCount);
        }

        if (!S_memberEarning.equals("nulldata")) {
            memberEarning.setText(S_memberEarning);
        }
        if (!S_memberAbove18.equals("nulldata")) {
            memberAbove18.setText(S_memberAbove18);
        }
        if (!S_memberBelow18.equals("nulldata")) {
            memberBelow18.setText(S_memberBelow18);
        }
        if (!S_memberMaleCount.equals("nulldata")) {
            memberMaleCount.setText(S_memberMaleCount);
        }
        if (!S_memberFemaleCount.equals("nulldata")) {
            memberFemaleCount.setText(S_memberFemaleCount);
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       /* memberFemaleCount.addTextChangedListener(getTextWatcher(FamilyMembersPage.MEMBER_FEMALE_COUNT));
        memberEarning.addTextChangedListener(getTextWatcher(FamilyMembersPage.MEMBER_EARNING_COUNT));
        memberCount.addTextChangedListener(getTextWatcher(FamilyMembersPage.FAMILY_MEMBERS_COUNT));
        memberAbove18.addTextChangedListener(getTextWatcher(FamilyMembersPage.MEMBER_MORE_THAN_18));
        memberBelow18.addTextChangedListener(getTextWatcher(FamilyMembersPage.MEMBER_LESS_THAN_18));
        memberMaleCount.addTextChangedListener(getTextWatcher(FamilyMembersPage.MEMBER_MALE_COUNT));*/

    }

    class GenericTextW implements TextWatcher {

        private View view;

        private GenericTextW(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
           // Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.family_members_count) {

                memberCount = (EditText) view.findViewById(R.id.family_members_count);
                if (TextUtils.isEmpty(text)) {

                    // memberCount.setError("* Please Enter Name (Mandatory)");
                } else {


                    if (text != null && editable.length() > 0) {

                        autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.FAMILY_MEMBERS_COUNT, text);


                        mPage.getData().putString(FamilyMembersPage.FAMILY_MEMBERS_COUNT, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

            }

            if (i1 == R.id.male_count) {

                memberMaleCount = (EditText) view.findViewById(R.id.male_count);
                if (TextUtils.isEmpty(text)) {

                    // memberMaleCount.setError("* Please Enter age (Mandatory)");

                } else {

                    autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_MALE_COUNT, text);

                    if (editable != null && editable.length() > 0) {

                        mPage.getData().putString(FamilyMembersPage.MEMBER_MALE_COUNT, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
            if (i1 == R.id.female_count) {

                memberFemaleCount = (EditText) view.findViewById(R.id.female_count);
                if (TextUtils.isEmpty(text)) {


                    //memberFemaleCount.setError("* Please Enter Years of Involvement (Mandatory)");

                } else {

                    autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_FEMALE_COUNT, text);

                    if (editable != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyMembersPage.MEMBER_FEMALE_COUNT, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
            if (i1 == R.id.less_than_18) {

                memberBelow18 = (EditText) view.findViewById(R.id.less_than_18);
                if (TextUtils.isEmpty(text)) {

                    //memberBelow18.setError("* Please Enter Monthly Income (Mandatory)");

                } else {


                    autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_LESS_THAN_18, text);

                    if (editable != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyMembersPage.MEMBER_LESS_THAN_18, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
            if (i1 == R.id.more_than_18) {

                memberAbove18 = (EditText) view.findViewById(R.id.more_than_18);
                if (TextUtils.isEmpty(text)) {

                    //memberAbove18.setError("* Please Enter Monthly Income (Mandatory)");

                } else {

                    autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_MORE_THAN_18, text);

                    if (editable != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyMembersPage.MEMBER_MORE_THAN_18, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
            if (i1 == R.id.earning_members) {

                memberEarning = (EditText) view.findViewById(R.id.earning_members);
                if (TextUtils.isEmpty(text)) {

                    //memberEarning.setError("* Please Enter Monthly Income (Mandatory)");

                } else {


                    autosave_data.putdata(getContext(), "FamilyMembersFragment", FamilyMembersPage.MEMBER_EARNING_COUNT, text);

                    if (editable != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyMembersPage.MEMBER_EARNING_COUNT, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            }


        }

    }

}
