package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class GuaranteesForBrrwrPage extends Page {
    public static final String GUARANTER_FOR_BORROWER = "Name Of Borrower";
    public static final String GUARANTER_LOAN_AMOUNT = "Loan Amount";
    public static final String GUARANTER_BANK_SOCIETY = "Bank/Credit-Co-perative Society";

    public GuaranteesForBrrwrPage(ModelCallbacks callbacks ,PageConfig pageConfig) {
        super(callbacks,pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return GuaranteesForBrrwrFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(GUARANTER_FOR_BORROWER, mData.getString(GUARANTER_FOR_BORROWER), getPageId(), -1));
        dest.add(new ReviewItem(GUARANTER_LOAN_AMOUNT, mData.getString(GUARANTER_LOAN_AMOUNT), getPageId(), -1));
        dest.add(new ReviewItem(GUARANTER_BANK_SOCIETY, mData.getString(GUARANTER_BANK_SOCIETY), getPageId(), -1));
    }
}


