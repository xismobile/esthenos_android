package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class GuarantorsInformationPage extends Page{

    private static final String[] marital_status = {"", "Married", "Unmarried", "Divorced", "Widow", "Widower", "Separated"};
    public static final String EMAIL_ID_KEY = "Email ID";
    public static final String OCCUPATION_KEY = "Occupation";
    public static final String FATHER_SUBTITLE_KEY = "Father's/Husband's SubTitle";
    public static final String PERMANENT_ADDRESS_KEY = "Permanent Address";
    public static final String SINGLE_GENDER_KEY = "Gender";
    public static final String UID_KEY = "UID";
    public static final String AGE_KEY = "Age";
    public static final String YOB_KEY = "DOB-YOB";
    public static final String CARD_KEY = "Type";
    public static final String NAME_KEY = "Name";
    public static final String NAME_SUBTITLE_KEY = "Name SubTitle";
    public static final String FATHER_HUSBAND_SUBTITLE_KEY = "Father's/Husband's SubTitle";
    public static final String SPOUSE_NAME_SUBTITLE_KEY = "Spouse SubTitle";
    public static final String MOTHER_NAME_SUBTITLE_KEY = "Mother SubTitle";

    public static final String TALUK_KEY = "Taluk";
    public static final String DISTRICT_KEY = "District";
    public static final String STATE_KEY = "State";
    public static final String COUNTRY_KEY = "Country";
    public static final String ADDRESS_KEY = "Address";
    public static final String PINCODE_KEY = "Pincode";
    public static final String TELE_PHONE_KEY = "Phone Number";
    public static final String MOBILE_PHONE_KEY = "Mobile Number";
    public static final String FATHER_OR_HUSBAND_NAME = "Father's Husband's Name";
    public static final String MOTHER_KEY = "Mother's Name";
    public static final String SPOUSE_KEY = "Spouse Name";
    public static final String LANDMARKS_KEY = "Landmark";
    public static final String NICK_NAME_KEY = "Nick Name";
    public static final String POLICE_KEY = "Police Station";
    public static final String POST_OFFICE_KEY = "Post Office";

    public static final String VOTER_ID_KEY = "Voter ID";
    public static final String VOTER_ID_NAME_KEY = "Voter ID Name";
    public static final String VOTER_ID_FATHER_OR_HUSBAND_NAME = "Voter ID Father's Husband's Name";

    public static final String PANCARD_KEY = "PAN Card ID";
    public static final String PANCARD_NAME_KEY = "PAN Card Name";
    public static final String PANCARD_FATHER_OR_HUSBAND_NAME = "PAN Card Father's Husband's Name";

    public static final String RATIONCARD_KEY = "Ration Card No";
    public static final String CENTERMEETING_KEY = "Center Meeting Place";

    public static final String SPOUSE_DOB_KEY = "Spouse DOB";
    public static final String SPOUSE_AGE_KEY = "Spouse Age";

    public static final String SINGLE_MARITAL_STATUS = "Marital Status";

    public GuarantorsInformationPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return GuarantorsInformationFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(SINGLE_GENDER_KEY, mData.getString(SINGLE_GENDER_KEY), getPageId(), -1));
        dest.add(new ReviewItem(UID_KEY, mData.getString(UID_KEY), getPageId(), -1));
        dest.add(new ReviewItem(NAME_KEY, mData.getString(NAME_SUBTITLE_KEY)+" "+mData.getString(NAME_KEY), getPageId(), -1));
        dest.add(new ReviewItem(FATHER_OR_HUSBAND_NAME, mData.getString(FATHER_HUSBAND_SUBTITLE_KEY)+" "+mData.getString(FATHER_OR_HUSBAND_NAME), getPageId(), -1));
        dest.add(new ReviewItem(AGE_KEY, mData.getString(AGE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(YOB_KEY, mData.getString(YOB_KEY), getPageId(), -1));
        dest.add(new ReviewItem(CARD_KEY, "AADHAAR", getPageId(), -1));
        dest.add(new ReviewItem(ADDRESS_KEY, mData.getString(ADDRESS_KEY), getPageId(), -1));
        dest.add(new ReviewItem(TALUK_KEY, mData.getString(TALUK_KEY), getPageId(), -1));
        dest.add(new ReviewItem(DISTRICT_KEY, mData.getString(DISTRICT_KEY), getPageId(), -1));
        dest.add(new ReviewItem(STATE_KEY, mData.getString(STATE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(PERMANENT_ADDRESS_KEY, mData.getString(PERMANENT_ADDRESS_KEY), getPageId(), -1));
        dest.add(new ReviewItem(PINCODE_KEY, mData.getString(PINCODE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(OCCUPATION_KEY, mData.getString(OCCUPATION_KEY), getPageId(), -1));
        dest.add(new ReviewItem(COUNTRY_KEY, mData.getString(COUNTRY_KEY), getPageId(), -1));
        dest.add(new ReviewItem(TELE_PHONE_KEY, mData.getString(TELE_PHONE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(MOBILE_PHONE_KEY, mData.getString(MOBILE_PHONE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EMAIL_ID_KEY, mData.getString(EMAIL_ID_KEY), getPageId(), -1));
        dest.add(new ReviewItem(VOTER_ID_KEY, mData.getString(VOTER_ID_KEY), getPageId(), -1));
        dest.add(new ReviewItem(VOTER_ID_NAME_KEY, mData.getString(VOTER_ID_NAME_KEY), getPageId(), -1));
        dest.add(new ReviewItem(VOTER_ID_FATHER_OR_HUSBAND_NAME, mData.getString(VOTER_ID_FATHER_OR_HUSBAND_NAME), getPageId(), -1));

        dest.add(new ReviewItem(LANDMARKS_KEY, mData.getString(LANDMARKS_KEY), getPageId(), -1));
        dest.add(new ReviewItem(NICK_NAME_KEY, mData.getString(NICK_NAME_KEY), getPageId(), -1));
        dest.add(new ReviewItem(POLICE_KEY, mData.getString(POLICE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(POST_OFFICE_KEY, mData.getString(POST_OFFICE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(MOTHER_KEY, mData.getString(MOTHER_NAME_SUBTITLE_KEY)+" "+mData.getString(MOTHER_KEY), getPageId(), -1));
        dest.add(new ReviewItem(SPOUSE_KEY, mData.getString(SPOUSE_NAME_SUBTITLE_KEY)+" "+mData.getString(SPOUSE_KEY), getPageId(), -1));

        dest.add(new ReviewItem(PANCARD_KEY, mData.getString(PANCARD_KEY), getPageId(), -1));
        dest.add(new ReviewItem(PANCARD_NAME_KEY, mData.getString(PANCARD_NAME_KEY), getPageId(), -1));
        dest.add(new ReviewItem(PANCARD_FATHER_OR_HUSBAND_NAME, mData.getString(PANCARD_FATHER_OR_HUSBAND_NAME), getPageId(), -1));

        dest.add(new ReviewItem(RATIONCARD_KEY,mData.getString(RATIONCARD_KEY),getPageId(),-1));
        dest.add(new ReviewItem(CENTERMEETING_KEY,mData.getString(CENTERMEETING_KEY),getPageId(),-1));

        dest.add(new ReviewItem(SPOUSE_AGE_KEY,mData.getString(SPOUSE_AGE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(SPOUSE_DOB_KEY,mData.getString(SPOUSE_DOB_KEY),getPageId(),-1));

        dest.add(new ReviewItem(SINGLE_MARITAL_STATUS, mData.getString(SINGLE_MARITAL_STATUS), getPageId(), -1));
    }

 /*   @Override
    public boolean isCompleted(){
        return  !TextUtils.isEmpty(mData.getString(NAME_KEY)) && !TextUtils.isEmpty(mData.getString(YOB_KEY))&&
                !TextUtils.isEmpty(mData.getString(FATHER_OR_HUSBAND_NAME)) && !TextUtils.isEmpty(mData.getString(SINGLE_GENDER_KEY))&&
                !TextUtils.isEmpty(mData.getString(ADDRESS_KEY))&& !TextUtils.isEmpty(mData.getString(TALUK_KEY)) &&
                !TextUtils.isEmpty(mData.getString(DISTRICT_KEY)) && !TextUtils.isEmpty(mData.getString(STATE_KEY)) &&
                !TextUtils.isEmpty(mData.getString(PERMANENT_ADDRESS_KEY)) && !TextUtils.isEmpty(mData.getString(COUNTRY_KEY))&&
                !TextUtils.isEmpty(mData.getString(PINCODE_KEY)) && !TextUtils.isEmpty(mData.getString(POST_OFFICE_KEY)) &&
                !TextUtils.isEmpty(mData.getString(OCCUPATION_KEY)) && !TextUtils.isEmpty(mData.getString(MOBILE_PHONE_KEY)) &&
                (!TextUtils.isEmpty(mData.getString(VOTER_ID_KEY)) || !TextUtils.isEmpty(mData.getString(RATIONCARD_KEY)) || !TextUtils.isEmpty(mData.getString(UID_KEY)))&&
                mData.getString(MOBILE_PHONE_KEY).length()==10&& !TextUtils.isEmpty(mData.getString(MOTHER_KEY)) &&
                !TextUtils.isEmpty(mData.getString(VOTER_ID_NAME_KEY)) && !TextUtils.isEmpty(mData.getString(VOTER_ID_FATHER_OR_HUSBAND_NAME)) &&
                !TextUtils.isEmpty(mData.getString(CENTERMEETING_KEY)); //&& if (mData.getString(SINGLE_MARITAL_STATUS)==marital_status[1]){ };


    }*/
   @Override
   public boolean isCompleted() {
       return !TextUtils.isEmpty(mData.getString(SINGLE_GENDER_KEY));
   }
}