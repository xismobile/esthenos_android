package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class ProgressOutPropertyFirstPage extends Page {


    public static final String KEY_PROGRESS_YEAR = "How many household members are 17-years old or Younger ?";
    public static final String KEY_PROGRESS_EDUCATION = "What is the general education level of the male head/spouse ?";
    public static final String KEY_PROGRESS_HOUSR_TYPE = "What is the household type ?";
    public static final String KEY_PROGRESS_ENERGY = "What is the primary source of energy for cooking ?";
    public static final String KEY_PROGRESS_THERMOS = "Does the hoiusehold possess any casseroles thermos of thermoware ?";

    public ProgressOutPropertyFirstPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return ProgressOutPropertyFirstFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_PROGRESS_YEAR,mData.getString(KEY_PROGRESS_YEAR),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_EDUCATION,mData.getString(KEY_PROGRESS_EDUCATION),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_HOUSR_TYPE,mData.getString(KEY_PROGRESS_HOUSR_TYPE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_ENERGY,mData.getString(KEY_PROGRESS_ENERGY),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_THERMOS,mData.getString(KEY_PROGRESS_THERMOS),getPageId(),-1));
    }
}
