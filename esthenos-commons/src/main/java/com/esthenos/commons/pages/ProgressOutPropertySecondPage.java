package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class ProgressOutPropertySecondPage extends Page {

    public static final String KEY_PROGRESS_TELEVISION = "Does the household possess a television and a VCR/VCD/DVD player ?";
    public static final String KEY_PROGRESS_MOBILE = "Does the household possess a mobile handset and a telephone instrument(landline) ?";
    public static final String KEY_PROGRESS_SEWING = "Does the household possess a sewing machine ?";
    public static final String KEY_PROGRESS_ALMIRAH = "Does the household possess an almirah/dressing table ?";
    public static final String KEY_PROGRESS_BICYCLE = "Does the household possess a bicycle, or motor car/jeep ?";


    public ProgressOutPropertySecondPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return ProgressOutPropertySecondFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_PROGRESS_TELEVISION,mData.getString(KEY_PROGRESS_TELEVISION),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_MOBILE,mData.getString(KEY_PROGRESS_MOBILE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_SEWING,mData.getString(KEY_PROGRESS_SEWING),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_ALMIRAH,mData.getString(KEY_PROGRESS_ALMIRAH),getPageId(),-1));
        dest.add(new ReviewItem(KEY_PROGRESS_BICYCLE,mData.getString(KEY_PROGRESS_BICYCLE),getPageId(),-1));
    }
}
