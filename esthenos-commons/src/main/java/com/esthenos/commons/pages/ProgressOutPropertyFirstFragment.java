package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class ProgressOutPropertyFirstFragment extends PageFragment<ProgressOutPropertyFirstPage> {

    Spinner mYear,mEducation,mType,mEnergy,mThemos;
    String[] valueYear = {" ","0","1","2","3","4 or More"};
    String[] valueEducation = {" ","No Male Head/Spouse","Not Literate, No Formal School,or Primary or Below"
            ,"Middle","Secondary or Higher Secondary","Diploma/Certificate Course,Graduate or Postgraduate and Above"};
    String[] valueType = {" ","Labour(Agricultural,Casual,or Other)",
            "Self-Employed(Agriculture or Non-Agriculture),Regular-Wage/Salary-Earning, or Others"};
    String[] valueEnergy = {" ","Firewood and Chips, Dung Cake,Kerosene,Charcoal,Coke or Coal, Gobar Gas,or Other"
            ,"LPG or Electricity","No Cooking Arrangement"};
    String[] valueThemos = {" ","No","Yes"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static ProgressOutPropertyFirstFragment create(String key) {
        ProgressOutPropertyFirstFragment fragment = new ProgressOutPropertyFirstFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_progress_out_property_first, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_progressoutPropertyfirstyear=autosave_data.getdata(getContext(), "ProgressOutPropertyFirstFragment",ProgressOutPropertyFirstPage.KEY_PROGRESS_YEAR);
        String s_progressoutPropertyfirsteducaion=autosave_data.getdata(getContext(), "ProgressOutPropertyFirstFragment",ProgressOutPropertyFirstPage.KEY_PROGRESS_EDUCATION);
        String s_progressoutPropertyfirsttype=autosave_data.getdata(getContext(), "ProgressOutPropertyFirstFragment",ProgressOutPropertyFirstPage.KEY_PROGRESS_HOUSR_TYPE);
        String s_progressoutPropertyfirstenergy=autosave_data.getdata(getContext(), "ProgressOutPropertyFirstFragment",ProgressOutPropertyFirstPage.KEY_PROGRESS_ENERGY);
        String s_progressoutPropertyfirstthemos=autosave_data.getdata(getContext(), "ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_THERMOS);

        mYear=(Spinner)rootView.findViewById(R.id.progress_year);
        ArrayAdapter yearAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueYear);
        yearAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mYear.setAdapter(yearAdapter);
        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_YEAR,mYear.getSelectedItem().toString());
        if (!s_progressoutPropertyfirstyear.equals("nulldata")){
            mYear.setSelection(yearAdapter.getPosition(s_progressoutPropertyfirstyear));
        }

        mEducation=(Spinner)rootView.findViewById(R.id.progress_general);
        ArrayAdapter educationAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueEducation);
        educationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEducation.setAdapter(educationAdapter);
        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_EDUCATION,mEducation.getSelectedItem().toString());
        if (!s_progressoutPropertyfirsteducaion.equals("nulldata")){
            mEducation.setSelection(educationAdapter.getPosition(s_progressoutPropertyfirsteducaion));
        }

        mType=(Spinner)rootView.findViewById(R.id.progress_h_type);
        ArrayAdapter typeAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueType);
        typeAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mType.setAdapter(typeAdapter);
        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_HOUSR_TYPE,mType.getSelectedItem().toString());
        if (!s_progressoutPropertyfirsttype.equals("nulldata")){
            mType.setSelection(typeAdapter.getPosition(s_progressoutPropertyfirsttype));
        }

        mEnergy=(Spinner)rootView.findViewById(R.id.progress_primary_source);
        ArrayAdapter energyAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueEnergy);
        energyAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEnergy.setAdapter(energyAdapter);
        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_ENERGY,mEnergy.getSelectedItem().toString());
        if (!s_progressoutPropertyfirstenergy.equals("nulldata")){
            mEnergy.setSelection(energyAdapter.getPosition(s_progressoutPropertyfirstenergy));
        }

        mThemos=(Spinner)rootView.findViewById(R.id.progress_themos);
        ArrayAdapter themeAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueThemos);
        themeAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mThemos.setAdapter(themeAdapter);
        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_THERMOS,mThemos.getSelectedItem().toString());
        if (!s_progressoutPropertyfirstthemos.equals("nulldata")){
            mThemos.setSelection(themeAdapter.getPosition(s_progressoutPropertyfirstthemos));
        }


        mYear.setOnItemSelectedListener(getOnItem());
        mEducation.setOnItemSelectedListener(getOnItem());
        mType.setOnItemSelectedListener(getOnItem());
        mEnergy.setOnItemSelectedListener(getOnItem());
        mThemos.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

//        mYear.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertyFirstPage.KEY_PROGRESS_YEAR));
//        mEducation.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertyFirstPage.KEY_PROGRESS_EDUCATION));
//        mType.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertyFirstPage.KEY_PROGRESS_HOUSR_TYPE));
//        mEnergy.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertyFirstPage.KEY_PROGRESS_ENERGY));
//        mThemos.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertyFirstPage.KEY_PROGRESS_THERMOS));

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.progress_year) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),"ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_YEAR, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_YEAR, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_general) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_EDUCATION, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_h_type) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_HOUSR_TYPE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_HOUSR_TYPE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_primary_source) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_ENERGY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_ENERGY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_themos) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertyFirstFragment", ProgressOutPropertyFirstPage.KEY_PROGRESS_THERMOS, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertyFirstPage.KEY_PROGRESS_THERMOS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

}
