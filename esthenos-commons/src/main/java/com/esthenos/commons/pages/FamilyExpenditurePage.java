package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class FamilyExpenditurePage extends Page {

    public static final String EXPENDITURE_FOOD_KEY = "Family Food Expenditure (Monthly)";
    public static final String EXPENDITURE_TRAVEL_KEY = "Family Travel Expenditure (Monthly)";
    public static final String EXPENDITURE_ENTERTAINMENT_KEY = "Family Entertainment Expenditure (Monthly)";
    public static final String EXPENDITURE_FESTIVAL_KEY = "Family Festival Expenditure (Monthly)";
    public static final String EXPENDITURE_MEDICAL_KEY = "Family Medical Expenditure (Monthly)";
    public static final String EXPENDITURE_EDUCATION_KEY = "Family Education Expenditure (Monthly)";
    public static final String EXPENDITURE_OTHER_KEY = "Family Other Expenditure (Monthly)";

    public FamilyExpenditurePage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FamilyExpenditureFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(EXPENDITURE_FOOD_KEY,mData.getString(EXPENDITURE_FOOD_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_TRAVEL_KEY,mData.getString(EXPENDITURE_TRAVEL_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_ENTERTAINMENT_KEY,mData.getString(EXPENDITURE_ENTERTAINMENT_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_FESTIVAL_KEY,mData.getString(EXPENDITURE_FESTIVAL_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_MEDICAL_KEY,mData.getString(EXPENDITURE_MEDICAL_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_EDUCATION_KEY,mData.getString(EXPENDITURE_EDUCATION_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EXPENDITURE_OTHER_KEY,mData.getString(EXPENDITURE_OTHER_KEY), getPageId(), -1));
    }
//    @Override
//    public boolean isCompleted(){
//        return !TextUtils.isEmpty(mData.getString(EXPENDITURE_FOOD_KEY)) && !TextUtils.isEmpty(mData.getString(EXPENDITURE_MEDICAL_KEY));
//    }
}
