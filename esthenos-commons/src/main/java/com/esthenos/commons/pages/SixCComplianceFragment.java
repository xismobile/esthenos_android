package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class SixCComplianceFragment extends PageFragment<SixCCompliancePage> {

    Spinner mKnowMember,mAgeBetween,mRelative,mCusEligible,mCusHerself,mCusWritten;

    String[] spinnerValue={"","NO","YES"};

    Autosave_Data autosave_data =new Autosave_Data();
    public static SixCComplianceFragment create(String key) {
        SixCComplianceFragment fragment = new SixCComplianceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_six_ccompliance, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_SixCComplianceKnowNumber=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_KNOW_MEMBERS);
        String s_SixCComplianceAgeBet=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_AGE_BETWEEN);
        String s_SixCComplianceRelative=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_RELATIVE);
        String s_SixCComplianceCusEligible=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_CUS_ELIGIBLE);
        String s_SixCComplianceCusHerself=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_CUS_HERSELF);
        String s_SixCComplianceCuswritten=autosave_data.getdata(getContext(), mPage.getFormId(),SixCCompliancePage.KEY_CUS_WRITTEN);

        mKnowMember=(Spinner)rootView.findViewById(R.id.comp_know_member);
        ArrayAdapter memberAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,spinnerValue);
        memberAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mKnowMember.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_KNOW_MEMBERS,mKnowMember.getSelectedItem().toString());
        if (!s_SixCComplianceKnowNumber.equals("nulldata")){
            mKnowMember.setSelection(memberAdapter.getPosition(s_SixCComplianceKnowNumber));
        }

        mAgeBetween=(Spinner)rootView.findViewById(R.id.comp_age_between);
        mAgeBetween.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_AGE_BETWEEN,mAgeBetween.getSelectedItem().toString());
        if (!s_SixCComplianceAgeBet.equals("nulldata")){
            mAgeBetween.setSelection(memberAdapter.getPosition(s_SixCComplianceAgeBet));
        }

        mRelative=(Spinner)rootView.findViewById(R.id.comp_has_relative);
        mRelative.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_RELATIVE,mRelative.getSelectedItem().toString());
        if (!s_SixCComplianceRelative.equals("nulldata")){
            mRelative.setSelection(memberAdapter.getPosition(s_SixCComplianceRelative));
        }

        mCusEligible=(Spinner)rootView.findViewById(R.id.comp_cus_eligible);
        mCusEligible.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_CUS_ELIGIBLE,mCusEligible.getSelectedItem().toString());
        if (!s_SixCComplianceCusEligible.equals("nulldata")){
            mCusEligible.setSelection(memberAdapter.getPosition(s_SixCComplianceCusEligible));
        }

        mCusHerself=(Spinner)rootView.findViewById(R.id.comp_herself);
        mCusHerself.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_CUS_HERSELF,mCusHerself.getSelectedItem().toString());
        if (!s_SixCComplianceCusHerself.equals("nulldata")){
            mCusHerself.setSelection(memberAdapter.getPosition(s_SixCComplianceCusHerself));
        }

        mCusWritten=(Spinner)rootView.findViewById(R.id.comp_know_cus_written);
        mCusWritten.setAdapter(memberAdapter);
        mPage.getData().putString(SixCCompliancePage.KEY_CUS_WRITTEN, mCusWritten.getSelectedItem().toString());
        if (!s_SixCComplianceCuswritten.equals("nulldata")){
            mCusWritten.setSelection(memberAdapter.getPosition(s_SixCComplianceCuswritten));
        }


        mKnowMember.setOnItemSelectedListener(getOnItem());
        mAgeBetween.setOnItemSelectedListener(getOnItem());
        mRelative.setOnItemSelectedListener(getOnItem());
        mCusEligible.setOnItemSelectedListener(getOnItem());
        mCusHerself.setOnItemSelectedListener(getOnItem());
        mCusWritten.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle bundle){
        super.onViewCreated(view,bundle);

//        mKnowMember.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_KNOW_MEMBERS));
//        mAgeBetween.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_AGE_BETWEEN));
//        mRelative.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_RELATIVE));
//        mCusEligible.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_CUS_ELIGIBLE));
//        mCusHerself.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_CUS_HERSELF));
//        mCusWritten.setOnItemSelectedListener(getOnItemSelectedListener(SixCCompliancePage.KEY_CUS_WRITTEN));

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.comp_know_member) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_KNOW_MEMBERS, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(SixCCompliancePage.KEY_KNOW_MEMBERS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.comp_age_between) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_AGE_BETWEEN, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(SixCCompliancePage.KEY_AGE_BETWEEN, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.comp_has_relative) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_RELATIVE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(SixCCompliancePage.KEY_RELATIVE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.comp_cus_eligible) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_CUS_ELIGIBLE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(SixCCompliancePage.KEY_CUS_ELIGIBLE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.comp_herself) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_CUS_HERSELF, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(SixCCompliancePage.KEY_CUS_HERSELF, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.comp_know_cus_written) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), SixCCompliancePage.KEY_CUS_WRITTEN, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(SixCCompliancePage.KEY_CUS_WRITTEN, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }
}
