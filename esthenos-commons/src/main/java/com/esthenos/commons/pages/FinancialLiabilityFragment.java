package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;

public class FinancialLiabilityFragment extends PageFragment<FinancialLiabilityPage> {

        EditText mLiabChit,mLiabBank,mLiabInsurance,mLiabFriends;
    public static final String TABLE_FINANCIALLIABILITIES = "FinancialLeabilities";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    String getshared;
    long i;

    Autosave_Data autosave_data =new Autosave_Data();

        public static FinancialLiabilityFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        FinancialLiabilityFragment fragment = new FinancialLiabilityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_financial_liability, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_FinancialLiabilityFragliabchit=autosave_data.getdata(getContext(), mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_CHIT_KEY);
        String s_FinancialLiabilityFragliabbank=autosave_data.getdata(getContext(), mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_BANK_KEY);
        String s_FinancialLiabilityFragliabInsurance=autosave_data.getdata(getContext(), mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_INSURANCE_KEY);
        String s_FinancialLiabilityFraglianFriend=autosave_data.getdata(getContext(), mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_FRIENDS_KEY);
        //..................................................
        mydb = new MySQLiteDB(getContext());
       // autosave = new AutoSaveData(getContext());
//
//        getshared = autosave.Sharedgetvalue(getContext(), "FamilyExpenditurename","Expenditurekey");
//        // Toast.makeText(getContext(),"shted data before if " + getshared,Toast.LENGTH_SHORT).show();
//        if (getshared != null) {
//            i = Long.parseLong(getshared);
//            Toast.makeText(getContext(), "get id " + getshared, Toast.LENGTH_SHORT).show();
//
//            DialogFragment dg = new DialogFragment() {
//                @Override
//                public Dialog onCreateDialog(Bundle savedInstanceState) {
//                    return new AlertDialog.Builder(getActivity())
//                            .setMessage(R.string.getbackdata)
//                            .setPositiveButton("Yes",
//                                    new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialogInterface, int id) {
//                                            HashMap<String, String> user = mydb.getUserDetails(TABLE_FINANCIALLIABILITIES, i);
//                                            //   Toast.makeText(getContext(),"Name :"+user.get("name") + "Nimckname : " + user.get("nickname") + " UID : " + user.get("uid"),Toast.LENGTH_SHORT).show();
//
////                                            mExpendFood.setText(user.get("data1"));
////                                            mExpendTravel.setText(user.get("data2"));
////                                            mExpendEntertainment.setText(user.get("data3"));
////                                            mExpendFestival.setText(user.get("data4"));
////                                            mExpendMedical.setText(user.get("data5"));
////                                            mExpendEducation.setText(user.get("data6"));
////                                            mExpendOther.setText(user.get("data7"));
//                                        }
//                                    })
//                            .setNegativeButton("No", null).create();
//                }
//            };
//            dg.show(getFragmentManager(), "Dialog hihiihhih");
//
//        }
//...........................................

        mLiabChit=(EditText)rootView.findViewById(R.id.liab_chit);
        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_CHIT_KEY,mLiabChit.getText().toString());
        if (!s_FinancialLiabilityFragliabchit.equals("nulldata")){
            mLiabChit.setText(s_FinancialLiabilityFragliabchit);
        }
//        mLiabChit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                  //  mydb.FirstAutoSaveData(TABLE_FINANCIALLIABILITIES, MySQLiteDB.LIABILITIES_CHIT_KEY, mLiabChit.getText().toString());
//                   // i = mydb.getLastInsertedId(TABLE_FINANCIALLIABILITIES);
//                 //   Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
//                  //  autosave.Sharedsetvalue(getContext(), "FamilyExpenditurename", "Expenditurekey", String.valueOf(i));
//                }
//            }
//        });

        mLiabBank=(EditText)rootView.findViewById(R.id.liab_bank);
        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_BANK_KEY,mLiabBank.getText().toString());
        if (!s_FinancialLiabilityFragliabbank.equals("nulldata")){
            mLiabBank.setText(s_FinancialLiabilityFragliabbank);
        }
//        mLiabBank.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                //    mydb.Autosavebyid(TABLE_FINANCIALLIABILITIES, MySQLiteDB.LIABILITIES_BANK_KEY, i, mLiabBank.getText().toString());
//                }
//            }
//        });
        mLiabInsurance=(EditText)rootView.findViewById(R.id.liab_insurance);
        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_INSURANCE_KEY,mLiabInsurance.getText().toString());
        if (!s_FinancialLiabilityFragliabInsurance.equals("nulldata")){
            mLiabInsurance.setText(s_FinancialLiabilityFragliabInsurance);
        }
//        mLiabInsurance.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                 //   mydb.Autosavebyid(TABLE_FINANCIALLIABILITIES, MySQLiteDB.LIABILITIES_INSURANCE_KEY, i, mLiabInsurance.getText().toString());
//                }
//            }
//        });
        mLiabFriends=(EditText)rootView.findViewById(R.id.liab_friean);
        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_FRIENDS_KEY,mLiabFriends.getText().toString());
        if (!s_FinancialLiabilityFraglianFriend.equals("nulldata")){
            mLiabFriends.setText(s_FinancialLiabilityFraglianFriend);
        }
//        mLiabFriends.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                  //  mydb.Autosavebyid(TABLE_FINANCIALLIABILITIES, MySQLiteDB.LIABILITIES_FRIENDS_KEY, i, mLiabFriends.getText().toString());
//                }
//            }
//        });

        mLiabChit.addTextChangedListener(new GenericTextFinancialyLiabilityFrag(mLiabChit));
        mLiabBank.addTextChangedListener(new GenericTextFinancialyLiabilityFrag(mLiabBank));
        mLiabInsurance.addTextChangedListener(new GenericTextFinancialyLiabilityFrag(mLiabInsurance));
        mLiabFriends.addTextChangedListener(new GenericTextFinancialyLiabilityFrag(mLiabFriends));
        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        mLiabChit.addTextChangedListener(getTextWatcher(FinancialLiabilityPage.LIABILITIES_CHIT_KEY));
//        mLiabBank.addTextChangedListener(getTextWatcher(FinancialLiabilityPage.LIABILITIES_BANK_KEY));
//        mLiabInsurance.addTextChangedListener(getTextWatcher(FinancialLiabilityPage.LIABILITIES_INSURANCE_KEY));
//        mLiabFriends.addTextChangedListener(getTextWatcher(FinancialLiabilityPage.LIABILITIES_FRIENDS_KEY));
    }

    class GenericTextFinancialyLiabilityFrag implements TextWatcher {

        private View view;

        private GenericTextFinancialyLiabilityFrag(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
           // Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.liab_chit) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_CHIT_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_CHIT_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.liab_bank) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_BANK_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_BANK_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.liab_insurance) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_INSURANCE_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_INSURANCE_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.liab_friean) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FinancialLiabilityPage.LIABILITIES_FRIENDS_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FinancialLiabilityPage.LIABILITIES_FRIENDS_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
 }

