package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FamilyIndeptnessDetailPage extends Page {

    public static final String FAMILY_INDENT_LOAN_SOURCE = "Loan Source";
    public static final String FAMILY_INDENT_NAME = "Name";
    public static final String FAMILY_INDENT_BORROW_AMT = "Borrowed Amount";
    public static final String FAMILY_INDENT_MONTHLY_REPAY = "Monthly Repayment";
    public static final String FAMILY_INDENT_OUTSTANDING = "Outstanding";
    public static final String FAMILY_INDENT_REPAYMENT_TRACK = "Repayment Track";

    public FamilyIndeptnessDetailPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FamilyIndeptnessDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(FAMILY_INDENT_LOAN_SOURCE,mData.getString(FAMILY_INDENT_LOAN_SOURCE),getPageId(),-1));
        dest.add(new ReviewItem(FAMILY_INDENT_NAME,mData.getString(FAMILY_INDENT_NAME),getPageId(),-1));
        dest.add(new ReviewItem(FAMILY_INDENT_BORROW_AMT,mData.getString(FAMILY_INDENT_BORROW_AMT),getPageId(),-1));
        dest.add(new ReviewItem(FAMILY_INDENT_MONTHLY_REPAY,mData.getString(FAMILY_INDENT_MONTHLY_REPAY),getPageId(),-1));
        dest.add(new ReviewItem(FAMILY_INDENT_OUTSTANDING,mData.getString(FAMILY_INDENT_OUTSTANDING),getPageId(),-1));
        dest.add(new ReviewItem(FAMILY_INDENT_REPAYMENT_TRACK,mData.getString(FAMILY_INDENT_REPAYMENT_TRACK),getPageId(),-1));
    }
}
