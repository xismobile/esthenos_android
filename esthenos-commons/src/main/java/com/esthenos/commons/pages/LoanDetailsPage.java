package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class LoanDetailsPage extends Page {
    public static final String SOUCE_LOAN_KEY = "Loan Detail";
    public static final String BANK_NAME_KEY = "Name of Bank";
    public static final String LOAN_TYPE_KEY = "Type of Loan";
    public static final String LOAN_AMOUNT_KEY = "Loan Amount Key";
    public static final String TENURE_KEY = "Tenure in Months";
    public static final String INTEREST_KEY = "Interest";
    public static final String EMI_KEY = "EMI Repayments";
    public static final String COLLATERAL_KEY = "Collateral Details";
    public static final String OUTSTANDING_LOAN_KEY = "Outstanding Loan Amount";

    public LoanDetailsPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return LoanDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(SOUCE_LOAN_KEY, mData.getString(SOUCE_LOAN_KEY), getPageId(), -1));
        dest.add(new ReviewItem(BANK_NAME_KEY, mData.getString(BANK_NAME_KEY), getPageId(), -1));
        dest.add(new ReviewItem(LOAN_TYPE_KEY, mData.getString(LOAN_TYPE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(LOAN_AMOUNT_KEY, mData.getString(LOAN_AMOUNT_KEY), getPageId(), -1));
        dest.add(new ReviewItem(TENURE_KEY, mData.getString(TENURE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(INTEREST_KEY, mData.getString(INTEREST_KEY), getPageId(), -1));
        dest.add(new ReviewItem(EMI_KEY, mData.getString(EMI_KEY), getPageId(), -1));
        dest.add(new ReviewItem(COLLATERAL_KEY, mData.getString(COLLATERAL_KEY), getPageId(), -1));
        dest.add(new ReviewItem(OUTSTANDING_LOAN_KEY, mData.getString(OUTSTANDING_LOAN_KEY), getPageId(), -1));
    }
}
