package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class LandDetailPage extends Page {
    public static final String LAND_DESCRIPTION = "Land Description";
    public static final String LAND_PROPERTY = "Loan outstanding against such property";
    public static final String LAND_RESOLVE_VALUE = "Estimated Resale value of property";

    public LandDetailPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return LandDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(LAND_DESCRIPTION, mData.getString(LAND_DESCRIPTION), getPageId(), -1));
        dest.add(new ReviewItem(LAND_PROPERTY, mData.getString(LAND_PROPERTY), getPageId(), -1));
        dest.add(new ReviewItem(LAND_RESOLVE_VALUE, mData.getString(LAND_RESOLVE_VALUE), getPageId(), -1));

    }
}


