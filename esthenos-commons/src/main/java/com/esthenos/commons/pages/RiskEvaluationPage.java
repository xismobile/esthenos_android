package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class RiskEvaluationPage extends Page {

    public static final String KEY_SEASONAL="Is there any seasonal fluctuation in Income ?";
    public static final String KEY_SUFF_HEALTH="In the Last 6 months has the family members suffered " +
            "                       from any major health,death or accident of family members ?";
    public static final String KEY_SUFF_FINANCIAL="In the Last 6 months has the family suffered from any " +
                                                    "financial shock due to loss of job or major loss in business ?";
    public static final String KEY_SPOUSE="Spouse/Adult member of the family knows about the loan applied for ?";
    public static final String KEY_CUSS_ELIGIBLE="Is the Customer eligible for Arohan's Life Insurance service ?";
    public static final String KEY_CUS_HERSELF="Does the customer herself/himself provide all financial information sought ?";

    public RiskEvaluationPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return RiskEvaluationFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_SEASONAL,mData.getString(KEY_SEASONAL),getPageId(),-1));
        dest.add(new ReviewItem(KEY_SUFF_HEALTH,mData.getString(KEY_SUFF_HEALTH),getPageId(),-1));
        dest.add(new ReviewItem(KEY_SUFF_FINANCIAL,mData.getString(KEY_SUFF_FINANCIAL),getPageId(),-1));
        dest.add(new ReviewItem(KEY_SPOUSE,mData.getString(KEY_SPOUSE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_CUSS_ELIGIBLE,mData.getString(KEY_CUSS_ELIGIBLE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_CUS_HERSELF,mData.getString(KEY_CUS_HERSELF),getPageId(),-1));

    }
}
