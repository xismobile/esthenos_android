package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class BusinessDetails extends Page {

    public static final String BIZ_ACTIVITY = "biz-activity";
    public static final String BIZ_INCOME_MONTHLY = "biz-income-monthly";

    public static final String BIZ_NUM_YEARS = "biz-num-years";
    public static final String BIZ_NUM_EMPLOYEES = "biz-num-employees";

    public static final String BIZ_EXPENSE_RENT = "biz-expense-rent";
    public static final String BIZ_EXPENSE_OTHER = "biz-expense-other";
    public static final String BIZ_EXPENSE_ADMIN = "biz-expense-admin";
    public static final String BIZ_EXPENSE_SALARY = "biz-expense-salary";
    public static final String BIZ_EXPENSE_WORKING_CAPITAL = "biz-expense-working-capital";

    public static final String BIZ_PREMISE = "biz-premise";
    public static final String BIZ_CATEGORY = "biz-category";
    public static final String BIZ_SEASONALITY = "biz-seasonality";


    public BusinessDetails(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return BusinessDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(BIZ_ACTIVITY, mData.getString(BIZ_ACTIVITY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_INCOME_MONTHLY, mData.getString(BIZ_INCOME_MONTHLY), getPageId(), -1));

        dest.add(new ReviewItem(BIZ_NUM_YEARS, mData.getString(BIZ_NUM_YEARS), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_NUM_EMPLOYEES, mData.getString(BIZ_NUM_EMPLOYEES), getPageId(), -1));

        dest.add(new ReviewItem(BIZ_EXPENSE_RENT, mData.getString(BIZ_EXPENSE_RENT), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_EXPENSE_OTHER, mData.getString(BIZ_EXPENSE_OTHER), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_EXPENSE_ADMIN, mData.getString(BIZ_EXPENSE_ADMIN), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_EXPENSE_SALARY, mData.getString(BIZ_EXPENSE_SALARY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_EXPENSE_WORKING_CAPITAL, mData.getString(BIZ_EXPENSE_WORKING_CAPITAL), getPageId(), -1));

        dest.add(new ReviewItem(BIZ_PREMISE, mData.getString(BIZ_PREMISE), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_CATEGORY, mData.getString(BIZ_CATEGORY), getPageId(), -1));
        dest.add(new ReviewItem(BIZ_SEASONALITY, mData.getString(BIZ_SEASONALITY), getPageId(), -1));
    }
}
