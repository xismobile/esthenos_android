package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class IncomefromBusinessFragment extends PageFragment<IncomefromBusinessPage> {

    private EditText mTotalSale,mAvgDailySale,mNoOfBussiness,mCostOfRaw,mPurchase,mFrecPurchase
            ,mOtherExp,mInterest,mTotalCost,mTotalProfit;
    Autosave_Data autosave_data = new Autosave_Data();
    public static IncomefromBusinessFragment create(String key) {
        IncomefromBusinessFragment fragment = new IncomefromBusinessFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
     View rootView=inflater.inflate(R.layout.fragment_incomefrom_business, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mNoOfBussiness=(EditText)rootView.findViewById(R.id.income_frec_purchase);
        mTotalProfit=(EditText)rootView.findViewById(R.id.income_total_profite);
        mCostOfRaw=(EditText)rootView.findViewById(R.id.income_calculate_cost);
        mFrecPurchase=(EditText)rootView.findViewById(R.id.income_daily_sale);
        mTotalCost=(EditText)rootView.findViewById(R.id.income_total_costing);
        mAvgDailySale=(EditText)rootView.findViewById(R.id.income_purchase);
        mTotalSale=(EditText)rootView.findViewById(R.id.income_total_sale);
        mPurchase=(EditText)rootView.findViewById(R.id.income_no_bisuness);
        mOtherExp=(EditText)rootView.findViewById(R.id.income_other_exp);
        mInterest=(EditText)rootView.findViewById(R.id.income_interest);



        String S_mNoOfBussiness = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_NO_BUSINESS);
        String S_mTotalProfit = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_PROFIT);
        String S_mCostOfRaw = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_CALCULATE);
        String S_mFrecPurchase = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_FREC_PURCHASE);
        String S_mTotalCost = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_COST);
        String S_mAvgDailySale = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_SALE);
        String S_mTotalSale = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_SALE);
        String S_mPurchase = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_PURCHAGE);
        String S_mOtherExp = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_OTHER);
        String S_mInterest = autosave_data.getdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_INTEREST);


        if (!S_mNoOfBussiness.equals("nulldata")) {
            mNoOfBussiness.setText(S_mNoOfBussiness);
        }
        if (!S_mTotalProfit.equals("nulldata")) {
            mTotalProfit.setText(S_mTotalProfit);
        }
        if (!S_mCostOfRaw.equals("nulldata")) {
            mCostOfRaw.setText(S_mCostOfRaw);
        }
        if (!S_mFrecPurchase.equals("nulldata")) {
            mFrecPurchase.setText(S_mFrecPurchase);
        }
        if (!S_mTotalCost.equals("nulldata")) {
            mTotalCost.setText(S_mTotalCost);
        }
        if (!S_mAvgDailySale.equals("nulldata")) {
            mAvgDailySale.setText(S_mAvgDailySale);
        }
        if (!S_mTotalSale.equals("nulldata")) {
            mTotalSale.setText(S_mTotalSale);
        }
        if (!S_mPurchase.equals("nulldata")) {
            mPurchase.setText(S_mPurchase);
        }
        if (!S_mOtherExp.equals("nulldata")) {
            mOtherExp.setText(S_mOtherExp);
        }
        if (!S_mInterest.equals("nulldata")) {
            mInterest.setText(S_mInterest);
        }










        mNoOfBussiness.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable v) {

                String Avgvalue=mAvgDailySale.getText().toString();
                String NoOfBussiness=mNoOfBussiness.getText().toString();

                int dailySale=Avgvalue.trim().length()!=0?Integer.parseInt(Avgvalue):0;
                int NoOfBuss = NoOfBussiness.trim().length()!=0?Integer.parseInt(NoOfBussiness):0;
                int totalSale = dailySale * NoOfBuss * 4;

                mTotalSale.setText(String.format("%d", totalSale));
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_NO_BUSINESS, v.toString());

            }
        });
        mTotalProfit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_PROFIT, s.toString());

            }
        });
        mCostOfRaw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_CALCULATE, s.toString());
            }
        });
        mFrecPurchase.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mPurchase.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "please fill Purchase field", Toast.LENGTH_LONG).show();
                } else {

                    String value=mPurchase.getText().toString();
                    String FreCPurchase=mFrecPurchase.getText().toString();

                    int purchase=value.trim().length()!=0?Integer.parseInt(value):0;
                    int FrecPur = FreCPurchase.trim().length()!=0?Integer.parseInt(FreCPurchase):0;
                    int CostOfRaw = purchase * FrecPur;

                    String otherValue=mOtherExp.getText().toString();
                    String interest= mInterest.getText().toString();

                    int otherExp=otherValue.trim().length()!=0?Integer.parseInt(otherValue):0;
                    int interestvalue=interest.trim().length()!=0?Integer.parseInt(interest):0;

                    int totalCost=otherExp+interestvalue+CostOfRaw;

                    mCostOfRaw.setText(String.format("%d", CostOfRaw));
                    mTotalCost.setText(String.format("%d",totalCost));

                    autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_FREC_PURCHASE, s.toString());
                }
            }
        });
        mTotalCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_COST, s.toString());
            }
        });
        mAvgDailySale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable v) {
                if (mAvgDailySale.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "please fill Avg Daily Sale", Toast.LENGTH_LONG).show();
                } else {
                    String Avgvalue=mAvgDailySale.getText().toString();
                    String NoOfBussiness=mNoOfBussiness.getText().toString();

                    int dailySale=Avgvalue.trim().length()!=0?Integer.parseInt(Avgvalue):0;
                    int NoOfBuss = NoOfBussiness.trim().length()!=0?Integer.parseInt(NoOfBussiness):0;
                    int totalSale = dailySale * NoOfBuss * 4;



                    mTotalSale.setText(String.format("%d", totalSale));

                    autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_SALE, v.toString());
                }
            }
        });
        mTotalSale.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_SALE, s.toString());
            }
        });
        mPurchase.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mPurchase.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "please fill Purchase field", Toast.LENGTH_LONG).show();
                } else {


                    String Purchasevalue=mPurchase.getText().toString();
                    String FreCPurchase=mFrecPurchase.getText().toString();
                    String otherValue=mOtherExp.getText().toString();
                    String interest= mInterest.getText().toString();

                    int purchase=Purchasevalue.trim().length()!=0?Integer.parseInt(Purchasevalue):0;
                    int FrecPur = FreCPurchase.trim().length()!=0?Integer.parseInt(FreCPurchase):0;
                    int otherExp=otherValue.trim().length()!=0?Integer.parseInt(otherValue):0;
                    int interestvalue=interest.trim().length()!=0?Integer.parseInt(interest):0;



                    int CostOfRaw = purchase * FrecPur;
                    int totalCost=otherExp+interestvalue+CostOfRaw;

                    mCostOfRaw.setText(String.format("%d", CostOfRaw));
                    mTotalCost.setText(String.format("%d",totalCost));
                    autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_TOTAL_PURCHAGE, s.toString());
                }
            }
        });
        mOtherExp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String otherValue=mOtherExp.getText().toString();
                String interest= mInterest.getText().toString();
                String Costvalue=mCostOfRaw.getText().toString();

                int CostOfRaw=Costvalue.trim().length()!=0?Integer.parseInt(Costvalue):0;
                int otherExp=otherValue.trim().length()!=0?Integer.parseInt(otherValue):0;
                int interestvalue=interest.trim().length()!=0?Integer.parseInt(interest):0;

                int totalCost=otherExp+interestvalue+CostOfRaw;
                mTotalCost.setText(String.format("%d",totalCost));
                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_OTHER, s.toString());

            }
        });
        mInterest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String otherValue=mOtherExp.getText().toString();
                String interest= mInterest.getText().toString();
                String Costvalue=mCostOfRaw.getText().toString();


                int CostOfRaw=Costvalue.trim().length()!=0?Integer.parseInt(Costvalue):0;
                int otherExp=otherValue.trim().length()!=0?Integer.parseInt(otherValue):0;
                int interestvalue=interest.trim().length()!=0?Integer.parseInt(interest):0;

                int totalCost=otherExp+interestvalue+CostOfRaw;

                mTotalCost.setText(String.format("%d",totalCost));

                autosave_data.putdata(getContext(), mPage.getFormId(), IncomefromBusinessPage.INCOME_DAILY_INTEREST, s.toString());

            }
        });








        mTotalProfit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ForTotalProfit=mTotalSale.getText().toString();
                String TotalCost=mTotalCost.getText().toString();

                int TotalCostValu=TotalCost.trim().length()!=0?Integer.parseInt(TotalCost):0;
                int TotalProfit=ForTotalProfit.trim().length()!=0?Integer.parseInt(ForTotalProfit):0;

                final int ValueForTotalProfit=TotalProfit- TotalCostValu;

                mTotalProfit.setText(String.format("%d",ValueForTotalProfit));
                mPage.getData().putString(IncomefromBusinessPage.INCOME_DAILY_TOTAL_PROFIT,mTotalProfit.getText().toString());
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanseState){
        super.onViewCreated(view, savedInstanseState);

        mFrecPurchase.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_FREC_PURCHASE));
        mNoOfBussiness.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_NO_BUSINESS));
        mCostOfRaw.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_CALCULATE));
        mTotalProfit.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_PROFIT));
        mPurchase.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_PURCHAGE));
        mTotalSale.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_SALE));
        mTotalCost.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_TOTAL_COST));
        mAvgDailySale.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_SALE));
        mInterest.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_INTEREST));
        mOtherExp.addTextChangedListener(getTextWatcher(IncomefromBusinessPage.INCOME_DAILY_OTHER));
    }

}
