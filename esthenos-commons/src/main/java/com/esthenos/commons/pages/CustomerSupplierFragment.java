package com.esthenos.commons.pages;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class CustomerSupplierFragment extends PageFragment<CustomerSupplierPage> {

    EditText mCustName,mCustAdd,mCustTelephone,mSuppName,mSuppAdd,mSuppTelephone;

    public static CustomerSupplierFragment create(String key) {
        CustomerSupplierFragment fragment = new CustomerSupplierFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_supplier, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());


        mCustName=(EditText)rootView.findViewById(R.id.customer_name);
        mPage.getData().putString(CustomerSupplierPage.CUSTOMER_NAME_KEY,mCustName.getText().toString());

        mCustAdd=(EditText)rootView.findViewById(R.id.customer_address);
        mPage.getData().putString(CustomerSupplierPage.CUSTOMER_Address_KEY,mCustAdd.getText().toString());

        mCustTelephone=(EditText)rootView.findViewById(R.id.customer_telephone);
        mPage.getData().putString(CustomerSupplierPage.CUSTOMER_TELEPHONE_KEY,mCustTelephone.getText().toString());

        mSuppName=(EditText)rootView.findViewById(R.id.supplier_name);
        mPage.getData().putString(CustomerSupplierPage.SUPPLIER_NAME,mSuppName.getText().toString());

        mSuppAdd=(EditText)rootView.findViewById(R.id.supplier_address);
        mPage.getData().putString(CustomerSupplierPage.SUPPLIER_ADDRESS,mSuppAdd.getText().toString());

        mSuppTelephone=(EditText)rootView.findViewById(R.id.supplier_telephone);
        mPage.getData().putString(CustomerSupplierPage.SUPPLIER_TELEPHONE,mSuppTelephone.getText().toString());

        return rootView;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCustName.addTextChangedListener(getTextWatcher(CustomerSupplierPage.CUSTOMER_NAME_KEY));
        mCustAdd.addTextChangedListener(getTextWatcher(CustomerSupplierPage.CUSTOMER_Address_KEY));
        mCustTelephone.addTextChangedListener(getTextWatcher(CustomerSupplierPage.CUSTOMER_TELEPHONE_KEY));
        mSuppName.addTextChangedListener(getTextWatcher(CustomerSupplierPage.SUPPLIER_NAME));
        mSuppAdd.addTextChangedListener(getTextWatcher(CustomerSupplierPage.SUPPLIER_ADDRESS));
        mSuppTelephone.addTextChangedListener(getTextWatcher(CustomerSupplierPage.SUPPLIER_TELEPHONE));
    }
    }
