package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;


public class AdditionalFamilyDetailFragment extends PageFragment<AdditionalFamilyDetailPage> {

    Autosave_Data autosave_data = new Autosave_Data();
    @NotEmpty
    private EditText mName,  mAge, mMonthlyIncome,mInvolvment;
    private Spinner mSex,mRelation, mEducation, mOccupations;
    private static final String[] education={"","Illiterate","Upto 5th class","Upto 10th Class", "Upto Class 12","Graduate",
                                             "PG and Above","Semi literate"};

    private static final String[] relation={"","Brother","Sister","Self","Spouse","Mother","Father","Daughter","Son","Grand Mother",
                                             "Grand Father","Grand Son","Grand Daughter","Relative","Father in Law","Mother in Law",
                                             "Brother in Law","Sister in Law","Daughter in Law"};

    private static final String[] ocupation={" ","Retailing","Manufacturing","Services","Housewife","Student"};
    private static final String[] sex={"","Male","Female"};
public static final String TAG="Gravity";


    public static AdditionalFamilyDetailFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        AdditionalFamilyDetailFragment fragment = new AdditionalFamilyDetailFragment();


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
       // Toast.makeText(getContext(), "Frag id=="+mPage.getPageId(), Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onCreate: Frag id=="+mPage.getFormId());
        mPage = (AdditionalFamilyDetailPage) mCallbacks.onGetPage(mKey);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_page_family_details, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mAge = (EditText) rootView.findViewById(R.id.f_age);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_AGE,mAge.getText().toString());

        mRelation = (Spinner) rootView.findViewById(R.id.f_relation);
        ArrayAdapter<String> relationAdapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,relation);
        relationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRelation.setAdapter(relationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_RELATION, mRelation.getSelectedItem().toString());

        mName = (EditText) rootView.findViewById(R.id.f_name);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_NAME,mName.getText().toString());

        mOccupations = (Spinner) rootView.findViewById(R.id.f_occupations);
        ArrayAdapter<String> ocupationAdapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,ocupation);
        ocupationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mOccupations.setAdapter(ocupationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_OCCUPATION_DETAILS,mOccupations.getSelectedItem().toString());

        mEducation = (Spinner) rootView.findViewById(R.id.f_education);
        ArrayAdapter<String> educationAdapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,education);
        educationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEducation.setAdapter(educationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_EDUCATION, mEducation.getSelectedItem().toString());

        mInvolvment=(EditText)rootView.findViewById(R.id.f_involment);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_INVOLVEMENY,mInvolvment.getText().toString());

        mMonthlyIncome = (EditText) rootView.findViewById(R.id.f_annual_income);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_ANNUAL_INCOME,mMonthlyIncome.getText().toString());

        mSex=(Spinner)rootView.findViewById(R.id.f_sex);
        ArrayAdapter sexAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sex);
        sexAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSex.setAdapter(sexAdapter);
        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_SEX,mSex.getSelectedItem().toString());



        String S_mSex = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_SEX);
        String S_mRelation = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_RELATION);
        String S_mEducation = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_EDUCATION);
        String S_mOccupations = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_OCCUPATION_DETAILS);
        String S_mName = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_NAME);
        String S_mAge = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_AGE);
        String S_mMonthlyIncome = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_ANNUAL_INCOME);
        String S_Involvment = autosave_data.getdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_INVOLVEMENY);


        if (!S_mSex.equals("nulldata")) {

            int P_sex = sexAdapter.getPosition(S_mSex);
            mSex.setSelection(P_sex);
        }
        if (!S_mRelation.equals("nulldata")) {

            int P_rel = relationAdapter.getPosition(S_mRelation);
            mRelation.setSelection(P_rel);
        }
        if (!S_mEducation.equals("nulldata")) {

            int P_edu = educationAdapter.getPosition(S_mEducation);
            mEducation.setSelection(P_edu);
        }
        if (!S_mOccupations.equals("nulldata")) {

            int P_occu = ocupationAdapter.getPosition(S_mOccupations);
            mOccupations.setSelection(P_occu);
        }

        if (!S_mName.equals("nulldata")) {
            mName.setText(S_mName);
        }
        if (!S_mAge.equals("nulldata")) {
            mAge.setText(S_mAge);
        }
        if (!S_mMonthlyIncome.equals("nulldata")) {
            mMonthlyIncome.setText(S_mMonthlyIncome);
        }
        if (!S_Involvment.equals("nulldata")) {
            mInvolvment.setText(S_Involvment);
        }



        mSex.setOnItemSelectedListener(getOnItemASDF());
        mRelation.setOnItemSelectedListener(getOnItemASDF());
        mEducation.setOnItemSelectedListener(getOnItemASDF());
        mOccupations.setOnItemSelectedListener(getOnItemASDF());



        mName.addTextChangedListener(new GenericTextASDF(mName));
        mAge.addTextChangedListener(new GenericTextASDF(mAge));
        mInvolvment.addTextChangedListener(new GenericTextASDF(mInvolvment));
        mMonthlyIncome.addTextChangedListener(new GenericTextASDF(mMonthlyIncome));


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
/*
        mOccupations.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailPage.KEY_OCCUPATION_DETAILS));
        mMonthlyIncome.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailPage.KEY_ANNUAL_INCOME));
        mInvolvment.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailPage.KEY_INVOLVEMENY));
        mSex.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailPage.KEY_SEX));
        mEducation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailPage.KEY_EDUCATION));
        mRelation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailPage.KEY_RELATION));
        mName.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailPage.KEY_NAME));
        mAge.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailPage.KEY_AGE));*/
    }




    public AdapterView.OnItemSelectedListener getOnItemASDF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.f_sex) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_SEX, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_SEX, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.f_relation) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_RELATION, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_RELATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.f_education) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.f_occupations) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_OCCUPATION_DETAILS, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailPage.KEY_OCCUPATION_DETAILS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTextASDF implements TextWatcher {

        private View view;

        private GenericTextASDF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in GenericTextASDF", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.f_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_NAME, text);


                    mPage.getData().putString(AdditionalFamilyDetailPage.KEY_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_age) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_AGE, text);


                    mPage.getData().putString(AdditionalFamilyDetailPage.KEY_AGE, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_annual_income) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_ANNUAL_INCOME, text);


                    mPage.getData().putString(AdditionalFamilyDetailPage.KEY_ANNUAL_INCOME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_involment) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), AdditionalFamilyDetailPage.KEY_INVOLVEMENY, text);


                    mPage.getData().putString(AdditionalFamilyDetailPage.KEY_INVOLVEMENY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


        }

    }
}
