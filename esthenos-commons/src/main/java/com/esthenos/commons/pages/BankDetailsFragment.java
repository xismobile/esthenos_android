package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;


public class BankDetailsFragment extends PageFragment<BankDetailsPage> {

    public static final String TABLE_BANKDETAIL = "BankDetails";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;

    @NotEmpty
    private EditText bankAccount, bankIFSCCode,mBranch,
            bankAccountOperational, bankAccountHolderName;

    private Spinner bankAccountType,bankName;

    final String[] BANK_ACCOUNT_TYPES = {"","Saving","Current"};
    final String[] BankNameList={" ","Assam Gramin Vikash Bank", "Axis Bank", "Bandhan Bank", "Bangiya Gramin " +
            "Vikash Bank", "Bank of Baroda, Bank of India", "Bank of Maharashtra", "Bharatiya Mahila Bank",
            "Bihar Gramin Bank", "Canara Bank", "Central Bank of India", "City Union Bank", "Corporation Bank",
            "Dena Bank", "Development Credit Bank", "Dhanlaxmi Bank", "Federal Bank", "HDFC Bank", "ICICI Bank",
            "IDBI Bank", "IDFC Bank", "Indian Bank", "Indian Overseas Bank", "IndusInd Bank", "ING Vysya Bank",
            "Jammu and Kashmir Bank", "Jharkhand Gramin Bank", "Karnataka Bank", "Karur Vysya Bank",
            "Kotak Mahindra Bank", "Lakshmi Vilas Bank", "Langpi Dehangi Rural Bank", "Madhya Bihar Gramin Bank",
            "Nainital Bank", "Odisha Gramya Bank", "Oriental Bank of Commerce", "Paschim Banga Gramin Bank",
            "Punjab & Sind Bank", "Punjab National Bank", "RBL Bank", "South Indian Bank", "State Bank of Bikaner & Jaipur",
            "State Bank of Hyderabad", "State Bank of India", "State Bank of Mysore", "State Bank of Patiala",
            "State Bank of Travancore", "Syndicate Bank", "Tamilnad Mercantile Bank", "UCO Bank", "Union Bank of India",
            "United Bank of India", "Uttar Bihar Gramin Bank", "Uttarbanga Kshetriya Gramin Bank", "Vijaya Bank, Yes Bank"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static BankDetailsFragment create(String key) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_KEY, key);
        BankDetailsFragment fragment = new BankDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bank_details_page, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mydb = new MySQLiteDB(getContext());
       // autosave = new AutoSaveData();
        String s_BankDetailfragBankname=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_NAME);
        String s_BankDetailfragBranch=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_BRANCH);
        String s_BankDetailfragbankaccount=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_NUMBER);
        String s_BankDetailfragifsccode=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_BRANCH_IFSC);
        String s_BankDetailfragholdername=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_HOLDER_NAME);
        String s_BankDetailfragoperationalpage=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_OPERATIONAL);
        String s_BankDetailfragAccounttype=autosave_data.getdata(getContext(), mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_TYPE);

        bankName = (Spinner) rootView.findViewById(R.id.bank_name);
        ArrayAdapter adapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,BankNameList);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bankName.setAdapter(adapter);
        mPage.getData().putString(BankDetailsPage.BANK_NAME, bankName.getSelectedItem().toString());
        if (!s_BankDetailfragBankname.equals("nulldata")){
            bankName.setSelection(adapter.getPosition(s_BankDetailfragBankname));
        }

        mBranch=(EditText)rootView.findViewById(R.id.branch_detail);
        mBranch.setText(mPage.getData().getString(BankDetailsPage.BANK_BRANCH));
        if (!s_BankDetailfragBranch.equals("nulldata")){
            mBranch.setText(s_BankDetailfragBranch);
        }


        bankAccount = (EditText) rootView.findViewById(R.id.account_number);
        bankAccount.setText(mPage.getData().getString(BankDetailsPage.BANK_ACCOUNT_NUMBER));
        if (!s_BankDetailfragbankaccount.equals("nulldata")){
            bankAccount.setText(s_BankDetailfragbankaccount);
        }


        bankIFSCCode = (EditText) rootView.findViewById(R.id.bank_ifsc_code);
        bankIFSCCode.setText(mPage.getData().getString(BankDetailsPage.BANK_BRANCH_IFSC));
        if (!s_BankDetailfragifsccode.equals("nulldata")){
            bankIFSCCode.setText(s_BankDetailfragifsccode);
        }


        bankAccountHolderName = (EditText) rootView.findViewById(R.id.account_holder_name);
        bankAccountHolderName.setText(mPage.getData().getString(BankDetailsPage.BANK_ACCOUNT_HOLDER_NAME));
        if (!s_BankDetailfragholdername.equals("nulldata")){
            bankAccountHolderName.setText(s_BankDetailfragholdername);
        }


        bankAccountOperational = (EditText) rootView.findViewById(R.id.account_operational_since);
        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_OPERATIONAL, bankAccountOperational.getText().toString());
        if (!s_BankDetailfragoperationalpage.equals("nulldata")){
            bankAccountOperational.setText(s_BankDetailfragoperationalpage);
        }


        bankAccountType = (Spinner) rootView.findViewById(R.id.bank_account_types);
        ArrayAdapter<String> adapterRelations = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, BANK_ACCOUNT_TYPES);
        adapterRelations.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bankAccountType.setAdapter(adapterRelations);
        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_TYPE, bankAccountType.getSelectedItem().toString());
        if (!s_BankDetailfragAccounttype.equals("nulldata")){
            bankAccountType.setSelection(adapterRelations.getPosition(s_BankDetailfragAccounttype));
        }


        bankName.setOnItemSelectedListener(getOnItem());
        mBranch.addTextChangedListener(new GenericTextBankDetailFrag(mBranch));
        bankAccount.addTextChangedListener(new GenericTextBankDetailFrag(bankAccount));
        bankIFSCCode.addTextChangedListener(new GenericTextBankDetailFrag(bankIFSCCode));
        bankAccountType.setOnItemSelectedListener(getOnItem());
        bankAccountHolderName.addTextChangedListener(new GenericTextBankDetailFrag(bankAccountHolderName));
        bankAccountOperational.addTextChangedListener(new GenericTextBankDetailFrag(bankAccountOperational));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        bankName.setOnItemSelectedListener(getOnItemSelectedListener(BankDetailsPage.BANK_NAME));
//        mBranch.addTextChangedListener(getTextWatcher(BankDetailsPage.BANK_BRANCH));
//        bankAccount.addTextChangedListener(getTextWatcher(BankDetailsPage.BANK_ACCOUNT_NUMBER));
//        bankIFSCCode.addTextChangedListener(getTextWatcher(BankDetailsPage.BANK_BRANCH_IFSC));
//        bankAccountType.setOnItemSelectedListener(getOnItemSelectedListener(BankDetailsPage.BANK_ACCOUNT_TYPE));
//        bankAccountHolderName.addTextChangedListener(getTextWatcher(BankDetailsPage.BANK_ACCOUNT_HOLDER_NAME));
//        bankAccountOperational.addTextChangedListener(getTextWatcher(BankDetailsPage.BANK_ACCOUNT_OPERATIONAL));
//        bankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.FirstAutoSaveData(TABLE_BANKDETAIL, MySQLiteDB.BANK_NAME, value);
//                i = mydb.getLastInsertedId(TABLE_BANKDETAIL);
//                Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
//                // autosave.Sharedsetvalue(getContext(),"FamilyMembername" ,"FamilyDetailKey" ,String.valueOf(i));
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        bankAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.Autosavebyid(TABLE_BANKDETAIL, MySQLiteDB.BANK_ACCOUNT_TYPE, i, value);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.bank_name) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), BankDetailsPage.BANK_NAME, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(BankDetailsPage.BANK_NAME, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.bank_account_types) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), BankDetailsPage.BANK_ACCOUNT_TYPE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_TYPE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

    class GenericTextBankDetailFrag implements TextWatcher {

        private View view;

        private GenericTextBankDetailFrag(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
           // Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.branch_detail) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BankDetailsPage.BANK_BRANCH,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BankDetailsPage.BANK_BRANCH, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.account_number) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_NUMBER,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_NUMBER, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bank_ifsc_code) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BankDetailsPage.BANK_BRANCH_IFSC,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BankDetailsPage.BANK_BRANCH_IFSC, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.account_holder_name) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_HOLDER_NAME,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_HOLDER_NAME, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.account_operational_since) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BankDetailsPage.BANK_ACCOUNT_OPERATIONAL,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BankDetailsPage.BANK_ACCOUNT_OPERATIONAL, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
