package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class BusinessInfoOtherDetailPage extends Page {

    public static final String BUSINESS_INFO_TYPE_EQUIPMENT = "Type Of Equipment/Machinery used in Business";
    public static final String BUSINESS_INFO_DETAIL = "Details of Manufacturer";
    public static final String BUSINESS_INFO_AGE = "Age Of Machinery";
    public static final String BUSINESS_INFO_ESTIMATE = "Estimated Value";
    public static final String BUSINESS_INFO_COLLATERAL = "Collateral Created or Security Given of Equipment";
    public static final String BUSINESS_INFO_NUM_EMP = "Number Of Employees(Already present in Mobile)";
    public static final String BUSINESS_INFO_NUM_RELATIVE = "Number of Relatives helping in Business";
    public static final String BUSINESS_INFO_PERMANENT = "Permanent";
    public static final String BUSINESS_INFO_CONTRACT = "Contract No";
    public static final String BUSINESS_INFO_WAGE = "Wage Payment to Employees";
    public static final String BUSINESS_INFO_AVERAGE = "Average Wage Paid";
    public static final String BUSINESS_INFO_METHOD = "Method of Reaching out to Customers to increase business";


    public BusinessInfoOtherDetailPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return BusinessInfoOtherDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(BUSINESS_INFO_TYPE_EQUIPMENT,mData.getString(BUSINESS_INFO_TYPE_EQUIPMENT),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_DETAIL,mData.getString(BUSINESS_INFO_DETAIL),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_AGE,mData.getString(BUSINESS_INFO_AGE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_ESTIMATE,mData.getString(BUSINESS_INFO_ESTIMATE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_COLLATERAL,mData.getString(BUSINESS_INFO_COLLATERAL),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_NUM_EMP,mData.getString(BUSINESS_INFO_NUM_EMP),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_NUM_RELATIVE,mData.getString(BUSINESS_INFO_NUM_RELATIVE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_PERMANENT,mData.getString(BUSINESS_INFO_PERMANENT),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_CONTRACT,mData.getString(BUSINESS_INFO_CONTRACT),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_WAGE,mData.getString(BUSINESS_INFO_WAGE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_AVERAGE,mData.getString(BUSINESS_INFO_AVERAGE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_INFO_METHOD,mData.getString(BUSINESS_INFO_METHOD),getPageId(),-1));

            }
}
