package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class BankDetailsPage extends Page {
    public static final String BANK_NAME = "Bank Name";
    public static final String BANK_BRANCH = "Bank Branch";
    public static final String BANK_BRANCH_IFSC = "Branch IFSC Code";
    public static final String BANK_ACCOUNT_NUMBER = "Account Number";
    public static final String BANK_ACCOUNT_HOLDER_NAME = "Account Holder Name";
    public static final String BANK_ACCOUNT_TYPE = "Bank Account Type";
    public static final String BANK_ACCOUNT_OPERATIONAL = "Account Operational Since";

    public BankDetailsPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return BankDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(BANK_NAME, mData.getString(BANK_NAME), getPageId(), -1));
        dest.add(new ReviewItem(BANK_BRANCH, mData.getString(BANK_BRANCH), getPageId(), -1));

        dest.add(new ReviewItem(BANK_BRANCH_IFSC, mData.getString(BANK_BRANCH_IFSC), getPageId(), -1));
        dest.add(new ReviewItem(BANK_ACCOUNT_NUMBER, mData.getString(BANK_ACCOUNT_NUMBER), getPageId(), -1));
        dest.add(new ReviewItem(BANK_ACCOUNT_HOLDER_NAME, mData.getString(BANK_ACCOUNT_HOLDER_NAME), getPageId(), -1));

        dest.add(new ReviewItem(BANK_ACCOUNT_TYPE, mData.getString(BANK_ACCOUNT_TYPE), getPageId(), -1));
        dest.add(new ReviewItem(BANK_ACCOUNT_OPERATIONAL, mData.getString(BANK_ACCOUNT_OPERATIONAL), getPageId(), -1));
    }
}
