package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FamilyMembersPage extends Page {
    public static final String FAMILY_MEMBERS_COUNT = "Total Number of Family Members";
    public static final String MEMBER_LESS_THAN_18 = "Members Less than 18";
    public static final String MEMBER_MORE_THAN_18 = "Members Above 18";
    public static final String MEMBER_MALE_COUNT = "Male Count";
    public static final String MEMBER_FEMALE_COUNT = "Female Count";
    public static final String MEMBER_EARNING_COUNT = "Total Earning Members";

    public FamilyMembersPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return FamilyMembersFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(MEMBER_MALE_COUNT, mData.getString(MEMBER_MALE_COUNT), getPageId(), -1));
        dest.add(new ReviewItem(MEMBER_FEMALE_COUNT, mData.getString(MEMBER_FEMALE_COUNT), getPageId(), -1));
        dest.add(new ReviewItem(MEMBER_EARNING_COUNT, mData.getString(MEMBER_EARNING_COUNT), getPageId(), -1));
        dest.add(new ReviewItem(MEMBER_LESS_THAN_18, mData.getString(MEMBER_LESS_THAN_18), getPageId(), -1));
        dest.add(new ReviewItem(MEMBER_MORE_THAN_18, mData.getString(MEMBER_MORE_THAN_18), getPageId(), -1));
        dest.add(new ReviewItem(FAMILY_MEMBERS_COUNT, mData.getString(FAMILY_MEMBERS_COUNT), getPageId(), -1));
    }

//    @Override
//    public boolean isCompleted() {
//        return !TextUtils.isEmpty(mData.getString(MEMBER_MALE_COUNT)) && !TextUtils.isEmpty(mData.getString(MEMBER_FEMALE_COUNT)) && !TextUtils.isEmpty(mData.getString(MEMBER_EARNING_COUNT)) && !TextUtils.isEmpty(mData.getString(MEMBER_LESS_THAN_18)) && !TextUtils.isEmpty(mData.getString(MEMBER_MORE_THAN_18)) && !TextUtils.isEmpty(mData.getString(FAMILY_MEMBERS_COUNT));
//    }
}
