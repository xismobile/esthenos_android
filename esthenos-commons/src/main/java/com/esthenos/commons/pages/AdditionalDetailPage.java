package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class AdditionalDetailPage extends Page {

    public static final String HANDSET_TYPE = "Handset Type";
    public static final String BILLING_TYPE = "Billing Type";
    public static final String INTERNET_DATA_USES = "Internet Data Uses";
    public static final String AVERAGE_MONTHLY_BILL = "Average Monthly Bill";
    public static final String MOBILE_SERVICES_PROVIDER = "Mobile Services Provider";

    public AdditionalDetailPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return AdditionalDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(HANDSET_TYPE, mData.getString(HANDSET_TYPE), getPageId(), -1));
        dest.add(new ReviewItem(MOBILE_SERVICES_PROVIDER, mData.getString(MOBILE_SERVICES_PROVIDER), getPageId(), -1));
        dest.add(new ReviewItem(INTERNET_DATA_USES, mData.getString(INTERNET_DATA_USES), getPageId(), -1));
        dest.add(new ReviewItem(BILLING_TYPE, mData.getString(BILLING_TYPE), getPageId(), -1));
        dest.add(new ReviewItem(AVERAGE_MONTHLY_BILL, mData.getString(AVERAGE_MONTHLY_BILL), getPageId(), -1));
    }
}
