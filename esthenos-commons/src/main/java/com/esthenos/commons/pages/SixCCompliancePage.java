package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class SixCCompliancePage extends Page{

    public static final String KEY_KNOW_MEMBERS="He/She Knows all other members very well and belongs " +
                                                "to a similar socio-economic level";
    public static final String KEY_AGE_BETWEEN="He/She is aged between 21-55";
    public static final String KEY_RELATIVE="He/She doesnt have any relatives in the JLG";
    public static final String KEY_CUS_ELIGIBLE="Is the Customer eligible for Arohan's Life Insurance service";
    public static final String KEY_CUS_HERSELF="Does the customer herself/himself provide all financial information sought ?";
    public static final String KEY_CUS_WRITTEN="Is the customer eligible for written-off on death";

    public SixCCompliancePage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return SixCComplianceFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_KNOW_MEMBERS,mData.getString(KEY_KNOW_MEMBERS),getPageId(),-1));
        dest.add(new ReviewItem(KEY_RELATIVE,mData.getString(KEY_RELATIVE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_AGE_BETWEEN,mData.getString(KEY_AGE_BETWEEN),getPageId(),-1));
        dest.add(new ReviewItem(KEY_CUS_ELIGIBLE,mData.getString(KEY_CUS_ELIGIBLE),getPageId(),-1));
        dest.add(new ReviewItem(KEY_CUS_HERSELF,mData.getString(KEY_CUS_HERSELF),getPageId(),-1));
        dest.add(new ReviewItem(KEY_CUS_WRITTEN,mData.getString(KEY_CUS_WRITTEN),getPageId(),-1));

    }
}
