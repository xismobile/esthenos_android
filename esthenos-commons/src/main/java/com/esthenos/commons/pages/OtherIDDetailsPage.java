package com.esthenos.commons.pages;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class OtherIDDetailsPage extends Page {
    public static final String PAN_CARD="Pan Card";
    public static final String VOTER_ID="Voter Id";
    public static final String PASSPORT="Passport";
    public static final String RATION_CARD="Ration Card";
    public static final String DRIVING_LICENSE="Driving License";

    public OtherIDDetailsPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return OtherIDDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(VOTER_ID, mData.getString(VOTER_ID), getPageId(),-1));
        dest.add(new ReviewItem(PAN_CARD, mData.getString(PAN_CARD), getPageId(),-1));
        dest.add(new ReviewItem(PASSPORT, mData.getString(PASSPORT), getPageId(),-1));
        dest.add(new ReviewItem(RATION_CARD, mData.getString(RATION_CARD), getPageId(),-1));
        dest.add(new ReviewItem(DRIVING_LICENSE,mData.getString(DRIVING_LICENSE), getPageId(),-1));
    }
}

