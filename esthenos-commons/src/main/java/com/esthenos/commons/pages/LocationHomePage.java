package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class LocationHomePage extends Page {

    public LocationHomePage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return LocationHomeFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

    }
}
