package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class BazaarBusinessDetailsFragment extends PageFragment<BazaarBusinessDetailsPage> {

    Spinner mTypeBusiness,mNoOfBusiness,mFrequency;
    EditText mValueBulk,mPresentValue,mAverageDaly,mAverageMargin,mMonthlyProfit,
                    mCredit,mCreditBuyer,mNoOfPermanent,mNoOfContract;

    private static final String[] typebusiness={"","Retail","Wholesale","Dealership/Distributorship"};
    private static final String[] NoOfBusiness={"","1","2","3","4","5","6","7"};
    private static final String[] Frequency={"","Daily","Weekly","Monthly"};
    Autosave_Data autosave_data =new Autosave_Data();

    public static BazaarBusinessDetailsFragment create(String key) {
        BazaarBusinessDetailsFragment fragment = new BazaarBusinessDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View rootView=inflater.inflate(R.layout.fragment_bazaar_business_details, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_Bazaarbusidetailtybusi=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_TYPE_BUSINESS_KEY);
        String s_Bazaarbusidetailnoofbusi=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_NO_OF_BUSINESS_KEY);
        String s_Bazaarbusidetailmfreq=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_FREQUENCY_KEY);
        String s_Bazaarbusidetailvaluebulk=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_VALUE_BULK_KEY);
        String s_Bazaarbusidetailpresentvalue=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_PRESENT_VALUE_KEY);
        String s_Bazaarbusidetailavedeli=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_DAILY_KEY);
        String s_Bazaarbusidetailavgmargin=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MARGIN_KEY);
        String s_Bazaarbusidetailmonthlyprofit=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MONTHLY_KEY);
        String s_Bazaarbusidetailcradit=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_CREDIT_KEY);
        String s_Bazaarbusidetailbuyer=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_CREDIT_BUYER_KEY);
        String s_Bazaarbusidetailpayment=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_NO_OF_PERMANENT_KEY);
        String s_Bazaarbusidetailcontract=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_NO_OF_CONTRACT_KEY);

        mTypeBusiness=(Spinner)rootView.findViewById(R.id.spiner_type_buss);
        ArrayAdapter businessAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,typebusiness);
        businessAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mTypeBusiness.setAdapter(businessAdapter);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_TYPE_BUSINESS_KEY, mTypeBusiness.getSelectedItem().toString());
        if (!s_Bazaarbusidetailtybusi.equals("nulldata")){
            mTypeBusiness.setSelection(businessAdapter.getPosition(s_Bazaarbusidetailtybusi));
        }


        mNoOfBusiness=(Spinner)rootView.findViewById(R.id.spiner_no_of_buss);
        ArrayAdapter NoOfBusinessAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,NoOfBusiness);
        NoOfBusinessAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mNoOfBusiness.setAdapter(NoOfBusinessAdapter);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_BUSINESS_KEY, mNoOfBusiness.getSelectedItem().toString());
        if (!s_Bazaarbusidetailnoofbusi.equals("nulldata")){
            mNoOfBusiness.setSelection(NoOfBusinessAdapter.getPosition(s_Bazaarbusidetailnoofbusi));
        }


        mFrequency=(Spinner)rootView.findViewById(R.id.spiner_frequency);
        ArrayAdapter frequencyAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,Frequency);
        frequencyAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mFrequency.setAdapter(frequencyAdapter);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_FREQUENCY_KEY, mFrequency.getSelectedItem().toString());
        if (!s_Bazaarbusidetailmfreq.equals("nulldata")){
            mFrequency.setSelection(frequencyAdapter.getPosition(s_Bazaarbusidetailmfreq));
        }


        mValueBulk=(EditText)rootView.findViewById(R.id.bazaar_value_bulk_data);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_VALUE_BULK_KEY,mValueBulk.getText().toString());
        if (!s_Bazaarbusidetailvaluebulk.equals("nulldata")){
            mValueBulk.setText(s_Bazaarbusidetailvaluebulk);
        }

        mPresentValue=(EditText)rootView.findViewById(R.id.bazaar_present_value_data);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_PRESENT_VALUE_KEY,mPresentValue.getText().toString());
        if (!s_Bazaarbusidetailpresentvalue.equals("nulldata")){
            mPresentValue.setText(s_Bazaarbusidetailpresentvalue);
        }

        mAverageDaly=(EditText)rootView.findViewById(R.id.bazaar_average_sale);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_DAILY_KEY,mAverageDaly.getText().toString());

        if (!s_Bazaarbusidetailavedeli.equals("nulldata")){
            mAverageDaly.setText(s_Bazaarbusidetailavedeli);
        }

        mAverageMargin=(EditText)rootView.findViewById(R.id.bazaar_average_margin_value);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MARGIN_KEY,mAverageMargin.getText().toString());
        if (!s_Bazaarbusidetailavgmargin.equals("nulldata")){
            mAverageMargin.setText(s_Bazaarbusidetailavgmargin);
        }

        mMonthlyProfit=(EditText)rootView.findViewById(R.id.bazaar_present_monthly_value);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MONTHLY_KEY,mMonthlyProfit.getText().toString());
        if (!s_Bazaarbusidetailmonthlyprofit.equals("nulldata")){
            mMonthlyProfit.setText(s_Bazaarbusidetailmonthlyprofit);
        }

        mCredit=(EditText)rootView.findViewById(R.id.bazaar_credit_value);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_CREDIT_KEY,mCredit.getText().toString());
        if (!s_Bazaarbusidetailcradit.equals("nulldata")){
            mCredit.setText(s_Bazaarbusidetailcradit);
        }

        mCreditBuyer=(EditText)rootView.findViewById(R.id.bazaar_present_credit_buyer_value);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_CREDIT_BUYER_KEY,mCreditBuyer.getText().toString());
        if (!s_Bazaarbusidetailbuyer.equals("nulldata")){
            mCreditBuyer.setText(s_Bazaarbusidetailbuyer);
        }

        mNoOfPermanent=(EditText)rootView.findViewById(R.id.bazaar_present_no_of_permanent_value);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_PERMANENT_KEY,mNoOfPermanent.getText().toString());
        if (!s_Bazaarbusidetailpayment.equals("nulldata")){
            mNoOfPermanent.setText(s_Bazaarbusidetailpayment);
        }

        mNoOfContract=(EditText)rootView.findViewById(R.id.bazaar_present_no_of_contract);
        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_CONTRACT_KEY,mNoOfContract.getText().toString());
        if (!s_Bazaarbusidetailcontract.equals("nulldata")){
            mNoOfContract.setText(s_Bazaarbusidetailcontract);
        }


        mTypeBusiness.setOnItemSelectedListener(getOnItem());
        mNoOfBusiness.setOnItemSelectedListener(getOnItem());
        mFrequency.setOnItemSelectedListener(getOnItem());

        mValueBulk.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mValueBulk));
        mPresentValue.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mPresentValue));
        mAverageDaly.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mAverageDaly));
        mAverageMargin.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mAverageMargin));
        mMonthlyProfit.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mMonthlyProfit));
        mCredit.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mCredit));
        mCreditBuyer.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mCreditBuyer));
        mNoOfPermanent.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mNoOfPermanent));
        mNoOfContract.addTextChangedListener(new GenericTextBazaarbusinessdedtailfrag(mNoOfContract));
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanseState){
        super.onViewCreated(view,savedInstanseState);

//        mTypeBusiness.setOnItemSelectedListener(getOnItemSelectedListener(BazaarBusinessDetailsPage.BAZAAR_TYPE_BUSINESS_KEY));
//        mNoOfBusiness.setOnItemSelectedListener(getOnItemSelectedListener(BazaarBusinessDetailsPage.BAZAAR_NO_OF_BUSINESS_KEY));
//        mFrequency.setOnItemSelectedListener(getOnItemSelectedListener(BazaarBusinessDetailsPage.BAZAAR_FREQUENCY_KEY));
//
//        mValueBulk.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_VALUE_BULK_KEY));
//        mPresentValue.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_PRESENT_VALUE_KEY));
//        mAverageDaly.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_DAILY_KEY));
//        mAverageMargin.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MARGIN_KEY));
//        mMonthlyProfit.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MONTHLY_KEY));
//        mCredit.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_CREDIT_KEY));
//        mCreditBuyer.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_CREDIT_BUYER_KEY));
//        mNoOfPermanent.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_NO_OF_PERMANENT_KEY));
//        mNoOfContract.addTextChangedListener(getTextWatcher(BazaarBusinessDetailsPage.BAZAAR_NO_OF_CONTRACT_KEY));

    }

    class GenericTextBazaarbusinessdedtailfrag implements TextWatcher {

        private View view;

        private GenericTextBazaarbusinessdedtailfrag(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.bazaar_value_bulk_data) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_VALUE_BULK_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_VALUE_BULK_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_present_value_data) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_PRESENT_VALUE_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_PRESENT_VALUE_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_average_sale) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_DAILY_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_DAILY_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_average_margin_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MARGIN_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MARGIN_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_present_monthly_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MONTHLY_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_AVERAGE_MONTHLY_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_credit_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_CREDIT_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_CREDIT_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_present_credit_buyer_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_CREDIT_BUYER_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_CREDIT_BUYER_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_present_no_of_permanent_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_NO_OF_PERMANENT_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_PERMANENT_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazaar_present_no_of_contract) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarBusinessDetailsPage.BAZAAR_NO_OF_CONTRACT_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_CONTRACT_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.spiner_type_buss) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        autosave_data.putdata(getContext(),mPage.getFormId(), BazaarBusinessDetailsPage.BAZAAR_TYPE_BUSINESS_KEY, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_TYPE_BUSINESS_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.spiner_no_of_buss) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), BazaarBusinessDetailsPage.BAZAAR_NO_OF_BUSINESS_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_NO_OF_BUSINESS_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.spiner_frequency) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), BazaarBusinessDetailsPage.BAZAAR_FREQUENCY_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(BazaarBusinessDetailsPage.BAZAAR_FREQUENCY_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }


}