package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class NomineeDetailsPage extends Page {
    public static final String AGE_DATA_KEY = "Nominee Age";
    public static final String NAME_DATA_KEY = "Nominee Name";
    public static final String PHONE_DATA_KEY = "Nominee Phone";
    public static final String GENDER_DATA_KEY = "Nominee Gender";
    public static final String RELATION_DATA_KEY = "Nominee Relation";

    public NomineeDetailsPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return NomineeDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(NAME_DATA_KEY, mData.getString(NAME_DATA_KEY), getPageId(), -1));
        dest.add(new ReviewItem(AGE_DATA_KEY, mData.getString(AGE_DATA_KEY), getPageId(), -1));
        dest.add(new ReviewItem(GENDER_DATA_KEY, mData.getString(GENDER_DATA_KEY), getPageId(), -1));
        dest.add(new ReviewItem(PHONE_DATA_KEY, mData.getString(PHONE_DATA_KEY), getPageId(), -1));
        dest.add(new ReviewItem(RELATION_DATA_KEY, mData.getString(RELATION_DATA_KEY), getPageId(), -1));
    }
}