package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;


public class LoanDetailsFragment extends PageFragment<LoanDetailsPage> {

    private String sourceData[]={"","Bank","Credit Co-operative Society","Relatives","Moneylender","Savings Group"};

    @NotEmpty
    private EditText mBankName, mLoanType, mLoanAmount, mTenure, mInterest, mEmi, mCollateral, mOutstandingLoan;
    private Spinner mSourceSpinner;

    public static LoanDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        LoanDetailsFragment fragment = new LoanDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_borrowing_loan, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mSourceSpinner=(Spinner)rootView.findViewById(R.id.borrower_source_spinner);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,sourceData);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSourceSpinner.setAdapter(adapter);
        mPage.getData().putString(LoanDetailsPage.SOUCE_LOAN_KEY,mSourceSpinner.getSelectedItem().toString());

        mBankName = (EditText) rootView.findViewById(R.id.bank_name);
        mPage.getData().putString(LoanDetailsPage.BANK_NAME_KEY, mBankName.getText().toString());

        mLoanType = (EditText) rootView.findViewById(R.id.loan_type);
        mPage.getData().putString(LoanDetailsPage.LOAN_TYPE_KEY, mLoanType.getText().toString());

        mLoanAmount = (EditText) rootView.findViewById(R.id.loan_amount);
        mPage.getData().putString(LoanDetailsPage.LOAN_AMOUNT_KEY, mLoanAmount.getText().toString());

        mTenure = (EditText) rootView.findViewById(R.id.tenure_month);
        mPage.getData().putString(LoanDetailsPage.TENURE_KEY, mTenure.getText().toString());

        mInterest = (EditText) rootView.findViewById(R.id.interestfield);
        mPage.getData().putString(LoanDetailsPage.INTEREST_KEY, mInterest.getText().toString());

        mEmi = (EditText) rootView.findViewById(R.id.emipayment);
        mPage.getData().putString(LoanDetailsPage.EMI_KEY, mEmi.getText().toString());

        mCollateral = (EditText) rootView.findViewById(R.id.coll_detail);
        mPage.getData().putString(LoanDetailsPage.COLLATERAL_KEY, mCollateral.getText().toString());

        mOutstandingLoan = (EditText) rootView.findViewById(R.id.out_loan);
        mPage.getData().putString(LoanDetailsPage.OUTSTANDING_LOAN_KEY, mOutstandingLoan.getText().toString());

        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSourceSpinner.setOnItemSelectedListener(getOnItemSelectedListener(LoanDetailsPage.SOUCE_LOAN_KEY));
        mBankName.addTextChangedListener(getTextWatcher(LoanDetailsPage.BANK_NAME_KEY));
        mLoanAmount.addTextChangedListener(getTextWatcher(LoanDetailsPage.LOAN_AMOUNT_KEY));
        mLoanType.addTextChangedListener(getTextWatcher(LoanDetailsPage.LOAN_TYPE_KEY));
        mTenure.addTextChangedListener(getTextWatcher(LoanDetailsPage.TENURE_KEY));
        mInterest.addTextChangedListener(getTextWatcher(LoanDetailsPage.INTEREST_KEY));
        mEmi.addTextChangedListener(getTextWatcher(LoanDetailsPage.EMI_KEY));
        mCollateral.addTextChangedListener(getTextWatcher(LoanDetailsPage.COLLATERAL_KEY));
        mOutstandingLoan.addTextChangedListener(getTextWatcher(LoanDetailsPage.OUTSTANDING_LOAN_KEY));
    }
}
