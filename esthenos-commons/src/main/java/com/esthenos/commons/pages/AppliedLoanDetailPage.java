package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class AppliedLoanDetailPage extends Page{

    public static final String LOAN_PURPOSE_KEY = "Main Loan Purpose";
    public static final String LOAN_REQUIRED_KEY = "Required Loan Amount";
    public static final String LOAN_REPAYMENT_KEY = "Repayment Option";
    public static final String LOAN_SUB_PURPOSE_KEY = "Sub Purpose";
    public static final String LOAN_AMOUNT_USED1_KEY = "Loan Amount to be used by 1";
    public static final String LOAN_AMOUNT_USED2_KEY = "Loan Amount to be used by 2";

    public AppliedLoanDetailPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return AppliedLoanDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(LOAN_PURPOSE_KEY,mData.getString(LOAN_PURPOSE_KEY), getPageId(),-1));
        dest.add(new ReviewItem(LOAN_REQUIRED_KEY,mData.getString(LOAN_REQUIRED_KEY), getPageId(),-1));
        dest.add(new ReviewItem(LOAN_REPAYMENT_KEY,mData.getString(LOAN_REPAYMENT_KEY), getPageId(),-1));
        dest.add(new ReviewItem(LOAN_SUB_PURPOSE_KEY,mData.getString(LOAN_SUB_PURPOSE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(LOAN_AMOUNT_USED1_KEY,mData.getString(LOAN_AMOUNT_USED1_KEY),getPageId(),-1));
        dest.add(new ReviewItem(LOAN_AMOUNT_USED2_KEY,mData.getString(LOAN_AMOUNT_USED2_KEY),getPageId(),-1));
    }
//    @Override
//    public boolean isCompleted(){
//        return  !TextUtils.isEmpty(mData.getString(LOAN_PURPOSE_KEY))&& !TextUtils.isEmpty(mData.getString(LOAN_REQUIRED_KEY))&&
//                !TextUtils.isEmpty(mData.getString(LOAN_REPAYMENT_KEY))&& !TextUtils.isEmpty(mData.getString(LOAN_SUB_PURPOSE_KEY))&&
//                !TextUtils.isEmpty(mData.getString(LOAN_AMOUNT_USED1_KEY));
//    }
}
