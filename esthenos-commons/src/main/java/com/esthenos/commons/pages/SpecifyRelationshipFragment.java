package com.esthenos.commons.pages;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class SpecifyRelationshipFragment extends PageFragment<SpecifyRelationshipPage> {

    private EditText mRelation;

    public static SpecifyRelationshipFragment create(String key) {
        SpecifyRelationshipFragment fragment = new SpecifyRelationshipFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_specify_relationship, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mRelation=(EditText)rootView.findViewById(R.id.specify_relation);
        mPage.getData().putString(SpecifyRelationshipPage.SPECIFY_RELATION, mRelation.getText().toString());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanseState){
        super.onViewCreated(view,savedInstanseState);

        mRelation.addTextChangedListener(getTextWatcher(SpecifyRelationshipPage.SPECIFY_RELATION));

    }
}
