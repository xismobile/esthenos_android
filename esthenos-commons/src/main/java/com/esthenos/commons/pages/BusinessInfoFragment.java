package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

public class BusinessInfoFragment extends PageFragment<BusinessInfoPage> {

    private EditText mBussName,mBussDesc,mBussGood,mBussRaw,mBussPan,mBussTax,mBussShop,mBussAddress,mBussArea,mBussareaValue;

    public static BusinessInfoFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        BusinessInfoFragment fragment = new BusinessInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (BusinessInfoPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_business_info, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mBussName = (EditText) rootView.findViewById(R.id.buss_name);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_NAME, mBussName.getText().toString());

        mBussDesc = (EditText) rootView.findViewById(R.id.buss_desc);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_DESC, mBussDesc.getText().toString());

        mBussGood = (EditText) rootView.findViewById(R.id.buss_goods);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_GOODS_DETAIL,mBussGood.getText().toString());

        mBussRaw = (EditText) rootView.findViewById(R.id.buss_materisl);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_RAW_DETAIL, mBussRaw.getText().toString());

        mBussPan = (EditText) rootView.findViewById(R.id.buss_pancard);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_PANCARD, mBussPan.getText().toString());

        mBussTax = (EditText) rootView.findViewById(R.id.buss_tax);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_TAX, mBussTax.getText().toString());

        mBussShop = (EditText) rootView.findViewById(R.id.buss_establish);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_SHOP_EST, mBussShop.getText().toString());

        mBussAddress = (EditText) rootView.findViewById(R.id.buss_address);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_ADDRESS, mBussAddress.getText().toString());

        mBussArea = (EditText) rootView.findViewById(R.id.buss_area);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_AREA, mBussArea.getText().toString());

        mBussareaValue = (EditText) rootView.findViewById(R.id.buss_area_value);
        mPage.getData().putString(BusinessInfoPage.BUSINESS_AREA_VALUE, mBussareaValue.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBussTax.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_TAX));
        mBussName.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_NAME));
        mBussDesc.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_DESC));
        mBussArea.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_AREA));
        mBussPan.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_PANCARD));
        mBussShop.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_SHOP_EST));
        mBussRaw.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_RAW_DETAIL));
        mBussAddress.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_ADDRESS));
        mBussGood.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_GOODS_DETAIL));
        mBussareaValue.addTextChangedListener(getTextWatcher(BusinessInfoPage.BUSINESS_AREA_VALUE));
    }
}
