package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.filters.InputFilterMinMax;
import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class NomineeDetailsFragment extends PageFragment<NomineeDetailsPage> {

    final String[] relations = {"","Father", "Spouse", "Son", "Daughter", "Sister", "Others", "None"};

    private Spinner mNomineeRelation,mNomineeGender;
    private EditText mNomineeName, mNomineeAge, mNomineePhone;
    private final String[] sexdata = {"","Male", "Female"};

    public static NomineeDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        NomineeDetailsFragment fragment = new NomineeDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_insurance_nominee_page, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mNomineeName = (EditText) rootView.findViewById(R.id.nominee_name);
        mPage.getData().putString(NomineeDetailsPage.NAME_DATA_KEY, mNomineeName.getText().toString());

        mNomineeGender = (Spinner) rootView.findViewById(R.id.nominee_gender);
        ArrayAdapter<String> adapter=new ArrayAdapter<>(getActivity(),R.layout.layout_spinner_item,sexdata);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mNomineeGender.setAdapter(adapter);
        mPage.getData().putString(NomineeDetailsPage.GENDER_DATA_KEY, mNomineeGender.getSelectedItem().toString());

        mNomineeAge = (EditText) rootView.findViewById(R.id.nominee_age);
        mNomineeAge.setFilters(new InputFilter[]{new InputFilterMinMax(0, 100)});
        mPage.getData().putString(NomineeDetailsPage.AGE_DATA_KEY, mNomineeAge.getText().toString());

        mNomineePhone = (EditText) rootView.findViewById(R.id.nominee_phone);
        mPage.getData().putString(NomineeDetailsPage.PHONE_DATA_KEY, mNomineePhone.getText().toString());

        mNomineeRelation = (Spinner) rootView.findViewById(R.id.nominee_relation);
        ArrayAdapter<String> adapterRelations = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, relations);
        adapterRelations.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mNomineeRelation.setAdapter(adapterRelations);
        mPage.getData().putString(NomineeDetailsPage.RELATION_DATA_KEY, mNomineeRelation.getSelectedItem().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNomineeAge.addTextChangedListener(getTextWatcher(NomineeDetailsPage.AGE_DATA_KEY));
        mNomineeName.addTextChangedListener(getTextWatcher(NomineeDetailsPage.NAME_DATA_KEY));
        mNomineePhone.addTextChangedListener(getTextWatcher(NomineeDetailsPage.PHONE_DATA_KEY));
        mNomineeGender.setOnItemSelectedListener(getOnItemSelectedListener(NomineeDetailsPage.GENDER_DATA_KEY));
        mNomineeRelation.setOnItemSelectedListener(getOnItemSelectedListener(NomineeDetailsPage.RELATION_DATA_KEY));
    }
}
