package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class CustomerSupplierPage extends Page {

    public static final String CUSTOMER_NAME_KEY ="Name";
    public static final String CUSTOMER_Address_KEY ="Address";
    public static final String CUSTOMER_TELEPHONE_KEY ="Telephone No";
    public static final String SUPPLIER_NAME ="Name";
    public static final String SUPPLIER_ADDRESS ="Address";
    public static final String SUPPLIER_TELEPHONE ="TelephoneNo";

    public CustomerSupplierPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return CustomerSupplierFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(CUSTOMER_NAME_KEY,mData.getString(CUSTOMER_NAME_KEY),getPageId(),-1));
        dest.add(new ReviewItem(CUSTOMER_Address_KEY,mData.getString(CUSTOMER_Address_KEY),getPageId(),-1));
        dest.add(new ReviewItem(CUSTOMER_TELEPHONE_KEY,mData.getString(CUSTOMER_TELEPHONE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(SUPPLIER_NAME,mData.getString(SUPPLIER_NAME),getPageId(),-1));
        dest.add(new ReviewItem(SUPPLIER_ADDRESS,mData.getString(SUPPLIER_ADDRESS),getPageId(),-1));
        dest.add(new ReviewItem(SUPPLIER_TELEPHONE,mData.getString(SUPPLIER_TELEPHONE),getPageId(),-1));
    }
}
