package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;

public class CreditCardDetailFragment extends PageFragment<CreditCardPage> {

    @NotEmpty
    private EditText mCardNum,mIssue;

    public static CreditCardDetailFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        CreditCardDetailFragment fragment = new CreditCardDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CreditCardPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_credit_card_detail, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mCardNum = (EditText) rootView.findViewById(R.id.card_number);
        mPage.getData().putString(CreditCardPage.CARD_NUM, mCardNum.getText().toString());

        mIssue = (EditText) rootView.findViewById(R.id.card_issue);
        mPage.getData().putString(CreditCardPage.ISSUE_BANK, mIssue.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCardNum.addTextChangedListener(getTextWatcher(CreditCardPage.CARD_NUM));
        mIssue.addTextChangedListener(getTextWatcher(CreditCardPage.ISSUE_BANK));
    }
}
