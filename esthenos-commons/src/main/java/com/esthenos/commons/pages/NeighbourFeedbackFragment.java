package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;


public class NeighbourFeedbackFragment extends PageFragment<NeighbourFeedbackPage> {
    public static final String TABLE_NEIGHBOUR = "NeighbourFeedBack";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;

    private EditText mOccupationFirst,mNameFirst,mLocationFirst,mPhoneFirst,
            mOccupationSecond,mNameSecond,mLocationSecond,mPhoneSecond;
    private Spinner mRemarkSecond,mRemarksFirst;

    private String[] remarks={"","Good","Average","Bad"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static NeighbourFeedbackFragment create(String key) {
        NeighbourFeedbackFragment fragment = new NeighbourFeedbackFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_neighbour_feedback, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_neighbourNamefirst=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_NAME);
        String s_neighbourlocaionfirst=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_LOCATION);
        String s_neighbourOccupationfirst=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_OCUPATION);
        String s_neighbourPhonefirst=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_PHONE);
        String s_neighbourRemarkfirst=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_REMARKS);
        String s_neighbourNamesecond=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_NAME_SECOND);
        String s_neighbourlocationsecond=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_LOCATION_SECOND);
        String s_neighbourOcupationsecond=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_OCUPATION_SECOND);
        String s_neighbourPhonesecond=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_PHONE_SECOND);
        String s_neighbourRemarksecond=autosave_data.getdata(getContext(), mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_REMARKS_SECOND);

        mydb = new MySQLiteDB(getContext());
       // autosave = new AutoSaveData(getContext());

        mOccupationFirst=(EditText)rootView.findViewById(R.id.neighbour_occupation_value);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION, mOccupationFirst.getText().toString());
        if (!s_neighbourOccupationfirst.equals("nulldata")){
            mOccupationFirst.setText(s_neighbourOccupationfirst);
        }

        mNameFirst=(EditText)rootView.findViewById(R.id.neighbour_name_value);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_NAME,mNameFirst.getText().toString());
        if (!s_neighbourNamefirst.equals("nulldata")){
            mNameFirst.setText(s_neighbourNamefirst);
        }


        mLocationFirst=(EditText)rootView.findViewById(R.id.neighbour_location_value);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_LOCATION, mLocationFirst.getText().toString());
        if (!s_neighbourlocaionfirst.equals("nulldata")){
            mLocationFirst.setText(s_neighbourlocaionfirst);
        }


        mPhoneFirst=(EditText)rootView.findViewById(R.id.neighbour_phone_value);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_PHONE,mPhoneFirst.getText().toString());
        if (! s_neighbourPhonefirst.equals("nulldata")){
            mPhoneFirst.setText( s_neighbourPhonefirst);
        }


        mRemarksFirst=(Spinner)rootView.findViewById(R.id.neighbour_remarks_value);
        ArrayAdapter remarksAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,remarks);
        remarksAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRemarksFirst.setAdapter(remarksAdapter);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_REMARKS, mRemarksFirst.getSelectedItem().toString());
        if (!s_neighbourRemarkfirst.equals("nulldata")){
            mRemarksFirst.setSelection(remarksAdapter.getPosition(s_neighbourRemarkfirst));
        }


        mOccupationSecond=(EditText)rootView.findViewById(R.id.neighbour_occupation_value_2);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION_SECOND, mOccupationSecond.getText().toString());
//        mOccupationSecond.setOnFocusChangeListener(new View.OnFocusChangeListener() {
        if (!s_neighbourOcupationsecond.equals("nulldata")){
            mOccupationSecond.setText(s_neighbourOcupationsecond);
        }



        mNameSecond=(EditText)rootView.findViewById(R.id.neighbour_name_value_2);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_NAME_SECOND,mNameSecond.getText().toString());
        if (!s_neighbourNamesecond.equals("nulldata")){
            mNameSecond.setText(s_neighbourNamesecond);
        }


        mLocationSecond=(EditText)rootView.findViewById(R.id.neighbour_location_value_2);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_LOCATION_SECOND, mLocationSecond.getText().toString());
        if (!s_neighbourlocationsecond.equals("nulldata")){
            mLocationSecond.setText(s_neighbourlocationsecond);
        }


        mPhoneSecond=(EditText)rootView.findViewById(R.id.neighbour_phone_value_2);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_PHONE_SECOND,mPhoneSecond.getText().toString());
        if (!s_neighbourPhonesecond.equals("nulldata")){
            mPhoneSecond.setText(s_neighbourPhonesecond);
        }



        mRemarkSecond=(Spinner)rootView.findViewById(R.id.neighbour_remarks_value_2);
        ArrayAdapter remarkSecondAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,remarks);
        remarkSecondAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRemarkSecond.setAdapter(remarkSecondAdapter);
        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_REMARKS_SECOND,mRemarkSecond.getSelectedItem().toString());
        if (!s_neighbourRemarksecond.equals("nulldata")){
            mRemarkSecond.setSelection(remarkSecondAdapter.getPosition(s_neighbourRemarksecond));
        }



        mRemarksFirst.setOnItemSelectedListener(getOnItem());
        mLocationFirst.addTextChangedListener(new GenericTextNeibhourFeedback(mLocationFirst));
        mNameFirst.addTextChangedListener(new GenericTextNeibhourFeedback(mNameFirst));
        mOccupationFirst.addTextChangedListener(new GenericTextNeibhourFeedback(mOccupationFirst));
        mPhoneFirst.addTextChangedListener(new GenericTextNeibhourFeedback(mPhoneFirst));

        mRemarkSecond.setOnItemSelectedListener(getOnItem());
        mLocationSecond.addTextChangedListener(new GenericTextNeibhourFeedback(mLocationSecond));
        mNameSecond.addTextChangedListener(new GenericTextNeibhourFeedback(mNameSecond));
        mOccupationSecond.addTextChangedListener(new GenericTextNeibhourFeedback(mOccupationSecond));
        mPhoneSecond.addTextChangedListener(new GenericTextNeibhourFeedback(mPhoneSecond));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstenState){
        super.onViewCreated(view,savedInstenState);

//        mRemarksFirst.setOnItemSelectedListener(getOnItemSelectedListener(NeighbourFeedbackPage.NEIGHBOUR_REMARKS));
//        mLocationFirst.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_LOCATION));
//        mNameFirst.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_NAME));
//        mOccupationFirst.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION));
//        mPhoneFirst.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_PHONE));
//
//        mRemarkSecond.setOnItemSelectedListener(getOnItemSelectedListener(NeighbourFeedbackPage.NEIGHBOUR_REMARKS_SECOND));
//        mLocationSecond.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_LOCATION_SECOND));
//        mNameSecond.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_NAME_SECOND));
//        mOccupationSecond.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION_SECOND));
//        mPhoneSecond.addTextChangedListener(getTextWatcher(NeighbourFeedbackPage.NEIGHBOUR_PHONE_SECOND));
//        mRemarksFirst.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//              //  mydb.Autosavebyid(TABLE_NEIGHBOUR, MySQLiteDB.NEIGHBOUR_REMARKS, i, value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        mRemarkSecond.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
////                mydb.Autosavebyid(TABLE_NEIGHBOUR, MySQLiteDB.NEIGHBOUR_REMARKS_SECOND, i, value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.neighbour_remarks_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), NeighbourFeedbackPage.NEIGHBOUR_REMARKS, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_REMARKS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.neighbour_remarks_value_2) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), NeighbourFeedbackPage.NEIGHBOUR_REMARKS_SECOND, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_REMARKS_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

    class GenericTextNeibhourFeedback implements TextWatcher {

        private View view;

        private GenericTextNeibhourFeedback(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.neighbour_name_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_NAME,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_NAME, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_occupation_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_OCUPATION,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_location_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_LOCATION,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_LOCATION, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_phone_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_PHONE,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_PHONE, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_occupation_value_2) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_OCUPATION_SECOND,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_OCUPATION_SECOND, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_name_value_2) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_NAME_SECOND,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_NAME_SECOND, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_location_value_2) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_LOCATION_SECOND,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_LOCATION_SECOND, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.neighbour_phone_value_2) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),NeighbourFeedbackPage.NEIGHBOUR_PHONE_SECOND,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(NeighbourFeedbackPage.NEIGHBOUR_PHONE_SECOND, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
