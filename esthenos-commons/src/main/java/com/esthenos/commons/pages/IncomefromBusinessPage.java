package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class IncomefromBusinessPage extends Page {

    public static final String INCOME_DAILY_SALE = "Avg Daily Sale";
    public static final String INCOME_DAILY_NO_BUSINESS = "No of Business day in a week";
    public static final String INCOME_DAILY_TOTAL_SALE = "total sale";
    public static final String INCOME_DAILY_TOTAL_PURCHAGE = "purchase";
    public static final String INCOME_DAILY_TOTAL_FREC_PURCHASE = "Frequency of purchase";
    public static final String INCOME_DAILY_TOTAL_CALCULATE = "calculate Cost of Raw Material";
    public static final String INCOME_DAILY_OTHER = "Other Expenses";
    public static final String INCOME_DAILY_INTEREST = "Interest payment to supplier";
    public static final String INCOME_DAILY_TOTAL_COST = "Total Costing";
    public static final String INCOME_DAILY_TOTAL_PROFIT = "Total Profit/Income From Business(1-2)";

    public IncomefromBusinessPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return IncomefromBusinessFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(INCOME_DAILY_TOTAL_SALE,mData.getString(INCOME_DAILY_TOTAL_SALE),getPageId(),-1));
        dest.add(new ReviewItem(INCOME_DAILY_TOTAL_COST,mData.getString(INCOME_DAILY_TOTAL_COST),getPageId(),-1));
        dest.add(new ReviewItem(INCOME_DAILY_TOTAL_PROFIT,mData.getString(INCOME_DAILY_TOTAL_PROFIT),getPageId(),-1));

    }

}
