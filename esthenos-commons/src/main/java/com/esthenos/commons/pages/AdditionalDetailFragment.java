package com.esthenos.commons.pages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;


public class AdditionalDetailFragment extends PageFragment<AdditionalDetailPage> {

    EditText mMonthlyBill;
    Button mSeviceProvider, mHandsetType, mNetPack, mBilling;

    String[] BillingType = {" ","Prepaid", "Postpaid"};
    String[] HandsetType = {" ","Regular", "Smart Phone"};
    String[] InternetData = {" ","Yes", "No"};
    String[] ProviderName = {" ","Airtel", "Vodafone", "Idea", "MTNL", "Tata", "Reliance"};

    public static AdditionalDetailFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        AdditionalDetailFragment fragment = new AdditionalDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (AdditionalDetailPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_additional_details, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mSeviceProvider = (Button) rootView.findViewById(R.id.select_mobileservices);
        mSeviceProvider.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Service Provider");
                builder.setSingleChoiceItems(ProviderName, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        mSeviceProvider.setText(ProviderName[item]);
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mHandsetType = (Button) rootView.findViewById(R.id.select_handsettype);
        mHandsetType.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Handset Type");
                builder.setSingleChoiceItems(HandsetType, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        mHandsetType.setText(HandsetType[item]);
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mNetPack = (Button) rootView.findViewById(R.id.select_Data);
        mNetPack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Internet Data Usage");
                builder.setSingleChoiceItems(InternetData, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        mNetPack.setText(InternetData[item]);
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mBilling = (Button) rootView.findViewById(R.id.select_billing);
        mBilling.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Billing Type");
                builder.setSingleChoiceItems(BillingType, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        mBilling.setText(BillingType[item]);
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mMonthlyBill = (EditText) rootView.findViewById(R.id.monthlybill);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mNetPack.addTextChangedListener(getTextWatcher(AdditionalDetailPage.INTERNET_DATA_USES));
        mBilling.addTextChangedListener(getTextWatcher(AdditionalDetailPage.BILLING_TYPE));
        mHandsetType.addTextChangedListener(getTextWatcher(AdditionalDetailPage.HANDSET_TYPE));
        mMonthlyBill.addTextChangedListener(getTextWatcher(AdditionalDetailPage.AVERAGE_MONTHLY_BILL));
        mSeviceProvider.addTextChangedListener(getTextWatcher(AdditionalDetailPage.MOBILE_SERVICES_PROVIDER));
    }
}
