package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class SixComplianceBazaarPage extends Page{

    public static final String SIX_BUSINESS_BAZAAR="He/She has a business in the bazar of three years";
    public static final String SIX_KNOW_MEMBERS="He/She Knows all other members very well and belongs " +
                                                                "to a similar socio-economic level";
    public static final String SIX_LICENSE="He/she has license for undertaking the business";
    public static final String SIX_RELATIVE="He/She doesnt have any relatives in the JLG";
    public static final String SIX_AGE_BETWEEN="He/She is aged between 21-55";

    public SixComplianceBazaarPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return SixComplianceBazaarFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(SIX_BUSINESS_BAZAAR,mData.getString(SIX_BUSINESS_BAZAAR),getPageId(),-1));
        dest.add(new ReviewItem(SIX_KNOW_MEMBERS,mData.getString(SIX_KNOW_MEMBERS),getPageId(),-1));
        dest.add(new ReviewItem(SIX_LICENSE,mData.getString(SIX_LICENSE),getPageId(),-1));
        dest.add(new ReviewItem(SIX_RELATIVE,mData.getString(SIX_RELATIVE),getPageId(),-1));
        dest.add(new ReviewItem(SIX_AGE_BETWEEN,mData.getString(SIX_AGE_BETWEEN),getPageId(),-1));

    }
}
