package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class ElectricityDetailsPage extends Page {

    public static final String POWER_SUPPLIER = "Power Supplier";
    public static final String POWER_MONTHLY_BILL = "Electricity Monthly Bill";

    public ElectricityDetailsPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return ElectricityDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(POWER_SUPPLIER, mData.getString(POWER_SUPPLIER), getPageId(), -1));
        dest.add(new ReviewItem(POWER_MONTHLY_BILL, mData.getString(POWER_MONTHLY_BILL), getPageId(), -1));
    }
}
