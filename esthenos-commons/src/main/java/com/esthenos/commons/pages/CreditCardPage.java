package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class CreditCardPage extends Page {
    public static final String CARD_NUM = "Card No.";
    public static final String ISSUE_BANK = "Issue Bank";

    public CreditCardPage(ModelCallbacks callbacks ,PageConfig pageConfig) {
        super(callbacks,pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return CreditCardDetailFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(CARD_NUM, mData.getString(CARD_NUM), getPageId(), -1));
        dest.add(new ReviewItem(ISSUE_BANK, mData.getString(ISSUE_BANK), getPageId(), -1));

    }
}

