package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FacilitatedProductPage extends Page {
    public static final String FACILITATED_INSURANCE_KEY= "Insurance";
    public static final String FACILITATED_NAME_KEY= "Name";
    public static final String FACILITATED_AGE_KEY= "Age";
    public static final String FACILITATED_GENDER_KEY= "Gender";
    public static final String FACILITATED_RELATIONSHIP_KEY= "Relationship";
    public static final String FACILITATED_NOMINEE_KEY= "Nominee is Minor..?";
    public static final String FACILITATED_SOLAR_KEY= "Solar Lamp";
    public static final String FACILITATED_NAME_APPOINTEE= "Name of Appointee";


    public FacilitatedProductPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FacilitatedProductFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(FACILITATED_INSURANCE_KEY,mData.getString(FACILITATED_INSURANCE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_NAME_KEY,mData.getString(FACILITATED_NAME_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_AGE_KEY,mData.getString(FACILITATED_AGE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_GENDER_KEY,mData.getString(FACILITATED_GENDER_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_RELATIONSHIP_KEY,mData.getString(FACILITATED_RELATIONSHIP_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_NOMINEE_KEY,mData.getString(FACILITATED_NOMINEE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_SOLAR_KEY,mData.getString(FACILITATED_SOLAR_KEY),getPageId(),-1));
        dest.add(new ReviewItem(FACILITATED_NAME_APPOINTEE,mData.getString(FACILITATED_NAME_APPOINTEE),getPageId(),-1));
    }
//    @Override
//    public boolean isCompleted(){
//        return  !TextUtils.isEmpty(mData.getString(FACILITATED_INSURANCE_KEY)) && !TextUtils.isEmpty(mData.getString(FACILITATED_NAME_KEY)); //&&
//                !TextUtils.isEmpty(mData.getString(FACILITATED_AGE_KEY))&& !TextUtils.isEmpty(mData.getString(FACILITATED_GENDER_KEY))&&
//                !TextUtils.isEmpty(mData.getString(FACILITATED_RELATIONSHIP_KEY))&& !TextUtils.isEmpty(mData.getString(FACILITATED_NOMINEE_KEY))&&
//                !TextUtils.isEmpty(mData.getString(FACILITATED_SOLAR_KEY));
  //  }
}
