package com.esthenos.commons.pages;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class SixComplianceBazaarFragment extends PageFragment<SixComplianceBazaarPage> {

    Spinner mKnowMember,mAgeBetween,mRelative,mBusbazaar,mLicense;

    String[] sppinerValue={"","NO","YES"};

    public static SixComplianceBazaarFragment create(String key) {
        SixComplianceBazaarFragment fragment = new SixComplianceBazaarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_six_compliance_bazaar, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mKnowMember=(Spinner)rootView.findViewById(R.id.six_know_member_value);
        ArrayAdapter adapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinerValue);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mKnowMember.setAdapter(adapter);
        mPage.getData().putString(SixComplianceBazaarPage.SIX_KNOW_MEMBERS, mKnowMember.getSelectedItem().toString());

        mAgeBetween=(Spinner)rootView.findViewById(R.id.six_age_between_value);
        mAgeBetween.setAdapter(adapter);
        mPage.getData().putString(SixComplianceBazaarPage.SIX_AGE_BETWEEN,mAgeBetween.getSelectedItem().toString());

        mRelative=(Spinner)rootView.findViewById(R.id.six_relative_value);
        mRelative.setAdapter(adapter);
        mPage.getData().putString(SixComplianceBazaarPage.SIX_RELATIVE,mRelative.getSelectedItem().toString());

        mBusbazaar=(Spinner)rootView.findViewById(R.id.six_bus_bazaar_value);
        mBusbazaar.setAdapter(adapter);
        mPage.getData().putString(SixComplianceBazaarPage.SIX_BUSINESS_BAZAAR,mBusbazaar.getSelectedItem().toString());

        mLicense=(Spinner)rootView.findViewById(R.id.six_license_value);
        mLicense.setAdapter(adapter);
        mPage.getData().putString(SixComplianceBazaarPage.SIX_LICENSE,mLicense.getSelectedItem().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle bundle){
        super.onViewCreated(view,bundle);

        mBusbazaar.setOnItemSelectedListener(getOnItemSelectedListener(SixComplianceBazaarPage.SIX_BUSINESS_BAZAAR));
        mKnowMember.setOnItemSelectedListener(getOnItemSelectedListener(SixComplianceBazaarPage.SIX_KNOW_MEMBERS));
        mAgeBetween.setOnItemSelectedListener(getOnItemSelectedListener(SixComplianceBazaarPage.SIX_AGE_BETWEEN));
        mRelative.setOnItemSelectedListener(getOnItemSelectedListener(SixComplianceBazaarPage.SIX_RELATIVE));
        mLicense.setOnItemSelectedListener(getOnItemSelectedListener(SixComplianceBazaarPage.SIX_LICENSE));
    }
}
