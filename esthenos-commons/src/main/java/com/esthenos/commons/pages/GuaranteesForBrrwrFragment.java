package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;


public class GuaranteesForBrrwrFragment extends PageFragment<GuaranteesForBrrwrPage> {

    @NotEmpty
    private EditText mGuaranterName,mGuaranterAmount,mGuaranterSociety;

    public static GuaranteesForBrrwrFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        GuaranteesForBrrwrFragment fragment = new GuaranteesForBrrwrFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (GuaranteesForBrrwrPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_guarantees_for_brrwr, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mGuaranterName = (EditText) rootView.findViewById(R.id.gurantors_name);
        mPage.getData().putString(CreditCardPage.CARD_NUM, mGuaranterName.getText().toString());

        mGuaranterAmount = (EditText) rootView.findViewById(R.id.guarantors_amount);
        mPage.getData().putString(CreditCardPage.ISSUE_BANK, mGuaranterAmount.getText().toString());

        mGuaranterSociety = (EditText) rootView.findViewById(R.id.guarantors_society);
        mPage.getData().putString(CreditCardPage.ISSUE_BANK, mGuaranterSociety.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mGuaranterName.addTextChangedListener(getTextWatcher(GuaranteesForBrrwrPage.GUARANTER_FOR_BORROWER));
        mGuaranterAmount.addTextChangedListener(getTextWatcher(GuaranteesForBrrwrPage.GUARANTER_LOAN_AMOUNT));
        mGuaranterSociety.addTextChangedListener(getTextWatcher(GuaranteesForBrrwrPage.GUARANTER_BANK_SOCIETY));
    }
}
