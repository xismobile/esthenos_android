package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;


public class LandDetailFragment extends PageFragment<LandDetailPage> {

    @NotEmpty
    private EditText mLanddesc,mLandproperty, mLandValue;

    public static LandDetailFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        LandDetailFragment fragment = new LandDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (LandDetailPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_land_detail, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mLanddesc = (EditText) rootView.findViewById(R.id.land_description);
        mPage.getData().putString(LandDetailPage.LAND_DESCRIPTION, mLanddesc.getText().toString());

        mLandproperty = (EditText) rootView.findViewById(R.id.land_property);
        mPage.getData().putString(LandDetailPage.LAND_PROPERTY, mLandproperty.getText().toString());

        mLandValue = (EditText) rootView.findViewById(R.id.land_value);
        mPage.getData().putString(LandDetailPage.LAND_RESOLVE_VALUE, mLandValue.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLanddesc.addTextChangedListener(getTextWatcher(LandDetailPage.LAND_DESCRIPTION));
        mLandproperty.addTextChangedListener(getTextWatcher(LandDetailPage.LAND_PROPERTY));
        mLandValue.addTextChangedListener(getTextWatcher(LandDetailPage.LAND_RESOLVE_VALUE));
    }
}

