package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class RiskEvaluationFragment extends PageFragment<RiskEvaluationPage> {

    Spinner mSeasonal,mSufferedHealth,mSufferedFinancial,mSpouse,mCusEligible,mCusHerself;

    String[] sppinValue={"","NO","YES"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static RiskEvaluationFragment create(String key) {
        RiskEvaluationFragment fragment = new RiskEvaluationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_risk_evaluation, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());


        String s_RiskEvalutionFragSeasonal=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_SEASONAL);
        String s_RiskEvalutionFragSufferedhelth=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_SUFF_HEALTH);
        String s_RiskEvalutionFragFinancial=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_SUFF_FINANCIAL);
        String s_RiskEvalutionFragSpouse=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_SPOUSE);
        String s_RiskEvalutionFragCusEligible=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_CUSS_ELIGIBLE);
        String s_RiskEvalutionFragCusHerself=autosave_data.getdata(getContext(), mPage.getFormId(),RiskEvaluationPage.KEY_CUS_HERSELF);

        mSeasonal=(Spinner)rootView.findViewById(R.id.risk_seasonal_value);
        ArrayAdapter seasonalAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        seasonalAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mSeasonal.setAdapter(seasonalAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_SEASONAL, mSeasonal.getSelectedItem().toString());
        if (!s_RiskEvalutionFragSeasonal.equals("nulldata")){
            mSeasonal.setSelection(seasonalAdapter.getPosition(s_RiskEvalutionFragSeasonal));
        }

        mSufferedHealth=(Spinner)rootView.findViewById(R.id.risk_sufferd_health);
        ArrayAdapter healthAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        healthAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mSufferedHealth.setAdapter(healthAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_SUFF_HEALTH, mSufferedHealth.getSelectedItem().toString());
        if (!s_RiskEvalutionFragSufferedhelth.equals("nulldata")){
            mSufferedHealth.setSelection(healthAdapter.getPosition(s_RiskEvalutionFragSufferedhelth));
        }

        mSufferedFinancial=(Spinner)rootView.findViewById(R.id.risk_suffered_financial);
        ArrayAdapter financialAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        financialAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mSufferedFinancial.setAdapter(financialAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_SUFF_FINANCIAL, mSufferedFinancial.getSelectedItem().toString());
        if (!s_RiskEvalutionFragFinancial.equals("nulldata")){
            mSufferedFinancial.setSelection(financialAdapter.getPosition(s_RiskEvalutionFragFinancial));
        }

        mSpouse=(Spinner)rootView.findViewById(R.id.risk_spouse_value);
        ArrayAdapter spouseAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        spouseAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mSpouse.setAdapter(spouseAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_SPOUSE, mSpouse.getSelectedItem().toString());
        if (!s_RiskEvalutionFragSpouse.equals("nulldata")){
            mSpouse.setSelection(spouseAdapter.getPosition(s_RiskEvalutionFragSpouse));
        }

        mCusEligible=(Spinner)rootView.findViewById(R.id.risk_customer_eligible_value);
        ArrayAdapter customerAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        customerAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mCusEligible.setAdapter(customerAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_CUSS_ELIGIBLE, mCusEligible.getSelectedItem().toString());
        if (!s_RiskEvalutionFragCusEligible.equals("nulldata")){
            mCusEligible.setSelection(customerAdapter.getPosition(s_RiskEvalutionFragCusEligible));
        }

        mCusHerself=(Spinner)rootView.findViewById(R.id.risk_customer_herself);
        ArrayAdapter herselfAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,sppinValue);
        herselfAdapter.setDropDownViewResource(R.layout.layout_spinner_item);
        mCusHerself.setAdapter(herselfAdapter);
        mPage.getData().putString(RiskEvaluationPage.KEY_CUS_HERSELF, mCusHerself.getSelectedItem().toString());
        if (!s_RiskEvalutionFragCusHerself.equals("nulldata")){
            mCusHerself.setSelection(herselfAdapter.getPosition(s_RiskEvalutionFragCusHerself));
        }


        mSeasonal.setOnItemSelectedListener(getOnItem());
        mSufferedHealth.setOnItemSelectedListener(getOnItem());
        mSufferedFinancial.setOnItemSelectedListener(getOnItem());
        mSpouse.setOnItemSelectedListener(getOnItem());
        mCusEligible.setOnItemSelectedListener(getOnItem());
        mCusHerself.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

//        mSeasonal.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_SEASONAL));
//        mSufferedHealth.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_SUFF_HEALTH));
//        mSufferedFinancial.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_SUFF_FINANCIAL));
//        mSpouse.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_SPOUSE));
//        mCusEligible.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_CUSS_ELIGIBLE));
//        mCusHerself.setOnItemSelectedListener(getOnItemSelectedListener(RiskEvaluationPage.KEY_CUS_HERSELF));

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.risk_seasonal_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_SEASONAL, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(RiskEvaluationPage.KEY_SEASONAL, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.risk_sufferd_health) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_SUFF_HEALTH, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(RiskEvaluationPage.KEY_SUFF_HEALTH, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.risk_suffered_financial) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_SUFF_FINANCIAL, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(RiskEvaluationPage.KEY_SUFF_FINANCIAL, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.risk_spouse_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_SPOUSE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(RiskEvaluationPage.KEY_SPOUSE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.risk_customer_eligible_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_CUSS_ELIGIBLE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(RiskEvaluationPage.KEY_CUSS_ELIGIBLE, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.risk_customer_herself) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), RiskEvaluationPage.KEY_CUS_HERSELF, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(RiskEvaluationPage.KEY_CUS_HERSELF, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

}
