package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class BazaarBusinessDetailsPage extends Page {

    public static final String BAZAAR_TYPE_BUSINESS_KEY = "Type of Business";
    public static final String BAZAAR_NO_OF_BUSINESS_KEY = "No of Business Days in a week";
    public static final String BAZAAR_FREQUENCY_KEY = "Frequency of Bulk purchase of Goods";
    public static final String BAZAAR_VALUE_BULK_KEY = "Value of Bulk Purchase in each cycle";
    public static final String BAZAAR_PRESENT_VALUE_KEY ="Present value of stock";
    public static final String BAZAAR_AVERAGE_DAILY_KEY = "Average Daily Sales(Range)";
    public static final String BAZAAR_AVERAGE_MARGIN_KEY = "Average Margin of sales(%)";
    public static final String BAZAAR_AVERAGE_MONTHLY_KEY = "Monthly profit from Business";
    public static final String BAZAAR_CREDIT_KEY = "Credit from Suppliers";
    public static final String BAZAAR_CREDIT_BUYER_KEY = "Credit to Buyers";
    public static final String BAZAAR_NO_OF_PERMANENT_KEY = "No of Permanent Labour(Excluding Family Members)";
    public static final String BAZAAR_NO_OF_CONTRACT_KEY = "No of Contract Labours";


    public BazaarBusinessDetailsPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return BazaarBusinessDetailsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(BAZAAR_TYPE_BUSINESS_KEY,mData.getString(BAZAAR_TYPE_BUSINESS_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_NO_OF_BUSINESS_KEY,mData.getString(BAZAAR_NO_OF_BUSINESS_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_FREQUENCY_KEY,mData.getString(BAZAAR_FREQUENCY_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_VALUE_BULK_KEY,mData.getString(BAZAAR_VALUE_BULK_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_PRESENT_VALUE_KEY,mData.getString(BAZAAR_PRESENT_VALUE_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_AVERAGE_DAILY_KEY,mData.getString(BAZAAR_AVERAGE_DAILY_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_AVERAGE_MARGIN_KEY,mData.getString(BAZAAR_AVERAGE_MARGIN_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_AVERAGE_MONTHLY_KEY,mData.getString(BAZAAR_AVERAGE_MONTHLY_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_CREDIT_KEY,mData.getString(BAZAAR_CREDIT_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_CREDIT_BUYER_KEY,mData.getString(BAZAAR_CREDIT_BUYER_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_NO_OF_PERMANENT_KEY,mData.getString(BAZAAR_NO_OF_PERMANENT_KEY),getPageId(),-1));
        dest.add(new ReviewItem(BAZAAR_NO_OF_CONTRACT_KEY,mData.getString(BAZAAR_NO_OF_CONTRACT_KEY),getPageId(),-1));

    }
    /*@Override
    public boolean isCompleted(){
        return  !TextUtils.isEmpty(mData.getString(BAZAAR_TYPE_BUSINESS_KEY))&& !TextUtils.isEmpty(mData.getString(BAZAAR_NO_OF_BUSINESS_KEY))&&
                !TextUtils.isEmpty(mData.getString(BAZAAR_FREQUENCY_KEY))&& !TextUtils.isEmpty(mData.getString(BAZAAR_VALUE_BULK_KEY))&&
                !TextUtils.isEmpty(mData.getString(BAZAAR_AVERAGE_DAILY_KEY))&& !TextUtils.isEmpty(mData.getString(BAZAAR_AVERAGE_MARGIN_KEY))&&
                !TextUtils.isEmpty(mData.getString(BAZAAR_AVERAGE_MONTHLY_KEY));
    }*/

}
