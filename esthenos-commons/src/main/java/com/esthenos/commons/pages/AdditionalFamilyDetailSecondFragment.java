package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.cards.AadhaarCard;
import com.esthenos.commons.cards.KYCCards;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.SingletonConfig;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;


public class AdditionalFamilyDetailSecondFragment extends PageFragment<AdditionalFamilyDetailSecondPage> {


    Autosave_Data autosave_data = new Autosave_Data();

    private static final String[] education = {"", "Illiterate", "Upto 5th class", "Upto 10th Class", "Upto Class 12", "Graduate",
            "PG and Above", "Semi literate"};
    private static final String[] relation = {"", "Brother", "Sister", "Self", "Spouse", "Mother", "Father", "Daughter", "Son", "Grand Mother",
            "Grand Father", "Grand Son", "Grand Daughter", "Relative", "Father in Law", "Mother in Law",
            "Brother in Law", "Sister in Law", "Daughter in Law"};
    private static final String[] ocupation = {" ", "Retailing", "Manufacturing", "Services", "Housewife", "Student"};
    private static final String[] sex = {"", "Male", "Female"};
    @NotEmpty
    private EditText mName, mAge, mMonthlyIncome, mInvolvment;
    private Spinner mSex, mRelation, mEducation, mOccupations;

    public static AdditionalFamilyDetailSecondFragment create(String key) {
        AdditionalFamilyDetailSecondFragment fragment = new AdditionalFamilyDetailSecondFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (AdditionalFamilyDetailSecondPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_additional_family_detail_second, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        SingletonConfig instance = SingletonConfig.getInstance();
        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final AadhaarCard aadhaarCard = kycCards.aadhaarCard;

        mAge = (EditText) rootView.findViewById(R.id.f_age_second);
        mAge.setText(aadhaarCard.getSpouseAge());
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_AGE_SECOND, mAge.getText().toString());

        mRelation = (Spinner) rootView.findViewById(R.id.f_relation_second);
        ArrayAdapter<String> relationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, relation);
        relationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRelation.setAdapter(relationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_RELATION_SECOND, mRelation.getSelectedItem().toString());

        mName = (EditText) rootView.findViewById(R.id.f_name_second);
        mName.setText(aadhaarCard.getSpouse());
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_NAME_SECOND, mName.getText().toString());

        mOccupations = (Spinner) rootView.findViewById(R.id.f_occupations_second);
        ArrayAdapter<String> ocupationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, ocupation);
        ocupationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mOccupations.setAdapter(ocupationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_OCCUPATION_DETAILS_SECOND, mOccupations.getSelectedItem().toString());

        mEducation = (Spinner) rootView.findViewById(R.id.f_education_second);
        ArrayAdapter<String> educationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, education);
        educationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEducation.setAdapter(educationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_EDUCATION_SECOND, mEducation.getSelectedItem().toString());

        mInvolvment = (EditText) rootView.findViewById(R.id.f_involment_second);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_INVOLVEMENY_SECOND, mInvolvment.getText().toString());

        mMonthlyIncome = (EditText) rootView.findViewById(R.id.f_annual_income_second);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_ANNUAL_INCOME_SECOND, mMonthlyIncome.getText().toString());

        mSex = (Spinner) rootView.findViewById(R.id.f_sex_second);
        ArrayAdapter sexAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, sex);
        sexAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSex.setAdapter(sexAdapter);
        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_SEX_SECOND, mSex.getSelectedItem().toString());


        String S_mSex = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_SEX_SECOND);
        String S_mRelation = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_RELATION_SECOND);
        String S_mEducation = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_EDUCATION_SECOND);
        String S_mOccupations = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_OCCUPATION_DETAILS_SECOND);
        String S_mName = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_NAME_SECOND);
        String S_mAge = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_AGE_SECOND);
        String S_mMonthlyIncome = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_ANNUAL_INCOME_SECOND);
        String S_Involvment = autosave_data.getdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_INVOLVEMENY_SECOND);


        if (!S_mSex.equals("nulldata")) {

            int P_sex = sexAdapter.getPosition(S_mSex);
            mSex.setSelection(P_sex);
        }
        if (!S_mRelation.equals("nulldata")) {

            int P_rel = relationAdapter.getPosition(S_mRelation);
            mRelation.setSelection(P_rel);
        }
        if (!S_mEducation.equals("nulldata")) {

            int P_edu = educationAdapter.getPosition(S_mEducation);
            mEducation.setSelection(P_edu);
        }
        if (!S_mOccupations.equals("nulldata")) {

            int P_occu = ocupationAdapter.getPosition(S_mOccupations);
            mOccupations.setSelection(P_occu);
        }

        if (!S_mName.equals("nulldata")) {
            mName.setText(S_mName);
        }
        if (!S_mAge.equals("nulldata")) {
            mAge.setText(S_mAge);
        }
        if (!S_mMonthlyIncome.equals("nulldata")) {
            mMonthlyIncome.setText(S_mMonthlyIncome);
        }
        if (!S_Involvment.equals("nulldata")) {
            mInvolvment.setText(S_Involvment);
        }



        mSex.setOnItemSelectedListener(getOnItemASDF());
        mRelation.setOnItemSelectedListener(getOnItemASDF());
        mEducation.setOnItemSelectedListener(getOnItemASDF());
        mOccupations.setOnItemSelectedListener(getOnItemASDF());



        mName.addTextChangedListener(new GenericTextASDF(mName));
        mAge.addTextChangedListener(new GenericTextASDF(mAge));
        mInvolvment.addTextChangedListener(new GenericTextASDF(mInvolvment));
        mMonthlyIncome.addTextChangedListener(new GenericTextASDF(mMonthlyIncome));








        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       /* mOccupations.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailSecondPage.KEY_OCCUPATION_DETAILS_SECOND));
        mMonthlyIncome.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailSecondPage.KEY_ANNUAL_INCOME_SECOND));
        mInvolvment.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailSecondPage.KEY_INVOLVEMENY_SECOND));
        mSex.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailSecondPage.KEY_SEX_SECOND));
        mEducation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailSecondPage.KEY_EDUCATION_SECOND));
        mRelation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailSecondPage.KEY_RELATION_SECOND));
        mName.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailSecondPage.KEY_NAME_SECOND));
        mAge.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailSecondPage.KEY_AGE_SECOND));*/


    }



    public AdapterView.OnItemSelectedListener getOnItemASDF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.f_sex_second) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_SEX_SECOND, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_SEX_SECOND,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.f_relation_second) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_RELATION_SECOND, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_RELATION_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.f_education_second) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_EDUCATION_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_EDUCATION_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.f_occupations_second) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_OCCUPATION_DETAILS_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_OCCUPATION_DETAILS_SECOND, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTextASDF implements TextWatcher {

        private View view;

        private GenericTextASDF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in GenericTextASDF", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.f_name_second) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_NAME_SECOND, text);


                    mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_NAME_SECOND, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_age_second) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_AGE_SECOND, text);


                    mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_AGE_SECOND, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_annual_income_second) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_ANNUAL_INCOME_SECOND, text);


                    mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_ANNUAL_INCOME_SECOND, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_involment_second) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailSecondFragment", AdditionalFamilyDetailSecondPage.KEY_INVOLVEMENY_SECOND, text);


                    mPage.getData().putString(AdditionalFamilyDetailSecondPage.KEY_INVOLVEMENY_SECOND, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


        }

    }
}
