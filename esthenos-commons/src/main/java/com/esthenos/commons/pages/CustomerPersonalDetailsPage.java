package com.esthenos.commons.pages;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class CustomerPersonalDetailsPage extends Page {

    public static final String SINGLE_PHYSICAL_DISABILITY_KEY = "Physical Disability";
    public static final String SINGLE_RELIGION= "Religion";
    public static final String SINGLE_CATEGORY = "Category";
    public static final String SINGLE_EDUCATION = "Education";

    public CustomerPersonalDetailsPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return CustomerPersonalDetailsFragment.create(getPageId());
    }

    @SuppressLint("LongLogTag")
    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(SINGLE_PHYSICAL_DISABILITY_KEY, mData.getString(SINGLE_PHYSICAL_DISABILITY_KEY), getPageId(), -1));
        dest.add(new ReviewItem(SINGLE_RELIGION, mData.getString(SINGLE_RELIGION), getPageId(), -1));
        dest.add(new ReviewItem(SINGLE_CATEGORY, mData.getString(SINGLE_CATEGORY), getPageId(), -1));
        dest.add(new ReviewItem(SINGLE_EDUCATION, mData.getString(SINGLE_EDUCATION), getPageId(), -1));

    }
   /*@Override
    public boolean isCompleted(){
        return !TextUtils.isEmpty(mData.getString(SINGLE_RELIGION)) &&!TextUtils.isEmpty(mData.getString(SINGLE_CATEGORY))&&
               !TextUtils.isEmpty(mData.getString(SINGLE_EDUCATION));
    }*/


}
