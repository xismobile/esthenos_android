package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;


public class FacilitatedProductFragment extends PageFragment<FacilitatedProductPage> {


    public static final String TABLE_FACILITATEDPRODUCT = "FacilitateProduct";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;

    private Spinner mInsurance,mNomineeMinor,mSolar,mGender;
    private EditText mName,mAge,mRelation,mAppointee;
    private TextView mAppointeeText;

    private final String[] insurance={"","Life", "General"};
    private final String [] yesno={"","Yes","No"};
    private final String[] gender={"","Male","Female"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static FacilitatedProductFragment create(String key) {
        FacilitatedProductFragment fragment = new FacilitatedProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.fragment_facilitated_product, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_facilitateName=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_NAME_KEY);
        String s_facilitateInsurence=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_INSURANCE_KEY);
        String s_facilitateNominee=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_NOMINEE_KEY);
        String s_facilitateSolar=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_SOLAR_KEY);
        String s_facilitateGender=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_GENDER_KEY);
        String s_facilitateRelation=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_RELATIONSHIP_KEY);
        String s_facilitateAge=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_AGE_KEY);
        String s_facilitateNameofApointee=autosave_data.getdata(getContext(), mPage.getFormId(),FacilitatedProductPage.FACILITATED_AGE_KEY);

        mAppointeeText = (TextView)rootView.findViewById(R.id.appointee);

        mydb = new MySQLiteDB(getContext());
       // autosave = new AutoSaveData();

        mInsurance=(Spinner)rootView.findViewById(R.id.insurance_value);
        ArrayAdapter insuranceAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,insurance);
        insuranceAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mInsurance.setAdapter(insuranceAdapter);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_INSURANCE_KEY, mInsurance.getSelectedItem().toString());
        if (!s_facilitateInsurence.equals("nulldata")){
            mInsurance.setSelection(insuranceAdapter.getPosition(s_facilitateInsurence));
        }

        mNomineeMinor=(Spinner)rootView.findViewById(R.id.nominee_minor_value);
        ArrayAdapter nomineeAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,yesno);
        nomineeAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mNomineeMinor.setAdapter(nomineeAdapter);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_NOMINEE_KEY, mNomineeMinor.getSelectedItem().toString());
        if (!s_facilitateNominee.equals("nulldata")){
            mNomineeMinor.setSelection(nomineeAdapter.getPosition(s_facilitateNominee));
        }


        mSolar=(Spinner)rootView.findViewById(R.id.solar_value);
        ArrayAdapter solarAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,yesno);
        solarAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSolar.setAdapter(solarAdapter);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_SOLAR_KEY, mSolar.getSelectedItem().toString());
        if (!s_facilitateSolar.equals("nulldata")){
            mSolar.setSelection(solarAdapter.getPosition(s_facilitateSolar));
        }

        mName=(EditText)rootView.findViewById(R.id.name_value);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_NAME_KEY, mName.getText().toString());
        if (!s_facilitateName.equals("nulldata")){
            mName.setText(s_facilitateName);
        }



        mAge=(EditText)rootView.findViewById(R.id.age_value);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_AGE_KEY,mAge.getText().toString());
        if (!s_facilitateAge.equals("nulldata")){
            mAge.setText(s_facilitateAge);
        }


        mRelation=(EditText)rootView.findViewById(R.id.relationship_value);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_RELATIONSHIP_KEY,mRelation.getText().toString());
        if (!s_facilitateRelation.equals("nulldata")){
            mRelation.setText(s_facilitateRelation);
        }



        mAppointee=(EditText)rootView.findViewById(R.id.minor_yes_value);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_NAME_APPOINTEE,mAppointee.getText().toString());
        if (!s_facilitateNameofApointee.equals("nulldata")){
            mAppointee.setText(s_facilitateNameofApointee);
        }

        mGender=(Spinner)rootView.findViewById(R.id.gender_value);
        ArrayAdapter genderAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,gender);
        genderAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mGender.setAdapter(genderAdapter);
        mPage.getData().putString(FacilitatedProductPage.FACILITATED_GENDER_KEY, mGender.getSelectedItem().toString());
        if (!s_facilitateGender.equals("nulldata")){
            mGender.setSelection(genderAdapter.getPosition(s_facilitateGender));
        }

        mAppointeeText.setVisibility(View.GONE);
        mAppointee.setVisibility(View.GONE);

        mInsurance.setOnItemSelectedListener(getOnItem());
        mNomineeMinor.setOnItemSelectedListener(getOnItem());
        mGender.setOnItemSelectedListener(getOnItem());
        mSolar.setOnItemSelectedListener(getOnItem());
        mRelation.addTextChangedListener(new GenericTextFPF(mRelation));

        mName.addTextChangedListener(new GenericTextFPF(mName));
        mAge.addTextChangedListener(new GenericTextFPF(mAge));
        mAppointee.addTextChangedListener(new GenericTextFPF(mAppointee));

        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanseState){
        super.onViewCreated(view,savedInstanseState);

//        mInsurance.setOnItemSelectedListener(getOnItemSelectedListener(FacilitatedProductPage.FACILITATED_INSURANCE_KEY));
//        mNomineeMinor.setOnItemSelectedListener(getOnItemSelectedListener(FacilitatedProductPage.FACILITATED_NOMINEE_KEY));
//        mGender.setOnItemSelectedListener(getOnItemSelectedListener(FacilitatedProductPage.FACILITATED_GENDER_KEY));
//        mSolar.setOnItemSelectedListener(getOnItemSelectedListener(FacilitatedProductPage.FACILITATED_SOLAR_KEY));
//        mRelation.addTextChangedListener(getTextWatcher(FacilitatedProductPage.FACILITATED_RELATIONSHIP_KEY));
//
//        mName.addTextChangedListener(getTextWatcher(FacilitatedProductPage.FACILITATED_NAME_KEY));
//        mAge.addTextChangedListener(getTextWatcher(FacilitatedProductPage.FACILITATED_AGE_KEY));

//        mNomineeMinor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//                boolean enabled = parent.getItemAtPosition(position).toString().equalsIgnoreCase("yes");
//                mAppointee.setVisibility(View.VISIBLE);
//                mAppointeeText.setVisibility(View.VISIBLE);
//                mPage.getData().putString(FacilitatedProductPage.FACILITATED_NOMINEE_KEY, mNomineeMinor.getItemAtPosition(position).toString());
////
////                String value = String.valueOf(parent.getItemAtPosition(position));
////                mydb.Autosavebyid(TABLE_FACILITATEDPRODUCT, MySQLiteDB.FACILITATED_NOMINEE_KEY, i, value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        mInsurance.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.FirstAutoSaveData(TABLE_FACILITATEDPRODUCT, MySQLiteDB.FACILITATED_INSURANCE_KEY, value);
//                i = mydb.getLastInsertedId(TABLE_FACILITATEDPRODUCT);
//                Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
//            }
//        });
//        mGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.Autosavebyid(TABLE_FACILITATEDPRODUCT, MySQLiteDB.FACILITATED_GENDER_KEY, i, value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        mSolar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.Autosavebyid(TABLE_FACILITATEDPRODUCT, MySQLiteDB.FACILITATED_SOLAR_KEY, i, value);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

    }

    class GenericTextFPF implements TextWatcher {

        private View view;

        private GenericTextFPF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.name_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FacilitatedProductPage.FACILITATED_NAME_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_NAME_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.relationship_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FacilitatedProductPage.FACILITATED_RELATIONSHIP_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_RELATIONSHIP_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.age_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FacilitatedProductPage.FACILITATED_AGE_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_AGE_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.minor_yes_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FacilitatedProductPage.FACILITATED_AGE_KEY,text);

                    if (editable.length() > 0) {
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_AGE_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.insurance_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        boolean enabled = parent.getItemAtPosition(position).toString().equalsIgnoreCase("yes");
                        mAppointee.setVisibility(View.VISIBLE);
                        mAppointeeText.setVisibility(View.VISIBLE);
                        autosave_data.putdata(getContext(),mPage.getFormId(), FacilitatedProductPage.FACILITATED_INSURANCE_KEY, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_INSURANCE_KEY,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.nominee_minor_value) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), FacilitatedProductPage.FACILITATED_NOMINEE_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_NOMINEE_KEY,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.solar_value) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), FacilitatedProductPage.FACILITATED_SOLAR_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_SOLAR_KEY,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.gender_value) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), FacilitatedProductPage.FACILITATED_GENDER_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(FacilitatedProductPage.FACILITATED_GENDER_KEY,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

}
