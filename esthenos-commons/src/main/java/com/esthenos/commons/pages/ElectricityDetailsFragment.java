package com.esthenos.commons.pages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;

public class ElectricityDetailsFragment extends PageFragment<ElectricityDetailsPage> {

    Button mPowerSupplier;
    String[] PowerSuppliers = {"","Best", "Tata Power", "Reliance"};
    EditText mElectricBill;

    private String mKey;
    private static final String ARG_KEY = "key";
    private ElectricityDetailsPage mPage;

    public static ElectricityDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        ElectricityDetailsFragment fragment = new ElectricityDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (ElectricityDetailsPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_electricity_details, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mPowerSupplier = (Button) rootView.findViewById(R.id.select_power);
        mPowerSupplier.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select Power Supplier");
                builder.setSingleChoiceItems(PowerSuppliers, -1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        mPowerSupplier.setText(PowerSuppliers[item]);
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        mElectricBill=(EditText)rootView.findViewById(R.id.elec_bill);
        mPage.getData().putString(ElectricityDetailsPage.POWER_MONTHLY_BILL, mElectricBill.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPowerSupplier.addTextChangedListener(getTextWatcher(ElectricityDetailsPage.POWER_SUPPLIER));
        mElectricBill.addTextChangedListener(getTextWatcher(ElectricityDetailsPage.POWER_MONTHLY_BILL));
    }
}

