package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.cards.AadhaarCard;
import com.esthenos.commons.cards.KYCCards;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tech.freak.wizardpager.ui.PageFragment;

public class AdditionalFamilyDetailFirstFragment extends PageFragment<AdditionalFamilyDetailFirstPage> {


    private static final String[] education = {"", "Illiterate", "Upto 5th class", "Upto 10th Class", "Upto Class 12", "Graduate",
            "PG and Above", "Semi literate"};
    private static final String[] relation = {"", "Brother", "Sister", "Self", "Spouse", "Mother", "Father", "Daughter", "Son", "Grand Mother",
            "Grand Father", "Grand Son", "Grand Daughter", "Relative", "Father in Law", "Mother in Law",
            "Brother in Law", "Sister in Law", "Daughter in Law"};
    private static final String[] ocupation = {" ", "Retailing", "Manufacturing", "Services", "Housewife", "Student"};
    private static final String[] sex = {"", "Male", "Female"};
    Autosave_Data autosave_data = new Autosave_Data();
    //..................................
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;
    SingletonConfig instance = SingletonConfig.getInstance();
    @NotEmpty
    private EditText mName, mAge, mMonthlyIncome, mInvolvment;
    //...................................
    private Spinner mSex, mRelation, mEducation, mOccupations;

    public static AdditionalFamilyDetailFirstFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        AdditionalFamilyDetailFirstFragment fragment = new AdditionalFamilyDetailFirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (AdditionalFamilyDetailFirstPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_additional_family_detail_first, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final AadhaarCard aadhaarCard = kycCards.aadhaarCard;

        //............................................
        autosave = new AutoSaveData(getContext());
        mydb = new MySQLiteDB(getContext());
        //..............................................


        mAge = (EditText) rootView.findViewById(R.id.f_age_first);
        mAge.setText(String.format("%d", aadhaarCard.getAge()));
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_AGE, mAge.getText().toString());
        //...........................................
        mAge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mAge.getText().toString().trim())) {


                        if (mAge.getText().length() == 1) {


                            String nowstring = mAge.getText().toString().trim();
                            mAge.setText("0" + nowstring);
                            //  mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_AGE, i, mAge.getText().toString());
                        }


                    } else {

                        mAge.setError("Please enter age ");
                    }


                }
            }
        });

        //.............................................


        mRelation = (Spinner) rootView.findViewById(R.id.f_relation_first);
        ArrayAdapter<String> relationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, relation);
        relationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRelation.setAdapter(relationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_RELATION, mRelation.getSelectedItem().toString());

        mName = (EditText) rootView.findViewById(R.id.f_name_first);
        mName.setText(aadhaarCard.getName());
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_NAME, mName.getText().toString());

        //........................................................
//        autosave.OnFocusChanged(mName,mName.getText().toString(),AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6,AdditionalFamilyDetailFirstPage.KEY_NAME);
//        Toast.makeText(getContext(),"data added.." + mName.getText().toString(),Toast.LENGTH_SHORT).show();
        mName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mName.getText().toString().trim())) {


                        if (mName.getText().length() >= 3) {

// mydb.NAME = mName.getText().toString();
                            // mydb.FirstAutoSaveData(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_NAME, mName.getText().toString());
                            //Toast.makeText(getContext(), "data added.." + mName.getText().toString(), Toast.LENGTH_SHORT).show();

                            // i = mydb.getLastInsertedId(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6);
                            //Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
                        }

                    } else {

                        mName.setError("Please enter name i.e min 3 character max 50 character");
                    }


                }

            }
        });
        //.......................................................


        mOccupations = (Spinner) rootView.findViewById(R.id.f_occupations_first);
        ArrayAdapter<String> ocupationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, ocupation);
        ocupationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mOccupations.setAdapter(ocupationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_OCCUPATION_DETAILS, mOccupations.getSelectedItem().toString());


        mEducation = (Spinner) rootView.findViewById(R.id.f_education_first);
        ArrayAdapter<String> educationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, education);
        educationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEducation.setAdapter(educationAdapter);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_EDUCATION, mEducation.getSelectedItem().toString());

        mInvolvment = (EditText) rootView.findViewById(R.id.f_involment_first);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_INVOLVEMENY, mInvolvment.getText().toString());
        mInvolvment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mInvolvment.getText().toString().trim())) {


                        if (mInvolvment.getText().length() > 1 && mInvolvment.getText().length() <= 2) {

// mydb.NAME = mName.getText().toString();
                            // mydb.FirstAutoSaveData(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_NAME, mName.getText().toString());
                            //Toast.makeText(getContext(), "data added.." + mName.getText().toString(), Toast.LENGTH_SHORT).show();

                            // i = mydb.getLastInsertedId(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6);
                            //mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_INVOLVEMENY, i, mInvolvment.getText().toString());
                            //Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
                        }

                    } else {

                        mInvolvment.setError("Please enter Years of Involvement");
                    }


                }
            }
        });

        mMonthlyIncome = (EditText) rootView.findViewById(R.id.f_annual_income_first);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_ANNUAL_INCOME, mMonthlyIncome.getText().toString());
        mMonthlyIncome.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                    @Override
                                                    public void onFocusChange(View v, boolean hasFocus) {
                                                        if (!hasFocus) {
                                                            if (!TextUtils.isEmpty(mMonthlyIncome.getText().toString().trim())) {
                                                                if (mMonthlyIncome.getText().length() >= 3 && mMonthlyIncome.getText().length() <= 5) {
                                                                    //   mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_ANNUAL_INCOME, i, mMonthlyIncome.getText().toString());
                                                                } else {

                                                                    mMonthlyIncome.setError("Please enter Years of Involvement");
                                                                }


                                                            }

                                                        }
                                                    }
                                                }
        );

        mSex = (Spinner) rootView.findViewById(R.id.f_sex_first);
        ArrayAdapter sexAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, sex);
        sexAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSex.setAdapter(sexAdapter);
        int position = getIndex(mSex, aadhaarCard.getGender());
        mSex.setSelection(position);
        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_SEX, mSex.getSelectedItem().toString());


        String S_mSex = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_SEX);
        String S_mRelation = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_RELATION);
        String S_mEducation = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_EDUCATION);
        String S_mOccupations = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_OCCUPATION_DETAILS);
        String S_mName = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_NAME);
        String S_mAge = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_AGE);
        String S_mMonthlyIncome = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_ANNUAL_INCOME);
        String S_Involvment = autosave_data.getdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_INVOLVEMENY);


        if (!S_mSex.equals("nulldata")) {

            int P_sex = sexAdapter.getPosition(S_mSex);
            mSex.setSelection(P_sex);
        }
        if (!S_mRelation.equals("nulldata")) {

            int P_rel = relationAdapter.getPosition(S_mRelation);
            mRelation.setSelection(P_rel);
        }
        if (!S_mEducation.equals("nulldata")) {

            int P_edu = educationAdapter.getPosition(S_mEducation);
            mEducation.setSelection(P_edu);
        }
        if (!S_mOccupations.equals("nulldata")) {

            int P_occu = ocupationAdapter.getPosition(S_mOccupations);
            mOccupations.setSelection(P_occu);
        }

        if (!S_mName.equals("nulldata")) {
            mName.setText(S_mName);
        }
        if (!S_mAge.equals("nulldata")) {
            mAge.setText(S_mAge);
        }
        if (!S_mMonthlyIncome.equals("nulldata")) {
            mMonthlyIncome.setText(S_mMonthlyIncome);
        }
        if (!S_Involvment.equals("nulldata")) {
            mInvolvment.setText(S_Involvment);
        }



        mSex.setOnItemSelectedListener(getOnItemAFDF());
        mRelation.setOnItemSelectedListener(getOnItemAFDF());
        mEducation.setOnItemSelectedListener(getOnItemAFDF());
        mOccupations.setOnItemSelectedListener(getOnItemAFDF());


        mName.addTextChangedListener(new GenericTextAFDF(mName));
        mAge.addTextChangedListener(new GenericTextAFDF(mAge));
        mMonthlyIncome.addTextChangedListener(new GenericTextAFDF(mMonthlyIncome));
        mInvolvment.addTextChangedListener(new GenericTextAFDF(mInvolvment));


        return rootView;
    }

    private int getIndex(Spinner spinner, String selected) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(selected)) {
                index = i;
                i = spinner.getCount();
            }
        }
        return index;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      /*  mOccupations.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailFirstPage.KEY_OCCUPATION_DETAILS));
        mMonthlyIncome.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailFirstPage.KEY_ANNUAL_INCOME));
        mInvolvment.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailFirstPage.KEY_INVOLVEMENY));
        mSex.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailFirstPage.KEY_SEX));
        mEducation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailFirstPage.KEY_EDUCATION));
        mRelation.setOnItemSelectedListener(getOnItemSelectedListener(AdditionalFamilyDetailFirstPage.KEY_RELATION));
        mName.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailFirstPage.KEY_NAME));
        mAge.addTextChangedListener(getTextWatcher(AdditionalFamilyDetailFirstPage.KEY_AGE));*/

        /*mRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(),"hii this is :"+ parent.getItemAtPosition(position) ,Toast.LENGTH_SHORT).show();
                String value = String.valueOf(parent.getItemAtPosition(position));
                Toast.makeText(getContext(), "hii this is :" + value, Toast.LENGTH_SHORT).show();
                mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_RELATION, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mOccupations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(),"hii this is :"+ parent.getItemAtPosition(position) ,Toast.LENGTH_SHORT).show();
                String value = String.valueOf(parent.getItemAtPosition(position));
                Toast.makeText(getContext(), "hii this is :" + value, Toast.LENGTH_SHORT).show();
                mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_OCCUPATION_DETAILS, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(),"hii this is :"+ parent.getItemAtPosition(position) ,Toast.LENGTH_SHORT).show();
                String value = String.valueOf(parent.getItemAtPosition(position));
                Toast.makeText(getContext(), "hii this is :" + value, Toast.LENGTH_SHORT).show();
                mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_SEX, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(),"hii this is :"+ parent.getItemAtPosition(position) ,Toast.LENGTH_SHORT).show();
                String value = String.valueOf(parent.getItemAtPosition(position));
                Toast.makeText(getContext(), "hii this is :" + value, Toast.LENGTH_SHORT).show();
                mydb.Autosavebyid(AdditionalFamilyDetailFirstPage.KEY_TABLE_FAMILY_1TO6, MySQLiteDB.KEY_EDUCATION, i, value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

    }


    public AdapterView.OnItemSelectedListener getOnItemAFDF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.f_sex_first) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_SEX, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_SEX,parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.f_relation_first) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_RELATION, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_RELATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.f_education_first) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.f_occupations_first) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_OCCUPATION_DETAILS, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_OCCUPATION_DETAILS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTextAFDF implements TextWatcher {

        private View view;

        private GenericTextAFDF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
           // Toast.makeText(getContext(), "in text watcher view:", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.f_name_first) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_NAME, text);


                    mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_age_first) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_AGE, text);


                    mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_AGE, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_annual_income_first) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_ANNUAL_INCOME, text);


                    mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_ANNUAL_INCOME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_involment_first) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), "AdditionalFamilyDetailFirstFragment", AdditionalFamilyDetailFirstPage.KEY_INVOLVEMENY, text);


                    mPage.getData().putString(AdditionalFamilyDetailFirstPage.KEY_INVOLVEMENY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


        }

    }

}