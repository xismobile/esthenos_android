package com.esthenos.commons.pages;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.cards.AadhaarCard;
import com.esthenos.commons.cards.KYCCards;
import com.esthenos.commons.cards.PanCard;
import com.esthenos.commons.cards.VoterIdCard;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.SingletonConfig;
import com.tech.freak.wizardpager.ui.PageFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class CustomerKYCDetailsFragment extends PageFragment<CustomerKYCDetailsPage> {

    Autosave_Data autosave_data = new Autosave_Data();

    private String mKey;
    private static final String ARG_KEY = "key";
    Calendar myCalendar = Calendar.getInstance();

     EditText nameView, uidView, f_or_h_name,mPersonalEmail,mPermanentAddress,
            yobView, talukView, districtView, addressView, mobileNumberView,
            countryView, pincodeView, stateView,mMotherName,mSpouse,mLandmarks;


    EditText telephoneNumberView, ageView,mNickName,mPoliceStaion,
            mPostOffice, voterId, voterIdName, voterId_f_or_h_name,
            panCardId, panCardName, panCard_f_or_h_name,mRtionCard,mCenterMeetiong,
            mSpouseAge,mSpouseDob;

    Spinner mGender,mSubTitle,mMotherSubtitle,mFatherSubTitle,mSpouseSubtitle,mOccupation,mMarital;

    TextView mSpouseNameText,mSpouseAgeText,mSpouseDobText;

    private static final String[] occupations={" ","Retailing","Manufacturing","Services","Housewife","Student"};
    private static final String[] gender={" ","Male","Female"};
    private static final String[] subTitle={" ","Mr.", "Mrs.", "Miss", "Late"};
    private static final String[] marital_status={"","Married","Unmarried","Divorced","Widow","Widower","Separated"};

    private SingletonConfig instance = SingletonConfig.getInstance();

    public static CustomerKYCDetailsFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        CustomerKYCDetailsFragment fragment = new CustomerKYCDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (CustomerKYCDetailsPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_page_customer_info, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        Log.d("000000000000000000",mPage.getFormId());

        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final PanCard panCard = kycCards.panCard;
        final AadhaarCard aadhaarCard = kycCards.aadhaarCard;
        VoterIdCard voterIdCard = kycCards.voterIdCard;

        mSpouseNameText = (TextView)rootView.findViewById(R.id.spouse_name_text);
        mSpouseDobText = (TextView)rootView.findViewById(R.id.spouse_dob_text);
        mSpouseAgeText = (TextView)rootView.findViewById(R.id.spouse_age_text);

        mPostOffice=(EditText)rootView.findViewById(R.id.post_office);
        mPage.getData().putString(CustomerKYCDetailsPage.POST_OFFICE_KEY,mPostOffice.getText().toString());

        mPoliceStaion=(EditText)rootView.findViewById(R.id.police_station);
        mPage.getData().putString(CustomerKYCDetailsPage.POLICE_KEY, mPoliceStaion.getText().toString());

        mMarital=(Spinner)rootView.findViewById(R.id.custeomer_page_marital_status);
        ArrayAdapter maritalAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,marital_status);
        maritalAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mMarital.setAdapter(maritalAdapter);
        mPage.getData().putString(CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS, mMarital.getSelectedItem().toString());

        mSubTitle=(Spinner)rootView.findViewById(R.id.kycsubtitle);
        ArrayAdapter adapterName=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,subTitle);
        adapterName.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSubTitle.setAdapter(adapterName);
        mPage.getData().putString(CustomerKYCDetailsPage.NAME_SUBTITLE_KEY, mSubTitle.getSelectedItem().toString());

        mMotherSubtitle=(Spinner)rootView.findViewById(R.id.mothersubtitle);
        mMotherSubtitle.setAdapter(adapterName);
        mPage.getData().putString(CustomerKYCDetailsPage.MOTHER_NAME_SUBTITLE_KEY, mMotherSubtitle.getSelectedItem().toString());

        mFatherSubTitle=(Spinner)rootView.findViewById(R.id.fatherubtitle);
        mFatherSubTitle.setAdapter(adapterName);
        mPage.getData().putString(CustomerKYCDetailsPage.FATHER_HUSBAND_SUBTITLE_KEY, mFatherSubTitle.getSelectedItem().toString());

        mSpouseSubtitle=(Spinner)rootView.findViewById(R.id.spousesubtitle);
        mSpouseSubtitle.setAdapter(adapterName);
        mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_NAME_SUBTITLE_KEY, mSpouseSubtitle.getSelectedItem().toString());

        mNickName=(EditText)rootView.findViewById(R.id.nick_name);
        mPage.getData().putString(CustomerKYCDetailsPage.NICK_NAME_KEY, mNickName.getText().toString());

        mPersonalEmail=(EditText)rootView.findViewById(R.id.personal_email_id);
        mPage.getData().putString(CustomerKYCDetailsPage.EMAIL_ID_KEY,mPersonalEmail.getText().toString());

        mMotherName=(EditText)rootView.findViewById(R.id.mother);
        mPage.getData().putString(CustomerKYCDetailsPage.MOTHER_KEY, mMotherName.getText().toString());

        mSpouse=(EditText)rootView.findViewById(R.id.spouse);
        mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_KEY,mSpouse.getText().toString());

        mLandmarks=(EditText)rootView.findViewById(R.id.landmark);
        mPage.getData().putString(CustomerKYCDetailsPage.LANDMARKS_KEY, mLandmarks.getText().toString());

        mOccupation=(Spinner)rootView.findViewById(R.id.occupation);
        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,occupations);
        occupationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mOccupation.setAdapter(occupationAdapter);
        mPage.getData().putString(CustomerKYCDetailsPage.OCCUPATION_KEY, mOccupation.getSelectedItem().toString());

        mPermanentAddress=(EditText)rootView.findViewById(R.id.permanent_address);
        mPage.getData().putString(CustomerKYCDetailsPage.PERMANENT_ADDRESS_KEY, mPermanentAddress.getText().toString());

        nameView = (EditText) rootView.findViewById(R.id.name);
        nameView.setText(aadhaarCard.getName());
        nameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                aadhaarCard.setName(nameView.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPage.getData().putString(CustomerKYCDetailsPage.NAME_KEY, nameView.getText().toString());

        uidView = (EditText) rootView.findViewById(R.id.uid);
        uidView.setText(aadhaarCard.getUid());
        mPage.getData().putString(CustomerKYCDetailsPage.UID_KEY, uidView.getText().toString());

        f_or_h_name = (EditText) rootView.findViewById(R.id.f_or_h_name);
        f_or_h_name.setText(aadhaarCard.getCo());
        mPage.getData().putString(CustomerKYCDetailsPage.FATHER_OR_HUSBAND_NAME, f_or_h_name.getText().toString());

        ageView = (EditText) rootView.findViewById(R.id.age);
        ageView.setText(Integer.toString(aadhaarCard.getAge()));
        mPage.getData().putString(CustomerKYCDetailsPage.AGE_KEY, ageView.getText().toString());

        yobView = (EditText) rootView.findViewById(R.id.dob);
        yobView.setText(Integer.toString(aadhaarCard.getYob()));
        yobView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        mPage.getData().putString(CustomerKYCDetailsPage.YOB_KEY, yobView.getText().toString());

        talukView = (EditText) rootView.findViewById(R.id.taluk);
        talukView.setText(aadhaarCard.getVtc());
        mPage.getData().putString(CustomerKYCDetailsPage.TALUK_KEY, talukView.getText().toString());

        districtView = (EditText) rootView.findViewById(R.id.district);
        districtView.setText(aadhaarCard.getDist());
        mPage.getData().putString(CustomerKYCDetailsPage.DISTRICT_KEY, districtView.getText().toString());

        stateView = (EditText) rootView.findViewById(R.id.state);
        stateView.setText(aadhaarCard.getState());
        mPage.getData().putString(CustomerKYCDetailsPage.STATE_KEY, stateView.getText().toString());

        addressView = (EditText) rootView.findViewById(R.id.address);
        addressView.setText(aadhaarCard.getAddress());
        mPage.getData().putString(CustomerKYCDetailsPage.ADDRESS_KEY, addressView.getText().toString());

        pincodeView = (EditText) rootView.findViewById(R.id.pincode);
        pincodeView.setText(aadhaarCard.getPc());
        mPage.getData().putString(CustomerKYCDetailsPage.PINCODE_KEY, pincodeView.getText().toString());

        countryView = (EditText) rootView.findViewById(R.id.country);
        mPage.getData().putString(CustomerKYCDetailsPage.COUNTRY_KEY, countryView.getText().toString());

        mobileNumberView = (EditText) rootView.findViewById(R.id.mobilePhoneNumber);
        mPage.getData().putString(CustomerKYCDetailsPage.MOBILE_PHONE_KEY, mobileNumberView.getText().toString());

        telephoneNumberView = (EditText) rootView.findViewById(R.id.telephoneNumber);
        mPage.getData().putString(CustomerKYCDetailsPage.TELE_PHONE_KEY, telephoneNumberView.getText().toString());

        voterId = (EditText) rootView.findViewById(R.id.voter_id_number);
        voterId.setText(voterIdCard.id);
        mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_KEY, voterId.getText().toString());

        voterIdName = (EditText) rootView.findViewById(R.id.voter_id_name);
        voterIdName.setText(voterIdCard.name);
        mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_NAME_KEY, voterIdName.getText().toString());

        voterId_f_or_h_name = (EditText) rootView.findViewById(R.id.voter_id_f_or_h_name);
        voterId_f_or_h_name.setText(voterIdCard.fatherHusbandName);
        mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, voterId.getText().toString());

        panCardId = (EditText) rootView.findViewById(R.id.pancard_number);
        panCardId.setText(panCard.id);
        mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_KEY, panCardId.getText().toString());

        panCardName = (EditText) rootView.findViewById(R.id.pancard_name);
        panCardName.setText(panCard.name);
        mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_NAME_KEY, panCardName.getText().toString());

        panCard_f_or_h_name = (EditText) rootView.findViewById(R.id.pancard_f_or_h_name);
        panCard_f_or_h_name.setText(panCard.fatherHusbandName);
        mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_FATHER_OR_HUSBAND_NAME, panCard_f_or_h_name.getText().toString());

        mRtionCard=(EditText)rootView.findViewById(R.id.ration_card);
        mPage.getData().putString(CustomerKYCDetailsPage.RATIONCARD_KEY, mRtionCard.getText().toString());

        mCenterMeetiong=(EditText)rootView.findViewById(R.id.center_meeting);
        mPage.getData().putString(CustomerKYCDetailsPage.CENTERMEETING_KEY, mCenterMeetiong.getText().toString());

        mGender=(Spinner)rootView.findViewById(R.id.single_page_gender);
        ArrayAdapter adapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,gender);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mGender.setAdapter(adapter);
        int position = getIndex(mGender,aadhaarCard.getGender());
        mGender.setSelection(position);

        mGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                aadhaarCard.setGender(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mPage.getData().putString(CustomerKYCDetailsPage.SINGLE_GENDER_KEY, mGender.getSelectedItem().toString());

        mSpouseAge = (EditText)rootView.findViewById(R.id.spouse_age);
        mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_AGE_KEY, mSpouseAge.getText().toString());

        mSpouseDob = (EditText)rootView.findViewById(R.id.spouse_dob);
        mSpouseDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), spousedate, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_DOB_KEY, mSpouseDob.getText().toString());

        mSpouse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                aadhaarCard.setSpouse(mSpouse.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mSpouseAgeText.setVisibility(View.GONE);
        mSpouseNameText.setVisibility(View.GONE);
        mSpouseDobText.setVisibility(View.GONE);
        mSpouse.setVisibility(View.GONE);
        mSpouseAge.setVisibility(View.GONE);
        mSpouseDob.setVisibility(View.GONE);
        mSpouseSubtitle.setVisibility(View.GONE);

//------ pref saving

        String S_panCard_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.PANCARD_FATHER_OR_HUSBAND_NAME);
        String S_voterId_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.VOTER_ID_FATHER_OR_HUSBAND_NAME );
        String S_mPermanentAddress = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PERMANENT_ADDRESS_KEY );
        String S_addressview = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.ADDRESS_KEY );
        String S_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.FATHER_OR_HUSBAND_NAME);
        String S_telephoneNumberView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.TELE_PHONE_KEY );
        String S_mobileNumberView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.MOBILE_PHONE_KEY );
        String S_voterIdName = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.VOTER_ID_NAME_KEY);
        String S_panCardName = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.PANCARD_NAME_KEY);
        String S_mPostOffice = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.POST_OFFICE_KEY );
        String S_mPersonalEmail = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.EMAIL_ID_KEY );
        String S_districtView = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.DISTRICT_KEY);
        String S_mLandmarks= autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.LANDMARKS_KEY );
        String S_mPoliceStaion = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.POLICE_KEY );
        String S_mNickName = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.NICK_NAME_KEY);
        String S_countryView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.COUNTRY_KEY);
        String S_mMotherName = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.MOTHER_KEY );
        String S_panCardId = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PANCARD_KEY );
        String S_voterId = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.VOTER_ID_KEY);
        String S_talukView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.TALUK_KEY );
        String S_stateView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.STATE_KEY );
        String S_mSpouse = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.SPOUSE_KEY );
        String S_nameView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.NAME_KEY );
        String S_uidView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.UID_KEY );
        String S_yobView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.YOB_KEY );
        String S_ageView = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.AGE_KEY );
        String S_mSpouseAge = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.SPOUSE_AGE_KEY );
        String S_mSpouseDob = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.SPOUSE_DOB_KEY );
        String S_mCenterMeetiong = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.CENTERMEETING_KEY );
        String S_mRtionCard = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.RATIONCARD_KEY );
        String S_pincode = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.PINCODE_KEY );

        if (!S_panCard_f_or_h_name.equals("nulldata")) {
            panCard_f_or_h_name.setText(S_panCard_f_or_h_name);
        }
        if (!S_voterId_f_or_h_name.equals("nulldata")) {
            voterId_f_or_h_name.setText(S_voterId_f_or_h_name);
        }
        if (!S_mPermanentAddress.equals("nulldata")) {
            mPermanentAddress.setText(S_mPermanentAddress);
        }
        if (!S_f_or_h_name.equals("nulldata")) {
            f_or_h_name.setText(S_f_or_h_name);
        }
        if (!S_telephoneNumberView.equals("nulldata")) {
            telephoneNumberView.setText(S_telephoneNumberView);
        }
        if (!S_mobileNumberView.equals("nulldata")) {
            mobileNumberView.setText(S_mobileNumberView);
        }
        if (!S_voterIdName.equals("nulldata")) {
            voterIdName.setText(S_voterIdName);
        }

        if (!S_pincode.equals("nulldata")) {
            pincodeView.setText(S_pincode);
        }

        if (!S_panCardName.equals("nulldata")) {
            panCardName.setText(S_panCardName);
        }
        if (!S_mPostOffice.equals("nulldata")) {
            mPostOffice.setText(S_mPostOffice);
        }
        if (!S_mPersonalEmail.equals("nulldata")) {
            mPersonalEmail.setText(S_mPersonalEmail);
        }
        if (!S_districtView.equals("nulldata")) {
            districtView.setText(S_districtView);
        }
        if (!S_mLandmarks.equals("nulldata")) {
            mLandmarks.setText(S_mLandmarks);
        }
        if (!S_mPoliceStaion.equals("nulldata")) {
            mPoliceStaion.setText(S_mPoliceStaion);
        }

        if (!S_addressview.equals("nulldata")) {
            addressView.setText(S_addressview);
        }

        if (!S_mNickName.equals("nulldata")) {
            mNickName.setText(S_mNickName);
        }
        if (!S_countryView.equals("nulldata")) {
            countryView.setText(S_countryView);
        }
        if (!S_mMotherName.equals("nulldata")) {
            mMotherName.setText(S_mMotherName);
        }
        if (!S_panCardId.equals("nulldata")) {
            panCardId.setText(S_panCardId);
        }
        if (!S_voterId.equals("nulldata")) {
            voterId.setText(S_voterId);
        }
        if (!S_talukView.equals("nulldata")) {
            talukView.setText(S_talukView);
        }
        if (!S_stateView.equals("nulldata")) {
            stateView.setText(S_stateView);
        }



        if (!S_mSpouse.equals("nulldata")) {
            mSpouse.setText(S_mSpouse);
        }
        if (!S_nameView.equals("nulldata")) {
            nameView.setText(S_nameView);
        }
        if (!S_uidView.equals("nulldata")) {
            uidView.setText(S_uidView);
        }
        if (!S_yobView.equals("nulldata")) {
            yobView.setText(S_yobView);
        }
        if (!S_ageView.equals("nulldata")) {
            ageView.setText(S_ageView);
        }


        if (!S_mSpouseAge.equals("nulldata")) {
            mSpouseAge.setText(S_mSpouseAge);
        }
        if (!S_mSpouseDob.equals("nulldata")) {
            mSpouseDob.setText(S_mSpouseDob);
        }
        if (!S_mCenterMeetiong.equals("nulldata")) {
            mCenterMeetiong.setText(S_mCenterMeetiong);
        }
        if (!S_mRtionCard.equals("nulldata")) {
            mRtionCard.setText(S_mRtionCard);
        }


        //////////////////////---------------------------------
        panCard_f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(panCard_f_or_h_name));
        voterId_f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(voterId_f_or_h_name));
        mPermanentAddress.addTextChangedListener(new GenericTWCKYCDF(mPermanentAddress));
        f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(f_or_h_name));
        telephoneNumberView.addTextChangedListener(new GenericTWCKYCDF(telephoneNumberView));
        mobileNumberView.addTextChangedListener(new GenericTWCKYCDF(mobileNumberView));
        voterIdName.addTextChangedListener(new GenericTWCKYCDF(voterIdName));
        panCardName.addTextChangedListener(new GenericTWCKYCDF(panCardName));
        mPostOffice.addTextChangedListener(new GenericTWCKYCDF(mPostOffice));
        mPersonalEmail.addTextChangedListener(new GenericTWCKYCDF(mPersonalEmail));
        districtView.addTextChangedListener(new GenericTWCKYCDF(districtView));
        mLandmarks.addTextChangedListener(new GenericTWCKYCDF(mLandmarks));
        mPoliceStaion.addTextChangedListener(new GenericTWCKYCDF(mPoliceStaion));
        addressView.addTextChangedListener(new GenericTWCKYCDF(addressView));
        pincodeView.addTextChangedListener(new GenericTWCKYCDF(pincodeView));
        mNickName.addTextChangedListener(new GenericTWCKYCDF(mNickName));
        countryView.addTextChangedListener(new GenericTWCKYCDF(countryView));
        mMotherName.addTextChangedListener(new GenericTWCKYCDF(mMotherName));
        panCardId.addTextChangedListener(new GenericTWCKYCDF(panCardId));
        voterId.addTextChangedListener(new GenericTWCKYCDF(voterId));
        talukView.addTextChangedListener(new GenericTWCKYCDF(talukView));
        stateView.addTextChangedListener(new GenericTWCKYCDF(stateView));
        mSpouse.addTextChangedListener(new GenericTWCKYCDF(mSpouse));
        nameView.addTextChangedListener(new GenericTWCKYCDF(nameView));
        uidView.addTextChangedListener(new GenericTWCKYCDF(uidView));
        yobView.addTextChangedListener(new GenericTWCKYCDF(yobView));
        ageView.addTextChangedListener(new GenericTWCKYCDF(ageView));
        mSpouseAge.addTextChangedListener(new GenericTWCKYCDF(mSpouseAge));
        mSpouseDob.addTextChangedListener(new GenericTWCKYCDF(mSpouseDob));
        mCenterMeetiong.addTextChangedListener(new GenericTWCKYCDF(mCenterMeetiong));
        mRtionCard.addTextChangedListener(new GenericTWCKYCDF(mRtionCard));









        mPersonalEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if (!check) {
                    mPersonalEmail.setError("Email address is not valid");
                } else {
                    mPersonalEmail.setError(null);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if (!check) {
                    mPersonalEmail.setError("Email address is not valid");
                } else {
                    mPersonalEmail.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if(!check){
                    mPersonalEmail.setError("Email address is not valid");
                }else{
                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.EMAIL_ID_KEY, s.toString());
                    mPersonalEmail.setError(null);
                }
            }
        });




        String S_mSubTitle = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.NAME_SUBTITLE_KEY);
        String S_mMotherSubtitle = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.MOTHER_NAME_SUBTITLE_KEY );
        String S_mFatherSubTitle = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.FATHER_SUBTITLE_KEY );
        String S_mSpouseSubtitle = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SPOUSE_NAME_SUBTITLE_KEY);
        String S_mGender = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.SINGLE_GENDER_KEY );
        String S_mOccupation = autosave_data.getdata(getContext(), mPage.getFormId(),CustomerKYCDetailsPage.OCCUPATION_KEY );
        String S_mMarital = autosave_data.getdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS);


        if (!S_mSubTitle.equals("nulldata")) {

            int P_mSub = adapterName.getPosition(S_mSubTitle);
            mSubTitle.setSelection(P_mSub);
        }

        if (!S_mMotherSubtitle.equals("nulldata")) {

            int P_mMot = adapterName.getPosition(S_mMotherSubtitle);
            mMotherSubtitle.setSelection(P_mMot);
        }

        if (!S_mFatherSubTitle.equals("nulldata")) {

            int P_mFat = adapterName.getPosition(S_mFatherSubTitle);
            mFatherSubTitle.setSelection(P_mFat);
        }

        if (!S_mSpouseSubtitle.equals("nulldata")) {

            int P_mSpo = adapterName.getPosition(S_mSpouseSubtitle);
            mSpouseSubtitle.setSelection(P_mSpo);
        }

        if (!S_mGender.equals("nulldata")) {

            int mGen =  adapter.getPosition(S_mGender);
            mGender.setSelection(mGen);
        }

        if (!S_mOccupation.equals("nulldata")) {

            int mOccu = occupationAdapter.getPosition(S_mOccupation);
            mOccupation.setSelection(mOccu);
        }

        if (!S_mMarital.equals("nulldata")) {

            int P_mMar = maritalAdapter.getPosition(S_mMarital);
            mMarital.setSelection(P_mMar);
        }

        //---------------spinner

        mSubTitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mMotherSubtitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mFatherSubTitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mSpouseSubtitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mGender.setOnItemSelectedListener(getOnItemCKYCDF());
        mOccupation.setOnItemSelectedListener(getOnItemCKYCDF());









        mMarital.setOnItemSelectedListener(getOnItemCKYCDF());

        mMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Married")){

                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Widow")){
                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Widower")){
                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else {
                    mSpouseAgeText.setVisibility(View.GONE);
                    mSpouseNameText.setVisibility(View.GONE);
                    mSpouseDobText.setVisibility(View.GONE);
                    mSpouse.setVisibility(View.GONE);
                    mSpouseAge.setVisibility(View.GONE);
                    mSpouseDob.setVisibility(View.GONE);
                    mSpouseSubtitle.setVisibility(View.GONE);
                }
                autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
                mPage.getData().putString(CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
















        return rootView;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(year);
        }

    };

    DatePickerDialog.OnDateSetListener spousedate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateSpouseAge(year);
        }

    };

    private int getIndex(Spinner spinner,String selected){
        int index = 0;
        for(int i=0;i<spinner.getCount();i++){
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(selected)){
                index = i;
                i = spinner.getCount();
            }
        }
        return index;
    }
    private void updateLabel(int selectedYear) {
        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final AadhaarCard aadhaarCard = kycCards.aadhaarCard;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        yobView.setText(sdf.format(myCalendar.getTime()));
        int age=Calendar.getInstance().get(Calendar.YEAR) - selectedYear;
        ageView.setText(String.format("%d",age));
        aadhaarCard.setAge(age);
    }

    private void updateSpouseAge(int selectedYear) {

        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final AadhaarCard aadhaarCard = kycCards.aadhaarCard;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        mSpouseDob.setText(sdf.format(myCalendar.getTime()));
        int age = Calendar.getInstance().get(Calendar.YEAR) - selectedYear;
        mSpouseAge.setText(String.format("%d", age));
        aadhaarCard.setSpouseAge(mSpouseAge.getText().toString());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




    }





    public AdapterView.OnItemSelectedListener getOnItemCKYCDF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.kycsubtitle) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(CustomerKYCDetailsPage.NAME_SUBTITLE_KEY, parent.getSelectedItem().toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.fatherubtitle) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.FATHER_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerKYCDetailsPage.FATHER_SUBTITLE_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.single_page_gender) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SINGLE_GENDER_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerKYCDetailsPage.SINGLE_GENDER_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.mothersubtitle) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.MOTHER_NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerKYCDetailsPage.MOTHER_NAME_SUBTITLE_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }
                if (parent.getId() == R.id.custeomer_page_marital_status) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerKYCDetailsPage.SINGLE_MARITAL_STATUS,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }
                if (parent.getId() == R.id.occupation) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.OCCUPATION_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerKYCDetailsPage.OCCUPATION_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spousesubtitle) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SPOUSE_NAME_SUBTITLE_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_NAME_SUBTITLE_KEY,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTWCKYCDF implements TextWatcher {

        private View view;

        private GenericTWCKYCDF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in GenericTextASDF", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.NAME_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.nick_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.NICK_NAME_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.NICK_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.uid) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.UID_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.UID_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.dob) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.YOB_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.YOB_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.age) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.AGE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.AGE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.address) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.ADDRESS_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.ADDRESS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.taluk) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.TALUK_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.TALUK_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.district) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.DISTRICT_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.DISTRICT_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.state) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.STATE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.STATE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.landmark) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.LANDMARKS_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.LANDMARKS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


            if (i1 == R.id.permanent_address) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PERMANENT_ADDRESS_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.PERMANENT_ADDRESS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pincode) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PINCODE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.PINCODE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.police_station) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.POLICE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.POLICE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.post_office) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.POST_OFFICE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.POST_OFFICE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.telephoneNumber) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.TELE_PHONE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.TELE_PHONE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.mobilePhoneNumber) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.MOBILE_PHONE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.MOBILE_PHONE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.personal_email_id) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.EMAIL_ID_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.EMAIL_ID_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.mother) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.MOTHER_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.MOTHER_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.voter_id_number) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.VOTER_ID_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.voter_id_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.VOTER_ID_NAME_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.voter_id_f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pancard_number) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PANCARD_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.pancard_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PANCARD_NAME_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pancard_f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.PANCARD_FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.PANCARD_FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


            if (i1 == R.id.center_meeting) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.CENTERMEETING_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.CENTERMEETING_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.ration_card) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.RATIONCARD_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.RATIONCARD_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }



            if (i1 == R.id.spouse) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SPOUSE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.spouse_age) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SPOUSE_AGE_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_AGE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.spouse_dob) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), CustomerKYCDetailsPage.SPOUSE_DOB_KEY, text);


                    mPage.getData().putString(CustomerKYCDetailsPage.SPOUSE_DOB_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

        }

    }



}
