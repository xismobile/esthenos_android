package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;

public class AppliedLoanDetailFragment extends PageFragment<AppliedLoanDetailPage> {
    public static final String TABLE_APPLIEDLOAN = "Applied_Loan";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;
    Autosave_Data autosave_data = new Autosave_Data();

   static String Adapno,s_Subpurpose;
    EditText mLoanAmoun;
    Spinner mRepayment, mLoanPerpose, mLoanSubPurpose, mLoanAmountUsed1, mLoanAmountUsed2;

    String[] repaymentType = {" ", "Monthly", "Fortnightly", "Weekly"};
    String[] LoanPurpose = {" ", "Agriculture & Allied(4001)", "Business(Micro-Enterprises)(4002)", "Education(4003)",
            "Renewable Energy(4004)", "Housing(4005)", "Miscellaneous(4006)"};
    String[] Agric_subLoan = {" ", "Farm labour", "Crop Cultivation", "Vegetable Cultivation", "Flower Cultivation",
            "Horticulture", "Irrigation", "Land development", "Land purchase & leasing",
            "Plough Animals/Cart/Transport", "Others - Agriculture", "Non Farm labour", "Dairy",
            "Poultry", "Piggery", "Fishery", "Goatery", "Sericulture", "Others-Animal Husbandry"};

    String[] Buss_micro_subLoan = {" ", "Machinery Purchase / Repairs", "Toy Making", "Paper packet making",
            "Idol Making", "Embrodiery", "Pottery Making", "Handloom & Weaving",
            "Handicrafts", "Jewellery Making", "Iron & Metal Works", "Hosiery",
            "Artisan - others", "Sweet Making", "Confectionaries", "Bangle Selling",
            "Leather items", "Food items", "Bidi rolling", "Firecrackers",
            "Furniture", "Others- Production & Manufacturing", "Iron & Metal Works",
            "Garments & Cloth", "Fruits & Vegetable", "Fish, Poultry, Meat & Eggs Sel",
            "Electrical & Machine parts", "Cosmetics", "Jewellery Selling",
            "Grocery", "Stationery Selling", "Newspapers, Magazine & Books",
            "Scrap Business", "Flower", "Spices & Condiments", "Footwear",
            "Bangle Selling", "Other - Retailing", "Sweet Shop", "Tailoring",
            "Electric & Electronic Repairs & Ser", "Plumber Mason Painter Carpenter", "Beautician",
            "Rickshaw puller", "Auto Driver", "Taxi Driver", "Tea / Snacks Shop",
            "Hotel for Meals", "Tutions", "Nurse", "Domestic Worker", "Laundry", "Barber", "Other Services"};

    String[] Edu_subLoan = {" ", "Education"};
    String[] Renew_subLoan = {" ", "Renewable Energy"};
    String[] House_subLoan = {" ", "House Repairs"};
    String[] Misc_subLoan = {" ", "Household Expenses", "Consumer Durable Purchase", "Repayment of Debt", "Medical", "Others"};
    String[] defaultvalue = {""};
    String[] loanAmountUsed = {"", "Brother", "Sister", "Self", "Spouse", "Mother", "Father", "Daughter", "Son", "Grand Mother",
            "Grand Father", "Grand Son", "Grand Daughter", "Relative", "Father in Law", "Mother in Law",
            "Brother in Law", "Sister in Law", "Daughter in Law"};


    public static AppliedLoanDetailFragment create(String key) {
        AppliedLoanDetailFragment fragment = new AppliedLoanDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_loan_detail, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mydb = new MySQLiteDB(getContext());

        String s_AppliedLoanPerpos = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_PURPOSE_KEY);
        String s_AppliedLoanamount = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_REQUIRED_KEY);
        String s_AppliedLoanrepayment = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_REPAYMENT_KEY);
        String s_AppliedLoanamountuse1 = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_AMOUNT_USED1_KEY);
        String s_AppliedLoanamountuse2 = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_AMOUNT_USED2_KEY);

        // autosave = new AutoSaveData(getContext());

        mLoanPerpose = (Spinner) rootView.findViewById(R.id.loan_perpose);
        ArrayAdapter<String> loanAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, LoanPurpose);
        loanAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mLoanPerpose.setAdapter(loanAdapter);
        mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
        if (!s_AppliedLoanPerpos.equals("nulldata")) {
            mLoanPerpose.setSelection(loanAdapter.getPosition(s_AppliedLoanPerpos));
        }


        mLoanSubPurpose = (Spinner) rootView.findViewById(R.id.loan_sub_perpose);



        mLoanAmoun = (EditText) rootView.findViewById(R.id.loan_require);
        mPage.getData().putString(AppliedLoanDetailPage.LOAN_REQUIRED_KEY, mLoanAmoun.getText().toString());
        if (!s_AppliedLoanamount.equals("nulldata")) {
            mLoanAmoun.setText(s_AppliedLoanamount);
        }
//        mLoanAmoun.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_APPLIEDLOAN, MySQLiteDB.LOAN_REQUIRED_KEY, i, mLoanAmoun.getText().toString());
//                }
//            }
//        });

        mRepayment = (Spinner) rootView.findViewById(R.id.spinner_loan_repayment_type);
        ArrayAdapter<String> repaymentadapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, repaymentType);
        repaymentadapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRepayment.setAdapter(repaymentadapter);
        mPage.getData().putString(AppliedLoanDetailPage.LOAN_REPAYMENT_KEY, mRepayment.getSelectedItem().toString());
        if (!s_AppliedLoanrepayment.equals("nulldata")) {
            mRepayment.setSelection(repaymentadapter.getPosition(s_AppliedLoanrepayment));
        }

        mLoanAmountUsed1 = (Spinner) rootView.findViewById(R.id.loan_used_amount_first);
        ArrayAdapter<String> loanusedAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, loanAmountUsed);
        loanusedAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mLoanAmountUsed1.setAdapter(loanusedAdapter);
        mPage.getData().putString(AppliedLoanDetailPage.LOAN_AMOUNT_USED1_KEY, mLoanAmountUsed1.getSelectedItem().toString());
        if (!s_AppliedLoanamountuse1.equals("nulldata")) {
            mLoanAmountUsed1.setSelection(loanusedAdapter.getPosition(s_AppliedLoanamountuse1));
        }

        mLoanAmountUsed2 = (Spinner) rootView.findViewById(R.id.loan_used_amount_second);
        mLoanAmountUsed2.setAdapter(loanusedAdapter);
        mPage.getData().putString(AppliedLoanDetailPage.LOAN_AMOUNT_USED2_KEY, mLoanAmountUsed2.getSelectedItem().toString());
        if (!s_AppliedLoanamountuse2.equals("nulldata")) {
            mLoanAmountUsed2.setSelection(loanusedAdapter.getPosition(s_AppliedLoanamountuse2));
        }

        mLoanAmountUsed1.setOnItemSelectedListener(getOnItem());
        mLoanAmountUsed2.setOnItemSelectedListener(getOnItem());
        mLoanPerpose.setOnItemSelectedListener(getOnItem());
        mLoanAmoun.addTextChangedListener(new GenericTextAppliedLoan(mLoanAmoun));
        mRepayment.setOnItemSelectedListener(getOnItem());
        mLoanSubPurpose.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        mLoanAmountUsed1.setOnItemSelectedListener(getOnItemSelectedListener(AppliedLoanDetailPage.LOAN_AMOUNT_USED1_KEY));
//        mLoanAmountUsed2.setOnItemSelectedListener(getOnItemSelectedListener(AppliedLoanDetailPage.LOAN_AMOUNT_USED2_KEY));

//        mLoanAmoun.addTextChangedListener(getTextWatcher(AppliedLoanDetailPage.LOAN_REQUIRED_KEY));
//        mRepayment.setOnItemSelectedListener(getOnItemSelectedListener(AppliedLoanDetailPage.LOAN_REPAYMENT_KEY));
//        mLoanSubPurpose.setOnItemSelectedListener(getOnItemSelectedListener(AppliedLoanDetailPage.LOAN_SUB_PURPOSE_KEY));


        /*String s_AppliedLoansubPerpos = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_SUB_PURPOSE_KEY);
        String s_subloadAdaptype = autosave_data.getdata(getContext(), mPage.getFormId(), "ALDFADAP");
        if (!s_AppliedLoansubPerpos.equals("nulldata") && s_subloadAdaptype.equals("1")) {
            mLoanPerpose.setSelection(repaymentadapter.getPosition(s_AppliedLoanPerpos));
        }*/


        /*
        mRepayment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_APPLIEDLOAN, MySQLiteDB.LOAN_REPAYMENT_KEY, i, value);
            }
        });

        //todo use setOnItemSelectedListener insted of  setOnItemClickListener
        mLoanAmountUsed1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_APPLIEDLOAN, MySQLiteDB.LOAN_AMOUNT_USED1_KEY,i,value );
            }
        });
        mLoanAmountUsed2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_APPLIEDLOAN, MySQLiteDB.LOAN_AMOUNT_USED2_KEY,i,value );
            }
        });
        mLoanSubPurpose.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_APPLIEDLOAN, MySQLiteDB.LOAN_SUB_PURPOSE_KEY,i,value );
            }
        });*/

        refresh();
        Log.d("Gravity","ALD Adapterno"+Adapno);
        Log.d("Gravity","ALD Subpurpose"+s_Subpurpose);

        mLoanPerpose.setOnItemSelectedListener(getOnItemSelectedListener(AppliedLoanDetailPage.LOAN_PURPOSE_KEY));
        mLoanPerpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Agriculture & Allied(4001)")) {
                    Log.d("Gravity","in Adap1");
                    ArrayAdapter<String> repaymentadapter1 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Agric_subLoan);
                    repaymentadapter1.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "1");
                    mLoanSubPurpose.setAdapter(repaymentadapter1);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());

                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Business(Micro-Enterprises)(4002)")) {
                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "2");
                    Log.d("Gravity","in Adap2");
                    ArrayAdapter<String> repaymentadapter2 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Buss_micro_subLoan);
                    repaymentadapter2.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    mLoanSubPurpose.setAdapter(repaymentadapter2);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Renewable Energy(4004)")) {
                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "3");
                    Log.d("Gravity","in Adap3");
                    ArrayAdapter<String> repaymentadapter3 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Renew_subLoan);
                    repaymentadapter3.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    mLoanSubPurpose.setAdapter(repaymentadapter3);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Housing(4005)")) {
                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "4");

                    ArrayAdapter<String> repaymentadapter4 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, House_subLoan);
                    Log.d("Gravity","in Adap4");
                    repaymentadapter4.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    mLoanSubPurpose.setAdapter(repaymentadapter4);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Miscellaneous(4006)")) {
                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "5");
                    Log.d("Gravity","in Adap5");
                    ArrayAdapter<String> repaymentadapter5 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Misc_subLoan);
                    repaymentadapter5.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    mLoanSubPurpose.setAdapter(repaymentadapter5);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Education(4003)")) {

                    autosave_data.putdata(getContext(), mPage.getFormId(), "ALDFADAP", "6");
                    Log.d("Gravity","in Adap6");ArrayAdapter<String> repaymentadapter6 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Edu_subLoan);
                    repaymentadapter6.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                    mLoanSubPurpose.setAdapter(repaymentadapter6);
                    mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, mLoanPerpose.getSelectedItem().toString());
                }

                Adapno = autosave_data.getdata(getContext(), mPage.getFormId(), "ALDFADAP");
                autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_PURPOSE_KEY, parent.getItemAtPosition(position).toString());
                refresh();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                //mLoanSubPurpose.setVisibility(View.INVISIBLE);
            }
        });



    }

    private void refresh() {

        Adapno = autosave_data.getdata(getContext(), mPage.getFormId(), "ALDFADAP");
        s_Subpurpose = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_SUB_PURPOSE_KEY);
        switch (Adapno) {
            case "1":
                ArrayAdapter<String> repaymentadapter1 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Agric_subLoan);
                repaymentadapter1.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter1);
                mLoanSubPurpose.setSelection(repaymentadapter1.getPosition(s_Subpurpose));
                break;
            case "2":
                ArrayAdapter<String> repaymentadapter2 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Buss_micro_subLoan);
                repaymentadapter2.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter2);
                mLoanSubPurpose.setSelection(repaymentadapter2.getPosition(s_Subpurpose));
                break;
            case "3":
                ArrayAdapter<String> repaymentadapter3 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Renew_subLoan);
                repaymentadapter3.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter3);
                mLoanSubPurpose.setSelection(repaymentadapter3.getPosition(s_Subpurpose));
                break;
            case "4":
                ArrayAdapter<String> repaymentadapter4 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, House_subLoan);
                repaymentadapter4.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter4);
                mLoanSubPurpose.setSelection(repaymentadapter4.getPosition(s_Subpurpose));
                break;
            case "5":
                ArrayAdapter<String> repaymentadapter5 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Misc_subLoan);
                repaymentadapter5.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter5);
                mLoanSubPurpose.setSelection(repaymentadapter5.getPosition(s_Subpurpose));
                break;
            case "6":
                ArrayAdapter<String> repaymentadapter6 = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, Edu_subLoan);
                repaymentadapter6.setDropDownViewResource(R.layout.layout_spinner_dropdown);
                mLoanSubPurpose.setAdapter(repaymentadapter6);
                mLoanSubPurpose.setSelection(repaymentadapter6.getPosition(s_Subpurpose));
                break;
        }


    }

    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.loan_sub_perpose) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();


                        autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_SUB_PURPOSE_KEY, parent.getItemAtPosition(position).toString());
                        s_Subpurpose = autosave_data.getdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_SUB_PURPOSE_KEY);
                        mPage.getData().putString(AppliedLoanDetailPage.LOAN_PURPOSE_KEY, String.valueOf(parent.getSelectedItemPosition()));
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.spinner_loan_repayment_type) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_REPAYMENT_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AppliedLoanDetailPage.LOAN_REPAYMENT_KEY, String.valueOf(parent.getSelectedItemPosition()));
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.loan_used_amount_first) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_AMOUNT_USED1_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AppliedLoanDetailPage.LOAN_AMOUNT_USED1_KEY, String.valueOf(parent.getSelectedItemPosition()));
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.loan_used_amount_second) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_AMOUNT_USED2_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(AppliedLoanDetailPage.LOAN_AMOUNT_USED2_KEY, String.valueOf(parent.getSelectedItemPosition()));
                        mPage.notifyDataChanged();
                    }
                }
//                if (parent.getId() == R.id.risk_customer_eligible_value) {
//                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
//                        autosave_data.putdata(getContext(),mPage.getFormId(), AppliedLoanDetailPage.KEY_CUSS_ELIGIBLE, parent.getItemAtPosition(position).toString());
//                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
//                        mPage.getData().putString(AppliedLoanDetailPage.KEY_CUSS_ELIGIBLE, String.valueOf(parent.getSelectedItemPosition()));
//                        mPage.notifyDataChanged();
//                    }
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

    class GenericTextAppliedLoan implements TextWatcher {

        private View view;

        private GenericTextAppliedLoan(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();

            if (i1 == R.id.loan_require) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(), mPage.getFormId(), AppliedLoanDetailPage.LOAN_REQUIRED_KEY, text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(AppliedLoanDetailPage.LOAN_REQUIRED_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }
}
