package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class ProgressOutPropertySecondFragment extends PageFragment<ProgressOutPropertySecondPage> {


    Spinner mTelevision,mMobile,mSewing,mAlmirah,mBicycle;

    String [] valueTelevision={"","No, Neither One","Yes, Only One","Yes, Both"};
    String [] valueMobile={"","No, Neither One","Yes, Only a Mobile","Yes, a Landline, Regardless of Mobile"};
    String [] valueSewing={"","Yes","No"};
    String [] valueBicycle={"","No, One","Yes, Bicycle Only,No Motorcycle/Scooter,or Car",
            "Motorcycle/Scooter, But no Car(Regardless of Bicycle)"
            ,"Motor Car/Jeep (Regardless of Others)"};

    Autosave_Data autosave_data =new Autosave_Data();

    public static ProgressOutPropertySecondFragment create(String key) {
        ProgressOutPropertySecondFragment fragment = new ProgressOutPropertySecondFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_progress_out_property_second, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_progressoutPropertysecondtelevision=autosave_data.getdata(getContext(), "ProgressOutPropertySecondFragment",ProgressOutPropertySecondPage.KEY_PROGRESS_TELEVISION);
        String s_progressoutPropertysecondmobile=autosave_data.getdata(getContext(), "ProgressOutPropertySecondFragment",ProgressOutPropertySecondPage.KEY_PROGRESS_MOBILE);
        String s_progressoutPropertysecondtesewing=autosave_data.getdata(getContext(), "ProgressOutPropertySecondFragment",ProgressOutPropertySecondPage.KEY_PROGRESS_SEWING);
        String s_progressoutPropertysecondalmirah=autosave_data.getdata(getContext(), "ProgressOutPropertySecondFragment",ProgressOutPropertySecondPage.KEY_PROGRESS_ALMIRAH);
        String s_progressoutPropertysecondbicycle=autosave_data.getdata(getContext(), "ProgressOutPropertySecondFragment",ProgressOutPropertySecondPage.KEY_PROGRESS_BICYCLE);

        mTelevision=(Spinner)rootView.findViewById(R.id.progress_television);
        ArrayAdapter televisionAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueTelevision);
        televisionAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mTelevision.setAdapter(televisionAdapter);
        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_TELEVISION,mTelevision.getSelectedItem().toString());
        if (!s_progressoutPropertysecondtelevision.equals("nulldata")){
            mTelevision.setSelection(televisionAdapter.getPosition(s_progressoutPropertysecondtelevision));
        }

        mMobile=(Spinner)rootView.findViewById(R.id.progress_mobile);
        ArrayAdapter mobileAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueMobile);
        mobileAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mMobile.setAdapter(mobileAdapter);
        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_MOBILE, mMobile.getSelectedItem().toString());
        if (!s_progressoutPropertysecondmobile.equals("nulldata")){
            mMobile.setSelection(mobileAdapter.getPosition(s_progressoutPropertysecondmobile));
        }

        mSewing=(Spinner)rootView.findViewById(R.id.progress_sewing);
        ArrayAdapter sewingAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueSewing);
        sewingAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSewing.setAdapter(sewingAdapter);
        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_SEWING,mSewing.getSelectedItem().toString());
        if (!s_progressoutPropertysecondtesewing.equals("nulldata")){
            mSewing.setSelection(sewingAdapter.getPosition(s_progressoutPropertysecondtesewing));
        }

        mAlmirah=(Spinner)rootView.findViewById(R.id.progress_almirah);
        ArrayAdapter almirahAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueSewing);
        almirahAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mAlmirah.setAdapter(almirahAdapter);
        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_ALMIRAH,mAlmirah.getSelectedItem().toString());
        if (!s_progressoutPropertysecondalmirah.equals("nulldata")){
            mAlmirah.setSelection(almirahAdapter.getPosition(s_progressoutPropertysecondalmirah));
        }

        mBicycle=(Spinner)rootView.findViewById(R.id.progress_bicycle);
        ArrayAdapter bicycleAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,valueBicycle);
        bicycleAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mBicycle.setAdapter(bicycleAdapter);
        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_BICYCLE, mBicycle.getSelectedItem().toString());
        if (!s_progressoutPropertysecondbicycle.equals("nulldata")){
            mBicycle.setSelection(bicycleAdapter.getPosition(s_progressoutPropertysecondbicycle));
        }


        mTelevision.setOnItemSelectedListener(getOnItem());
        mMobile.setOnItemSelectedListener(getOnItem());
        mSewing.setOnItemSelectedListener(getOnItem());
        mAlmirah.setOnItemSelectedListener(getOnItem());
        mBicycle.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

//        mTelevision.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertySecondPage.KEY_PROGRESS_TELEVISION));
//        mMobile.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertySecondPage.KEY_PROGRESS_MOBILE));
//        mSewing.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertySecondPage.KEY_PROGRESS_SEWING));
//        mAlmirah.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertySecondPage.KEY_PROGRESS_ALMIRAH));
//        mBicycle.setOnItemSelectedListener(getOnItemSelectedListener(ProgressOutPropertySecondPage.KEY_PROGRESS_BICYCLE));

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.progress_television) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),"ProgressOutPropertySecondFragment", ProgressOutPropertySecondPage.KEY_PROGRESS_TELEVISION, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_TELEVISION,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_mobile) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertySecondFragment", ProgressOutPropertySecondPage.KEY_PROGRESS_MOBILE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_MOBILE,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_sewing) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertySecondFragment", ProgressOutPropertySecondPage.KEY_PROGRESS_SEWING, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_SEWING,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_almirah) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertySecondFragment", ProgressOutPropertySecondPage.KEY_PROGRESS_ALMIRAH, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_ALMIRAH,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.progress_bicycle) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"ProgressOutPropertySecondFragment", ProgressOutPropertySecondPage.KEY_PROGRESS_BICYCLE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(ProgressOutPropertySecondPage.KEY_PROGRESS_BICYCLE,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }
}
