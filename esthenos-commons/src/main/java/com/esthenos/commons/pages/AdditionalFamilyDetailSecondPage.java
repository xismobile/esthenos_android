package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class AdditionalFamilyDetailSecondPage extends  Page {

    public static final String KEY_OCCUPATION_DETAILS_SECOND = "Occupations Details";
    public static final String KEY_INVOLVEMENY_SECOND = "Years of Involvement";
    public static final String KEY_ANNUAL_INCOME_SECOND = "Monthly Income";
    public static final String KEY_RELATION_SECOND = "Relationship";
    public static final String KEY_EDUCATION_SECOND = "Education";
    public static final String KEY_NAME_SECOND = "Name";
    public static final String KEY_AGE_SECOND = "Age";
    public static final String KEY_SEX_SECOND = "Sex";

    public AdditionalFamilyDetailSecondPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return AdditionalFamilyDetailSecondFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_OCCUPATION_DETAILS_SECOND, mData.getString(KEY_OCCUPATION_DETAILS_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ANNUAL_INCOME_SECOND, mData.getString(KEY_ANNUAL_INCOME_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_EDUCATION_SECOND, mData.getString(KEY_EDUCATION_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_RELATION_SECOND, mData.getString(KEY_RELATION_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NAME_SECOND, mData.getString(KEY_NAME_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_AGE_SECOND, mData.getString(KEY_AGE_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_SEX_SECOND, mData.getString(KEY_SEX_SECOND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_INVOLVEMENY_SECOND, mData.getString(KEY_INVOLVEMENY_SECOND), getPageId(), -1));
    }

//    @Override
//    public boolean isCompleted() {
//        return
//                !TextUtils.isEmpty(mData.getString(KEY_NAME_SECOND));
//    }
}

