package com.esthenos.commons.pages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.customcamera.CustomCameraActivity;
import com.tech.freak.wizardpager.ui.PageFragmentCallbacks;


public class ImageCaptureFragment extends Fragment implements View.OnFocusChangeListener {

    private String mKey;
    private static final String ARG_KEY = "key";

    public static final String BARCODE_KEY = "barcode_key";
    private static final int UPLOAD_KYC = 1000;

    private View viewInFocus;
    private ImageCapturePage mPage;
    private PageFragmentCallbacks mCallbacks;

    public static ImageCaptureFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        ImageCaptureFragment fragment = new ImageCaptureFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ImageCaptureFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (ImageCapturePage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.capture_aadhaar_image, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        ImageView iv = (ImageView) rootView.findViewById(R.id.aadhaar_image);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent customCam = new Intent(getActivity(), CustomCameraActivity.class);
                        customCam.putExtra("card_type", mPage.getPageType());
                        customCam.putExtra("card_name", mPage.getFormId());
                        customCam.putExtra("card_owner", mPage.getPageOwner());
                        startActivityForResult(customCam, UPLOAD_KYC);
                    }
                });
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof PageFragmentCallbacks)) {
            throw new ClassCastException("Activity must implement PageFragmentCallbacks");
        }
        mCallbacks = (PageFragmentCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view != viewInFocus && !b)
            viewInFocus = view;
    }
}
