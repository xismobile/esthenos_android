package com.esthenos.commons.pages;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;


public class BusinessDetailsFragment extends PageFragment<BusinessDetails> {

    final String[] bizSeasons = {"","Yearly", "Quarterly", "Monthly", "Others"};
    final String[] bizPremises = {"","Own Premise", "Rented Premise", "Leased Premise"};
    final String[] bizCategories = {"","Trading", "Services", "Agriculture", "Allied Agriculture", "Small Manufacturing", "Labour"};

    private Spinner bizCateory, bizSeasonality, bizPremise;
    private EditText bizActivity, bizIncomeMonthly, bizNumYears, bizNumEmployees;
    private EditText bizExpenseRent, bizExpenseOther, bizExpenseAdmin, bizExpenseWorkingCapital, bizExpenseEmpSalary;

    public static Fragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        BusinessDetailsFragment fragment = new BusinessDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_business_details, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        bizCateory = (Spinner) rootView.findViewById(R.id.business_category);
        ArrayAdapter<String> adapterCategories = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, bizCategories);
        adapterCategories.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bizCateory.setAdapter(adapterCategories);
        mPage.getData().putString(BusinessDetails.BIZ_CATEGORY, bizCateory.getSelectedItem().toString());

        bizActivity = (EditText) rootView.findViewById(R.id.business_activity);
        mPage.getData().putString(BusinessDetails.BIZ_ACTIVITY, bizActivity.getText().toString());

        bizIncomeMonthly = (EditText) rootView.findViewById(R.id.business_income_monthly);
        mPage.getData().putString(BusinessDetails.BIZ_INCOME_MONTHLY, bizIncomeMonthly.getText().toString());

        bizNumYears = (EditText) rootView.findViewById(R.id.business_num_years);
        mPage.getData().putString(BusinessDetails.BIZ_NUM_YEARS, bizNumYears.getText().toString());

        bizNumEmployees = (EditText) rootView.findViewById(R.id.business_num_employees);
        mPage.getData().putString(BusinessDetails.BIZ_NUM_EMPLOYEES, bizNumEmployees.getText().toString());

        bizExpenseRent = (EditText) rootView.findViewById(R.id.business_expense_rent);
        mPage.getData().putString(BusinessDetails.BIZ_EXPENSE_RENT, bizExpenseRent.getText().toString());

        bizExpenseOther = (EditText) rootView.findViewById(R.id.business_expense_other);
        mPage.getData().putString(BusinessDetails.BIZ_EXPENSE_OTHER, bizExpenseOther.getText().toString());

        bizExpenseAdmin = (EditText) rootView.findViewById(R.id.business_expense_admin);
        mPage.getData().putString(BusinessDetails.BIZ_EXPENSE_ADMIN, bizExpenseAdmin.getText().toString());

        bizExpenseWorkingCapital = (EditText) rootView.findViewById(R.id.business_expense_working_capital);
        mPage.getData().putString(BusinessDetails.BIZ_EXPENSE_WORKING_CAPITAL, bizExpenseWorkingCapital.getText().toString());

        bizExpenseEmpSalary = (EditText) rootView.findViewById(R.id.business_expense_employee_salary);
        mPage.getData().putString(BusinessDetails.BIZ_EXPENSE_SALARY, bizExpenseEmpSalary.getText().toString());

        bizPremise = (Spinner) rootView.findViewById(R.id.business_premise);
        ArrayAdapter<String> adapterPremises = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, bizPremises);
        adapterPremises.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bizPremise.setAdapter(adapterPremises);
        mPage.getData().putString(BusinessDetails.BIZ_PREMISE, bizPremise.getSelectedItem().toString());

        bizSeasonality = (Spinner) rootView.findViewById(R.id.business_seasonality);
        ArrayAdapter<String> adapterSeasons = new ArrayAdapter<>(getActivity(), R.layout.layout_spinner_item, bizSeasons);
        adapterSeasons.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        bizSeasonality.setAdapter(adapterSeasons);
        mPage.getData().putString(BusinessDetails.BIZ_SEASONALITY, bizSeasonality.getSelectedItem().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bizActivity.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_ACTIVITY));
        bizIncomeMonthly.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_INCOME_MONTHLY));

        bizNumYears.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_NUM_YEARS));
        bizNumEmployees.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_NUM_EMPLOYEES));

        bizExpenseRent.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_EXPENSE_RENT));
        bizExpenseOther.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_EXPENSE_OTHER));
        bizExpenseAdmin.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_EXPENSE_ADMIN));
        bizExpenseEmpSalary.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_EXPENSE_SALARY));
        bizExpenseWorkingCapital.addTextChangedListener(getTextWatcher(BusinessDetails.BIZ_EXPENSE_WORKING_CAPITAL));

        bizPremise.setOnItemSelectedListener(getOnItemSelectedListener(BusinessDetails.BIZ_PREMISE));
        bizCateory.setOnItemSelectedListener(getOnItemSelectedListener(BusinessDetails.BIZ_CATEGORY));
        bizSeasonality.setOnItemSelectedListener(getOnItemSelectedListener(BusinessDetails.BIZ_SEASONALITY));
    }
}
