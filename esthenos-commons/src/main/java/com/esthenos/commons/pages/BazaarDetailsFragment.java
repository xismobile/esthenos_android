package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.tech.freak.wizardpager.ui.PageFragment;


public class BazaarDetailsFragment extends PageFragment<BazaarDetailsPage> {


    private EditText mBazarName,mBazarAddress,mBazarStall,mBazarYear,mBazarCommodites,mUniqueId;
    private Spinner mBusinessProof,mTypeOfProof;

    Autosave_Data autosave_data =new Autosave_Data();

    private final String[] businessProof = {"","Own","Rented"};
    private final String[] typeofProof = {"", "ID Proof","Copy of License","Copy of Agreement"};

    public static BazaarDetailsFragment create(String key) {
        BazaarDetailsFragment fragment = new BazaarDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View rootView=inflater.inflate(R.layout.fragment_bazaar_details, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_Bazaardetailname=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_NAME);
        String s_Bazaardetailaddress=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_ADDRESS);
        String s_Bazaardetailbazarstall=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_STALL);
        String s_Bazaardetailuidproof=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_UNIQUE);
        String s_Bazaardetailyob=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_YEAR);
        String s_Bazaardetailcs=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_COMMODITIES);
        String s_Bazaardetailbp=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_BUSINESS_PROOF);
        String s_Bazaardetailtp=autosave_data.getdata(getContext(), mPage.getFormId(),BazaarDetailsPage.BAZAR_TYPE_OF_PROOF);

        mBazarName=(EditText)rootView.findViewById(R.id.bazar_name_value);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_NAME,mBazarName.getText().toString());
        if (!s_Bazaardetailname.equals("nulldata")){
            mBazarName.setText(s_Bazaardetailname);
        }

        mBazarAddress=(EditText)rootView.findViewById(R.id.bazar_address_value);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_ADDRESS,mBazarAddress.getText().toString());
        if (!s_Bazaardetailaddress.equals("nulldata")){
            mBazarAddress.setText(s_Bazaardetailaddress);
        }

        mBazarStall=(EditText)rootView.findViewById(R.id.bazar_stall_value);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_STALL,mBazarStall.getText().toString());
        if (!s_Bazaardetailbazarstall.equals("nulldata")){
            mBazarStall.setText(s_Bazaardetailbazarstall);
        }

        mBusinessProof = (Spinner)rootView.findViewById(R.id.bazar_business_proof);
        ArrayAdapter<String> businessAdapter = new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,businessProof);
        businessAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mBusinessProof.setAdapter(businessAdapter);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_BUSINESS_PROOF,mBusinessProof.getSelectedItem().toString());
        if (!s_Bazaardetailbp.equals("nulldata")){
            mBusinessProof.setSelection(businessAdapter.getPosition(s_Bazaardetailbp));
        }

        mTypeOfProof = (Spinner)rootView.findViewById(R.id.bazar_stall_type_proof);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,typeofProof);
        typeAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mTypeOfProof.setAdapter(typeAdapter);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_TYPE_OF_PROOF, mTypeOfProof.getSelectedItem().toString());
        if (!s_Bazaardetailtp.equals("nulldata")){
            mTypeOfProof.setSelection(typeAdapter.getPosition(s_Bazaardetailtp));
        }

        mUniqueId = (EditText)rootView.findViewById(R.id.bazar_stall_unique_id);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_UNIQUE, mUniqueId.getText().toString());
        if (!s_Bazaardetailuidproof.equals("nulldata")){
            mUniqueId.setText(s_Bazaardetailuidproof);
        }

        mBazarYear=(EditText)rootView.findViewById(R.id.bazar_year_value);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_YEAR,mBazarYear.getText().toString());
        if (!s_Bazaardetailyob.equals("nulldata")){
            mBazarYear.setText(s_Bazaardetailyob);
        }

        mBazarCommodites=(EditText)rootView.findViewById(R.id.bazar_commodities_value);
        mPage.getData().putString(BazaarDetailsPage.BAZAR_COMMODITIES, mBazarCommodites.getText().toString());
        if (!s_Bazaardetailcs.equals("nulldata")){
            mBazarCommodites.setText(s_Bazaardetailcs);
        }


        mBazarName.addTextChangedListener(new GenericTextBazaardetailfrag(mBazarName));
        mBazarAddress.addTextChangedListener(new GenericTextBazaardetailfrag(mBazarAddress));
        mBazarStall.addTextChangedListener(new GenericTextBazaardetailfrag(mBazarStall));
        mBazarYear.addTextChangedListener(new GenericTextBazaardetailfrag(mBazarYear));
        mBazarCommodites.addTextChangedListener(new GenericTextBazaardetailfrag(mBazarCommodites));
        mUniqueId.addTextChangedListener(new GenericTextBazaardetailfrag(mUniqueId));
        mBusinessProof.setOnItemSelectedListener(getOnItem());
        mTypeOfProof.setOnItemSelectedListener(getOnItem());
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle SavedInstanceState){
        super.onViewCreated(view,SavedInstanceState);

//        mBazarName.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_NAME));
//        mBazarAddress.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_ADDRESS));
//        mBazarStall.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_STALL));
//        mBazarYear.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_YEAR));
//        mBazarCommodites.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_COMMODITIES));
//        mUniqueId.addTextChangedListener(getTextWatcher(BazaarDetailsPage.BAZAR_UNIQUE));
//        mBusinessProof.setOnItemSelectedListener(getOnItemSelectedListener(BazaarDetailsPage.BAZAR_BUSINESS_PROOF));
//        mTypeOfProof.setOnItemSelectedListener(getOnItemSelectedListener(BazaarDetailsPage.BAZAR_TYPE_OF_PROOF));

    }

    class GenericTextBazaardetailfrag implements TextWatcher {

        private View view;

        private GenericTextBazaardetailfrag(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.bazar_name_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_NAME,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_NAME, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazar_address_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_ADDRESS,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_ADDRESS, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazar_stall_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_STALL,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_STALL, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazar_stall_unique_id) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_UNIQUE,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_UNIQUE, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazar_year_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_YEAR,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_YEAR, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.bazar_commodities_value) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),BazaarDetailsPage.BAZAR_COMMODITIES,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_COMMODITIES, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.bazar_business_proof) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        autosave_data.putdata(getContext(),mPage.getFormId(), BazaarDetailsPage.BAZAR_BUSINESS_PROOF, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(BazaarDetailsPage.BAZAR_BUSINESS_PROOF, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.bazar_stall_type_proof) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), BazaarDetailsPage.BAZAR_TYPE_OF_PROOF, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(BazaarDetailsPage.BAZAR_TYPE_OF_PROOF, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }
}
