package com.esthenos.commons.pages;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;

public class CustomerPersonalDetailsFragment extends PageFragment<CustomerPersonalDetailsPage> {

    public static final String TABLE_PERSONALDETAIL_NAME = "PersonalDetails";


    private static final String[] physical = {"", "None", "Partial", "Total"};
    private static final String[] religion = {"", "Hindu", "Muslim", "Christian", "Jain", "Sikh", "Other"};
    private static final String[] category = {"", "General", "Other Backward Class", "Schedule Cast", "Schedule Tribe", "Other"};
    private static final String[] education = {"", "Illiterate", "Upto 5th class", "Upto 10th Class", "Upto Class 12", "Graduate",
            "PG and Above", "Semi literate"};
    //...............................................
    public MySQLiteDB mydb;
    Spinner mPhysical, mReligion, mCategory, mEducation;
    long i;
    //..............................................

   Autosave_Data autosave_data =new Autosave_Data();

    public static CustomerPersonalDetailsFragment create(String key) {
        CustomerPersonalDetailsFragment fragment = new CustomerPersonalDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        String S_physical=autosave_data.getdata(getContext(), "CustomerPersonalDetailsFragment",CustomerPersonalDetailsPage.SINGLE_PHYSICAL_DISABILITY_KEY);
        String S_religion=autosave_data.getdata(getContext(), "CustomerPersonalDetailsFragment",CustomerPersonalDetailsPage.SINGLE_RELIGION);
        String S_category=autosave_data.getdata(getContext(), "CustomerPersonalDetailsFragment",CustomerPersonalDetailsPage.SINGLE_CATEGORY);
        String S_education=autosave_data.getdata(getContext(), "CustomerPersonalDetailsFragment",CustomerPersonalDetailsPage.SINGLE_EDUCATION);


        View rootView = inflater.inflate(R.layout.fragment_customer_personal_details, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mPhysical = (Spinner) rootView.findViewById(R.id.single_page_physical);
        ArrayAdapter physicalAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, physical);
        physicalAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mPhysical.setAdapter(physicalAdapter);
        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_PHYSICAL_DISABILITY_KEY, mPhysical.getSelectedItem().toString());
        mPhysical.setOnItemSelectedListener(getOnItem());
        if (!S_physical.equals("nulldata")){

            int P_phy = physicalAdapter.getPosition(S_physical);
            mPhysical.setSelection(P_phy) ;
        }




        mReligion = (Spinner) rootView.findViewById(R.id.single_page_religion);
        ArrayAdapter religionAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, religion);
        religionAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mReligion.setAdapter(religionAdapter);
        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_RELIGION, mReligion.getSelectedItem().toString());
        //......................................................



        mReligion.setOnItemSelectedListener(getOnItem());

        if (!S_religion.equals("nulldata")){

            int P_rel = religionAdapter.getPosition(S_religion);
            mReligion.setSelection(P_rel) ;
        }

        /*{

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(getContext(), "hhriligian a : " + parent.getItemAtPosition(position), Toast.LENGTH_LONG).show();
                // Autosavebyid((String) getText(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
//.........................................................
        mCategory = (Spinner) rootView.findViewById(R.id.single_page_category);
        ArrayAdapter categoryAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, category);
        categoryAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mCategory.setAdapter(categoryAdapter);
        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_CATEGORY, mCategory.getSelectedItem().toString());
        mCategory.setOnItemSelectedListener(getOnItem());


        mCategory.setOnItemSelectedListener(getOnItem());

        if (!S_category.equals("nulldata")){

            int P_cat = categoryAdapter.getPosition(S_category);
            mCategory.setSelection(P_cat) ;
        }


        mEducation = (Spinner) rootView.findViewById(R.id.single_page_education);
        ArrayAdapter educationAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, education);
        educationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mEducation.setAdapter(educationAdapter);


        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, mEducation.getSelectedItem().toString());

        mEducation.setOnItemSelectedListener(getOnItem());

        if (!S_education.equals("nulldata")){

            int P_edu = educationAdapter.getPosition(S_education);
            mEducation.setSelection(P_edu) ;
        }

        //----------------


        Log.v("88888888888888888", String.valueOf(mPage));

        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //================ todo Alfishan : mydb not initalized error occure so i added below line
        mydb = new MySQLiteDB(getContext());
    }

    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.single_page_religion) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),"CustomerPersonalDetailsFragment", CustomerPersonalDetailsPage.SINGLE_RELIGION, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_RELIGION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.single_page_category) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"CustomerPersonalDetailsFragment", CustomerPersonalDetailsPage.SINGLE_CATEGORY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_CATEGORY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.single_page_education) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),"CustomerPersonalDetailsFragment", CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.single_page_physical) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),"CustomerPersonalDetailsFragment", CustomerPersonalDetailsPage.SINGLE_PHYSICAL_DISABILITY_KEY,parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_PHYSICAL_DISABILITY_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }
}
