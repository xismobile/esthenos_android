package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;


public class FamilyExpenditureFragment extends PageFragment<FamilyExpenditurePage> {

    public static final String TABLE_FAMILY_EXPENDITURE = "FamilyExpenditure";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    String getshared;
    long i;

    Autosave_Data autosave_data =new Autosave_Data();
    EditText mExpendFood, mExpendFestival, mExpendEntertainment, mExpendTravel, mExpendMedical, mExpendEducation, mExpendOther;

    public static FamilyExpenditureFragment create(String key) {
        FamilyExpenditureFragment fragment = new FamilyExpenditureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }
    //...................................................
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (FamilyExpenditurePage) mCallbacks.onGetPage(mKey);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family_expenditure, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());
//..................................................
        mydb = new MySQLiteDB(getContext());
        String s_FamilyExpenditureFragExpendFood=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_FOOD_KEY);
        String s_FamilyExpenditureFragExpendTravel=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_TRAVEL_KEY);
        String s_FamilyExpenditureFragExpendEntertainment=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_ENTERTAINMENT_KEY);
        String s_FamilyExpenditureFragExpentFastival=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_FESTIVAL_KEY);
        String s_FamilyExpenditureFragExpendMedical=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_MEDICAL_KEY);
        String s_FamilyExpenditureFragExpendEducation=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_EDUCATION_KEY);
        String s_FamilyExpenditureFragExpendOther=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_OTHER_KEY);

//...........................................
        mExpendFood = (EditText) rootView.findViewById(R.id.family_expend_food);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_FOOD_KEY, mExpendFood.getText().toString());
        if (!s_FamilyExpenditureFragExpendFood.equals("nulldata")){
            mExpendFood.setText(s_FamilyExpenditureFragExpendFood);
        }
//        mExpendFood.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.FirstAutoSaveData(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_FOOD_KEY, mExpendFood.getText().toString());
//                    i = mydb.getLastInsertedId(TABLE_FAMILY_EXPENDITURE);
//                    Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
//                    autosave.Sharedsetvalue(getContext(),"FamilyExpenditurename", "Expenditurekey", String.valueOf(i));
//                }
//            }
//        });

        mExpendTravel = (EditText) rootView.findViewById(R.id.family_expend_travel);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_TRAVEL_KEY, mExpendTravel.getText().toString());
        if (!s_FamilyExpenditureFragExpendTravel.equals("nulldata")){
            mExpendTravel.setText(s_FamilyExpenditureFragExpendTravel);
        }
//        mExpendTravel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_TRAVEL_KEY, i, mExpendTravel.getText().toString());
//                }
//            }
//        });

        mExpendEntertainment = (EditText) rootView.findViewById(R.id.family_expend_entertainment);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_ENTERTAINMENT_KEY, mExpendEntertainment.getText().toString());
        if (!s_FamilyExpenditureFragExpendEntertainment.equals("nulldata")){
            mExpendEntertainment.setText(s_FamilyExpenditureFragExpendEntertainment);
        }
//        mExpendEntertainment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_ENTERTAINMENT_KEY, i, mExpendEntertainment.getText().toString());
//                }
//            }
//        });

        mExpendFestival = (EditText) rootView.findViewById(R.id.family_expend_festival);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_FESTIVAL_KEY, mExpendFestival.getText().toString());
        if (!s_FamilyExpenditureFragExpentFastival.equals("nulldata")){
            mExpendFestival.setText(s_FamilyExpenditureFragExpentFastival);
        }
//        mExpendFestival.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_FESTIVAL_KEY, i, mExpendFestival.getText().toString());
//                }
//            }
//        });

        mExpendMedical = (EditText) rootView.findViewById(R.id.family_expend_medical);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_MEDICAL_KEY, mExpendMedical.getText().toString());
        if (!s_FamilyExpenditureFragExpendMedical.equals("nulldata")){
            mExpendMedical.setText(s_FamilyExpenditureFragExpendMedical);
        }
//        mExpendMedical.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_MEDICAL_KEY, i, mExpendMedical.getText().toString());
//                }
//            }
//        });

        mExpendEducation = (EditText) rootView.findViewById(R.id.family_expend_education);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_EDUCATION_KEY, mExpendEducation.getText().toString());
        if (!s_FamilyExpenditureFragExpendEducation.equals("nulldata")){
            mExpendEducation.setText(s_FamilyExpenditureFragExpendEducation);
        }
//        mExpendEducation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_EDUCATION_KEY, i, mExpendEducation.getText().toString());
//                }
//            }
//        });

        mExpendOther = (EditText) rootView.findViewById(R.id.family_expend_other);
        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_OTHER_KEY, mExpendOther.getText().toString());
        if (!s_FamilyExpenditureFragExpendOther.equals("nulldata")){
            mExpendOther.setText(s_FamilyExpenditureFragExpendOther);
        }
//        mExpendOther.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    mydb.Autosavebyid(TABLE_FAMILY_EXPENDITURE, MySQLiteDB.EXPENDITURE_OTHER_KEY, i, mExpendOther.getText().toString());
//                }
//            }
//        });


        mExpendFood.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendFood));
        mExpendTravel.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendTravel));
        mExpendEntertainment.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendEntertainment));
        mExpendFestival.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendFestival));
        mExpendMedical.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendMedical));
        mExpendEducation.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendEducation));
        mExpendOther.addTextChangedListener(new GenericTextFamilyEpenditureFrag(mExpendOther));


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        mExpendFood.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_FOOD_KEY));
//        mExpendTravel.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_TRAVEL_KEY));
//        mExpendEntertainment.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_ENTERTAINMENT_KEY));
//        mExpendFestival.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_FESTIVAL_KEY));
//        mExpendMedical.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_MEDICAL_KEY));
//        mExpendEducation.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_EDUCATION_KEY));
//        mExpendOther.addTextChangedListener(getTextWatcher(FamilyExpenditurePage.EXPENDITURE_OTHER_KEY));
    }
    class GenericTextFamilyEpenditureFrag implements TextWatcher {

        private View view;

        private GenericTextFamilyEpenditureFrag(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.family_expend_food) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_FOOD_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_FOOD_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_travel) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_TRAVEL_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_TRAVEL_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_education) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_EDUCATION_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_EDUCATION_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_entertainment) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_ENTERTAINMENT_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_ENTERTAINMENT_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_festival) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_FESTIVAL_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_FESTIVAL_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_other) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_OTHER_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_OTHER_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.family_expend_medical) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyExpenditurePage.EXPENDITURE_MEDICAL_KEY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyExpenditurePage.EXPENDITURE_MEDICAL_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
    }

}
