package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class AdditionalFamilyDetailFirstPage extends Page {

    public static final String KEY_OCCUPATION_DETAILS = "Occupations Details";
    public static final String KEY_INVOLVEMENY = "Years of Involvement";
    public static final String KEY_ANNUAL_INCOME = "Monthly Income";
    public static final String KEY_RELATION = "Relationship";
    public static final String KEY_EDUCATION = "Education";
    public static final String KEY_NAME = "Name";
    public static final String KEY_AGE = "Age";
    public static final String KEY_SEX = "Sex";
    public static final String KEY_TABLE_FAMILY_1TO6 = "Detailoffamilymember";

    public AdditionalFamilyDetailFirstPage(ModelCallbacks callbacks, PageConfig pageConfig) {
        super(callbacks, pageConfig);
    }

    @Override
    public Fragment createFragment() {
        return AdditionalFamilyDetailFirstFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_OCCUPATION_DETAILS, mData.getString(KEY_OCCUPATION_DETAILS), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ANNUAL_INCOME, mData.getString(KEY_ANNUAL_INCOME), getPageId(), -1));
        dest.add(new ReviewItem(KEY_EDUCATION, mData.getString(KEY_EDUCATION), getPageId(), -1));
        dest.add(new ReviewItem(KEY_RELATION, mData.getString(KEY_RELATION), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NAME, mData.getString(KEY_NAME), getPageId(), -1));
        dest.add(new ReviewItem(KEY_AGE, mData.getString(KEY_AGE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_SEX, mData.getString(KEY_SEX), getPageId(), -1));
        dest.add(new ReviewItem(KEY_INVOLVEMENY, mData.getString(KEY_INVOLVEMENY), getPageId(), -1));
    }

//    public boolean isCompleted() {
//        return !TextUtils.isEmpty(mData.getString(KEY_OCCUPATION_DETAILS)) && !TextUtils.isEmpty(mData.getString(KEY_ANNUAL_INCOME)); //&&
//                !TextUtils.isEmpty(mData.getString(KEY_EDUCATION)) && !TextUtils.isEmpty(mData.getString(KEY_RELATION)) &&
//                !TextUtils.isEmpty(mData.getString(KEY_NAME)) && !TextUtils.isEmpty(mData.getString(KEY_AGE)) &&
//                !TextUtils.isEmpty(mData.getString(KEY_SEX)) && !TextUtils.isEmpty(mData.getString(KEY_INVOLVEMENY));
//    }
}

