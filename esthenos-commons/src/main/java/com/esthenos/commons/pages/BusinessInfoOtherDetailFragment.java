package com.esthenos.commons.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;

public class BusinessInfoOtherDetailFragment extends PageFragment<BusinessInfoOtherDetailPage> {

    String[] wagePay = {"","Daily","Weekly","Monthly"};

    EditText mTypeEquip,mDetail,mAge,mEstimate,mCollateral,mNumberOfEmp,mNumOfRelative,mPermanent,
                mContract,mAverage,mMethod;
    Spinner sWage;

    public static BusinessInfoOtherDetailFragment create(String key) {
        BusinessInfoOtherDetailFragment fragment = new BusinessInfoOtherDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY,key);
        fragment.setArguments(args);
        return fragment;
    }

    public BusinessInfoOtherDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_business_info_other_detail, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mTypeEquip=(EditText)rootView.findViewById(R.id.buss_info_type_equipment);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_TYPE_EQUIPMENT,mTypeEquip.getText().toString());

        mDetail=(EditText)rootView.findViewById(R.id.buss_info_detail);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_DETAIL,mDetail.getText().toString());

        mAge=(EditText)rootView.findViewById(R.id.buss_info_age);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_AGE,mAge.getText().toString());

        mEstimate=(EditText)rootView.findViewById(R.id.buss_info_estimate);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_ESTIMATE,mEstimate.getText().toString());

        mCollateral=(EditText)rootView.findViewById(R.id.buss_info_collateral);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_COLLATERAL,mCollateral.getText().toString());

        mNumberOfEmp=(EditText)rootView.findViewById(R.id.buss_info_employee);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_NUM_EMP,mNumberOfEmp.getText().toString());

        mNumOfRelative=(EditText)rootView.findViewById(R.id.buss_info_relative);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_NUM_RELATIVE,mNumOfRelative.getText().toString());

        mPermanent=(EditText)rootView.findViewById(R.id.buss_info_permanent);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_PERMANENT,mPermanent.getText().toString());

        mContract=(EditText)rootView.findViewById(R.id.buss_info_contract);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_CONTRACT,mContract.getText().toString());

        sWage=(Spinner)rootView.findViewById(R.id.buss_info_wage);
        ArrayAdapter wageAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,wagePay);
        wageAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sWage.setAdapter(wageAdapter);

        mAverage=(EditText)rootView.findViewById(R.id.buss_info_average);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_AVERAGE,mAverage.getText().toString());

        mMethod=(EditText)rootView.findViewById(R.id.buss_info_method);
        mPage.getData().putString(BusinessInfoOtherDetailPage.BUSINESS_INFO_METHOD,mMethod.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTypeEquip.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_TYPE_EQUIPMENT));
        mDetail.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_DETAIL));
        mAge.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_AGE));
        mEstimate.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_ESTIMATE));
        mCollateral.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_COLLATERAL));
        mNumberOfEmp.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_NUM_EMP));
        mNumOfRelative.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_NUM_RELATIVE));
        mPermanent.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_PERMANENT));
        mContract.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_CONTRACT));
        sWage.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoOtherDetailPage.BUSINESS_INFO_WAGE));
        mAverage.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_AVERAGE));
        mMethod.addTextChangedListener(getTextWatcher(BusinessInfoOtherDetailPage.BUSINESS_INFO_METHOD));
    }
    }
