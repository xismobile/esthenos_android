package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FinancialLiabilityPage extends Page {

    public static final String LIABILITIES_CHIT_KEY = "Liabilities-Chits";
    public static final String LIABILITIES_BANK_KEY = "Liabilities-Bank";
    public static final String LIABILITIES_INSURANCE_KEY = "Liabilities-Insurance";
    public static final String LIABILITIES_FRIENDS_KEY = "Liabilities-Friends & Family(Hand loans)";

    public FinancialLiabilityPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FinancialLiabilityFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(LIABILITIES_CHIT_KEY,mData.getString(LIABILITIES_CHIT_KEY),getPageId(), -1));
        dest.add(new ReviewItem(LIABILITIES_BANK_KEY, mData.getString(LIABILITIES_BANK_KEY), getPageId(), -1));
        dest.add(new ReviewItem(LIABILITIES_INSURANCE_KEY, mData.getString(LIABILITIES_INSURANCE_KEY), getPageId(), -1));
        dest.add(new ReviewItem(LIABILITIES_FRIENDS_KEY, mData.getString(LIABILITIES_FRIENDS_KEY), getPageId(), -1));
    }

}
