package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class BusinessInfoPage extends Page {

    public static final String BUSINESS_NAME = "Business Name";
    public static final String BUSINESS_DESC = "Description Business";
    public static final String BUSINESS_GOODS_DETAIL = "Details of Finished Goods";
    public static final String BUSINESS_RAW_DETAIL = "Details of Principal Raw Materials";
    public static final String BUSINESS_PANCARD = "Pancard No.";
    public static final String BUSINESS_TAX = "VAT/Service Tax Regn No";
    public static final String BUSINESS_SHOP_EST = "Shops & Establishment No";
    public static final String BUSINESS_ADDRESS = "Address of Place of Business";
    public static final String BUSINESS_AREA = "Area occupied of Business(sq. ft)";
    public static final String BUSINESS_AREA_VALUE = "Area Market value";

    public BusinessInfoPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return BusinessInfoFragment.create(getPageId()) ;
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(BUSINESS_NAME, mData.getString(BUSINESS_NAME), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_DESC, mData.getString(BUSINESS_DESC), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_GOODS_DETAIL, mData.getString(BUSINESS_GOODS_DETAIL), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_RAW_DETAIL, mData.getString(BUSINESS_RAW_DETAIL), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_PANCARD, mData.getString(BUSINESS_PANCARD), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_TAX, mData.getString(BUSINESS_TAX), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_SHOP_EST, mData.getString(BUSINESS_SHOP_EST), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_ADDRESS, mData.getString(BUSINESS_ADDRESS), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_AREA, mData.getString(BUSINESS_AREA), getPageId(), -1));
        dest.add(new ReviewItem(BUSINESS_AREA_VALUE, mData.getString(BUSINESS_AREA_VALUE), getPageId(), -1));
    }
}
