package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class BusinessInfoBillingCyclePage extends Page {

    public static final String BUSINESS_BILLING_INSTITUTION = "Credit period in days to Institutions";
    public static final String BUSINESS_BILLING_INDIVIDUAL = "Credit period in days to Individuals";
    public static final String BUSINESS_BILLING_RAW_PURCHASE = "Number of days credit for raw material purchase";
    public static final String BUSINESS_BILLING_STORAGE = "Place of storage for production of Raw Material";
    public static final String BUSINESS_BILLING_SOURCE = "Source of FUNDING for purchase of Material";
    public static final String BUSINESS_BILLING_SALE = "Sale and Purchase Book";
    public static final String BUSINESS_BILLING_FORMAL = "Formal Books of Account";
    public static final String BUSINESS_BILLING_CHARTER = "Chartered Accountant";
    public static final String BUSINESS_BILLING_BANK = "Bank Account";

    public BusinessInfoBillingCyclePage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return BusinessInfoBillingCycleFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(BUSINESS_BILLING_INSTITUTION,mData.getString(BUSINESS_BILLING_INSTITUTION),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_INDIVIDUAL,mData.getString(BUSINESS_BILLING_INDIVIDUAL),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_RAW_PURCHASE,mData.getString(BUSINESS_BILLING_RAW_PURCHASE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_STORAGE,mData.getString(BUSINESS_BILLING_STORAGE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_SOURCE,mData.getString(BUSINESS_BILLING_SOURCE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_SALE,mData.getString(BUSINESS_BILLING_SALE),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_FORMAL,mData.getString(BUSINESS_BILLING_FORMAL),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_CHARTER,mData.getString(BUSINESS_BILLING_CHARTER),getPageId(),-1));
        dest.add(new ReviewItem(BUSINESS_BILLING_BANK,mData.getString(BUSINESS_BILLING_BANK),getPageId(),-1));

    }
}
