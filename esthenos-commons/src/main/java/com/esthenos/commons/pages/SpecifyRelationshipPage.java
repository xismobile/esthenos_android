package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class SpecifyRelationshipPage extends Page {

    public static final String SPECIFY_RELATION= "Specify Relationship";
    public SpecifyRelationshipPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return SpecifyRelationshipFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(SPECIFY_RELATION,mData.getString(SPECIFY_RELATION),getPageId(),-1));
    }
}
