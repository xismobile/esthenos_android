package com.esthenos.commons.pages;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;

public class BusinessInfoBillingCycleFragment extends PageFragment<BusinessInfoBillingCyclePage> {

    Spinner sInstitution,sIndividual,sPurchase,sStorage,sSource,sSale,sFormal,sCharter,sBank;

    final String[] period = {"","7","15","30"};
    final String[] storage = {"","Place of Business","Godown","Shop or Retail Outlet"};
    final String[] source = {"","Own Funds","Loan Funds"};
    final String[] option = {"","Yes","No"};

    public static BusinessInfoBillingCycleFragment create(String key) {
        BusinessInfoBillingCycleFragment fragment = new BusinessInfoBillingCycleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    public BusinessInfoBillingCycleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {// Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_business_info_billing_cycle, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        sInstitution=(Spinner)rootView.findViewById(R.id.buss_billing_institution);
        ArrayAdapter institutionAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,period);
        institutionAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sInstitution.setAdapter(institutionAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_INSTITUTION, sInstitution.getSelectedItem().toString());

        sIndividual=(Spinner)rootView.findViewById(R.id.buss_billing_individual);
        ArrayAdapter individualAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,period);
        individualAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sIndividual.setAdapter(individualAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_INDIVIDUAL, sIndividual.getSelectedItem().toString());

        sPurchase=(Spinner)rootView.findViewById(R.id.buss_billing_raw_purchase);
        ArrayAdapter purchaseAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,period);
        purchaseAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sPurchase.setAdapter(purchaseAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_RAW_PURCHASE, sPurchase.getSelectedItem().toString());

        sStorage=(Spinner)rootView.findViewById(R.id.buss_billing_storage);
        ArrayAdapter storageAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,storage);
        storageAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sStorage.setAdapter(storageAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_STORAGE, sStorage.getSelectedItem().toString());

        sSource=(Spinner)rootView.findViewById(R.id.buss_billing_source);
        ArrayAdapter sourceAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,source);
        sourceAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sSource.setAdapter(sourceAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_SOURCE, sSource.getSelectedItem().toString());

        sSale=(Spinner)rootView.findViewById(R.id.buss_billing_sale);
        ArrayAdapter saleAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,option);
        saleAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sSale.setAdapter(saleAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_SALE, sSale.getSelectedItem().toString());

        sFormal=(Spinner)rootView.findViewById(R.id.buss_billing_formal);
        ArrayAdapter formalAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,option);
        formalAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sFormal.setAdapter(formalAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_FORMAL, sFormal.getSelectedItem().toString());

        sCharter=(Spinner)rootView.findViewById(R.id.buss_billing_charter);
        ArrayAdapter charterAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,option);
        charterAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sCharter.setAdapter(charterAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_CHARTER, sCharter.getSelectedItem().toString());

        sBank=(Spinner)rootView.findViewById(R.id.buss_billing_bank);
        ArrayAdapter bankAdapter=new ArrayAdapter(getActivity(),R.layout.layout_spinner_item,option);
        bankAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sBank.setAdapter(bankAdapter);
        mPage.getData().putString(BusinessInfoBillingCyclePage.BUSINESS_BILLING_BANK, sBank.getSelectedItem().toString());

        return rootView;
    }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

            sInstitution.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_INSTITUTION));
            sIndividual.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_INDIVIDUAL));
            sPurchase.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_RAW_PURCHASE));
            sStorage.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_STORAGE));
            sSource.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_SOURCE));
            sSale.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_SALE));
            sFormal.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_FORMAL));
            sCharter.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_CHARTER));
            sBank.setOnItemSelectedListener(getOnItemSelectedListener(BusinessInfoBillingCyclePage.BUSINESS_BILLING_BANK));

    }

}
