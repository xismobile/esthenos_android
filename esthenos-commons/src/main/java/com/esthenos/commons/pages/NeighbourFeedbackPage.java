package com.esthenos.commons.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;


public class NeighbourFeedbackPage extends Page {

    public static final String NEIGHBOUR_OCUPATION="Occupation";
    public static final String NEIGHBOUR_LOCATION = "Location";
    public static final String NEIGHBOUR_NAME="Neighbour Name";
    public static final String NEIGHBOUR_REMARKS="Remarks";
    public static final String NEIGHBOUR_PHONE="Phone No";

    public static final String NEIGHBOUR_OCUPATION_SECOND="Occupation 2nd";
    public static final String NEIGHBOUR_LOCATION_SECOND = "Location 2nd";
    public static final String NEIGHBOUR_NAME_SECOND="Neighbour Name 2nd";
    public static final String NEIGHBOUR_REMARKS_SECOND="Remarks 2nd";
    public static final String NEIGHBOUR_PHONE_SECOND="Phone No 2nd";
    public NeighbourFeedbackPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return NeighbourFeedbackFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(NEIGHBOUR_OCUPATION,mData.getString(NEIGHBOUR_OCUPATION),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_LOCATION,mData.getString(NEIGHBOUR_LOCATION),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_NAME,mData.getString(NEIGHBOUR_NAME),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_REMARKS,mData.getString(NEIGHBOUR_REMARKS),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_PHONE,mData.getString(NEIGHBOUR_PHONE),getPageId(),-1));

        dest.add(new ReviewItem(NEIGHBOUR_OCUPATION_SECOND,mData.getString(NEIGHBOUR_OCUPATION_SECOND),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_LOCATION_SECOND,mData.getString(NEIGHBOUR_LOCATION_SECOND),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_NAME_SECOND,mData.getString(NEIGHBOUR_NAME_SECOND),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_REMARKS_SECOND,mData.getString(NEIGHBOUR_REMARKS_SECOND),getPageId(),-1));
        dest.add(new ReviewItem(NEIGHBOUR_PHONE_SECOND,mData.getString(NEIGHBOUR_PHONE_SECOND),getPageId(),-1));

    }
//    @Override
//    public boolean isCompleted(){
//        return  !TextUtils.isEmpty(mData.getString(NEIGHBOUR_NAME))&&!TextUtils.isEmpty(mData.getString(NEIGHBOUR_OCUPATION))&&
//                !TextUtils.isEmpty(mData.getString(NEIGHBOUR_REMARKS))&&!TextUtils.isEmpty(mData.getString(NEIGHBOUR_NAME_SECOND))&&
//                !TextUtils.isEmpty(mData.getString(NEIGHBOUR_OCUPATION_SECOND))&&!TextUtils.isEmpty(mData.getString(NEIGHBOUR_REMARKS_SECOND));
//    }
}
