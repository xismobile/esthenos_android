package com.esthenos.commons.pages;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.cards.AadhaarCard;
import com.esthenos.commons.cards.KYCCards;
import com.esthenos.commons.cards.PanCard;
import com.esthenos.commons.cards.VoterIdCard;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.esthenos.commons.utils.SingletonConfig;
import com.tech.freak.wizardpager.ui.PageFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class GuarantorsInformationFragment extends PageFragment<GuarantorsInformationPage> {


    Autosave_Data autosave_data = new Autosave_Data();
    String S_uidViewCustomerKYC,S_voterIdCustomerKYC,S_panCardIdCustomerKYC,S_mRtionCardCustomerKYC;
    public static final String TABLE_GUARANTERINFO = "GuaranterInfo";
    private static final String ARG_KEY = "key";
    private static final String[] occupations = {" ", "Retailing", "Manufacturing", "Services", "Housewife", "Student"};
    private static final String[] gender = {" ", "Male", "Female"};
    private static final String[] subTitle = {" ", "Mr.", "Mrs.", "Miss", "Late"};
    private static final String[] marital_status = {"", "Married", "Unmarried", "Divorced", "Widow", "Widower", "Separated"};
    Calendar myCalendar = Calendar.getInstance();
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;
    EditText nameView, uidView, f_or_h_name, mPersonalEmail, mPermanentAddress,
            yobView, talukView, districtView, addressView, mobileNumberView,
            countryView, pincodeView, stateView, mMotherName, mSpouse, mLandmarks;
    EditText telephoneNumberView, ageView, mNickName, mPoliceStaion,
            mPostOffice, voterId, voterIdName, voterId_f_or_h_name,
            panCardId, panCardName, panCard_f_or_h_name, mRtionCard, mCenterMeetiong,
            mSpouseAge, mSpouseDob;
    Spinner mGender, mSubTitle, mMotherSubtitle, mFatherSubTitle, mSpouseSubtitle, mOccupation, mMarital;
    TextView mSpouseNameText, mSpouseAgeText, mSpouseDobText;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel(year);
        }

    };
    DatePickerDialog.OnDateSetListener spousedate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateSpouseAge(year);
        }

    };
    private String mKey;
    private SingletonConfig instance = SingletonConfig.getInstance();

    public static GuarantorsInformationFragment create(String key) {
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        GuarantorsInformationFragment fragment = new GuarantorsInformationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        S_uidViewCustomerKYC = autosave_data.getdata(getContext(), "applicant_kyc_details",CustomerKYCDetailsPage.UID_KEY );
        S_voterIdCustomerKYC = autosave_data.getdata(getContext(), "applicant_kyc_details", CustomerKYCDetailsPage.VOTER_ID_KEY);
        S_panCardIdCustomerKYC = autosave_data.getdata(getContext(), "applicant_kyc_details", CustomerKYCDetailsPage.PANCARD_KEY );
        S_mRtionCardCustomerKYC = autosave_data.getdata(getContext(), "applicant_kyc_details",CustomerKYCDetailsPage.RATIONCARD_KEY );
        Bundle args = getArguments();
        mKey = args.getString(ARG_KEY);
        mPage = (GuarantorsInformationPage) mCallbacks.onGetPage(mKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_grantor_information, null, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());


        mydb = new MySQLiteDB(getContext());
        KYCCards kycCards = instance.getKYCCards(mPage.getPageOwner());
        final PanCard panCard = kycCards.panCard;
        AadhaarCard aadhaarCard = kycCards.aadhaarCard;
        VoterIdCard voterIdCard = kycCards.voterIdCard;

        mSpouseNameText = (TextView) rootView.findViewById(R.id.spouse_name_text);
        mSpouseDobText = (TextView) rootView.findViewById(R.id.spouse_dob_text);
        mSpouseAgeText = (TextView) rootView.findViewById(R.id.spouse_age_text);

        mPostOffice = (EditText) rootView.findViewById(R.id.post_office);
        mPage.getData().putString(GuarantorsInformationPage.POST_OFFICE_KEY, mPostOffice.getText().toString());
        mPostOffice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mPostOffice.getText().toString().trim())) {
                        // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPOST_OFFICE_KEY, i, mPostOffice.getText().toString());

                    } else {

                        mPostOffice.setError("Please enter Post Office");
                    }


                }
            }
        });

        mPoliceStaion = (EditText) rootView.findViewById(R.id.police_station);
        mPage.getData().putString(GuarantorsInformationPage.POLICE_KEY, mPoliceStaion.getText().toString());
        mPoliceStaion.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mPoliceStaion.getText().toString().trim())) {


                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPOLICE_KEY, i, mPoliceStaion.getText().toString());

                    } else {

                        mPoliceStaion.setError("Please enter police station");
                    }


                }
            }
        });

        mMarital = (Spinner) rootView.findViewById(R.id.custeomer_page_marital_status);
        ArrayAdapter maritalAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, marital_status);
        maritalAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mMarital.setAdapter(maritalAdapter);
        mPage.getData().putString(GuarantorsInformationPage.SINGLE_MARITAL_STATUS, mMarital.getSelectedItem().toString());


        mSubTitle = (Spinner) rootView.findViewById(R.id.kycsubtitle);
        ArrayAdapter adapterName = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, subTitle);
        adapterName.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mSubTitle.setAdapter(adapterName);
        mPage.getData().putString(GuarantorsInformationPage.NAME_SUBTITLE_KEY, mSubTitle.getSelectedItem().toString());

        mMotherSubtitle = (Spinner) rootView.findViewById(R.id.mothersubtitle);
        mMotherSubtitle.setAdapter(adapterName);
        mPage.getData().putString(GuarantorsInformationPage.MOTHER_NAME_SUBTITLE_KEY, mMotherSubtitle.getSelectedItem().toString());

        mFatherSubTitle = (Spinner) rootView.findViewById(R.id.fatherubtitle);
        mFatherSubTitle.setAdapter(adapterName);
        mPage.getData().putString(GuarantorsInformationPage.FATHER_HUSBAND_SUBTITLE_KEY, mFatherSubTitle.getSelectedItem().toString());

        mSpouseSubtitle = (Spinner) rootView.findViewById(R.id.spousesubtitle);
        mSpouseSubtitle.setAdapter(adapterName);
        mPage.getData().putString(GuarantorsInformationPage.SPOUSE_NAME_SUBTITLE_KEY, mSpouseSubtitle.getSelectedItem().toString());

        mNickName = (EditText) rootView.findViewById(R.id.nick_name);
        mPage.getData().putString(GuarantorsInformationPage.NICK_NAME_KEY, mNickName.getText().toString());
        mNickName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mNickName.getText().toString().trim())) {


                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERNICK_NAME_KEY, i, mNickName.getText().toString());


                    } else {

                        mNickName.setHintTextColor(Color.GREEN);
                        mNickName.setHint("Nick name");
                    }


                }
            }
        });

        mPersonalEmail = (EditText) rootView.findViewById(R.id.personal_email_id);
        mPage.getData().putString(GuarantorsInformationPage.EMAIL_ID_KEY, mPersonalEmail.getText().toString());
        mPersonalEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mPersonalEmail.getText().toString().trim())) {


                     //   mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTEREMAIL_ID_KEY, i, mPersonalEmail.getText().toString());


                    } else {

                        mPersonalEmail.setHintTextColor(Color.GREEN);
                        mPersonalEmail.setHint("enter E-mail if any (Non-Mandatory");
                    }


                }
            }
        });

        mMotherName = (EditText) rootView.findViewById(R.id.mother);
        mPage.getData().putString(GuarantorsInformationPage.MOTHER_KEY, mMotherName.getText().toString());
        mMotherName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mMotherName.getText().toString().trim())) {

                     //   mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERMOTHER_KEY, i, mMotherName.getText().toString());

                    } else {

                        mMotherName.setError("* Please enter Mother Name (Mandatory)");
                    }


                }
            }
        });

        mSpouse = (EditText) rootView.findViewById(R.id.spouse);
        mPage.getData().putString(GuarantorsInformationPage.SPOUSE_KEY, mSpouse.getText().toString());
        mSpouse.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mSpouse.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERSPOUSE_KEY, i, mSpouse.getText().toString());

                    } else {

                        mSpouse.setError("* Please enter Spouse Name (Mandatory)");
                    }


                }
            }
        });

        mLandmarks = (EditText) rootView.findViewById(R.id.landmark);
        mPage.getData().putString(GuarantorsInformationPage.LANDMARKS_KEY, mLandmarks.getText().toString());
        mLandmarks.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mLandmarks.getText().toString().trim())) {


                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERLANDMARKS_KEY, i, mLandmarks.getText().toString());


                    } else {

                        mLandmarks.setHintTextColor(Color.GREEN);
                        mLandmarks.setHint(" Enter Land mark if any (Non-Mandatory");
                    }


                }
            }
        });

        mOccupation = (Spinner) rootView.findViewById(R.id.occupation);
        ArrayAdapter<String> occupationAdapter = new ArrayAdapter<String>(getActivity(), R.layout.layout_spinner_item, occupations);
        occupationAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mOccupation.setAdapter(occupationAdapter);
        mPage.getData().putString(GuarantorsInformationPage.OCCUPATION_KEY, mOccupation.getSelectedItem().toString());

        mPermanentAddress = (EditText) rootView.findViewById(R.id.permanent_address);
        mPage.getData().putString(GuarantorsInformationPage.PERMANENT_ADDRESS_KEY, mPermanentAddress.getText().toString());
        mPermanentAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mPermanentAddress.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPERMANENT_ADDRESS_KEY, i, mPermanentAddress.getText().toString());

                    } else {

                        mPermanentAddress.setError("* Please enter Permanent Address (Mandatory)");
                    }


                }
            }
        });

        nameView = (EditText) rootView.findViewById(R.id.name);
        nameView.setText(aadhaarCard.getName());
        mPage.getData().putString(GuarantorsInformationPage.NAME_KEY, nameView.getText().toString());
        nameView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(nameView.getText().toString().trim())) {

                     //   mydb.FirstAutoSaveData(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERNAME_KEY, nameView.getText().toString());
                    //    i = mydb.getLastInsertedId(TABLE_GUARANTERINFO);
                        // Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
                    } else {

                        nameView.setError("* Please enter Guarantor Name (Mandatory)");
                    }


                }
            }
        });

        uidView = (EditText) rootView.findViewById(R.id.uid);
        uidView.setText(aadhaarCard.getUid());
        mPage.getData().putString(GuarantorsInformationPage.UID_KEY, uidView.getText().toString());
        uidView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(uidView.getText().toString().trim())) {

                   //     mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERUID_KEY, i, uidView.getText().toString());

                    } else {

                        uidView.setHintTextColor(Color.GREEN);
                        uidView.setHint(" Enter Adhar UID if any (Non-Mandatory");
                    }

                }
            }
        });
        f_or_h_name = (EditText) rootView.findViewById(R.id.f_or_h_name);
        f_or_h_name.setText(aadhaarCard.getCo());
        mPage.getData().putString(GuarantorsInformationPage.FATHER_OR_HUSBAND_NAME, f_or_h_name.getText().toString());
        f_or_h_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(f_or_h_name.getText().toString().trim())) {

                     //   mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERFATHER_OR_HUSBAND_NAME, i, f_or_h_name.getText().toString());
                    } else {

                        f_or_h_name.setError("* Please enter Father's/Husband Name (Mandatory)");
                    }


                }
            }
        });

        ageView = (EditText) rootView.findViewById(R.id.age);
        ageView.setText(Integer.toString(aadhaarCard.getAge()));
        mPage.getData().putString(GuarantorsInformationPage.AGE_KEY, ageView.getText().toString());
        ageView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(ageView.getText().toString().trim())) {

//                    //    mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERFATHER_OR_HUSBAND_NAME, i, f_or_h_name.getText().toString());
                    } else {

                        ageView.setError("* Please enter age (Mandatory)");
                    }


                  //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERAGE_KEY, i, ageView.getText().toString());
                }
            }
        });

        yobView = (EditText) rootView.findViewById(R.id.dob);
        yobView.setText(Integer.toString(aadhaarCard.getYob()));
        yobView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        mPage.getData().putString(GuarantorsInformationPage.YOB_KEY, yobView.getText().toString());
        yobView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                   // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERYOB_KEY, i, yobView.getText().toString());
                }
            }
        });

        talukView = (EditText) rootView.findViewById(R.id.taluk);
        talukView.setText(aadhaarCard.getVtc());
        mPage.getData().putString(GuarantorsInformationPage.TALUK_KEY, talukView.getText().toString());
        talukView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(talukView.getText().toString().trim())) {

                        //mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERTALUK_KEY, i, talukView.getText().toString());
                    } else {

                        talukView.setError("* Please enter Taluka (Mandatory)");
                    }


                }
            }
        });

        districtView = (EditText) rootView.findViewById(R.id.district);
        districtView.setText(aadhaarCard.getDist());
        mPage.getData().putString(GuarantorsInformationPage.DISTRICT_KEY, districtView.getText().toString());
        districtView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(districtView.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERDISTRICT_KEY, i, districtView.getText().toString());
                    } else {

                        districtView.setError("* Please enter district  (Mandatory)");
                    }

                }
            }
        });

        stateView = (EditText) rootView.findViewById(R.id.state);
        stateView.setText(aadhaarCard.getState());
        mPage.getData().putString(GuarantorsInformationPage.STATE_KEY, stateView.getText().toString());
        stateView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(stateView.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERSTATE_KEY, i, stateView.getText().toString());
                    } else {

                        stateView.setError("* Please enter State (Mandatory)");
                    }


                }
            }
        });

        addressView = (EditText) rootView.findViewById(R.id.address);
        addressView.setText(aadhaarCard.getAddress());
        mPage.getData().putString(GuarantorsInformationPage.ADDRESS_KEY, addressView.getText().toString());
        addressView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(addressView.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERADDRESS_KEY, i, addressView.getText().toString());
                    } else {

                        addressView.setError("* Please enter Address (Mandatory)");
                    }


                }
            }
        });

        pincodeView = (EditText) rootView.findViewById(R.id.pincode);
        pincodeView.setText(aadhaarCard.getPc());
        mPage.getData().putString(GuarantorsInformationPage.PINCODE_KEY, pincodeView.getText().toString());
        pincodeView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(pincodeView.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPINCODE_KEY, i, pincodeView.getText().toString());
                    } else {

                        pincodeView.setError("* Please enter Pin code (Mandatory)");
                    }

                }
            }
        });

        countryView = (EditText) rootView.findViewById(R.id.country);
        mPage.getData().putString(GuarantorsInformationPage.COUNTRY_KEY, countryView.getText().toString());
        countryView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(countryView.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERCOUNTRY_KEY, i, countryView.getText().toString());
                    } else {

                        countryView.setError("* Please enter Country (Mandatory)");
                    }


                }
            }
        });

        mobileNumberView = (EditText) rootView.findViewById(R.id.mobilePhoneNumber);
        mPage.getData().putString(GuarantorsInformationPage.MOBILE_PHONE_KEY, mobileNumberView.getText().toString());
        mobileNumberView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mobileNumberView.getText().toString().trim()) && mobileNumberView.getText().toString().trim().length() >=10 ) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERMOBILE_PHONE_KEY, i, mobileNumberView.getText().toString());
                    } else {


                        mobileNumberView.setError("* Enter 10 digit Mobile (Mandatory)");
                    }


                }
            }
        });

        telephoneNumberView = (EditText) rootView.findViewById(R.id.telephoneNumber);
        mPage.getData().putString(GuarantorsInformationPage.TELE_PHONE_KEY, telephoneNumberView.getText().toString());
        telephoneNumberView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(telephoneNumberView.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERTELE_PHONE_KEY, i, telephoneNumberView.getText().toString());
                    } else {

                        telephoneNumberView.setHintTextColor(Color.GREEN);
                        telephoneNumberView.setHint(" Enter Mobile if any (Non-Mandatory");
                    }


                }
            }
        });

        voterId = (EditText) rootView.findViewById(R.id.voter_id_number);
        voterId.setText(voterIdCard.id);
        mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_KEY, voterId.getText().toString());
        voterId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(voterId.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERVOTER_ID_KEY, i, voterId.getText().toString());
                    } else {

                        voterId.setError("* Please enter Voter ID (Mandatory)");
                    }


                }
            }
        });

        voterIdName = (EditText) rootView.findViewById(R.id.voter_id_name);
        voterIdName.setText(voterIdCard.name);
        mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_NAME_KEY, voterIdName.getText().toString());
        voterIdName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(voterIdName.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERVOTER_ID_NAME_KEY, i, voterIdName.getText().toString());
                    } else {

                        voterIdName.setError("* Please enter Voter ID Name (Mandatory)");
                    }


                }
            }
        });

        voterId_f_or_h_name = (EditText) rootView.findViewById(R.id.voter_id_f_or_h_name);
        voterId_f_or_h_name.setText(voterIdCard.fatherHusbandName);
        mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, voterId.getText().toString());
        voterId_f_or_h_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(voterId_f_or_h_name.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERVOTER_ID_FATHER_OR_HUSBAND_NAME, i, voterId_f_or_h_name.getText().toString());

                    } else {

                        voterId_f_or_h_name.setError("* Please enter Voter ID Father's/Husband Name (Mandatory)");
                    }


                }
            }
        });

        panCardId = (EditText) rootView.findViewById(R.id.pancard_number);
        panCardId.setText(panCard.id);
        mPage.getData().putString(GuarantorsInformationPage.PANCARD_KEY, panCardId.getText().toString());
        panCardId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(panCardId.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPANCARD_KEY, i, panCardId.getText().toString());

                    } else {

                        panCardId.setHintTextColor(Color.GREEN);
                        panCardId.setHint(" Enter PAN Card Number if any (Non-Mandatory");


                    }


                }
            }
        });

        panCardName = (EditText) rootView.findViewById(R.id.pancard_name);
        panCardName.setText(panCard.name);
        mPage.getData().putString(GuarantorsInformationPage.PANCARD_NAME_KEY, panCardName.getText().toString());
        panCardName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(panCardName.getText().toString().trim())) {

                     //   mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPANCARD_NAME_KEY, i, panCardName.getText().toString());

                    } else {

                        panCardName.setHintTextColor(Color.GREEN);
                        panCardName.setHint(" Enter PAN Card Name if any (Non-Mandatory");


                    }


                }
            }
        });

        panCard_f_or_h_name = (EditText) rootView.findViewById(R.id.pancard_f_or_h_name);
        panCard_f_or_h_name.setText(panCard.fatherHusbandName);
        mPage.getData().putString(GuarantorsInformationPage.PANCARD_FATHER_OR_HUSBAND_NAME, panCard_f_or_h_name.getText().toString());
        panCard_f_or_h_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {




                    if (!TextUtils.isEmpty(panCard_f_or_h_name.getText().toString().trim())) {

                       // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERPANCARD_FATHER_OR_HUSBAND_NAME, i, panCard_f_or_h_name.getText().toString());

                    } else {

                        panCard_f_or_h_name.setHintTextColor(Color.GREEN);
                        panCard_f_or_h_name.setHint(" Enter PAN Card Father's/Husband Name if any (Non-Mandatory");


                    }


                }
            }
        });

        mRtionCard = (EditText) rootView.findViewById(R.id.ration_card);
        mPage.getData().putString(GuarantorsInformationPage.RATIONCARD_KEY, mRtionCard.getText().toString());
        mRtionCard.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                    if (!TextUtils.isEmpty(mRtionCard.getText().toString().trim())) {

                      //  mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERRATIONCARD_KEY, i, mRtionCard.getText().toString());

                    } else {

                        mRtionCard.setHintTextColor(Color.GREEN);
                        mRtionCard.setHint(" Enter Ration Card No if any (Non-Mandatory");


                    }


                }
            }
        });

        mCenterMeetiong = (EditText) rootView.findViewById(R.id.center_meeting);
        mPage.getData().putString(GuarantorsInformationPage.CENTERMEETING_KEY, mCenterMeetiong.getText().toString());
        mCenterMeetiong.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mCenterMeetiong.getText().toString().trim())) {

                     //   mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERCENTERMEETING_KEY, i, mCenterMeetiong.getText().toString());

                    } else {

                        mCenterMeetiong.setHintTextColor(Color.GREEN);
                        mCenterMeetiong.setHint(" Enter Center Meeting place if any (Non-Mandatory");


                    }


                }
            }
        });

        mGender = (Spinner) rootView.findViewById(R.id.single_page_gender);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, gender);
        adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mGender.setAdapter(adapter);
        mPage.getData().putString(GuarantorsInformationPage.SINGLE_GENDER_KEY, mGender.getSelectedItem().toString());

        mSpouseAge = (EditText) rootView.findViewById(R.id.spouse_age);
        mPage.getData().putString(GuarantorsInformationPage.SPOUSE_AGE_KEY, mSpouseAge.getText().toString());
        mSpouseAge.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {


                    if (!TextUtils.isEmpty(mSpouseAge.getText().toString().trim())) {



                    } else {


                        mSpouseAge.setError("* Enter Spouse Age (Mandatory");


                    }


                }
            }
        });




        mSpouseDob = (EditText) rootView.findViewById(R.id.spouse_dob);
        mSpouseDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getActivity(), spousedate, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        mPage.getData().putString(GuarantorsInformationPage.SPOUSE_DOB_KEY, mSpouseDob.getText().toString());

        mSpouseAgeText.setVisibility(View.GONE);
        mSpouseNameText.setVisibility(View.GONE);
        mSpouseDobText.setVisibility(View.GONE);
        mSpouse.setVisibility(View.GONE);
        mSpouseAge.setVisibility(View.GONE);
        mSpouseDob.setVisibility(View.GONE);
        mSpouseSubtitle.setVisibility(View.GONE);









        //------ pref saving

        String S_panCard_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.PANCARD_FATHER_OR_HUSBAND_NAME);
        String S_voterId_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.VOTER_ID_FATHER_OR_HUSBAND_NAME );
        String S_mPermanentAddress = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PERMANENT_ADDRESS_KEY );
        String S_addressview = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.ADDRESS_KEY );
        String S_f_or_h_name = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.FATHER_OR_HUSBAND_NAME);
        String S_telephoneNumberView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.TELE_PHONE_KEY );
        String S_mobileNumberView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.MOBILE_PHONE_KEY );
        String S_voterIdName = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.VOTER_ID_NAME_KEY);
        String S_panCardName = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.PANCARD_NAME_KEY);
        String S_mPostOffice = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.POST_OFFICE_KEY );
        String S_mPersonalEmail = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.EMAIL_ID_KEY );
        String S_districtView = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.DISTRICT_KEY);
        String S_mLandmarks= autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.LANDMARKS_KEY );
        String S_mPoliceStaion = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.POLICE_KEY );
        String S_mNickName = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.NICK_NAME_KEY);
        String S_countryView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.COUNTRY_KEY);
        String S_mMotherName = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.MOTHER_KEY );
        String S_panCardId = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PANCARD_KEY );
        String S_voterId = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.VOTER_ID_KEY);
        String S_talukView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.TALUK_KEY );
        String S_stateView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.STATE_KEY );
        String S_mSpouse = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.SPOUSE_KEY );
        String S_nameView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.NAME_KEY );
        String S_uidView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.UID_KEY );
        String S_yobView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.YOB_KEY );
        String S_ageView = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.AGE_KEY );
        String S_mSpouseAge = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.SPOUSE_AGE_KEY );
        String S_mSpouseDob = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.SPOUSE_DOB_KEY );
        String S_mCenterMeetiong = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.CENTERMEETING_KEY );
        String S_mRtionCard = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.RATIONCARD_KEY );
        String S_pincode = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.PINCODE_KEY );

        if (!S_panCard_f_or_h_name.equals("nulldata")) {
            panCard_f_or_h_name.setText(S_panCard_f_or_h_name);
        }
        if (!S_voterId_f_or_h_name.equals("nulldata")) {
            voterId_f_or_h_name.setText(S_voterId_f_or_h_name);
        }
        if (!S_mPermanentAddress.equals("nulldata")) {
            mPermanentAddress.setText(S_mPermanentAddress);
        }
        if (!S_f_or_h_name.equals("nulldata")) {
            f_or_h_name.setText(S_f_or_h_name);
        }
        if (!S_telephoneNumberView.equals("nulldata")) {
            telephoneNumberView.setText(S_telephoneNumberView);
        }
        if (!S_mobileNumberView.equals("nulldata")) {
            mobileNumberView.setText(S_mobileNumberView);
        }
        if (!S_voterIdName.equals("nulldata")) {
            voterIdName.setText(S_voterIdName);
        }

        if (!S_pincode.equals("nulldata")) {
            pincodeView.setText(S_pincode);
        }

        if (!S_panCardName.equals("nulldata")) {
            panCardName.setText(S_panCardName);
        }
        if (!S_mPostOffice.equals("nulldata")) {
            mPostOffice.setText(S_mPostOffice);
        }
        if (!S_mPersonalEmail.equals("nulldata")) {
            mPersonalEmail.setText(S_mPersonalEmail);
        }
        if (!S_districtView.equals("nulldata")) {
            districtView.setText(S_districtView);
        }
        if (!S_mLandmarks.equals("nulldata")) {
            mLandmarks.setText(S_mLandmarks);
        }
        if (!S_mPoliceStaion.equals("nulldata")) {
            mPoliceStaion.setText(S_mPoliceStaion);
        }

        if (!S_addressview.equals("nulldata")) {
            addressView.setText(S_addressview);
        }

        if (!S_mNickName.equals("nulldata")) {
            mNickName.setText(S_mNickName);
        }
        if (!S_countryView.equals("nulldata")) {
            countryView.setText(S_countryView);
        }
        if (!S_mMotherName.equals("nulldata")) {
            mMotherName.setText(S_mMotherName);
        }
        if (!S_panCardId.equals("nulldata")) {
            panCardId.setText(S_panCardId);
        }
        if (!S_voterId.equals("nulldata")) {
            voterId.setText(S_voterId);
        }
        if (!S_talukView.equals("nulldata")) {
            talukView.setText(S_talukView);
        }
        if (!S_stateView.equals("nulldata")) {
            stateView.setText(S_stateView);
        }



        if (!S_mSpouse.equals("nulldata")) {
            mSpouse.setText(S_mSpouse);
        }
        if (!S_nameView.equals("nulldata")) {
            nameView.setText(S_nameView);
        }
        if (!S_uidView.equals("nulldata")) {
            uidView.setText(S_uidView);
        }
        if (!S_yobView.equals("nulldata")) {
            yobView.setText(S_yobView);
        }
        if (!S_ageView.equals("nulldata")) {
            ageView.setText(S_ageView);
        }


        if (!S_mSpouseAge.equals("nulldata")) {
            mSpouseAge.setText(S_mSpouseAge);
        }
        if (!S_mSpouseDob.equals("nulldata")) {
            mSpouseDob.setText(S_mSpouseDob);
        }
        if (!S_mCenterMeetiong.equals("nulldata")) {
            mCenterMeetiong.setText(S_mCenterMeetiong);
        }
        if (!S_mRtionCard.equals("nulldata")) {
            mRtionCard.setText(S_mRtionCard);
        }


        //////////////////////---------------------------------
        panCard_f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(panCard_f_or_h_name));
        voterId_f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(voterId_f_or_h_name));
        mPermanentAddress.addTextChangedListener(new GenericTWCKYCDF(mPermanentAddress));
        f_or_h_name.addTextChangedListener(new GenericTWCKYCDF(f_or_h_name));
        telephoneNumberView.addTextChangedListener(new GenericTWCKYCDF(telephoneNumberView));
        mobileNumberView.addTextChangedListener(new GenericTWCKYCDF(mobileNumberView));
        voterIdName.addTextChangedListener(new GenericTWCKYCDF(voterIdName));
        panCardName.addTextChangedListener(new GenericTWCKYCDF(panCardName));
        mPostOffice.addTextChangedListener(new GenericTWCKYCDF(mPostOffice));
        mPersonalEmail.addTextChangedListener(new GenericTWCKYCDF(mPersonalEmail));
        districtView.addTextChangedListener(new GenericTWCKYCDF(districtView));
        mLandmarks.addTextChangedListener(new GenericTWCKYCDF(mLandmarks));
        mPoliceStaion.addTextChangedListener(new GenericTWCKYCDF(mPoliceStaion));
        addressView.addTextChangedListener(new GenericTWCKYCDF(addressView));
        pincodeView.addTextChangedListener(new GenericTWCKYCDF(pincodeView));
        mNickName.addTextChangedListener(new GenericTWCKYCDF(mNickName));
        countryView.addTextChangedListener(new GenericTWCKYCDF(countryView));
        mMotherName.addTextChangedListener(new GenericTWCKYCDF(mMotherName));
        panCardId.addTextChangedListener(new GenericTWCKYCDF(panCardId));
        voterId.addTextChangedListener(new GenericTWCKYCDF(voterId));
        talukView.addTextChangedListener(new GenericTWCKYCDF(talukView));
        stateView.addTextChangedListener(new GenericTWCKYCDF(stateView));
        mSpouse.addTextChangedListener(new GenericTWCKYCDF(mSpouse));
        nameView.addTextChangedListener(new GenericTWCKYCDF(nameView));
        uidView.addTextChangedListener(new GenericTWCKYCDF(uidView));
        yobView.addTextChangedListener(new GenericTWCKYCDF(yobView));
        ageView.addTextChangedListener(new GenericTWCKYCDF(ageView));
        mSpouseAge.addTextChangedListener(new GenericTWCKYCDF(mSpouseAge));
        mSpouseDob.addTextChangedListener(new GenericTWCKYCDF(mSpouseDob));
        mCenterMeetiong.addTextChangedListener(new GenericTWCKYCDF(mCenterMeetiong));
        mRtionCard.addTextChangedListener(new GenericTWCKYCDF(mRtionCard));









        mPersonalEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if (!check) {
                    mPersonalEmail.setError("Email address is not valid");
                } else {
                    mPersonalEmail.setError(null);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if (!check) {
                    mPersonalEmail.setError("Email address is not valid");
                } else {
                    mPersonalEmail.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

                boolean check = !TextUtils.isEmpty(mPersonalEmail.getText().toString()) && Patterns
                        .EMAIL_ADDRESS.matcher(mPersonalEmail.getText().toString()).matches();
                if(!check){
                    mPersonalEmail.setError("Email address is not valid");
                }else{
                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.EMAIL_ID_KEY, s.toString());
                    mPersonalEmail.setError(null);
                }
            }
        });




        String S_mSubTitle = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.NAME_SUBTITLE_KEY);
        String S_mMotherSubtitle = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.MOTHER_NAME_SUBTITLE_KEY );
        String S_mFatherSubTitle = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.FATHER_SUBTITLE_KEY );
        String S_mSpouseSubtitle = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SPOUSE_NAME_SUBTITLE_KEY);
        String S_mGender = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.SINGLE_GENDER_KEY );
        String S_mOccupation = autosave_data.getdata(getContext(), mPage.getFormId(),GuarantorsInformationPage.OCCUPATION_KEY );
        String S_mMarital = autosave_data.getdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SINGLE_MARITAL_STATUS);


        if (!S_mSubTitle.equals("nulldata")) {

            int P_mSub = adapterName.getPosition(S_mSubTitle);
            mSubTitle.setSelection(P_mSub);
        }

        if (!S_mMotherSubtitle.equals("nulldata")) {

            int P_mMot = adapterName.getPosition(S_mMotherSubtitle);
            mMotherSubtitle.setSelection(P_mMot);
        }

        if (!S_mFatherSubTitle.equals("nulldata")) {

            int P_mFat = adapterName.getPosition(S_mFatherSubTitle);
            mFatherSubTitle.setSelection(P_mFat);
        }

        if (!S_mSpouseSubtitle.equals("nulldata")) {

            int P_mSpo = adapterName.getPosition(S_mSpouseSubtitle);
            mSpouseSubtitle.setSelection(P_mSpo);
        }

        if (!S_mGender.equals("nulldata")) {

            int mGen =  adapter.getPosition(S_mGender);
            mGender.setSelection(mGen);
        }

        if (!S_mOccupation.equals("nulldata")) {

            int mOccu = occupationAdapter.getPosition(S_mOccupation);
            mOccupation.setSelection(mOccu);
        }

        if (!S_mMarital.equals("nulldata")) {

            int P_mMar = maritalAdapter.getPosition(S_mMarital);
            mMarital.setSelection(P_mMar);
        }

        //---------------spinner

        mSubTitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mMotherSubtitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mFatherSubTitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mSpouseSubtitle.setOnItemSelectedListener(getOnItemCKYCDF());
        mGender.setOnItemSelectedListener(getOnItemCKYCDF());
        mOccupation.setOnItemSelectedListener(getOnItemCKYCDF());









        mMarital.setOnItemSelectedListener(getOnItemCKYCDF());

        mMarital.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Married")){

                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Widow")){
                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else if(parent.getItemAtPosition(position).toString().equalsIgnoreCase("Widower")){
                    mSpouseSubtitle.setVisibility(View.VISIBLE);
                    mSpouse.setVisibility(View.VISIBLE);
                    mSpouseAge.setVisibility(View.VISIBLE);
                    mSpouseDob.setVisibility(View.VISIBLE);
                    mSpouseDobText.setVisibility(View.VISIBLE);
                    mSpouseAgeText.setVisibility(View.VISIBLE);
                    mSpouseNameText.setVisibility(View.VISIBLE);

                } else {
                    mSpouseAgeText.setVisibility(View.GONE);
                    mSpouseNameText.setVisibility(View.GONE);
                    mSpouseDobText.setVisibility(View.GONE);
                    mSpouse.setVisibility(View.GONE);
                    mSpouseAge.setVisibility(View.GONE);
                    mSpouseDob.setVisibility(View.GONE);
                    mSpouseSubtitle.setVisibility(View.GONE);
                }
                autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
                mPage.getData().putString(GuarantorsInformationPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });











        return rootView;
    }

    private void updateLabel(int selectedYear) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        yobView.setText(sdf.format(myCalendar.getTime()));
        int age = Calendar.getInstance().get(Calendar.YEAR) - selectedYear;
        ageView.setText(String.format("%d", age));
    }

    private void updateSpouseAge(int selectedYear) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        mSpouseDob.setText(sdf.format(myCalendar.getTime()));
        int age = Calendar.getInstance().get(Calendar.YEAR) - selectedYear;
        mSpouseAge.setText(String.format("%d", age));
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // todo commented becouse getting error
/*
        mMotherSubtitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERMOTHER_NAME_SUBTITLE_KEY, i, val);
            }
        });
        mFatherSubTitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val = String.valueOf(parent.getItemAtPosition(position));
               // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERFATHER_HUSBAND_SUBTITLE_KEY, i, val);
            }
        });
        mSpouseSubtitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val = String.valueOf(parent.getItemAtPosition(position));
                //mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERSPOUSE_NAME_SUBTITLE_KEY, i, val);
            }
        });
        mGender.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val = String.valueOf(parent.getItemAtPosition(position));
               // mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTERNAME_SUBTITLE_KEY, i, val);
            }
        });
        mOccupation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String val = String.valueOf(parent.getItemAtPosition(position));
                mydb.Autosavebyid(TABLE_GUARANTERINFO, MySQLiteDB.GUARANTEROCCUPATION_KEY, i, val);
            }
        });

*/


    }



    public AdapterView.OnItemSelectedListener getOnItemCKYCDF() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //  Toast.makeText(getContext(), "in on getOnItemAFDF :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.kycsubtitle) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(GuarantorsInformationPage.NAME_SUBTITLE_KEY, parent.getSelectedItem().toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.fatherubtitle) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.FATHER_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(GuarantorsInformationPage.FATHER_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

                if (parent.getId() == R.id.single_page_gender) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SINGLE_GENDER_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(GuarantorsInformationPage.SINGLE_GENDER_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }


                if (parent.getId() == R.id.mothersubtitle) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.MOTHER_NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(GuarantorsInformationPage.MOTHER_NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }
                if (parent.getId() == R.id.custeomer_page_marital_status) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(GuarantorsInformationPage.SINGLE_MARITAL_STATUS, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }
                if (parent.getId() == R.id.occupation) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.OCCUPATION_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(GuarantorsInformationPage.OCCUPATION_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }

                if (parent.getId() == R.id.spousesubtitle) {

                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SPOUSE_NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.getData().putString(GuarantorsInformationPage.SPOUSE_NAME_SUBTITLE_KEY, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                        // Toast.makeText(getContext(), "phy selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                    }


                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/


            }
        };
    }


    class GenericTWCKYCDF implements TextWatcher {

        private View view;

        private GenericTWCKYCDF(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
            //Toast.makeText(getContext(), "in GenericTextASDF", Toast.LENGTH_SHORT).show();
            if (i1 == R.id.name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.NAME_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.nick_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.NICK_NAME_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.NICK_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            if (i1 == R.id.uid) {

                if (!TextUtils.isEmpty(text)) {
                    uidView = (EditText)view.findViewById(R.id.uid);
                        if (S_uidViewCustomerKYC.equals(text)){
                            uidView.requestFocus();
                            uidView.setError("Your ID and Customer ID is Same, Please Enter Different ID");

                            DialogFragment dg = new DialogFragment() {
                                @Override
                                public Dialog onCreateDialog(Bundle savedInstanceState) {
                                    return new AlertDialog.Builder(getActivity())
                                            .setMessage("Please Enter Different ID")
                                            .setNegativeButton("OK",
                                                    null).create();
                                }
                            };
                            dg.show(getFragmentManager(), "Same ID Dialog");

                        }else{
                            autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.UID_KEY, text);


                            mPage.getData().putString(GuarantorsInformationPage.UID_KEY, editable.toString());
                            try {
                                mPage.notifyDataChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                }
            }

            if (i1 == R.id.f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(GuarantorsInformationPage.FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.dob) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.YOB_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.YOB_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.age) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.AGE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.AGE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.address) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.ADDRESS_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.ADDRESS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.taluk) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.TALUK_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.TALUK_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.district) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.DISTRICT_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.DISTRICT_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.state) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.STATE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.STATE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.landmark) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.LANDMARKS_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.LANDMARKS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


            if (i1 == R.id.permanent_address) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PERMANENT_ADDRESS_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.PERMANENT_ADDRESS_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pincode) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PINCODE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.PINCODE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.police_station) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.POLICE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.POLICE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.post_office) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.POST_OFFICE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.POST_OFFICE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.telephoneNumber) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.TELE_PHONE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.TELE_PHONE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.mobilePhoneNumber) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.MOBILE_PHONE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.MOBILE_PHONE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.personal_email_id) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.EMAIL_ID_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.EMAIL_ID_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.mother) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.MOTHER_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.MOTHER_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.voter_id_number) {

                if (!TextUtils.isEmpty(text)) {

                    voterId = (EditText)view.findViewById(R.id.voter_id_number);
                    if (S_voterIdCustomerKYC.equals(text)){
                        voterId.setError("Your VoterID and Customer VoterID is Same, Please Enter Different VoterID");

                        DialogFragment dg = new DialogFragment() {
                            @Override
                            public Dialog onCreateDialog(Bundle savedInstanceState) {
                                return new AlertDialog.Builder(getActivity())
                                        .setMessage("Please Enter Different VoterID")
                                        .setNegativeButton("OK",
                                                null).create();
                            }
                        };
                        dg.show(getFragmentManager(), "Same ID Dialog");

                    }else{

                        autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.VOTER_ID_KEY, text);

                        mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_NAME_KEY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }

            if (i1 == R.id.voter_id_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.VOTER_ID_NAME_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.voter_id_f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(GuarantorsInformationPage.VOTER_ID_FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pancard_number) {

                if (!TextUtils.isEmpty(text)) {

                        panCardId = (EditText)view.findViewById(R.id.pancard_number);
                        if (S_panCardIdCustomerKYC.equals(text)){
                            panCardId.setError("Your PanCardID and Customer PanCardID is Same, Please Enter Different PanCardID");

                            DialogFragment dg = new DialogFragment() {
                                @Override
                                public Dialog onCreateDialog(Bundle savedInstanceState) {
                                    return new AlertDialog.Builder(getActivity())
                                            .setMessage("Please Enter Different PanCardID")
                                            .setNegativeButton("OK",
                                                    null).create();
                                }
                            };
                            dg.show(getFragmentManager(), "Same ID Dialog");

                        }else{
                            autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PANCARD_KEY, text);

                            mPage.getData().putString(GuarantorsInformationPage.PANCARD_KEY, editable.toString());
                            try {
                                mPage.notifyDataChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                }

            }
            if (i1 == R.id.pancard_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PANCARD_NAME_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.PANCARD_NAME_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.pancard_f_or_h_name) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.PANCARD_FATHER_OR_HUSBAND_NAME, text);


                    mPage.getData().putString(GuarantorsInformationPage.PANCARD_FATHER_OR_HUSBAND_NAME, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }


            if (i1 == R.id.center_meeting) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.CENTERMEETING_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.CENTERMEETING_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }

            if (i1 == R.id.ration_card) {

                //S_mRtionCardCustomerKYC.........
                if (!TextUtils.isEmpty(text)) {

                        mRtionCard = (EditText)view.findViewById(R.id.ration_card);
                        if (S_mRtionCardCustomerKYC.equals(text)){
                            mRtionCard.setError("Your RationID and Customer RationID is Same, Please Enter Different RationID");

                            DialogFragment dg = new DialogFragment() {
                                @Override
                                public Dialog onCreateDialog(Bundle savedInstanceState) {
                                    return new AlertDialog.Builder(getActivity())
                                            .setMessage("Please Enter Different RationID")
                                            .setNegativeButton("OK",
                                                    null).create();
                                }
                            };
                            dg.show(getFragmentManager(), "Same ID Dialog");

                        }else{
                            autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.RATIONCARD_KEY, text);

                            mPage.getData().putString(GuarantorsInformationPage.RATIONCARD_KEY, editable.toString());
                            try {
                                mPage.notifyDataChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                }

            }

            if (i1 == R.id.spouse) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SPOUSE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.SPOUSE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.spouse_age) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SPOUSE_AGE_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.SPOUSE_AGE_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }

            }
            if (i1 == R.id.spouse_dob) {

                if (!TextUtils.isEmpty(text)) {


                    autosave_data.putdata(getContext(), mPage.getFormId(), GuarantorsInformationPage.SPOUSE_DOB_KEY, text);


                    mPage.getData().putString(GuarantorsInformationPage.SPOUSE_DOB_KEY, editable.toString());
                    try {
                        mPage.notifyDataChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }

        }

    }

}
