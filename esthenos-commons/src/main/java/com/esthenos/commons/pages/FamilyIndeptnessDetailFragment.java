package com.esthenos.commons.pages;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.MySQLiteDB;
import com.tech.freak.wizardpager.ui.PageFragment;

public class FamilyIndeptnessDetailFragment extends PageFragment<FamilyIndeptnessDetailPage> {
    public static final String TABLE_FAMILYINDENT = "Family_Indebtnes";
    MySQLiteDB mydb;
    AutoSaveData autosave;
    long i;

    private EditText mName,mBorrowedAmt,mMonthlyRepay,mOutstandin;
    private Spinner mLoanSourse,mRepayment;

    private String[] LoanSourse={"","MFI","Bank"};
    private String[] PaymentTrack={"","Regular","Irregular"};
    Autosave_Data autosave_data =new Autosave_Data();

    public static FamilyIndeptnessDetailFragment create(String key) {
        FamilyIndeptnessDetailFragment fragment = new FamilyIndeptnessDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY,key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_family_indeptness_detail, container, false);
        ((TextView)rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        String s_FamilyIndeptnessDeailFragname=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_NAME);
        String s_FamilyIndeptnessDeailFragborroamt=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_BORROW_AMT);
        String s_FamilyIndeptnessDeailFragmonthalyrepay=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_MONTHLY_REPAY);
        String s_FamilyIndeptnessDeailFragoutstandin=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_OUTSTANDING);
        String s_FamilyIndeptnessDeailFragloansource=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_LOAN_SOURCE);
        String s_FamilyIndeptnessDeailFragrepayment=autosave_data.getdata(getContext(), mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_REPAYMENT_TRACK);


        mydb = new MySQLiteDB(getContext());
       // autosave = new AutoSaveData(getContext());

        mName=(EditText)rootView.findViewById(R.id.indent_name);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_NAME, mName.getText().toString());
        if (!s_FamilyIndeptnessDeailFragname.equals("nulldata")){
            mName.setText(s_FamilyIndeptnessDeailFragname);
        }


        mBorrowedAmt=(EditText)rootView.findViewById(R.id.indent_borrowed_amt);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_BORROW_AMT, mBorrowedAmt.getText().toString());
        if (!s_FamilyIndeptnessDeailFragborroamt.equals("nulldata")){
            mBorrowedAmt.setText(s_FamilyIndeptnessDeailFragborroamt);
        }


        mMonthlyRepay=(EditText)rootView.findViewById(R.id.indent_monthly_repay);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_MONTHLY_REPAY,mMonthlyRepay.getText().toString());
        if (!s_FamilyIndeptnessDeailFragmonthalyrepay.equals("nulldata")){
            mMonthlyRepay.setText(s_FamilyIndeptnessDeailFragmonthalyrepay);
        }


        mOutstandin=(EditText)rootView.findViewById(R.id.indent_outstanding);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_OUTSTANDING,mOutstandin.getText().toString());
        if (!s_FamilyIndeptnessDeailFragoutstandin.equals("nulldata")){
            mOutstandin.setText(s_FamilyIndeptnessDeailFragoutstandin);
        }


        mLoanSourse=(Spinner)rootView.findViewById(R.id.indent_loan_sourse);
        ArrayAdapter<String> loanAdapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,LoanSourse);
        loanAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mLoanSourse.setAdapter(loanAdapter);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_LOAN_SOURCE,mLoanSourse.getSelectedItem().toString());
        if (!s_FamilyIndeptnessDeailFragloansource.equals("nulldata")){
            mLoanSourse.setSelection(loanAdapter.getPosition(s_FamilyIndeptnessDeailFragloansource));
        }

        mRepayment=(Spinner)rootView.findViewById(R.id.indent_repayment_track);
        ArrayAdapter<String> repaymentAdapter=new ArrayAdapter<String>(getActivity(),R.layout.layout_spinner_item,PaymentTrack);
        repaymentAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        mRepayment.setAdapter(repaymentAdapter);
        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_REPAYMENT_TRACK, mRepayment.getSelectedItem().toString());
        if (!s_FamilyIndeptnessDeailFragrepayment.equals("nulldata")){
            mRepayment.setSelection(repaymentAdapter.getPosition(s_FamilyIndeptnessDeailFragrepayment));
        }

        mLoanSourse.setOnItemSelectedListener(getOnItem());
        mRepayment.setOnItemSelectedListener(getOnItem());
        mName.addTextChangedListener(new GenericTextFamilyIndeptnessDetail(mName));
        mBorrowedAmt.addTextChangedListener(new GenericTextFamilyIndeptnessDetail(mBorrowedAmt));
        mMonthlyRepay.addTextChangedListener(new GenericTextFamilyIndeptnessDetail(mMonthlyRepay));
        mOutstandin.addTextChangedListener(new GenericTextFamilyIndeptnessDetail(mOutstandin));
        return rootView;
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

//        mLoanSourse.setOnItemSelectedListener(getOnItemSelectedListener(FamilyIndeptnessDetailPage.FAMILY_INDENT_LOAN_SOURCE));
//        mRepayment.setOnItemSelectedListener(getOnItemSelectedListener(FamilyIndeptnessDetailPage.FAMILY_INDENT_REPAYMENT_TRACK));
//        mName.addTextChangedListener(getTextWatcher(FamilyIndeptnessDetailPage.FAMILY_INDENT_NAME));
//        mBorrowedAmt.addTextChangedListener(getTextWatcher(FamilyIndeptnessDetailPage.FAMILY_INDENT_BORROW_AMT));
//        mMonthlyRepay.addTextChangedListener(getTextWatcher(FamilyIndeptnessDetailPage.FAMILY_INDENT_MONTHLY_REPAY));
//        mOutstandin.addTextChangedListener(getTextWatcher(FamilyIndeptnessDetailPage.FAMILY_INDENT_OUTSTANDING));
//        mLoanSourse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.FirstAutoSaveData(TABLE_FAMILYINDENT, MySQLiteDB.FAMILY_INDENT_LOAN_SOURCE, value);
//                i = mydb.getLastInsertedId(TABLE_FAMILYINDENT);
//                Toast.makeText(getContext(), "id getted : " + i, Toast.LENGTH_LONG).show();
//                // autosave.Sharedsetvalue(getContext(),"FamilyMembername" ,"FamilyDetailKey" ,String.valueOf(i));
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        mRepayment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String value = String.valueOf(parent.getItemAtPosition(position));
//                mydb.Autosavebyid(TABLE_FAMILYINDENT, MySQLiteDB.FAMILY_INDENT_REPAYMENT_TRACK, i, value);
//            }
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

    }
    public AdapterView.OnItemSelectedListener getOnItem() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   Toast.makeText(getContext(), "in on selected :== ", Toast.LENGTH_SHORT).show();


                if (parent.getId() == R.id.indent_repayment_track) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        //  Toast.makeText(getContext(), "mReligion selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();

                        autosave_data.putdata(getContext(),mPage.getFormId(), FamilyIndeptnessDetailPage.FAMILY_INDENT_REPAYMENT_TRACK, parent.getItemAtPosition(position).toString());

                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_REPAYMENT_TRACK,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }
                if (parent.getId() == R.id.indent_loan_sourse) {
                    if (parent.getItemAtPosition(position).toString() != null && parent.getItemAtPosition(position).toString().length() > 0) {
                        autosave_data.putdata(getContext(),mPage.getFormId(), FamilyIndeptnessDetailPage.FAMILY_INDENT_LOAN_SOURCE, parent.getItemAtPosition(position).toString());
                        //  Toast.makeText(getContext(), "mCat selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_LOAN_SOURCE,  parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /*if (parent.getId() == R.id.single_page_education) {

                    if (parent.getSelectedItemPosition()== 0) {
                        Toast.makeText(getContext(), "mEdu selected value :== " + parent.getSelectedItem(), Toast.LENGTH_SHORT).show();
                        mPage.getData().putString(CustomerPersonalDetailsPage.SINGLE_EDUCATION, parent.getItemAtPosition(position).toString());
                        mPage.notifyDataChanged();
                    }
                }*/
            }
        };
    }

    class GenericTextFamilyIndeptnessDetail implements TextWatcher {

        private View view;

        private GenericTextFamilyIndeptnessDetail(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            int i1 = view.getId();
           // Toast.makeText(getContext(), "in text watcher view:" + view.getId() + "==" + R.id.family_members_count, Toast.LENGTH_SHORT).show();
            if (i1 == R.id.indent_name) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_NAME,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_NAME, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.indent_borrowed_amt) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_BORROW_AMT,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_BORROW_AMT, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.indent_monthly_repay) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_MONTHLY_REPAY,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_MONTHLY_REPAY, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (i1 == R.id.indent_outstanding) {
                if (!TextUtils.isEmpty(text)) {

                    autosave_data.putdata(getContext(),mPage.getFormId(),FamilyIndeptnessDetailPage.FAMILY_INDENT_OUTSTANDING,text);

                    if (text != null && editable.length() > 0) {
                        mPage.getData().putString(FamilyIndeptnessDetailPage.FAMILY_INDENT_OUTSTANDING, editable.toString());
                        try {
                            mPage.notifyDataChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
