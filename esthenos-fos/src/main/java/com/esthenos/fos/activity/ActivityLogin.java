package com.esthenos.fos.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.esthenos.commons.utils.AutoSaveData;
import com.esthenos.commons.utils.Autosave_Data;
import com.esthenos.commons.utils.Constants;
import com.esthenos.commons.utils.SingletonConfig;
import com.esthenos.fos.R;


public class ActivityLogin extends AppCompatActivity {

    private final String TAG = "ActivityLogin";
    AutoSaveData autoSaveData;
    Autosave_Data data = new Autosave_Data();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        autoSaveData = new AutoSaveData(getApplicationContext());
        autoSaveData.clearCache(getApplicationContext(), 7);
        data.isFolderExist();


        final EditText mEmailText = (EditText) findViewById(R.id.emailText);
        final EditText mPasswordText = (EditText) findViewById(R.id.passwordText);
        final Button button = (Button) findViewById(R.id.login_button);

        final TextView footer = (TextView) findViewById(R.id.BottomFooter1);
        footer.setText(String.format("%s, %s", getString(R.string.app_name), Constants.getVersion(getApplicationContext())));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            SingletonConfig instance = SingletonConfig.getLoginToken(
                                    mEmailText.getText().toString(),
                                    mPasswordText.getText().toString()
                            );

                            if (instance != null) {
                                logUser(instance);
                                Intent intent = new Intent(ActivityLogin.this, ActivityUserPerformance.class);
                                ActivityLogin.this.startActivity(intent);

                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog alertDialog = new AlertDialog.Builder(ActivityLogin.this).create();
                                        alertDialog.setTitle("Error");
                                        alertDialog.setMessage("Authentication Failed");
                                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                });
                                        alertDialog.show();
                                    }
                                });
                            }

                        } catch (Exception e) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ActivityLogin.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                                }
                            });
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    private void logUser(SingletonConfig instance) {
        Crashlytics.setUserEmail(instance.getUser().email);
        Crashlytics.setUserIdentifier(instance.getUser().id);
    }
}
