package com.esthenos.fos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.esthenos.fos.R;
import com.esthenos.fos.kyc.KYCFOSActivity;

import static com.esthenos.commons.utils.Constants.ACTIVITY;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_DOCS_OTHER;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_FORM;
import static com.esthenos.commons.utils.Constants.ACTIVITY_APPLICANT_KYC;
import static com.esthenos.commons.utils.Constants.ACTIVITY_GUARANTOR_KYC;
import static com.esthenos.commons.utils.Constants.UPLOAD_KYC;


public class ActivityAddMember extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_member_activity);

        TextView buttonkyc = (TextView) findViewById(R.id.add_kyc_button);
        buttonkyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCFOSActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_KYC);
                ActivityAddMember.this.startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        TextView buttonOtherKyc = (TextView) findViewById(R.id.add_other_button);
        buttonOtherKyc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCFOSActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_DOCS_OTHER);
                ActivityAddMember.this.startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        TextView buttonKycG = (TextView) findViewById(R.id.add_guarantors_button);
        buttonKycG.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCFOSActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_GUARANTOR_KYC);
                ActivityAddMember.this.startActivityForResult(intent, UPLOAD_KYC);
            }
        });

        TextView buttonFillAPP = (TextView) findViewById(R.id.fill_application);
        buttonFillAPP.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddMember.this, KYCFOSActivity.class);
                intent.putExtra(ACTIVITY, ACTIVITY_APPLICANT_FORM);
                ActivityAddMember.this.startActivity(intent);
            }
        });

        ImageView imgback = (ImageView) findViewById(R.id.back_btn);
        imgback.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityLocBusiness.class);
                startActivity(intent);
            }
        });
    }
}
