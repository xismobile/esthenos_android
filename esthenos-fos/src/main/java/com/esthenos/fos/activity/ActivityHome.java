package com.esthenos.fos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.esthenos.commons.utils.Constants;
import com.esthenos.fos.R;
import com.esthenos.commons.service.SyncService;

import java.util.Calendar;
import java.util.Locale;

public class ActivityHome extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView greetingText = (TextView) findViewById(R.id.tvGreeting);
        greetingText.setText(Constants.setGreeting());

        Button mButtonOffline = (Button) findViewById(R.id.Button_offline);
        mButtonOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityLocBusiness.class);
                intent.putExtra("tag_location_header", getString(R.string.tag_location_header));
                intent.putExtra("tag_location_button", getString(R.string.tag_location_button));
                ActivityHome.this.startActivity(intent);
            }
        });

        Button mButtonAppSubmit = (Button) findViewById(R.id.submit_status);
        mButtonAppSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this, SyncService.class);
                startService(intent);
                Toast.makeText(ActivityHome.this, "Service Started", Toast.LENGTH_LONG).show();
            }
        });
    }
}
