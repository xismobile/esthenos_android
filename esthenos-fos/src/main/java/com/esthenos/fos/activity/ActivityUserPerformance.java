package com.esthenos.fos.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.fos.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class ActivityUserPerformance extends Activity {

    private HorizontalBarChart targetloannochart, targetloanamoutchar, disburstmentchart,mSanctionChar,mScrutinyChart;
    private BootstrapButton mNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_performance);

        targetloannochart = (HorizontalBarChart) findViewById(R.id.target_loans_no);
        targetloanamoutchar = (HorizontalBarChart) findViewById(R.id.target_loans_amount);
        disburstmentchart = (HorizontalBarChart) findViewById(R.id.pending_disburtments);
        mScrutinyChart = (HorizontalBarChart) findViewById(R.id.scrutiny_chart);
        mSanctionChar = (HorizontalBarChart) findViewById(R.id.sanction_chart);

        BarData data = new BarData(getXAxisValues(), getDataSet());
        targetloannochart.setData(data);
        targetloannochart.setDescription("");
        targetloannochart.animateXY(2000, 2000);
        targetloannochart.invalidate();
        targetloannochart.getLegend().setEnabled(false);
        targetloannochart.getAxisLeft().setDrawLabels(false);
        targetloannochart.getAxisRight().setDrawLabels(false);
        targetloannochart.getXAxis().setDrawLabels(false);
        targetloannochart.setScaleEnabled(false);
        targetloannochart.getXAxis().setEnabled(false); // hides horizontal grid lines inside chart
        targetloannochart.getAxisRight().setEnabled(false); // hides horizontal grid lines with below line
        YAxis leftAxis = targetloannochart.getAxisLeft();
        leftAxis.setEnabled(false);

        BarData data1 = new BarData(getXAxisValues(), getDataSet1());
        targetloanamoutchar.setData(data1);
        targetloanamoutchar.animateXY(2000, 2000);
        targetloanamoutchar.setDescription("");
        targetloanamoutchar.invalidate();
        targetloanamoutchar.getLegend().setEnabled(false);
        targetloanamoutchar.getAxisLeft().setDrawLabels(false);
        targetloanamoutchar.getAxisRight().setDrawLabels(false);
        targetloanamoutchar.getXAxis().setDrawLabels(false);
        targetloanamoutchar.setScaleEnabled(false);
        targetloanamoutchar.getXAxis().setEnabled(false); // hides horizontal grid lines inside chart
        targetloanamoutchar.getAxisRight().setEnabled(false); // hides horizontal grid lines with below line
        leftAxis = targetloanamoutchar.getAxisLeft();
        leftAxis.setEnabled(false);

        BarData data2 = new BarData(getXAxisValues(), getDataSet2());
        disburstmentchart.setData(data2);
        disburstmentchart.animateXY(2000, 2000);
        disburstmentchart.setDescription("");
        disburstmentchart.invalidate();
        disburstmentchart.getLegend().setEnabled(false);
        disburstmentchart.getAxisLeft().setDrawLabels(false);
        disburstmentchart.getAxisRight().setDrawLabels(false);
        disburstmentchart.getXAxis().setDrawLabels(false);
        disburstmentchart.setScaleEnabled(false);
        disburstmentchart.getXAxis().setEnabled(false); // hides horizontal grid lines inside chart
        disburstmentchart.getAxisRight().setEnabled(false); // hides horizontal grid lines with below line
        leftAxis = disburstmentchart.getAxisLeft();
        leftAxis.setEnabled(false);

        BarData data3 = new BarData(getXAxisValues(), getDataSet3());
        mScrutinyChart.setData(data3);
        mScrutinyChart.animateXY(2000, 2000);
        mScrutinyChart.setDescription("");
        mScrutinyChart.invalidate();
        mScrutinyChart.getLegend().setEnabled(false);
        mScrutinyChart.getAxisLeft().setDrawLabels(false);
        mScrutinyChart.getAxisRight().setDrawLabels(false);
        mScrutinyChart.getXAxis().setDrawLabels(false);
        mScrutinyChart.setScaleEnabled(false);
        mScrutinyChart.getXAxis().setEnabled(false); // hides horizontal grid lines inside chart
        mScrutinyChart.getAxisRight().setEnabled(false); // hides horizontal grid lines with below line
        leftAxis = mScrutinyChart.getAxisLeft();
        leftAxis.setEnabled(false);

        BarData data4 = new BarData(getXAxisValues(), getDataSet4());
        mSanctionChar.setData(data4);
        mSanctionChar.animateXY(2000, 2000);
        mSanctionChar.setDescription("");
        mSanctionChar.invalidate();
        mSanctionChar.getLegend().setEnabled(false);
        mSanctionChar.getAxisLeft().setDrawLabels(false);
        mSanctionChar.getAxisRight().setDrawLabels(false);
        mSanctionChar.getXAxis().setDrawLabels(false);
        mSanctionChar.setScaleEnabled(false);
        mSanctionChar.getXAxis().setEnabled(false); // hides horizontal grid lines inside chart
        mSanctionChar.getAxisRight().setEnabled(false); // hides horizontal grid lines with below line
        leftAxis = mSanctionChar.getAxisLeft();
        leftAxis.setEnabled(false);

        mNext = (BootstrapButton) findViewById(R.id.next);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityHome.class);
                ActivityUserPerformance.this.startActivity(intent);
            }
        });
    }

    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(65.000f, 0); // Jan
        valueSet1.add(v1e1);

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(80.000f, 0); // Jan
        valueSet2.add(v2e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));
        // barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<BarDataSet> getDataSet1() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(45.000f, 0); // Jan
        valueSet1.add(v1e1);

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(70.000f, 0); // Jan
        valueSet2.add(v2e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));
        //barDataSet1.setColors(ColorTemplate.JOYFUL_COLORS);
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<BarDataSet> getDataSet2() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(12.000f, 0); // Jan
        valueSet1.add(v1e1);

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(25.000f, 0); // Jan
        valueSet2.add(v2e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<BarDataSet> getDataSet3() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(12.000f, 0); // Jan
        valueSet1.add(v1e1);

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(25.000f, 0); // Jan
        valueSet2.add(v2e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<BarDataSet> getDataSet4() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(12.000f, 0); // Jan
        valueSet1.add(v1e1);

        ArrayList<BarEntry> valueSet2 = new ArrayList<>();
        BarEntry v2e1 = new BarEntry(25.000f, 0); // Jan
        valueSet2.add(v2e1);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Target");
        barDataSet1.setColor(Color.rgb(0, 200, 0));
        BarDataSet barDataSet2 = new BarDataSet(valueSet2, "Achieved");
        barDataSet2.setColors(ColorTemplate.PASTEL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("JAN");
        return xAxis;
    }
}
