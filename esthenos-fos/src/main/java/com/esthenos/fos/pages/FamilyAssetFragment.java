package com.esthenos.fos.pages;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.esthenos.commons.R;
import com.tech.freak.wizardpager.ui.PageFragment;

public class FamilyAssetFragment extends PageFragment<FamilyAssetPage> {

    String[] houseType = {"Family Owned", "Self Owned", "Rented/Leased", "Govt Allocated", "None"};
    String[] qualityOfHouse = {"Pakka/Concrete", "Kaccha/Mud", "Semi/Pakka", "Thatched Roof", "Tilled Roof", "None"};
    String[] hasRentAgreement = {"Yes", "No"};

    Spinner sHouseType, sQuality, sRentAgreement;
    EditText mStaying, mRentedHouse, mRentedShop, mAssetLand, mAssetOrchard, mMonthlyRent;

    public static FamilyAssetFragment create(String key) {
        FamilyAssetFragment fragment = new FamilyAssetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_family_asset_fos, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        sRentAgreement = (Spinner) rootView.findViewById(R.id.spiner_rent_agreement);
        ArrayAdapter hasRentAgreementAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, hasRentAgreement);
        hasRentAgreementAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sRentAgreement.setAdapter(hasRentAgreementAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_RENT_AGREEMENT, sRentAgreement.getSelectedItem().toString());

        mMonthlyRent = (EditText) rootView.findViewById(R.id.asset_monthly_rent);
        mPage.getData().putString(FamilyAssetPage.KEY_MONTHLY_RENT, mMonthlyRent.getText().toString());

        sHouseType = (Spinner) rootView.findViewById(R.id.spiner_types_house);
        ArrayAdapter houseAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, houseType);
        houseAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sHouseType.setAdapter(houseAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_TYPES_OF_HOUSE, sHouseType.getSelectedItem().toString());

        sQuality = (Spinner) rootView.findViewById(R.id.spiner_quality_house);
        ArrayAdapter qualityAdapter = new ArrayAdapter(getActivity(), R.layout.layout_spinner_item, qualityOfHouse);
        qualityAdapter.setDropDownViewResource(R.layout.layout_spinner_dropdown);
        sQuality.setAdapter(qualityAdapter);
        mPage.getData().putString(FamilyAssetPage.KEY_QUALITY_OF_HOUSE, sQuality.getSelectedItem().toString());

        mStaying = (EditText) rootView.findViewById(R.id.asset_stying_in_house);
        mPage.getData().putString(FamilyAssetPage.KEY_STAYING_IN_HOUSE, mStaying.getText().toString());

        mRentedHouse = (EditText) rootView.findViewById(R.id.asset_rented_house);
        mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_HOUSE, mRentedHouse.getText().toString());

        mRentedShop = (EditText) rootView.findViewById(R.id.asset_rented_shop);
        mPage.getData().putString(FamilyAssetPage.KEY_NUMBER_OF_SHOP, mRentedShop.getText().toString());

        mAssetLand = (EditText) rootView.findViewById(R.id.family_asset_land);
        mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_LAND, mAssetLand.getText().toString());

        mAssetOrchard = (EditText) rootView.findViewById(R.id.family_asset_orchand);
        mPage.getData().putString(FamilyAssetPage.KEY_ASSETS_ORCHARD, mAssetOrchard.getText().toString());

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sHouseType.setOnItemSelectedListener(getOnItemSelectedListener(FamilyAssetPage.KEY_TYPES_OF_HOUSE));
        sQuality.setOnItemSelectedListener(getOnItemSelectedListener(FamilyAssetPage.KEY_QUALITY_OF_HOUSE));
        mStaying.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_STAYING_IN_HOUSE));
        mRentedHouse.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_NUMBER_OF_HOUSE));
        mRentedShop.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_NUMBER_OF_SHOP));
        mAssetLand.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_ASSETS_LAND));
        mAssetOrchard.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_ASSETS_ORCHARD));

        sRentAgreement.setOnItemSelectedListener(getOnItemSelectedListener(FamilyAssetPage.KEY_RENT_AGREEMENT));
        mMonthlyRent.addTextChangedListener(getTextWatcher(FamilyAssetPage.KEY_MONTHLY_RENT));

        sHouseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boolean enabled = parent.getItemAtPosition(position).toString().equalsIgnoreCase("rented/leased");
                mMonthlyRent.setEnabled(enabled);
                sRentAgreement.setEnabled(enabled);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}

