package com.esthenos.fos.pages;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.esthenos.commons.customcamera.CustomCameraActivity;
import com.esthenos.commons.utils.Constants;
import com.esthenos.fos.R;
import com.tech.freak.wizardpager.ui.PageFragment;

import java.lang.reflect.Method;

public class HypothecationGoodsFragment extends PageFragment<HypothecationGoodsPage> implements View.OnClickListener {

    EditText mDetails, mMarketValue, mPurchaseYear, mPurchasePrice, mPurchasePurpose;
    BootstrapButton mGoodsImage,mDocImage;

    public static HypothecationGoodsFragment create(String key) {
        HypothecationGoodsFragment fragment = new HypothecationGoodsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.esthenos.commons.R.layout.fragment_hypothecation_goods, container, false);
        ((TextView) rootView.findViewById(android.R.id.title)).setText(mPage.getTitle());

        mDetails = (EditText) rootView.findViewById(com.esthenos.commons.R.id.goods_details);
        mPage.getData().putString(HypothecationGoodsPage.KEY_GOODS_DETAILS, mDetails.getText().toString());

        mMarketValue = (EditText) rootView.findViewById(com.esthenos.commons.R.id.goods_market_value);
        mPage.getData().putString(HypothecationGoodsPage.KEY_MARKET_VALUE, mMarketValue.getText().toString());

        mPurchaseYear = (EditText) rootView.findViewById(com.esthenos.commons.R.id.goods_purchase_year);
        mPage.getData().putString(HypothecationGoodsPage.KEY_PURCHASE_YEAR, mPurchaseYear.getText().toString());

        mPurchasePrice = (EditText) rootView.findViewById(com.esthenos.commons.R.id.goods_purchase_price);
        mPage.getData().putString(HypothecationGoodsPage.KEY_PURCHASE_PRICE, mPurchasePrice.getText().toString());

        mPurchasePurpose = (EditText) rootView.findViewById(com.esthenos.commons.R.id.goods_purchase_purpose);
        mPage.getData().putString(HypothecationGoodsPage.KEY_PURCHASE_PURPOSE, mPurchasePurpose.getText().toString());

        mGoodsImage=(BootstrapButton)rootView.findViewById(R.id.goods_image);
        mGoodsImage.setOnClickListener(this);
        mDocImage=(BootstrapButton)rootView.findViewById(R.id.goods_docs_image);
        mDocImage.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDetails.addTextChangedListener(getTextWatcher(HypothecationGoodsPage.KEY_GOODS_DETAILS));
        mMarketValue.addTextChangedListener(getTextWatcher(HypothecationGoodsPage.KEY_MARKET_VALUE));
        mPurchaseYear.addTextChangedListener(getTextWatcher(HypothecationGoodsPage.KEY_PURCHASE_YEAR));
        mPurchasePrice.addTextChangedListener(getTextWatcher(HypothecationGoodsPage.KEY_PURCHASE_PRICE));
        mPurchasePurpose.addTextChangedListener(getTextWatcher(HypothecationGoodsPage.KEY_PURCHASE_PURPOSE));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.goods_image :
                TakeImage();
                break;
            case R.id.goods_docs_image:
                TakeImage();
                break;
            default:
        }
    }

    public void TakeImage() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent customCam = new Intent(getActivity(), CustomCameraActivity.class);
                customCam.putExtra("card_type", mPage.getPageType());
                customCam.putExtra("card_name", mPage.getFormId());
                customCam.putExtra("card_owner", mPage.getPageOwner());
                startActivityForResult(customCam, Constants.UPLOAD_KYC);
            }
        });
    }
}
