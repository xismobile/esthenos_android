package com.esthenos.fos.pages;


import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class HypothecationGoodsPage extends Page {

    public static final String KEY_GOODS_DOCS = "Goods Docs Image";
    public static final String KEY_GOODS_IMAGE = "Goods Image";
    public static final String KEY_GOODS_DETAILS = "Goods Details";

    public static final String KEY_MARKET_VALUE = "Market Value";
    public static final String KEY_PURCHASE_YEAR = "Purchase Year";
    public static final String KEY_PURCHASE_PRICE = "Purchase Price";
    public static final String KEY_PURCHASE_PURPOSE = "Purchase Purpose";

    public HypothecationGoodsPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return HypothecationGoodsFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {
        dest.add(new ReviewItem(KEY_GOODS_DOCS, mData.getString(KEY_GOODS_DOCS), getPageId(), -1));
        dest.add(new ReviewItem(KEY_GOODS_IMAGE, mData.getString(KEY_GOODS_IMAGE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_GOODS_DETAILS, mData.getString(KEY_GOODS_DETAILS), getPageId(), -1));

        dest.add(new ReviewItem(KEY_MARKET_VALUE, mData.getString(KEY_MARKET_VALUE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_PURCHASE_YEAR, mData.getString(KEY_PURCHASE_YEAR), getPageId(), -1));
        dest.add(new ReviewItem(KEY_PURCHASE_PRICE, mData.getString(KEY_PURCHASE_PRICE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_PURCHASE_PURPOSE, mData.getString(KEY_PURCHASE_PURPOSE), getPageId(), -1));
    }
}
