package com.esthenos.fos.pages;

import android.support.v4.app.Fragment;

import com.tech.freak.wizardpager.model.ModelCallbacks;
import com.tech.freak.wizardpager.model.Page;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.ReviewItem;

import java.util.ArrayList;

public class FamilyAssetPage extends Page {
    public static final String KEY_TYPES_OF_HOUSE = "Type of House";
    public static final String KEY_MONTHLY_RENT = "Monthly Rent";
    public static final String KEY_RENT_AGREEMENT = "Rent Agreement";
    public static final String KEY_QUALITY_OF_HOUSE = "Quality of House";
    public static final String KEY_STAYING_IN_HOUSE = "How long are you staying in house (in years)";
    public static final String KEY_NUMBER_OF_HOUSE = "Family Assets-Number of Rented Houses or Flats";
    public static final String KEY_NUMBER_OF_SHOP = "Family Assets-Number of Rented Shops or Godowns";
    public static final String KEY_ASSETS_LAND = "Family Assets-Land(Acres)";
    public static final String KEY_ASSETS_ORCHARD = "Family Assets-Orchard (Acres)";

    public FamilyAssetPage(ModelCallbacks callbacks, PageConfig page) {
        super(callbacks, page);
    }

    @Override
    public Fragment createFragment() {
        return FamilyAssetFragment.create(getPageId());
    }

    @Override
    public void getReviewItems(ArrayList<ReviewItem> dest) {

        dest.add(new ReviewItem(KEY_TYPES_OF_HOUSE, mData.getString(KEY_TYPES_OF_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_QUALITY_OF_HOUSE, mData.getString(KEY_QUALITY_OF_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_STAYING_IN_HOUSE, mData.getString(KEY_STAYING_IN_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NUMBER_OF_HOUSE, mData.getString(KEY_NUMBER_OF_HOUSE), getPageId(), -1));
        dest.add(new ReviewItem(KEY_NUMBER_OF_SHOP, mData.getString(KEY_NUMBER_OF_SHOP), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ASSETS_LAND, mData.getString(KEY_ASSETS_LAND), getPageId(), -1));
        dest.add(new ReviewItem(KEY_ASSETS_ORCHARD, mData.getString(KEY_ASSETS_ORCHARD), getPageId(), -1));
        dest.add(new ReviewItem(KEY_RENT_AGREEMENT, mData.getString(KEY_RENT_AGREEMENT), getPageId(), -1));
        dest.add(new ReviewItem(KEY_MONTHLY_RENT, mData.getString(KEY_MONTHLY_RENT), getPageId(), -1));
    }
}
