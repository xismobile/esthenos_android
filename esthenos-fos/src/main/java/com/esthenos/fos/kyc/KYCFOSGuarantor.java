package com.esthenos.fos.kyc;

import android.content.Context;

import com.esthenos.commons.kyc.CapturePageAadhaarCard;
import com.esthenos.commons.pages.ImageCapturePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.GUARANTOR1;
import static com.esthenos.commons.utils.Constants.GUARANTOR2;
import static com.esthenos.commons.utils.Constants.PAGE_BRANCH;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_OTHER;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_VOTERID;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_AADHAR;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_KYC;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_OTHER;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_PAN;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_VOTERID;


public class KYCFOSGuarantor extends AbstractWizardModel {

    public KYCFOSGuarantor(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
            new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, "Add Guarantor's KYC Documents"))
                .addBranch("Add Guarantor 1 KYC",
                    new BranchPage(this, new PageConfig(GUARANTOR1, PAGE_BRANCH, "Add Guarantor 1 KYC"))
                        .addBranch(PAGE_TITLE_AADHAR,
                            new CapturePageAadhaarCard(this, new PageConfig(GUARANTOR1, PAGE_TYPE_AADHAR, PAGE_TITLE_AADHAR))
                        )
                        .addBranch(PAGE_TITLE_VOTERID,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_VOTERID, PAGE_TITLE_VOTERID))
                        )
                        .addBranch(PAGE_TITLE_PAN,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_PAN, PAGE_TITLE_PAN))
                        )
                        .addBranch(PAGE_TITLE_OTHER,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR1, PAGE_TYPE_KYC, PAGE_TITLE_OTHER, "_other"))
                        )
                )

                .addBranch("Add Guarantor 2 KYC",
                    new BranchPage(this, new PageConfig(GUARANTOR2, PAGE_BRANCH, "Add Guarantor 2 KYC"))
                        .addBranch(PAGE_TITLE_AADHAR,
                            new CapturePageAadhaarCard(this, new PageConfig(GUARANTOR2, PAGE_TYPE_AADHAR, PAGE_TITLE_AADHAR))
                        )
                        .addBranch(PAGE_TITLE_VOTERID,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR2, PAGE_TYPE_VOTERID, PAGE_TITLE_VOTERID))
                        )
                        .addBranch(PAGE_TITLE_PAN,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR2, PAGE_TYPE_PAN, PAGE_TITLE_PAN))
                        )
                        .addBranch(PAGE_TITLE_OTHER,
                            new ImageCapturePage(this, new PageConfig(GUARANTOR2, PAGE_TYPE_KYC, PAGE_TITLE_OTHER, "_other"))
                        )
                )
        );
    }
}
