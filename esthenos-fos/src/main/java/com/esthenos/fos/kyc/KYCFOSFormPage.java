package com.esthenos.fos.kyc;

import android.content.Context;

import com.esthenos.commons.pages.AdditionalDetailPage;
import com.esthenos.commons.pages.AdditionalFamilyDetailPage;
import com.esthenos.commons.pages.AppliedLoanDetailPage;
import com.esthenos.commons.pages.BankDetailsPage;
import com.esthenos.commons.pages.BusinessDetails;
import com.esthenos.commons.pages.BusinessInfoBillingCyclePage;
import com.esthenos.commons.pages.BusinessInfoOtherDetailPage;
import com.esthenos.commons.pages.BusinessInfoPage;
import com.esthenos.commons.pages.CreditCardPage;
import com.esthenos.commons.pages.CustomerKYCDetailsPage;
import com.esthenos.commons.pages.CustomerPersonalDetailsPage;
import com.esthenos.commons.pages.CustomerSupplierPage;
import com.esthenos.commons.pages.ElectricityDetailsPage;
import com.esthenos.commons.pages.FamilyExpenditurePage;
import com.esthenos.commons.pages.FamilyMembersPage;
import com.esthenos.commons.pages.FinancialLiabilityPage;
import com.esthenos.commons.pages.GuaranteesForBrrwrPage;
import com.esthenos.commons.pages.ImageCapturePage;
import com.esthenos.commons.pages.LandDetailPage;
import com.esthenos.commons.pages.LoanDetailsPage;
import com.esthenos.commons.pages.NomineeDetailsPage;
import com.esthenos.commons.pages.OtherIDDetailsPage;
import com.esthenos.fos.pages.FamilyAssetPage;
import com.esthenos.fos.pages.HypothecationGoodsPage;
import com.esthenos.commons.pages.LocationHomePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.MultipleFixedChoicePage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.APPLICANT;
import static com.esthenos.commons.utils.Constants.GUARANTOR1;
import static com.esthenos.commons.utils.Constants.GUARANTOR2;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_BUSINESS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_FAMILY_DETAILS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_FAMILY_EXPENDITURE;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_HYPOTHECATION;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_KYC;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_LOAN_DETAILS;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_NOMINEE;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_OTHER;
import static com.esthenos.commons.utils.Constants.PAGE_TYPE_PERSONAL;

public class KYCFOSFormPage extends AbstractWizardModel {

    public KYCFOSFormPage(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(

                new CustomerKYCDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_KYC, "Personal Information")
                ),

                new CustomerKYCDetailsPage(this,
                        new PageConfig(GUARANTOR1, PAGE_TYPE_KYC, "Guarantor 1 Details")
                ),

                new CustomerKYCDetailsPage(this,
                        new PageConfig(GUARANTOR2, PAGE_TYPE_KYC, "Guarantor 2 Details")
                ),

                new CustomerPersonalDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Personal Details")
                ),

                new FamilyAssetPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Asset Page", "_assets")
                ),

                new MultipleFixedChoicePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Other Assets", "_other_assets")
                ).setChoices("Investment Fix Deposit", "Flat/ House on rent", "Bike", "Car", "Truck", "Tractor", "Van", "TV", "Fridge", "Washing Machine", "Others"),

                new FamilyMembersPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Family Member Details", "_members")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 1", "_details1")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 2", "_details2")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 3", "_details3")
                ),

                new AdditionalFamilyDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_DETAILS, "Details of Family Member 4", "_details3")
                ),

                new BankDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Personal Info: Bank Detail 1", "_bank_details1", false)
                ),

                new BankDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Personal Info: Bank Detail 2", "_bank_details2", false)
                ),


                new CreditCardPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Credit Card Info", "_credit_card_details", false)
                ),

                new AdditionalDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Telecommunication Details", "_phone_details")
                ),

                new ElectricityDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Electricity Details", "_electricity_details", false)
                ),

                new NomineeDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_NOMINEE, "Nominee Details")
                ),

                new LandDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Details Of Land/Immovable Property", "_land_details")
                ),

                new BusinessInfoPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Business Information", "_info")
                ),

                new BusinessInfoOtherDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Business Info:Other Details","_detail4")
                ),

                new CustomerSupplierPage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Details of Customer1 and Supplier1","_customer1")
                ),

                new CustomerSupplierPage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Details of Customer2 and Supplier2","_customer2")
                ),

                new CustomerSupplierPage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Details of Customer3 and Supplier3","_customer3")
                ),

                new CustomerSupplierPage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Details of Customer4 and Supplier4","_customer4")
                ),

                new CustomerSupplierPage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Details of Customer5 and Supplier5","_customer5")
                ),

                new BusinessInfoBillingCyclePage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS,"Business Info:Billing Cycle and other details","_billing")
                ),

                new BusinessDetails(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Primary Business Details", "_details1")
                ),

                new BusinessDetails(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Secondary Business Details", "_details2")
                ),

                new BusinessDetails(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Tertiary Business Details", "_details3")
                ),

                new BankDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Business Info: Bank1 Details", "_bank_details3", false)
                ),

                new BankDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Business Info: Bank2 Details", "_bank_details4", false)
                ),

                new FamilyExpenditurePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_FAMILY_EXPENDITURE, "Family Expenditure")
                ),

                new FinancialLiabilityPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Financial Liabilities", "_liabilities")
                ),

                new LoanDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Details Of Borrowings:Loan 1", "_details1")
                ),

                new LoanDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Details Of Borrowings:Loan 2", "_details2")
                ),

                new LoanDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Details Of Borrowings:Loan 3", "_details3")
                ),

                new LoanDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Details Of Borrowings:Loan 4", "_details4")
                ),

                new GuaranteesForBrrwrPage(this,
                        new PageConfig(GUARANTOR1, PAGE_TYPE_OTHER, "Details of Guarantees furnished on behalf of other Borrowers-1", "_details1")
                ),

                new GuaranteesForBrrwrPage(this,
                        new PageConfig(GUARANTOR1, PAGE_TYPE_OTHER, "Details of Guarantees furnished on behalf of other Borrowers-2", "_details2")
                ),

                new AppliedLoanDetailPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_LOAN_DETAILS, "Applied Loan Details", "_applied_loan")
                ),

                new HypothecationGoodsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_HYPOTHECATION, "Primary Asset for Hypothecation", "_details1")
                ),

                new HypothecationGoodsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_HYPOTHECATION, "Secondary Asset for Hypothecation", "_details2")
                ),

                new HypothecationGoodsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_HYPOTHECATION, "Tertiary Asset for Hypothecation", "_details3")
                ),

                new OtherIDDetailsPage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Other ID Detail Page", "_id_details", false)
                ),

                new ImageCapturePage(this,
                        new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Borrower's House", "_home_image", false)
                ),

                new LocationHomePage(this,
                        new PageConfig(APPLICANT,PAGE_TYPE_BUSINESS, "Home Location", "_home_loc")
                )
        );
    }
}
