/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.esthenos.fos.kyc;

import android.content.Context;

import com.esthenos.commons.pages.ImageCapturePage;
import com.tech.freak.wizardpager.model.AbstractWizardModel;
import com.tech.freak.wizardpager.model.BranchPage;
import com.tech.freak.wizardpager.model.PageConfig;
import com.tech.freak.wizardpager.model.PageList;

import static com.esthenos.commons.utils.Constants.*;
import static com.esthenos.commons.utils.Constants.PAGE_BRANCH;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_BUSINESS;
import static com.esthenos.commons.utils.Constants.PAGE_TITLE_PERSONAL;


public class KYCFOSOtherDocs extends AbstractWizardModel {

    public KYCFOSOtherDocs(Context context) {
        super(context);
    }

    @Override
    protected PageList onNewRootPageList() {
        return new PageList(
            new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, "Add Other Documents"))
                .addBranch(PAGE_TITLE_PERSONAL,
                    new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, PAGE_TITLE_PERSONAL))
                        .addBranch("Bank Account Statement",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Bank Account Statement", "_bank_statement"))
                        )
                        .addBranch("Ration Card",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Ration Card", "_ration_card"))
                        )
                        .addBranch("Residential Property",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Residential Property", "_residential_doc"))
                        )
                        .addBranch("Borrowers Photograph",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Borrowers Photograph", "_borrower"))
                        )
                        .addBranch("Mobile Bill",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Mobile Bill", "_bill_mobile"))
                        )
                        .addBranch("Electricity Bill",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Electricity Bill", "_bill_electricity"))
                        )
                        .addBranch("Other Document",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_PERSONAL, "Other Document", "_other_doc"))
                        )
                )

                .addBranch(PAGE_TITLE_BUSINESS,
                    new BranchPage(this, new PageConfig(APPLICANT, PAGE_BRANCH, PAGE_TITLE_BUSINESS))
                        .addBranch("Shops & Establishment Certificate",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Shops & Establishment Certificate", "_biz_shops_doc"))
                        )
                        .addBranch("Bank Account Statement",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Bank Account Statement", "_biz_bank_statement"))
                        )
                        .addBranch("Income Tax/ Sales Tax Returns",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Income Tax/ Sales Tax Returns", "_biz_tax_doc"))
                        )
                        .addBranch("Business Property Document",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Business Property Document", "_biz_property_doc"))
                        )
                        .addBranch("Vehicle RC Book/Card",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Vehicle RC Book/Card", "_biz_vehicle_rc_book"))
                        )
                        .addBranch("Other Document",
                                new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_BUSINESS, "Other Document", "_biz_other_doc"))
                        )
                )
                .addBranch("Other Document 1",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Other Document 1", "_biz_other_doc1"))
                )
                .addBranch("Other Document 2",
                        new ImageCapturePage(this, new PageConfig(APPLICANT, PAGE_TYPE_OTHER, "Other Document 2", "_biz_other_doc2"))
                )
        );
    }
}
